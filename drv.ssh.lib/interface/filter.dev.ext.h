#ifndef _FILTERDEVEXT_H_8710AB34_F653_48D2_A001_CC5D45410F3D_INCLUDED
#define _FILTERDEVEXT_H_8710AB34_F653_48D2_A001_CC5D45410F3D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Aug-2020 at 7:25:23a, UTC+7, Novosibirsk, Monday;
	This is SSH Tunnel WFP filter device extension interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 8:09:37.280 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "filter.defs.h"
#include "filter.notify.h"

namespace ssh { namespace driver {

#pragma pack(push, 1)

	typedef struct __ip_header {
		UCHAR       iphVerLen;      // version and length;
		UCHAR       ipTOS;          // type of service;
		USHORT      ipLength;       // total datagram length;
		USHORT      ipID;           // identification;
		USHORT      ipFlags;        // flags;
		UCHAR       ipTTL;          // time to live;
		UCHAR       ipProtocol;     // protocol;
		USHORT      ipChecksum;     // header checksum;
		ULONG       ipSource;       // source address;
		ULONG       ipDestination;  // destination address;
	} IP_HEADER;

	typedef struct __tcp_header {
		USHORT      sourcePort;
		USHORT      destinationPort;
		ULONG       sequence;
		ULONG       ack;
	} TCP_HEADER, UDP_HEADER;

#pragma pack(pop)

	typedef struct __device_extension {
		NOTIFICATION_QUEUE queue;
		KSPIN_LOCK         rangeslock;

		IP_RANGE*  blockedranges;
		ULONG      blockedcount , blockedlabelsid;

		IP_RANGE*  allowedranges;
		ULONG      allowedcount , allowedlabelsid;

		KSPIN_LOCK destinationportslock;
		KSPIN_LOCK sourceportslock;

		USHORT* destinationports;
		USHORT  destinationportcount;
		USHORT* sourceports;
		USHORT  sourceportcount;

		INT     block;

		UINT32  connect4;
		UINT32  accept4 ;
		UINT32  connect6;
		UINT32  accept6 ;
	} DEV_EXTENSION;

	NTSTATUS   InitDeviceExtension(DEV_EXTENSION*);
}}

typedef ssh::driver::DEV_EXTENSION  t_dev_ext;

extern t_dev_ext* g_dev_ext;

#endif/*_FILTERDEVEXT_H_8710AB34_F653_48D2_A001_CC5D45410F3D_INCLUDED*/