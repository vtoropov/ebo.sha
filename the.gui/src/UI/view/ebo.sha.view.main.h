#ifndef _EBOSHAVIEWMAIN_H_EB67E4E1_A429_48C6_B20C_895C6CCFF921_INCLUDED
#define _EBOSHAVIEWMAIN_H_EB67E4E1_A429_48C6_B20C_895C6CCFF921_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Oct-2020 at 6:12:20p, UTC+7, Novosibirsk, Thursday;
	This is Ebo Sha optima tool main view interface declaration file.
*/
#include "ebo.sha.view.bas.h"
#include "shared.web.browser.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	using ex_ui::web::CUrlLocator ;
	using ex_ui::web::CWebBrowser ;
	using ex_ui::web::IWebBrowserEventHandler;

	class CMain : public CView_Base, public IWebBrowserEventHandler { typedef CView_Base TView;
	private:
		class CMainWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CMain;
		private:
			CMain&      m_view ;
			CWebBrowser m_browser;

		public:
			 CMainWnd(CMain&);
			~CMainWnd(void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CMainWnd   m_wnd;

	public:
		 CMain (void);
		~CMain (void);

	private: // IWebBrowserEventHandler
#pragma warning (disable: 4481)
		virtual HRESULT    BrowserEvent_BeforeNavigate  (LPCTSTR lpszUrl)                            override sealed;
		virtual HRESULT    BrowserEvent_DocumentComplete(LPCTSTR lpszUrl, const bool bStreamObject)  override sealed;
#pragma warning (default: 4481)
	public:
		HRESULT    Refresh (void) override;
	};
}}}}

typedef ebo::sha::gui::view::CMain  TMainView;

#endif/*_EBOSHAVIEWMAIN_H_EB67E4E1_A429_48C6_B20C_895C6CCFF921_INCLUDED*/