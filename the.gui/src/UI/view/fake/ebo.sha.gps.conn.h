#ifndef _EBOSHAGPSCONN_H_8524243E_8A85_4A1D_B8C1_6E39EA90BCC3_INCLUDED
#define _EBOSHAGPSCONN_H_8524243E_8A85_4A1D_B8C1_6E39EA90BCC3_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2021 at 8:11:37.008 pm, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool fake GPS driver connection sub-view interface declaration file.
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.view.empty.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CFake_Connect : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CFake_Connect;
		private:
			CFake_Connect& m_view ;
			TEmptyView     m_empty;

		private:
			 CViewWnd (CFake_Connect&);
			~CViewWnd (void);

		private:   // TWindow
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CFake_Connect (void);
		~CFake_Connect (void);
	};

}}}}

typedef ebo::sha::gui::view::CFake_Connect TFakeConnect;
typedef ebo::sha::gui::view::CFake_Connect TFakeConn;

#endif/*_EBOSHAGPSCONN_H_8524243E_8A85_4A1D_B8C1_6E39EA90BCC3_INCLUDED*/