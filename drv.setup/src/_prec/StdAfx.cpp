/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Aug-2016 at 6:20:45a, GMT+7, Phuket, Rawai, Tuesday;
	This is Evalu8 Outlook add-in precompiled headers implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack generic driver setup project on 12-Jan-2021 at 6:30:00.206 pm, UTC+7, Novosibirsk, Tuesday;
*/

#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)
