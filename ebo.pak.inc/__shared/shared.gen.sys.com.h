#ifndef _SHAREDLITESYSTEMCORE_H_5D2FFEE1_0785_4c17_ABF2_99598CF8D1FB_INCLUDED
#define _SHAREDLITESYSTEMCORE_H_5D2FFEE1_0785_4c17_ABF2_99598CF8D1FB_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Mar-2011 at 1:48:50am, GMT+3, Rostov-on-Don, Monday;
	This is Row27 project COM libraries auto-initializer interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to PulsePay project on 2-May-2012 at 8:57:47am, GMT+3, Rostov-on-Don, Wednesday;
	Adopted to Platinum project on 19-Mar-2014 at 9:47:36am, GMT+4, Taganrog, Wednesday;
	Adopted to File Guardian project on 11-Jul-2018 at 10:29:00a, UTC+7, Phuket, Rawai, Wednesday;
	Adopted to FakeGPS driver project on 13-Dec-2019 at 10:15:18a, UTC+7, Novosibirsk, Friday;
*/
#include "shared.gen.sys.err.h"

namespace shared { namespace sys_core
{
	using shared::sys_core::CError;

	class CCoIniter {
	protected:
		CError   m_error;
	public:
		 CCoIniter(const bool bMultiThreaded);
		 CCoIniter(const DWORD _flag);
		~CCoIniter(void);
	public:
		TErrorRef   Error(VOID) CONST;
		bool        IsSuccess(VOID) CONST;
	private:
		CCoIniter(const CCoIniter&);
		CCoIniter& operator= (const CCoIniter&);

	public:
		operator const bool (void) const;
	};

	class CCoApartmentThreaded : public CCoIniter {
	                            typedef CCoIniter TBase;
	public:
		CCoApartmentThreaded(void);
	};

	class CCoSecAuthLevel {
		// https://docs.microsoft.com/en-us/windows/desktop/com/com-authentication-level-constants
	public:
		enum _e : DWORD {
			eDefault    = RPC_C_AUTHN_LEVEL_DEFAULT, // DCOM to choose the authentication level using its normal security blanket negotiation algorithm;
			eNone       = RPC_C_AUTHN_LEVEL_NONE   , // no authentication;
			eConnect    = RPC_C_AUTHN_LEVEL_CONNECT, // for a relationship with the server; datagram transports always use RPC_AUTHN_LEVEL_PKT instead;
			eRemoteCall = RPC_C_AUTHN_LEVEL_CALL   , // for remote procedure call; datagram transports use RPC_C_AUTHN_LEVEL_PKT instead;
			eExpected   = RPC_C_AUTHN_LEVEL_PKT    , // authenticates that all data received is from the expected client;
			eIntegrity  = RPC_C_AUTHN_LEVEL_PKT_INTEGRITY, // verifies that none of the data transferred between client and server has been modified;
			ePrivacy    = RPC_C_AUTHN_LEVEL_PKT_PRIVACY    // encrypts the argument value of each remote procedure call;
		};
	};

	class CCoSecImpLevel {
		// https://docs.microsoft.com/en-us/windows/desktop/com/com-impersonation-level-constants
	public:
		enum _e : DWORD {
			eDefault    = RPC_C_IMP_LEVEL_DEFAULT    , // DCOM can choose the impersonation level using its normal security blanket negotiation algorithm;
			eAnonymous  = RPC_C_IMP_LEVEL_ANONYMOUS  , // a client is anonymous to the server;
			eIdentity   = RPC_C_IMP_LEVEL_IDENTIFY   , // a server can obtain the client's identity and to impersonate the client for ACL checking;
			eImpersonate= RPC_C_IMP_LEVEL_IMPERSONATE, // a server can impersonate a client's security context while acting on behalf of the client; (for local resource only);
			eDelegate   = RPC_C_IMP_LEVEL_DELEGATE     // a server may use a client's security context on other machines to access local and remote resources as a client;
		};
	};

	class CCoSecurityProvider {
	protected:
		CError      m_error;
	public:
		CCoSecurityProvider(void);
	public:
		TErrorRef   Error(VOID) CONST;
		HRESULT     Init (const CCoSecAuthLevel::_e, const CCoSecImpLevel::_e);
		HRESULT     InitDefault(void);
		HRESULT     InitNoIdentity(void);
	};
}}

#endif/*_SHAREDLITESYSTEMCORE_H_5D2FFEE1_0785_4c17_ABF2_99598CF8D1FB_INCLUDED*/