/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Aug-2020 at 4:21:45p, UTC+7, Novosibirsk, Monday;
	This is SSH Tunnel WFP filter IP address control management interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 8:09:37.520 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "filter.ip.ctrl.h"

using namespace ssh::driver;

/////////////////////////////////////////////////////////////////////////////

namespace ssh { namespace driver { namespace _impl {

	INT __cdecl cmp_unsigned_short(LPCVOID _p_a1, LPCVOID _p_a2) {
		return ( *(USHORT*)_p_a1 - *(USHORT*)_p_a2 );
	}

}}}
using namespace ssh::driver::_impl;
/////////////////////////////////////////////////////////////////////////////

ULONG    CAddress::Fake (const IN6_ADDR* _from) {
	if (NULL == _from)
		return 0;
	const FAKEV6ADDR* p_fake = (const FAKEV6ADDR*)_from;
	if (NULL == p_fake)
		return 0;

	if (p_fake->teredo.prefix == 0x0000120) {
		return ~NTOHL(p_fake->teredo.clientip);
	}
	else if (p_fake->sixtofour.prefix == 0x0220) {
		return NTOHL(p_fake->sixtofour.clientip);
	}
	else
		return 0;
}

NTSTATUS CAddress::Fill (const ADDR_THUNK& _by, SSH_TNL_ADDR& _to) {

	NTSTATUS nt_status = S_OK;

	if (NULL == _by._addr6) {
		_to.addr4.sin_family = AF_INET;
		_to.addr4.sin_addr.s_addr = NTOHL(_by._addr);
		_to.addr4.sin_port   = NTOHS(_by._port);
	}
	else {
		_to.addr6.sin6_family = AF_INET6;
		_to.addr6.sin6_addr   = *_by._addr6;
		_to.addr6.sin6_port   = NTOHS(_by._port);
	}

	return nt_status;
}

NTSTATUS CAddress::Fill (const ADDR_THUNK& _src, const ADDR_THUNK& _dest, t_raw_notify* _to) {

	NTSTATUS nt_status = S_OK;

	if (NULL == _to) return (nt_status = STATUS_INVALID_PARAMETER);

	nt_status = CAddress::Fill(_src , _to->source);
	nt_status = CAddress::Fill(_dest, _to->dest  );

	return nt_status;
}

NTSTATUS CAddress::Reset(ADDR_THUNK& _thunk) {
	::RtlZeroMemory(&_thunk, sizeof(ADDR_THUNK));
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

INT  CPorts::CDestination::IsAllowed(const USHORT  _u_port) {
	__unused(_u_port);
	if (NULL == g_dev_ext)
		return 0;
	const
	INT* p_found = (INT*)::bsearch(&_u_port, g_dev_ext->destinationports, g_dev_ext->destinationportcount, sizeof(USHORT), cmp_unsigned_short);
	return (NULL != p_found ? *p_found : 0);
}

VOID CPorts::CDestination::Set (const USHORT* _p_ports, const USHORT _count) {

	if (NULL == g_dev_ext)
		return;

	USHORT* old_ports = NULL;
	USHORT* new_ports = NULL;
	KIRQL irq = 0;

	if (_p_ports && _count > 0) {
		old_ports = g_dev_ext->destinationports;
		new_ports = (USHORT*)::ExAllocatePoolWithTag(NonPagedPool, sizeof(USHORT) * _count, AUX_IP_PORTS_TAG);
		if (NULL == new_ports)
			return;
		RtlCopyMemory(new_ports, _p_ports, sizeof(USHORT) * _count);
	}
	else {
		g_dev_ext->destinationportcount = 0;
	}

	KeAcquireSpinLock(&g_dev_ext->destinationportslock, &irq);

	g_dev_ext->destinationports     = new_ports;
	g_dev_ext->destinationportcount = _count;

	KeReleaseSpinLock(&g_dev_ext->destinationportslock,  irq);

	if (old_ports) {
		ExFreePoolWithTag(old_ports, AUX_IP_PORTS_TAG);
	}
}

/////////////////////////////////////////////////////////////////////////////

INT  CPorts::CSource::IsAllowed(const USHORT  _u_port) {
	__unused(_u_port);
	if (NULL == g_dev_ext)
		return 0;
	// http://www.cplusplus.com/reference/cstdlib/bsearch/
	const
	INT* p_found = (INT*)::bsearch(&_u_port, g_dev_ext->sourceports, g_dev_ext->sourceportcount, sizeof(USHORT), cmp_unsigned_short);
	return (NULL != p_found ? *p_found : 0);
}

VOID CPorts::CSource::Set (const USHORT* _p_ports, const USHORT _count) {

	if (NULL == g_dev_ext)
		return;

	USHORT* old_ports = NULL;
	USHORT* new_ports = NULL;
	KIRQL irq = 0;

	if (_p_ports && _count > 0) {
		old_ports = g_dev_ext->sourceports;
		new_ports = (USHORT*) ::ExAllocatePoolWithTag(NonPagedPool, sizeof(USHORT) * _count, AUX_IP_PORTS_DST_TAG);
		if (NULL == new_ports)
			return;

		RtlCopyMemory(new_ports, _p_ports, sizeof(USHORT) * _count);
	}
	else {
		g_dev_ext->sourceportcount = 0;
	}

	KeAcquireSpinLock(&g_dev_ext->sourceportslock, &irq);

	g_dev_ext->sourceports     = new_ports;
	g_dev_ext->sourceportcount = _count;

	KeReleaseSpinLock(&g_dev_ext->sourceportslock,  irq);

	if (old_ports) {
		ExFreePoolWithTag(old_ports, AUX_IP_PORTS_DST_TAG);
	}
}

/////////////////////////////////////////////////////////////////////////////

CAction CRanges::Get(const ULONG _u_ip) {

	const IP_RANGE* p_range = NULL;
	KIRQL irq;
	CAction action = CAction::allow;

	if ( NULL == g_dev_ext)
		return action;

	KeAcquireSpinLock(&g_dev_ext->rangeslock, &irq);

	if (g_dev_ext->allowedcount) {
		p_range = CRanges::In(g_dev_ext->allowedranges, g_dev_ext->allowedcount, _u_ip);

		if (p_range) {
			action = CAction::allow_exp;
		}
	}

	if (NULL == p_range) {
		if (g_dev_ext->blockedcount) {
			p_range = CRanges::In(g_dev_ext->blockedranges, g_dev_ext->blockedcount, _u_ip);

			if (p_range) {
				action = CAction::block;
			}
		}
	}

	KeReleaseSpinLock(&g_dev_ext->rangeslock,  irq);

	return action;
}

const IP_RANGE* CRanges::In(const IP_RANGE* _p_ranges, const INT _count, const ULONG _ip) {

	if (NULL == _p_ranges)
		return NULL;

	const IP_RANGE* iter = _p_ranges;
	const IP_RANGE* last = _p_ranges + _count;

	INT cnt_local = _count;

	while(0 < cnt_local) {

		INT n_cnt = cnt_local >> 1;

		const IP_RANGE* p_mid = iter + n_cnt;

		if (p_mid->start < _ip) {
			iter       = p_mid  + 1;
			cnt_local -= n_cnt  + 1;
		}
		else {
			cnt_local = n_cnt;
		}
	}

	if (iter != last) {
		if (iter->start != _ip) --iter;
	}
	else {
		--iter;
	}

	return (iter >= _p_ranges && iter->start <= _ip && _ip <= iter->end) ? iter : NULL;
}

VOID  CRanges::Set(const IP_RANGES* _p_ranges, const INT _n_block) {

	if (NULL == _p_ranges || NULL == g_dev_ext)
		return;

	IP_RANGE* new_ranges = NULL;
	IP_RANGE* old_ranges = NULL;

	ULONG ncount = 0;
	ULONG labelsid = 0;
	KIRQL irq = 0;

	if (_p_ranges && _p_ranges->count > 0) {
		ncount     = _p_ranges->count;
		labelsid   = _p_ranges->labelsid;

		new_ranges = (IP_RANGE*)::ExAllocatePoolWithTag(NonPagedPool, _p_ranges->count * sizeof(IP_RANGE), AUX_IP_RANGE_TAG);
		if (NULL  == new_ranges)
			return;

		RtlCopyMemory(new_ranges, _p_ranges->ranges, _p_ranges->count * sizeof(IP_RANGE));
	}
	else {
		labelsid = 0xFFFFFFFF;
	}

	KeAcquireSpinLock(&g_dev_ext->rangeslock, &irq);

	if (_n_block) {
		old_ranges = g_dev_ext->blockedcount ? g_dev_ext->blockedranges : NULL;

		g_dev_ext->blockedcount    = ncount;
		g_dev_ext->blockedranges   = new_ranges;
		g_dev_ext->blockedlabelsid = labelsid;
	}
	else {
		old_ranges = g_dev_ext->allowedcount ? g_dev_ext->allowedranges : NULL;

		g_dev_ext->allowedcount    = ncount;
		g_dev_ext->allowedranges   = new_ranges;
		g_dev_ext->allowedlabelsid = labelsid;
	}

	KeReleaseSpinLock(&g_dev_ext->rangeslock, irq);

	if (old_ranges) {
		ExFreePoolWithTag(old_ranges, AUX_IP_RANGE_TAG);
	}
}