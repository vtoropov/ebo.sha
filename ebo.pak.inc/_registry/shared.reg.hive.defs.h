#ifndef _SHAREDREGISTRYCOMMONDEFS_H_1D8DF89F_E2B5_477a_BFF7_F525BB0CD5D7_INCLUDED
#define _SHAREDREGISTRYCOMMONDEFS_H_1D8DF89F_E2B5_477a_BFF7_F525BB0CD5D7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 20-Aug-2017 at 1:45:50a, UTC+7, Phuket, Rawai, Sunday;
	This is shared ms-windows registry wrapper library common definition declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 5:27:47p, UTC+7, Phuket, Rawai, Monday;
	Adopted to Ebo Pack on 13-Sep-2019 at 4:01:35p, UTC+7, Novosibirsk, Friday;
*/
#include "shared.gen.sys.err.h"

namespace shared { namespace registry
{
	using shared::sys_core::CError;

	class CRegOptions
	{
	public:
		enum {
			eNone             = 0x0,
			eAutoCompletePath = 0x1, // the regestry path is automatically composed by using application name from the executable
			eDoNotModifyPath  = 0x2, // the registry path is used as is, without any modification
		};
	protected:
		DWORD       m_opts;
	public:
		CRegOptions(const DWORD _opts = CRegOptions::eAutoCompletePath);
	public:
		bool        DoNotCare(void)const;
		bool        IsAutoComplete(void)const;
		bool        IsModifyPath(void)const;
		DWORD       Value(void)const;
		VOID        Value(const DWORD);
	};
	
	class CRegistryBase {
	protected:
		mutable
		CError      m_error;
		CRegOptions m_options;
		HKEY        m_root;
	protected:
		CRegistryBase(const HKEY hRoot, LPCWSTR lpszModuleName = NULL);
	public:
		TErrorRef    Error  (void) const;
		const
		CRegOptions& Options(void) const;
		CRegOptions& Options(void)      ;
		HKEY         Root   (void) const;
		VOID         Root   (const HKEY);
	protected:
		HRESULT      OpenKey(::ATL::CRegKey&, LPCWSTR lpszFolder) const; // opens key for specified registry folder and value name;
	};

	class CRegistryArgs {
	private:
		CRegOptions m_options;
	public:
		CRegistryArgs(const CRegOptions&);
	public:
		CAtlString     Format (LPCWSTR lpszFolder) const;
		HRESULT        IsValid(LPCWSTR lpszFolder, LPCWSTR lpszValueName)const;
		HRESULT        IsFolderName(LPCWSTR lpszFolder)const;
		HRESULT        IsValueName (LPCWSTR lpszValue )const;
	};

	class CRegistryType {
	private:
		DWORD   m_type;
	public:
		 CRegistryType(const CRegistryBase&, LPCWSTR lpszFolder, LPCWSTR lpszValueName);
		~CRegistryType(void);
	public:
		bool    IsDword(void)const;
		bool    IsMultiline(void)const;
		bool    IsNone (void)const; // it looks like a value is not found and a type cannot be determined;
		bool    IsText (void)const;
		DWORD   Type   (void)const;
	};

	typedef ::std::vector<::ATL::CStringW>  TMultiString;
}}

typedef shared::registry::CRegOptions    TRegOptions;
typedef shared::registry::CRegistryType  TRegDataType;

#endif/*_SHAREDREGISTRYCOMMONDEFS_H_1D8DF89F_E2B5_477a_BFF7_F525BB0CD5D7_INCLUDED*/