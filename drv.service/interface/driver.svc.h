#ifndef _DRIVERSVC_H_A679F5A3_7887_4EA3_978A_C26851558B00_INCLUDED
#define _DRIVERSVC_H_A679F5A3_7887_4EA3_978A_C26851558B00_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Aug-2020 at 8:01:48p, UTC+7, Novosibirsk, Thursday;
	This is SSH-Tunnel WFP driver service management interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 7:01:08.945 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "sys.svc.error.h"
#include "sys.svc.service.h"
#include "sys.svc.man.h"
#include "sys.svc.crt.h"

#include "driver.svc.props.h"

namespace shared { namespace driver { namespace client {

	using shared::service::CSvcError;
	using shared::service::CService ;
	using shared::service::CService_Handle;
	using shared::service::CCrtData ;

	class CService_Ex : public CService { typedef CService TBase;
	private:
		TDrvSvcPropSet  m_props;

	public:
		 CService_Ex (void);
		 CService_Ex (const CService_Ex&);
		~CService_Ex (void);

	public:
		const
		TDrvSvcPropSet& Properties (void) const;
		TDrvSvcPropSet& Properties (void)      ;

	public:
		 CService_Ex& operator = (const CService_Ex&);
		 CService_Ex& operator <<(const TDrvSvcPropSet&);
	};

	// TODO: this class interface needs reviewing, at least crt-data is an input argument in all methods;
	class CServiceMan {
	private:
		CSvcError m_error;

	public:
		 CServiceMan (void);
		~CServiceMan (void);

	public:
		HRESULT     Create(const CCrtData&, CService& _svc_ssh);
		HRESULT     Delete(const CCrtData&);
		TErrorRef   Error (void) const;
		HRESULT     Start (const CCrtData&);   // TODO: no waiting a pending event yet;
		HRESULT     Stop  (const CCrtData&);   // TODO: the same as above; possible pending operation is not checked and is not awaited;
		HRESULT     Status(const CCrtData&, CService& _svc_ssh);
	};

}}}

typedef shared::driver::client::CService_Ex   TDrvService;
typedef shared::driver::client::CServiceMan   TDrvServiceMan;

#endif/*_DRIVERSVC_H_A679F5A3_7887_4EA3_978A_C26851558B00_INCLUDED*/