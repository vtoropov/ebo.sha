/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Sep-2020 at 1:20:54p, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack generic driver setup information device interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 12-Jan-2021 at 7:17:27.052 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "drv.setup.info.h"

using namespace shared::driver::setup;

/////////////////////////////////////////////////////////////////////////////

CDevInfo:: CDevInfo (void) { this->Reset();}
CDevInfo:: CDevInfo (const CDevInfo& _ref) : CDevInfo() { *this = _ref; }
CDevInfo::~CDevInfo (void) {}

/////////////////////////////////////////////////////////////////////////////

const GUID&    CDevInfo::Class (void) const { return m_data.ClassGuid; }
HRESULT        CDevInfo::Class (const GUID _class) {

	const errno_t err_no = ::memcpy_s(&m_data.ClassGuid, sizeof(GUID), &_class, sizeof(GUID));
	return (NO_ERROR == err_no ? S_OK : E_OUTOFMEMORY);
}
const bool     CDevInfo::Is    (void) const { return (!::IsEqualGUID(GUID_NULL, m_data.ClassGuid)); }
const
TDevInfoData&  CDevInfo::Ref   (void) const { return m_data; }
TDevInfoData&  CDevInfo::Ref   (void)       { return m_data; }
HRESULT        CDevInfo::Reset (void) { HRESULT hr_ = S_OK;
	::RtlZeroMemory(&m_data, sizeof(TDevInfoData)); m_data.cbSize = sizeof(TDevInfoData);
	return hr_;
}
const
TDevInfoDataPtr  CDevInfo::Ptr (void)  { return &m_data; }
CStringW       CDevInfo::ToStr (LPCWSTR _lp_sz_sep) const {

	static LPCWSTR lp_sz_pat = _T(
		"Class = %s"
		"%s"
		"Node  = %u"
	);

	CStringW cs_guid;
	LPOLESTR lp_ole_guid = NULL;

	HRESULT  hr_ = ::StringFromCLSID(m_data.ClassGuid, &lp_ole_guid);
	if (SUCCEEDED(hr_)) {
		cs_guid = lp_ole_guid;
		::CoTaskMemFree((LPVOID)lp_ole_guid); lp_ole_guid = NULL;
	}
	else
		cs_guid = _T("#error::no_guid");

	CStringW cs_data; cs_data.Format(
		lp_sz_pat, cs_guid.GetString(), _lp_sz_sep, m_data.DevInst
	);

	return cs_data;
}

/////////////////////////////////////////////////////////////////////////////

CDevInfo&  CDevInfo::operator = (const CDevInfo& _ref) { *this = _ref.Ref(); return *this; }
CDevInfo&  CDevInfo::operator = (const TDevInfoData& _dat) { *this << _dat.ClassGuid; m_data.DevInst = _dat.DevInst; return *this; }
CDevInfo&  CDevInfo::operator <<(const GUID& _class) { this->Class(_class); return *this; }

/////////////////////////////////////////////////////////////////////////////

CDevInfo::operator const TDevInfoData&   (void) const { return  m_data; }
CDevInfo::operator       TDevInfoData&   (void)       { return  m_data; }
CDevInfo::operator const TDevInfoDataPtr (void)       { return &m_data; }

/////////////////////////////////////////////////////////////////////////////

CDevInfoSet:: CDevInfoSet (void) : m_p_set(NULL) { m_error << __MODULE__ << S_OK; }
CDevInfoSet:: CDevInfoSet (const CDevInfoSet& _ref) : CDevInfoSet() { *this = _ref; }
CDevInfoSet::~CDevInfoSet (void) { this->Erase() ; }

/////////////////////////////////////////////////////////////////////////////

HRESULT    CDevInfoSet::Erase (void) {
	m_error << __MODULE__ << S_OK;

	if (this->Is() == false)
		return (m_error = __DwordToHresult(ERROR_INVALID_STATE));

	// https://docs.microsoft.com/en-us/windows/win32/api/setupapi/nf-setupapi-setupdidestroydeviceinfolist
	const BOOL b_result = ::SetupDiDestroyDeviceInfoList(m_p_set);
	if (FALSE == b_result)
		m_error = __LastErrToHresult();
	else
		m_p_set = NULL;

	return m_error;
}

TErrorRef  CDevInfoSet::Error (void) const { return m_error; }

bool       CDevInfoSet::Is    (void) const { return (NULL != m_p_set && INVALID_HANDLE_VALUE != m_p_set ); }

HRESULT    CDevInfoSet::Get   (const GUID& _dev_iface, LPCWSTR _lp_sz_enum_name, const HWND _h_gui, const DWORD _dw_flags) {
	m_error << __MODULE__ << S_OK;

	if (CTypes(_dw_flags).Not())
		return ((m_error << E_INVALIDARG) = _T("No setup type is provided;"));

	const bool b_enumerator_provided = (NULL != _lp_sz_enum_name && 0 != ::wcslen(_lp_sz_enum_name));

	if (b_enumerator_provided && CTypes(_dw_flags).Has(CTypes::e_iface) == false)
		return ((m_error << E_INVALIDARG) = _T("DIGCF_DEVICEINTERFACE flag must be provided;"));

	if (this->Is())
		return (m_error = __DwordToHresult(ERROR_ALREADY_INITIALIZED));
	// https://docs.microsoft.com/en-us/windows/win32/api/setupapi/nf-setupapi-setupdigetclassdevsw
	this->m_p_set = SetupDiGetClassDevs(
		&_dev_iface, _lp_sz_enum_name, _h_gui, _dw_flags
	);
	if (this->Is() == false)
		m_error = __LastErrToHresult();

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CDevInfoSet&  CDevInfoSet::operator = (const CDevInfoSet& _ref) {
	if (this->Is())
		this->Erase();

	this->m_p_set = _ref.m_p_set; CDevInfoSet& ref_ = const_cast<CDevInfoSet&>(_ref); ref_.m_p_set = NULL;

	return *this;
}

CDevInfoSet&  CDevInfoSet::operator <<(const GUID& _cls) { if (this->Is()) this->Erase(); this->Get(_cls); return *this; }

/////////////////////////////////////////////////////////////////////////////

CDevInfoSet::operator const HDEVINFO& (void) const { return m_p_set; }