#ifndef _SFXIMAGECTRL_H_23A8EDE4_06F4_4AE8_A01C_2BF3DC41CC95_INCLUDED
#define _SFXIMAGECTRL_H_23A8EDE4_06F4_4AE8_A01C_2BF3DC41CC95_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 26-May-2012 at 2:05:57pm, GMT+3, Rostov-on-Don, Saturday;
	This is ST shared picture control class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Sfx image control project on 11-Nov-2020 at 9:21:41a, UTC+7, Novosibirsk, Wednesday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "sfx.image.ext.h"
#include "sfx.image.fmt.h"
#include "sfx.image.lay.h"

namespace ST_Ctrls {

	using shared::sys_core::CError ;
	using ex_ui::controls::CBorders;
	using ex_ui::controls::CMargins;

	using ex_ui::draw::defs::IRenderer;

	interface IImageEvents : public ex_ui::controls::IControlEvent {
		virtual HRESULT   IImageEvt_OnChanged (void) PURE;
	};

	class CPicture {
	protected:
		HANDLE       m_wnd_ptr;
		CError       m_error  ;
		UINT         m_ctrl_id;
		CBorders     m_borders;
		TPictureLay  m_layout ;
		CImage       m_image  ;

	public:
		 CPicture (void);
		~CPicture (void);

	public:
		HRESULT      Create  (const HWND hParent, const RECT& _rc_area, const UINT _ctrl_id);
		HRESULT      Destroy (void)      ;

	public:
		const
		CBorders&    Borders (void) const;  // gets picture frame (read-only);
		CBorders&    Borders (void)      ;  // gets picture frame (read-write);
		TErrorRef    Error   (void) const;
		const
		TPictureFormat&  Format  (void) const;
		TPictureFormat&  Format  (void)      ;
		const
		CImage&      Image   (void) const;
		CImage&      Image   (void)      ;
		const
		TPictureLay& Layout  (void) const;
		TPictureLay& Layout  (void)      ;
		HRESULT      Renderer(IRenderer*  const); // sets parent window renderer;
		HRESULT      Refresh (void)      ;
		CWindow      Window  (void) const;

	private: // non-copyable;
		CPicture (const CPicture&);
		CPicture& operator = (const CPicture&);
	};
	
}

typedef ST_Ctrls::CPicture  TImageCtrl;

#endif/*_SFXIMAGECTRL_H_23A8EDE4_06F4_4AE8_A01C_2BF3DC41CC95_INCLUDED*/