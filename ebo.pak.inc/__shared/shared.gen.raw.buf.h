#ifndef _GENERICSTGDATA_H_4916FDBD_664D_48f6_A467_B1F03F356B69_INCLUDED
#define _GENERICSTGDATA_H_4916FDBD_664D_48f6_A467_B1F03F356B69_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2014 at 6:56:06am, GMT+4, Taganrog, Wednesday;
	This is Ebo Pack shared library generic raw data class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 11:58:22a, UTC+7, Phuket, Rawai, Monday;
*/
#include "shared.gen.sys.err.h"
#include <vector>
namespace shared { namespace common
{
	using shared::sys_core::CError;

	typedef const PBYTE PCBYTE;

	class CRawBuffer {
	protected:
		PBYTE       m_pData ;
		DWORD       m_dwSize;

	protected:
		mutable CError m_error;

	protected:
		 CRawBuffer(void);
		~CRawBuffer(void);

	public:
		HRESULT     Clear  (void);
		bool        IsEmpty(void) const;
		bool        IsValid(void) const;
	};

	class CRawData : public  CRawBuffer {
	                 typedef CRawBuffer TBuffer;
	public:
	    CRawData(void);
		CRawData(const CRawData&);
	    CRawData(const _variant_t&);       // creates from bstr data
	    CRawData(const DWORD dwSize);
	    CRawData(const PBYTE pData, const DWORD dwSize);
	   ~CRawData(void);

	public:
		HRESULT     Append (const CRawData&);
		HRESULT     Append (const PBYTE pData, const DWORD dwSize);
		HRESULT     CopyToVariantAsArray(_variant_t&)const; // data is interpreted as binary and is copied to safe array as is;
		HRESULT     CopyToVariantAsUtf16(_variant_t&)const; // data is interpreted as Utf8 text and is converted to Utf16 bstr;
		HRESULT     CopyToVariantAsUtf8 (_variant_t&)const; // data is interpreted as Utf8 text and is copied to bstr as is;
		HRESULT     Create (const DWORD dwSize);
		HRESULT     Create (const PBYTE pData, const DWORD dwSize);
		HRESULT     Create (LPCWSTR _lp_sz_file);        // creates buffer from file;
		LPBYTE      Detach (void)      ;
		TErrorRef   Error  (void) const;
		PCBYTE      GetData(void) const;
		PBYTE       GetData(void)      ;
		DWORD       GetSize(void) const;
		HRESULT     Reset  (void)      ;
		HRESULT     ToFile (LPCWSTR _lp_sz_path) const;  // saves data dto file specified; 
		HRESULT     ToStringUtf16(CStringW& )    const;  // assumes the buffer contains unicode string;
		HRESULT     ToStringUtf8 (CStringA& )    const;  // assumes the buffer contains ansi string;

	public:
		operator const    bool (void)const;              // returns a validity state of the object;
		operator const    DWORD(void)const;              // returns a buffer size;
		operator const    PBYTE(void)const;              // returns a buffer data pointer (ra);
		operator          PBYTE(void)     ;              // returns a buffer data pointer (rw);

	public:
		CRawData& operator+=(const CRawData&);
		CRawData& operator= (const CRawData&);
		CRawData& operator= (const _variant_t&);         // creates from bstr data;
		CRawData& operator= (const ::std::vector<BYTE>&);
	};
}}

#endif/*_GENERICSTGDATA_H_4916FDBD_664D_48f6_A467_B1F03F356B69_INCLUDED*/