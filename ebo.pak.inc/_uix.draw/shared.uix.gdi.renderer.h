#ifndef _UIXDRAWRENDERER_H_B10C71E6_F9B5_49d6_A9EE_82013C781AAB_INCLUDED
#define _UIXDRAWRENDERER_H_B10C71E6_F9B5_49d6_A9EE_82013C781AAB_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Feb-2015 at 00:32:11AM, GMT+3, Taganrog, Friday;
	This is Ebo Pack shared UIX draw library renderer interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 11:18:17p, UTC+7, Phuket, Rawai, Monday;
*/
#include "shared.uix.gdi.provider.h"
#include "shared.uix.png.wrap.h"
#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.draw.shape.h"
#include "shared.uix.gdi.draw.tile.h"
#include "shared.uix.gdi.z_buf.h"

namespace ex_ui { namespace draw {
	using ex_ui::draw::CPngBitmapPtr;
	using ex_ui::draw::CZBuffer;

	// a renderer: https://en.wiktionary.org/wiki/renderer
	// to render : https://en.wikipedia.org/wiki/Render

	typedef ::std::vector<RECT> TExcluded;

	class CExclusion : public ex_ui::draw::defs::IExclusion {
	private:
		TExcluded   m_rects;

	public:
		 CExclusion (void) ;
		 CExclusion (const CExclusion&);
		~CExclusion (void) ;

	public: // IExclusion
		HRESULT     Add  (const RECT&)       override; // checks for rectangle emptiness;
		HRESULT     Apply(const HDC)   const override;
		HRESULT     Clear(void)              override;
		ULONG       Count(void)        const override;
		RECT&       Get  (const ULONG _ndx)  override;

	public:
		CExclusion& operator = (const CExclusion&);
		CExclusion& operator+= (const RECT&);          // appends a rectangle to excluded areas;
		CExclusion& operator-= (const ULONG _ndx );    // removes a rectangle by index;
		CExclusion& operator<< (const HDC);            // applies exclusion rectangle(s) to device context provided;
	};

	class CRenderer : public ex_ui::draw::defs::IRenderer_Base {
	private:
		CExclusion  m_excluded;

	public:
		 CRenderer (void);
		 CRenderer (const CRenderer&);
		~CRenderer (void);

	public:
		ex_ui::draw::defs::IExclusion& Excluded(void) override;

	public:
		HRESULT   Draw (const HDC, const RECT& _area, const UINT nResId) const;
		HRESULT   Draw (const HDC, const RECT& _area, TBitmap* const) const; // a bitmap is auto-centered;

	public:
		HRESULT   Draw (TZBuffer&, const COLORREF, const RECT& _rc_draw, const RECT& _rc_exclusion) const; // draws solid color rectangle, no alpha;

	public:
		CRenderer& operator = (const CRenderer&);
		CRenderer& operator <<(const CExclusion&);
	};

	class CRenderer_Cached : public CRenderer { typedef CRenderer TBase;
	protected:
		CPngBitmap m_cache; // can be used for direct assign of outer bitmap, for example, to result of drawing function on erase bkgnd message;

	public:
		 CRenderer_Cached (void);
		 CRenderer_Cached (const CRenderer_Cached&);
		~CRenderer_Cached (void);

	public:
		const
		CPngBitmap& Cached(void) const;
		CPngBitmap& Cached(void)      ;

	public:
		HRESULT     Draw  (const HDC _target, const RECT& _draw_rect) const; // draws rectangle area from cached bitmap to target device;

	public:
		CRenderer_Cached& operator = (const CRenderer_Cached&);
		CRenderer_Cached& operator <<(const HBITMAP);     // updates a cached bitmap;
		CRenderer_Cached& operator <<(const CPngBitmap&); // updates a cached bitmap; (replace an original if any by making bitmap clone);
	};

	class CRenderer_Tile : public CRenderer { typedef CRenderer TBase;
	protected:
		TTileMode  m_mode;

	public:
		 CRenderer_Tile (const TTileMode = TTileMode::e_any);
		 CRenderer_Tile (const CRenderer_Tile&);
		~CRenderer_Tile (void);

	public:
		HRESULT    Draw (const HDC, const RECT& _area, const UINT nResId) const;
		HRESULT    Draw (const HDC, const RECT& _area, TBitmap* const) const;

	public:
		HRESULT    Draw (const HDC, const TTiles&);                      // draws tile colors only; a mode of direction is ignored;
		HRESULT    Draw (const HDC, const TTiles&, const CTiledImages&); // draws tile images only; a mode of direction is ignored;

	public:
		TTileMode  Mode (void) const;
		TTileMode& Mode (void)      ;

	public:
		CRenderer_Tile& operator = (const CRenderer_Tile&);
		CRenderer_Tile& operator <<(const TTileMode::_mode);
	};
	
	class CRendererImpl : public ex_ui::draw::defs::IRenderer
	{
	protected:
		mutable CPngBitmapPtr
		           m_bkgnd_cache ;
		CWindow&   m_owner_ref   ;

	public:
		 CRendererImpl(::ATL::CWindow&  owner_ref);
		~CRendererImpl(void);

	protected:
		HRESULT    DrawParentBackground(const HWND hChild, const HDC hSurface, RECT& rcUpdated) override;

	public:
		HRESULT    GetImageSize(SIZE& __in_out_ref) const;

	public:
		static
		HRESULT    ImageSize(CPngBitmapPtr&, SIZE&);

	private: // non-copyable;
		CRendererImpl(const CRendererImpl&);
		CRendererImpl& operator= (const CRendererImpl&);
	};

	class CRendererImpl_Ex : public CRendererImpl {
	                        typedef CRendererImpl TBase;
	private:
		mutable
		CPngBitmapPtr  m_tile_ptr; // it very looks like as superfluous and must be replaced by something more simplistic;

	public:
		 CRendererImpl_Ex(::ATL::CWindow&  owner_ref);
		 CRendererImpl_Ex(const UINT nResId, ::ATL::CWindow&  owner_ref);
		~CRendererImpl_Ex(void);

	private:
#pragma warning(disable:4481)
		HRESULT    DrawBackground(const HDC hSurface, const RECT& rcDrawArea) override sealed;
#pragma warning(default:4481)
	public:
		HRESULT    Draw (const HDC, const RECT& rcDrawArea);
		HRESULT    Draw (CZBuffer&, const RECT& rcDrawArea);

	public:
		HRESULT    LoadFile(LPCWSTR pszFilePath);
		HRESULT    LoadTile(const UINT nResId);

	public:
		bool       IsValid (void) const;
		SIZE       TileSize(void) const;

	private:
		CRendererImpl_Ex(const CRendererImpl_Ex&);
		CRendererImpl_Ex& operator= (const CRendererImpl_Ex&);
	};
}}

typedef ex_ui::draw::CExclusion       TExclusion;
typedef ex_ui::draw::CRenderer        TRenderer ;
typedef ex_ui::draw::CRenderer_Cached TRndCached;
typedef ex_ui::draw::CRenderer_Tile   TRndTile  ;
typedef ex_ui::draw::CRendererImpl    TRndImpl  ;
typedef ex_ui::draw::CRendererImpl_Ex TRndImpl_2;

#endif/*_UIXDRAWRENDERER_H_B10C71E6_F9B5_49d6_A9EE_82013C781AAB_INCLUDED*/