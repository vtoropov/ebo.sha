#ifndef _DRIVERSVCPROPS_H_C41FAEF4_11C8_4F44_A1AC_7FC3018A1770_INCLUDED
#define _DRIVERSVCPROPS_H_C41FAEF4_11C8_4F44_A1AC_7FC3018A1770_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Jan-2021 at 9:20:45.364 pm, UTC+7, Novosibirsk, Thursday;
	This is Ebo Pack generic driver service property set interface declaration file.
*/
#include "sys.svc.crt.h"
#include "sys.svc.service.h"
#include "shared.fs.defs.h"

namespace shared { namespace driver { namespace client {

	using namespace shared::service;

	class CServiceProps {
	public:
		enum _id : UINT { e_name, e_type, e_path, e_state, e_start, e_desc, e__count };

	private:
		CStringW   m_values[_id::e__count];

	public:
		 CServiceProps (void);
		 CServiceProps (const CServiceProps&);
		~CServiceProps (void);

	public:
		UINT     Count   (void) const;
		LPCWSTR  Property(const UINT _ndx) const;
		HRESULT  Property(const UINT _ndx, LPCWSTR _lp_sz_value);
		VOID     Undef   (void) ; // sets properties to undefined values; it is required in case when UI needs to show something in case of error;
		VOID     Update  (const CCrtData&);
		VOID     Update  (const CService&);

	public:
		CServiceProps& operator = (const CServiceProps&);
		CServiceProps& operator <<(const CService&);
		CServiceProps& operator <<(const CCrtData&);
	};

}}}

typedef shared::driver::client::CServiceProps  TDrvSvcPropSet;

#endif/*_DRIVERSVCPROPS_H_C41FAEF4_11C8_4F44_A1AC_7FC3018A1770_INCLUDED*/