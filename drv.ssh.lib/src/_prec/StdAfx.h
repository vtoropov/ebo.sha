#ifndef _STDAFX_H_4BE97360_F93F_4e5e_BBA9_1131455A19E1_INCLUDED
#define _STDAFX_H_4BE97360_F93F_4e5e_BBA9_1131455A19E1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-May-2018 at 12:17:27pm, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian driver common library precompiled headers definition file (thefileguardian.com).
	-----------------------------------------------------------------------------
	Adopted to Ebo virtual audio device/connector project on 20-Jun-2020 at 9:02:21p, UTC+7, Novosibirsk, Saturday;
	Adopted to SSH Tunnel driver WFP filter project on 2-Aug-2020 at 11:29:57p, UTC+7, Novosibirsk, Sunday;
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 8:09:37.588 pm, UTC+7, Novosibirsk, Wednesday;
*/

#ifndef WINVER                 // Specifies that the minimum required platform is Windows Vista.
#define WINVER         0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT           // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT   0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS         // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINDOWS 0x0600  // Change this to the appropriate value to target other platforms.
#endif

#define __unused(x) (void)(x)
#define __extern_c extern "C"

//
// these definitions are very important, otherwise, WFP callouts are compiled with errors:
// error C2065: 'NET_BUFFER_LIST': undeclared identifier
//
//#define NT 1
//#define NDIS60 1
#define NDIS_SUPPORT_NDIS6 1

__extern_c {
#pragma warning(push, 0)
	#include <wdm.h>
	#include <ntddk.h>
	#include <ntstrsafe.h>
	#include <ndis.h>
	#include <fwpmk.h>
	#include <fwpsk.h>
#pragma warning(pop)
}

#include <ks.h>
#include <stdlib.h>   // for bsearch()
#include <wsk.h>

#pragma prefast(disable:__WARNING_ENCODE_MEMBER_FUNCTION_POINTER, "Not valid for kernel mode drivers")
/////////////////////////////////////////////////////////////////////////////
//
// macro utilities
//

#define FlagOnAll( F, T )  \
    (FlagOn( F, T ) == T)

#define SetFlagInterlocked(_ptrFlags,_flagToSet) \
    ((VOID)InterlockedOr(((volatile LONG*)(_ptrFlags)),_flagToSet))

#ifndef _T
#define _T(x) L ## x
#endif
#ifndef S_OK
#define S_OK STATUS_SUCCESS
#endif
#ifndef SUCCEEDED
#define SUCCEEDED(_nt_status) (S_OK == _nt_status)
#endif
#ifndef FAILED
#define FAILED(_nt_status) (S_OK != _nt_status)
#endif

#define PAGE_CODE_EX(_return)                            \
		if (PASSIVE_LEVEL != KeGetCurrentIrql()) {       \
			return _return;                              \
		}

#ifndef AUX_STRING_POOL_TAG
#define AUX_STRING_POOL_TAG  'auxs'
#endif

#ifndef AUX_LIST_NODE_TAG
#define AUX_LIST_NODE_TAG    'list'
#endif

#ifndef AUX_IP_RANGE_TAG
#define AUX_IP_RANGE_TAG     'rang'
#endif

#ifndef AUX_IP_PORTS_TAG
#define AUX_IP_PORTS_TAG     'port'
#endif

#ifndef AUX_IP_PORTS_DST_TAG
#define AUX_IP_PORTS_DST_TAG 'dest'
#endif

/*
#define HTONL(l) (\
	(((l) & 0xff000000) >> 24) | \
	(((l) & 0x00ff0000) >>  8) | \
	(((l) & 0x0000ff00) <<  8) | \
	(((l) & 0x000000ff) << 24)   \
)
#define NTOHL(l) HTONL(l)

#define HTONS(s) (\
	(((s) & 0xff00) >> 8) | \
	(((s) & 0x00ff) << 8)   \
)
#define NTOHS(s) HTONS(s)
*/

// these are intrisics for the BSWAP instruction, much faster than the above macros;
// https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/byteswap-uint64-byteswap-ulong-byteswap-ushort
#define HTONL(l) _byteswap_ulong(l)
#define NTOHL(l) HTONL(l)
#define HTONS(s) _byteswap_ushort(s)
#define NTOHS(s) HTONS(s)

#endif/*_STDAFX_H_4BE97360_F93F_4e5e_BBA9_1131455A19E1_INCLUDED*/