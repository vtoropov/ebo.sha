#ifndef _EBOSHAVIEWEMPTY_H_43F29E6D_CEE5_485D_846D_E7C15B179290_INCLUDED
#define _EBOSHAVIEWEMPTY_H_43F29E6D_CEE5_485D_846D_E7C15B179290_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Nov-2019 at 8:13:04.085 am, UTC+7, Novosibirsk, Monday;
	This is Ebo Pack no functionality test view interface declaration file.
*/
#include "ebo.sha.view.bas.h"

#include "sfx.image.ctrl.h"
#include "sfx.image.fmt.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CEmptyView : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef  CViewWnd_Base TViewWnd; friend class CEmptyView;
		private:
			CEmptyView&  m_view;
			TImageCtrl   m_na_image;
			bool         m_borders ;

		public:
			 CViewWnd (CEmptyView&);
			~CViewWnd (void);

		private:
#pragma warning(disable:4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning(default:4481)
		};
	private:
		CViewWnd   m_wnd ;

	public:
		 CEmptyView(void);
		~CEmptyView(void);

	public:
		VOID   DrawBorder(const bool _b_on_off);
	};
}}}}

typedef ebo::sha::gui::view::CEmptyView  TEmptyView;

#endif/*_EBOSHAVIEWEMPTY_H_43F29E6D_CEE5_485D_846D_E7C15B179290_INCLUDED*/