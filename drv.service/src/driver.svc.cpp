/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Aug-2020 at 8:15:43p, UTC+7, Novosibirsk, Thursday;
	This is SSH-Tunnel WFP driver service management interface implementation file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 7:01:19.229 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "StdAfx.h"
#include "driver.svc.h"

using namespace shared::driver::client;
using namespace shared::service;

/////////////////////////////////////////////////////////////////////////////

TDrvService:: CService_Ex (void) : TBase() {}
TDrvService:: CService_Ex (const TDrvService& _ref) : TDrvService() { *this = _ref; }
TDrvService::~CService_Ex (void) {}

/////////////////////////////////////////////////////////////////////////////
const
TDrvSvcPropSet& TDrvService::Properties (void) const { return this->m_props; }
TDrvSvcPropSet& TDrvService::Properties (void)       { return this->m_props; }

/////////////////////////////////////////////////////////////////////////////

TDrvService&  TDrvService::operator = (const TDrvService& _ref) { (TBase&)*this = (const TBase&)_ref; *this << _ref.Properties(); return *this; }
TDrvService&  TDrvService::operator <<(const TDrvSvcPropSet& _props) { this->Properties() = _props; return *this; }

/////////////////////////////////////////////////////////////////////////////

TDrvServiceMan:: CServiceMan(void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
TDrvServiceMan::~CServiceMan(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     TDrvServiceMan::Create(const CCrtData& _crt, CService& _svc_ssh) {
	m_error << __MODULE__ << S_OK;

	if (_crt.IsValid() == false)
		return (m_error = E_INVALIDARG);

	CManager svc_man;

	HRESULT hr_ = svc_man.Handle().Open();
	if (FAILED(hr_))
		return (m_error = svc_man.Error());

	hr_ = svc_man.Create(_crt, _svc_ssh);
	if (FAILED(hr_))
		return (m_error = svc_man.Error());

	return m_error;
}

HRESULT     TDrvServiceMan::Delete(const CCrtData& _crt) {
	m_error << __MODULE__ << S_OK;

	if (_crt.IsValid() == false)
		return (m_error = E_INVALIDARG);

	CManager svc_man;

	HRESULT hr_ = svc_man.Handle().Open();
	if (FAILED(hr_))
		return (m_error = svc_man.Error());

	hr_ = svc_man.Delete(_crt.Name());
	if (FAILED(hr_))
		return (m_error = svc_man.Error());


	return m_error;
}

TErrorRef   TDrvServiceMan::Error (void) const { return m_error; }

HRESULT     TDrvServiceMan::Start (const CCrtData& _crt) {
	m_error << __MODULE__ << S_OK;

	if (_crt.IsValid() == false)
		return (m_error = E_INVALIDARG);

	CService svc_ssh;
	CManager svc_man;

	HRESULT hr_ = svc_man.Handle().Open();
	if (FAILED(hr_))
		return (m_error = svc_man.Error());

	hr_ = svc_man.Open(_crt, GENERIC_READ, svc_ssh);
	if (FAILED(hr_))
		return (m_error = svc_man.Error());

	if (svc_ssh.State().Running()) {
		return ((m_error << __DwordToHresult(ERROR_INVALID_STATE)) = _T("The service is already running;"));
	}
	svc_ssh.Close();

	hr_ = svc_man.Start(_crt.Name());
	if (FAILED(hr_))
		return (m_error = svc_man.Error());

	return m_error;
}

HRESULT     TDrvServiceMan::Stop  (const CCrtData& _crt) {
	m_error << __MODULE__ << S_OK;

	if (_crt.IsValid() == false)
		return (m_error = E_INVALIDARG);

	CService svc_ssh;
	CManager svc_man;

	HRESULT hr_ = svc_man.Handle().Open();
	if (FAILED(hr_))
		return (m_error = svc_man.Error());

	hr_ = svc_man.Open(_crt, GENERIC_READ, svc_ssh);
	if (FAILED(hr_))
		return (m_error = svc_man.Error());

	if (svc_ssh.State().Stopped()) {
		return ((m_error << __DwordToHresult(ERROR_INVALID_STATE)) = _T("The service is already stopped;"));
	}
	svc_ssh.Close();

	hr_ = svc_man.Stop(_crt.Name());
	if (FAILED(hr_))
		return (m_error = svc_man.Error());

	return m_error;
}

HRESULT     TDrvServiceMan::Status(const CCrtData& _crt, CService& _svc_ssh) {
	m_error << __MODULE__ << S_OK;

	if (_crt.IsValid() == false)
		return (m_error = _crt.Error());

	CManager svc_man;

	HRESULT hr_ = svc_man.Handle().Open();
	if (FAILED(hr_))
		return (m_error = svc_man.Error());

	hr_ = svc_man.Open(_crt, GENERIC_READ, _svc_ssh);
	if (FAILED(hr_))
		return (m_error = svc_man.Error());

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////