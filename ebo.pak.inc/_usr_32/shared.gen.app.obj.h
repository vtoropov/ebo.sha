#ifndef _SHAREDLITEGENERICAPPLICATIONOBJECT_H_9D10F860_5FE9_4b74_91D1_9E9D329E142F_INCLUDED
#define _SHAREDLITEGENERICAPPLICATIONOBJECT_H_9D10F860_5FE9_4b74_91D1_9E9D329E142F_INCLUDED
/*
	Created by Tech_dog (VToropov) on 11-Jun-2016 at 1:28:28p, GMT+7, Phuket, Rawai, Saturday;
	This is Shared Lite Library Generic Application class declaration file.
*/
#include <Shellapi.h>
#include <map>
#pragma comment(lib, "version.lib")

#include "shared.app.cmd.ln.h"
#include "shared.gen.ver.info.h"

namespace shared { namespace user32 {

	class CApplication {
	public:
		class CObject  {
		private:
			CONST
			CApplication& m_app_obj_ref;
			HANDLE        m_mutex      ;
			CAtlString    m_mutex_name ;
			DWORD         m_proc_state ;
			
		public:
			explicit
			 CObject(CONST CApplication&);
			~CObject(VOID);
		public:
			bool          IsSingleton(void) const;
			HRESULT       RegisterSingleton(LPCTSTR pMutexName);
			HRESULT       UnregisterSingleton(VOID);
		};
	private:
		HRESULT          m_hResult;     // last result
		mutable 
		CAtlString       m_app_name;
		CVersion         m_version ;
		CObject          m_object  ;
		CCommandLine     m_cmd_line;
	public:
		 CApplication(void);
		~CApplication(void);
	public:
		const
		TCmdLine&        CommandLine(void) const;
#if defined(_DEBUG)
		TCmdLine&        CommandLine(void);
#endif
		CAtlString       GetFileName(const bool bNoExtension = false) const;
		HRESULT          GetLastResult(void) const;
		LPCTSTR          GetName(void) const;
		HRESULT          GetPath(::ATL::CAtlString& __in_out_ref) const;
		HRESULT          GetPathFromAppFolder(LPCTSTR lpszPattern, ::ATL::CAtlString& __in_out_ref) const;
		const CObject&   Instance(VOID)const;
		CObject&         Instance(VOID);
		const CVersion&  Version (VOID)const;
	public:
		static HRESULT   GetFullPath(::ATL::CAtlString&);
		static bool      Is64bit(VOID);
		static bool      IsRelatedToAppFolder(LPCTSTR pPath);
	private:
		CApplication(const CApplication&);
		CApplication& operator= (const CApplication&);
	};

	CApplication&  GetAppObjectRef(void); // gets the static object reference; it is used in a client of version 2
}}

#endif/*_SHAREDLITEGENERICAPPLICATIONOBJECT_H_9D10F860_5FE9_4b74_91D1_9E9D329E142F_INCLUDED*/