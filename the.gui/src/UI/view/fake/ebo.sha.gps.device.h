#ifndef _EBOSHAGPSDEVICE_H_7DA55BE8_B220_4CD2_8A30_17E28433E2F9_INCLUDED
#define _EBOSHAGPSDEVICE_H_7DA55BE8_B220_4CD2_8A30_17E28433E2F9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Nov-2020 at 1:54:04a, UTC+7, Novosibirsk, Thursday;
	This is Ebo Sha Optima Tool fake GPS device sub-view interface declaration file.
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.gui.cmp.cap.h"
#include "ebo.sha.gui.cmp.edit.h"
#include "ebo.sha.gui.fmt.edit.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CFake_Dev : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CFake_Dev;
		private:
			CFake_Dev&  m_view    ;
			TCaptionComponent m_caps[2];
			TEditRa     m_iface[3];
			TEditRa     m_class[3];
			TInfoPanel  m_info    ;
			TButtonCtrl m_browse  ;

		private:
			 CViewWnd (CFake_Dev&);
			~CViewWnd (void);

		private:   // TWindow
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CFake_Dev (void);
		~CFake_Dev (void);
	};

}}}}

typedef ebo::sha::gui::view::CFake_Dev  TFakeDev;

#endif/*_EBOSHAGPSDEVICE_H_7DA55BE8_B220_4CD2_8A30_17E28433E2F9_INCLUDED*/