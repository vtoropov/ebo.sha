#ifndef _SHAREDLITEGENERICHANDLE_H_212F82EC_7E06_468a_8DEA_B2A98F4F2D6F_INCLUDED
#define _SHAREDLITEGENERICHANDLE_H_212F82EC_7E06_468a_8DEA_B2A98F4F2D6F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Feb-2011 at 11:28:04p, GMT+3, Rostov-on-Don, Sunday;
	This is Row27 project shared library generic handle class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to Platinum project on 29-Nov-2014 at 0:42:43am, GMT+3, Taganrog, Saturday;
	Adopted to File Guardian project on 27-May-2018 at 3:21:42p, UTC+7, Phuket, Rawai, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.syn.obj.h"

namespace shared { namespace common
{
	using shared::sys_core::CError;
	// https://docs.microsoft.com/en-us/windows/win32/api/handleapi/nf-handleapi-duplicatehandle
	class CAutoHandle
	{
	protected:
		HANDLE     m_handle;

	public:
		 CAutoHandle(void);
	explicit CAutoHandle(const CAutoHandle&); // duplicates a handle;
		 CAutoHandle(const HANDLE);       // assigns a handle;
		~CAutoHandle(void);

	public:
		CAutoHandle& operator = (const CAutoHandle&); // duplicates a handle;
		CAutoHandle& operator = (const HANDLE);       // assigns a handle;

	public:
		operator   HANDLE(void);
		operator   HANDLE(void) const;

	public:
		PHANDLE    operator&(void);

	public:
		HRESULT    Clone (const HANDLE); // no given handle verification is done; duplication the handle is made anyway;
		HANDLE     Handle(void) const;
		bool       Is    (void) const;
		VOID       Reset (void);
	};

	class CAutoHandleArray
	{
	private:
		PHANDLE    m_handles;
		DWORD      m_size;
		CError     m_error;
	public:
	    CAutoHandleArray(const DWORD dwSize);
	   ~CAutoHandleArray(void);
	public:
		TErrorRef  Error  (void)const;
		bool       Is     (void)const;
		PHANDLE    Handles(void)const;
		DWORD      Size   (void)const;
	public:
		HANDLE     operator[] (const INT) const;
		HANDLE&    operator[] (const INT)      ;
	private:
		CAutoHandleArray(const CAutoHandleArray&);
		CAutoHandleArray& operator= (const CAutoHandleArray&);
	};

	using shared::sys_core::CSyncObject;

	//
	// TODO: actually we need the handle with reference counting, otherwise, protected access doesn't protect from
	//       destroying the handle while it is still used in one of the worker threads;
	//
	class CHandleSafe
	{
	private:
		HANDLE      m_handle  ;
		CSyncObject m_sync_obj;
	public:
		 CHandleSafe(const HANDLE = INVALID_HANDLE_VALUE);
		~CHandleSafe(void);
	public:
		CHandleSafe& operator=(const HANDLE);
	public:
		operator    HANDLE(void);
		operator    HANDLE(void) const;
	public:
		PHANDLE     operator&(void);
	private:
		CHandleSafe(const CHandleSafe&);
		CHandleSafe& operator= (const CHandleSafe&);
	public:
		HANDLE      Handle (void) const;
		bool        Is     (void) const;
		VOID        Reset  (void);
	};

	class CGlobalAlloca
	{
	private:
		mutable
		CError      m_error;
		HGLOBAL     m_global;
		DWORD       m_dwSize;
		LPVOID      m_pData;
		bool        m_bMovable;
	public:
	    CGlobalAlloca(const PVOID _data, const DWORD _size, const bool bMovable = true);
	   ~CGlobalAlloca(void);
	public:
		LPVOID      Data   (void) const;
		HGLOBAL     Detach (void)      ;
		TErrorRef   Error  (void) const;
		HGLOBAL     Handle (void) const;
		bool        IsValid(void) const;
		DWORD       Size   (void) const;
	private:
		CGlobalAlloca(const CGlobalAlloca&);
		CGlobalAlloca& operator= (const CGlobalAlloca&);
	};

	class CStdHandle : public CAutoHandle {
	                  typedef CAutoHandle THandle;
	protected:
		DWORD     m_std_type;
		CError    m_error;

	public:
		 CStdHandle (const DWORD _handle_type);
		~CStdHandle (void);

	public:
		HRESULT   Close(void)      ;
		TErrorRef Error(void) const;
		DWORD     Type (void) const;
	public:
		CStdHandle& operator=(const HANDLE);
	};
}}

#endif/*_SHAREDLITEGENERICHANDLE_H_212F82EC_7E06_468a_8DEA_B2A98F4F2D6F_INCLUDED*/