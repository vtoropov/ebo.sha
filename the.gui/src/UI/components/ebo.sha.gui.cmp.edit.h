#ifndef _EBOSHAGUICMPEDIT_H_BDA6B88D_115A_41B6_895A_84C93167EA42_INCLUDED
#define _EBOSHAGUICMPEDIT_H_BDA6B88D_115A_41B6_895A_84C93167EA42_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2021 at 6:31:32.469 pm, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool generic label-edit pair GUI component interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:01:04.651 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.gen.clrs.h"
#include "sfx.label.ctrl.h"

namespace ebo { namespace sha { namespace gui {

	class CEdit_Ra {
	protected:
		TLabelCtrl  m_label;
		TLabelCtrl  m_edit ;

	public:
		 CEdit_Ra (void);
		~CEdit_Ra (void);

	public:
		const
		TLabelCtrl&   Edit (void) const; // a reference to read-only pseudo-edit control as to lable;
		TLabelCtrl&   Edit (void)      ; // a reference to read-only pseudo-edit control as to lable;
		const
		TLabelCtrl&   Label(void) const; // a reference to label control that is used as edit caption;
		TLabelCtrl&   Label(void)      ; // a reference to label control that is used as edit caption;

	public:
		HRESULT   Create (const HWND _parent, const RECT& _rc_label, const RECT& _rc_edit);
		HRESULT   Destroy(void);

	private: // non-copyable;
		CEdit_Ra (const CEdit_Ra&);
		CEdit_Ra& operator = (const CEdit_Ra&);
	};

}}}

typedef ebo::sha::gui::CEdit_Ra  TEditRa;

#endif/*_EBOSHAGUICMPEDIT_H_BDA6B88D_115A_41B6_895A_84C93167EA42_INCLUDED*/