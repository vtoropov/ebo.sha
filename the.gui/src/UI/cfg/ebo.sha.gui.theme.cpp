/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2020 at 8:37:53.978 am, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Sha Optima Tool GUI generic format color theme interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:03:27.692 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "StdAfx.h"
#include "ebo.sha.gui.theme.h"

using namespace ebo::sha::theme;
using namespace ebo::sha::theme::colors;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace format { namespace _impl {

}}}}}
using namespace ebo::sha::gui::format::_impl;
/////////////////////////////////////////////////////////////////////////////

TColorMarker:: CColor_Marker (void) : m_palette(TThemePalette::e_none), m_ui_part(TThemePart::e_none), m_ui_state(TThemeState::e_default) {}
TColorMarker:: CColor_Marker (const TColorMarker& _ref) : TColorMarker() { *this = _ref; }
TColorMarker:: CColor_Marker (const TThemePalette _palette, const TThemePart _part, const TThemeElement _element, const TThemeState _state) : TColorMarker() {
	*this << _palette << _part << _element << _state;
}
TColorMarker::~CColor_Marker (void) {}

/////////////////////////////////////////////////////////////////////////////

const TThemeElement   TColorMarker::Element (void) const { return m_element ; }
const TThemePalette   TColorMarker::Palette (void) const { return m_palette ; }
const TThemePart      TColorMarker::Part    (void) const { return m_ui_part ; }
const TThemeState     TColorMarker::State   (void) const { return m_ui_state; }

/////////////////////////////////////////////////////////////////////////////

const bool TColorMarker::Is (void) const { return !(TThemePalette::e_none == m_palette || TThemePart::e_none == m_ui_part || TThemeElement::e_none == m_element); }

/////////////////////////////////////////////////////////////////////////////

TColorMarker& TColorMarker::operator = (const CColor_Marker& _ref) { *this << _ref.Palette() << _ref.Part() << _ref.State() << _ref.Element(); return *this; }
TColorMarker& TColorMarker::operator <<(const CTheme_Element _element) { this->m_element = _element; return *this; }
TColorMarker& TColorMarker::operator <<(const CTheme_Palette _palette) { this->m_palette = _palette; return *this; }
TColorMarker& TColorMarker::operator <<(const CTheme_Part _part) { this->m_ui_part = _part; return *this; }
TColorMarker& TColorMarker::operator <<(const CTheme_State _state) { this->m_ui_state = _state; return *this; }

/////////////////////////////////////////////////////////////////////////////

TColorMatrix:: CColor_Matrix (void) {
	
	m_palettes = {
		{
			TThemePalette::e_dark, {
				{ TThemePart::e_button , {{ TThemeElement::e_back  , {{ TThemeState::e_default, RGB(  70,  70,  70) }, { TThemeState::e_hovered , RGB( 102,  90,  71) }, { TThemeState::e_selected, RGB(  70,  70,  70) }} },
				                          { TThemeElement::e_border, {{ TThemeState::e_default, RGB( 115, 115, 115) }, { TThemeState::e_hovered , RGB( 229, 183,  66) },
				                                                      { TThemeState::e_disable, RGB( 115, 115, 115) }, { TThemeState::e_selected, RGB( 229, 183,  66) }} },
	                                      { TThemeElement::e_fore  , {{ TThemeState::e_default, RGB( 230, 230, 230) }, { TThemeState::e_hovered , RGB( 229, 183,  66) },
				                                                      { TThemeState::e_disable, RGB( 115, 115, 115) }, { TThemeState::e_selected, RGB( 183, 163,  66) }} }}},
				{ TThemePart::e_caption, {{ TThemeElement::e_back  , {{ TThemeState::e_default, RGB(  70,  70,  70) }} },
				                          { TThemeElement::e_fore  , {{ TThemeState::e_default, RGB( 230, 230, 230) }} } }},
				{ TThemePart::e_edit   , {{ TThemeElement::e_back  , {{ TThemeState::e_default, RGB(  70,  70,  70) }, { TThemeState::e_disable , RGB(  70,  70,  70) }} },
				                          { TThemeElement::e_border, {{ TThemeState::e_default, RGB( 115, 115, 115) }, { TThemeState::e_disable , RGB( 115, 115, 115) }} },
				                          { TThemeElement::e_fore  , {{ TThemeState::e_default, RGB( 230, 230, 230) }, { TThemeState::e_disable , RGB( 230, 230, 230) }} }}},
				{ TThemePart::e_form   , {{ TThemeElement::e_back  , {{ TThemeState::e_default, RGB(  61,  61,  61) }} },
				                          { TThemeElement::e_border, {{ TThemeState::e_default, RGB( 229, 183,  66) }} },
				                          { TThemeElement::e_fore  , {{ TThemeState::e_default, RGB( 230, 230, 230) }} } }},
				{ TThemePart::e_label  , {{ TThemeElement::e_back  , {{ TThemeState::e_default, RGB(  70,  70,  70) }} },
				                          { TThemeElement::e_fore  , {{ TThemeState::e_default, RGB( 230, 230, 230) }} } }},
				{ TThemePart::e_panel  , {{ TThemeElement::e_back  , {{ TThemeState::e_default, RGB(  61,  61,  61) }} },
				                          { TThemeElement::e_border, {{ TThemeState::e_default, RGB( 229, 183,  66) }, { TThemeState::e_disable , RGB( 115, 115, 115) }} }}}//,
			//	{ TThemePart::, {{, {{ }} }} }
			}
		}
	};
}
TColorMatrix:: CColor_Matrix (const TColorMatrix& _ref) : TColorMatrix() { *this = _ref; }
TColorMatrix::~CColor_Matrix (void) {}

/////////////////////////////////////////////////////////////////////////////

TColorMatrix&  TColorMatrix::operator = (const TColorMatrix& _ref) { this->m_palettes = _ref.m_palettes; return *this; }
const COLORREF TColorMatrix::operator <<(const CColor_Marker& _marker) const {

	COLORREF clr_result = CLR_NONE;
	if (_marker.Is()) {
		TColor_Palette::const_iterator it_palette = m_palettes.find(_marker.Palette());
		if (it_palette != m_palettes.end() ) {
			TColor_Part::const_iterator it_part = it_palette->second.find(_marker.Part());
			if (it_part != it_palette->second.end()) {
				TColor_Element::const_iterator it_element = it_part->second.find(_marker.Element());
				if (it_element != it_part->second.end()) {
					TColor_State::const_iterator it_state = it_element->second.find(_marker.State());
					if (it_state != it_element->second.end()) {
						clr_result = it_state->second;
					}
				}
			}
		}
	}

	return clr_result;
}

/////////////////////////////////////////////////////////////////////////////

TTheme:: CTheme (void) : m_current(TThemePalette::e_dark) {}
TTheme:: CTheme (const TTheme& _ref) : TTheme() { *this = _ref; }
TTheme::~CTheme (void) {}

/////////////////////////////////////////////////////////////////////////////

const TColorMatrix&  TTheme::Matrix  (void) const { return m_clr_mtx; }
const TThemePalette  TTheme::Palette (void) const { return m_current; }

/////////////////////////////////////////////////////////////////////////////

COLORREF TTheme::Get (const TThemePart _part, const TThemeElement _el, const TThemeState _state) const {
	return ((const COLORREF)(m_clr_mtx << TColorMarker(this->Palette(), _part, _el, _state)));
}

/////////////////////////////////////////////////////////////////////////////

TTheme&  TTheme::operator = (const TTheme& _ref) { this->m_clr_mtx = _ref.Matrix(); this->m_current = _ref.Palette();  return *this; }

/////////////////////////////////////////////////////////////////////////////

namespace shared {
	
	TTheme&   Get_Theme (void) {
		static TTheme the_theme;
		return the_theme;
	}

}