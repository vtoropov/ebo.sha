/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Nov-2020 at 2:06:28a, UTC+7, Novosibirsk, Thursday;
	This is Ebo Sha Optima Tool fake GPS device sub-view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.gps.device.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;
using namespace ex_ui::controls;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CFake_Dev_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_cap_0  = IDC_EBO_SHA_FAK_DEV_CAP_0,
			e_btn_browse = IDC_EBO_SHA_BUT_REF_H
		};
	}; typedef CFake_Dev_Ctl This_Ctl;

	class CFake_Dev_Res {
	public:
		enum _res : WORD {
			e_none = 0,
			e_logo_iface = IDR_EBO_SHA_PUZ_AUT_M,
			e_logo_class = IDR_EBO_SHA_NOU_GAT_L
		};
	}; typedef CFake_Dev_Res This_Res;

	class CFake_Dev_Layout {
	public:
		typedef TCtrlDefLayout::_pos _pos;
	private:
		CFake_Dev& m_view;
		RECT       m_rect;

	public:
		 CFake_Dev_Layout (CFake_Dev& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CFake_Dev_Layout (void) {}

	public:
		RECT   GetImage (const DWORD _ndx) {
			static const SIZE sz_logo = { 72, 72 };
			RECT rc_logo = {
				10, (0 == _ndx ? 0 : _pos::e_avt_shift), 10 + sz_logo.cx, (0 == _ndx ? sz_logo.cy : _pos::e_avt_shift + sz_logo.cy)
			};
			return rc_logo;
		}
		RECT   GetTitle (const DWORD _ndx) { return this->_get_cap_rect(_ndx); }

	public:
		RECT   GetLab_Class(const DWORD _ndx) {
			_ndx;
			RECT rc_lab = {_pos::e_lab_left, _pos::e_cap_shift + (_pos::e_cap_height + _pos::e_lab_gap), _pos::e_lab_left + _pos::e_lab_width, 0 };

			rc_lab.top += (_pos::e_lab_height + _pos::e_lab_gap) * _ndx; rc_lab.bottom = rc_lab.top + _pos::e_lab_height;

			return rc_lab;
		}

		RECT   GetEdt_Class(const DWORD _ndx) {
			_ndx;
			RECT rc_edit = {_pos::e_lab_left   + _pos::e_lab_width, _pos::e_cap_shift + (_pos::e_cap_height + _pos::e_lab_gap), 
			                _pos::e_lab_left   + _pos::e_lab_width + _pos::e_edt_width, 0
			};
			rc_edit.top += (_pos::e_edt_height + _pos::e_lab_gap - _pos::e_edt_border_thickness) * _ndx;
			rc_edit.top -= (_pos::e_edt_border_thickness * (1 + _ndx)); rc_edit.bottom = rc_edit.top + _pos::e_edt_height;
			return rc_edit;
		}
		/////////////////////////////////////////////////////////////////////////////
		RECT   GetLab_Iface(const DWORD _ndx) {
			_ndx;
			RECT rc_lab = {_pos::e_lab_left, m_rect.top + (_pos::e_cap_height + _pos::e_lab_gap), _pos::e_lab_left + _pos::e_lab_width, 0 };

			rc_lab.top += (_pos::e_lab_height + _pos::e_lab_gap) * _ndx; rc_lab.bottom = rc_lab.top + _pos::e_lab_height;

			return rc_lab;
		}

		RECT   GetEdt_Iface(const DWORD _ndx) {
			_ndx;
			RECT rc_edit = {_pos::e_lab_left   + _pos::e_lab_width, m_rect.top + (_pos::e_cap_height + _pos::e_lab_gap), 
			                _pos::e_lab_left   + _pos::e_lab_width + _pos::e_edt_width, 0
			};
			rc_edit.top += (_pos::e_edt_height + _pos::e_lab_gap - _pos::e_edt_border_thickness) * _ndx;
			rc_edit.top -= (_pos::e_edt_border_thickness * (1 + _ndx)); rc_edit.bottom = rc_edit.top + _pos::e_edt_height;

			if (2 == _ndx)
				rc_edit.right += 180;
			return rc_edit;
		}
		/////////////////////////////////////////////////////////////////////////////

		RECT   InfArea (VOID) {
			RECT rc_inf = m_rect; rc_inf.top = rc_inf.bottom - 75;
			return rc_inf;
		}

		RECT   GetBtn_Browse (const SIZE& _req_sz) {
			_req_sz;
			RECT rc_edt = this->GetEdt_Class(2);
			RECT rc_btn = {
			     rc_edt.right - _req_sz.cx, rc_edt.bottom + _pos::e_lab_gap, rc_edt.right, rc_edt.bottom + _pos::e_lab_gap + _req_sz.cy
			};
			return rc_btn;
		}

		/////////////////////////////////////////////////////////////////////////////
		VOID   Update (TCaptionComponent* _p_caps, const DWORD _d_count) {
			if (NULL == _p_caps || 0 == _d_count)
				return;
			HDWP pos_def = ::BeginDeferWindowPos(_d_count);
			for (DWORD i_ = 0; i_ < _d_count; i_++)
				_p_caps[i_].Title().Layout().Update(this->_get_cap_rect(i_)); // DeferWindowPos() must be used for positioning a window;
			::EndDeferWindowPos(pos_def);
		}

	private:
		RECT  _get_cap_rect(const DWORD _ndx) {
			RECT rc_cap = m_rect; rc_cap.top = (0 ==_ndx ? 0 : _pos::e_cap_shift); rc_cap.bottom = rc_cap.top + _pos::e_cap_height;
			return rc_cap;
		}
	};

	class CFake_Dev_Init {
	private:
		CFake_Dev& m_view ;
		CError     m_error;

	public:
		 CFake_Dev_Init (CFake_Dev& _view) : m_view(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }
		~CFake_Dev_Init (void) {}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;

			return m_error;
		}
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CFake_Dev::CViewWnd:: CViewWnd(CFake_Dev& _view) : TViewWnd(_view), m_view(_view), m_info(_view), m_browse(_view) {}
CFake_Dev::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CFake_Dev::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	CFake_Dev_Layout layout_(m_view);
	CFake_Dev_Init init_(m_view); init_.OnCreate();

	HRESULT hr_ = m_caps[0].Create(*this, _T("Interface Info"), layout_.GetTitle(0), layout_.GetImage(0));
	if (SUCCEEDED(hr_)) {
		m_caps[0].Format(This_Res::e_logo_iface);
		m_caps[0].Image().Format().Bkgnd().Sectors().Append(shared::Get_Theme().Get(TThemePart::e_caption,  TThemeElement::e_back), TTileSize(80, 30));
		m_caps[0].Image().Format().Bkgnd().Sectors().Append(shared::Get_Format().Panel().Bkgnd().Solid(), TTileSize(80, 50));
		m_caps[0].Image().Layout().Update();
	}

	LPCWSTR lp_sz_iface_cap[] = {_T("GUID"), _T("Flags"), _T("Path")};

	for (UINT i_ = 0; i_ < _countof(m_iface) && i_ < _countof(lp_sz_iface_cap); i_++) {
		shared::Get_Format().Control().Edit() >> m_iface[i_].Edit().Borders();
		shared::Get_Format().Control().Edit() >> m_iface[i_].Edit().Format();
		shared::Get_Format().Control().Edit() >> m_iface[i_].Label().Format();

		m_iface[i_].Create(*this, layout_.GetLab_Iface(i_), layout_.GetEdt_Iface(i_)); m_iface[i_].Label().Text(lp_sz_iface_cap[i_]);
	}

	hr_ = m_caps[1].Create(*this, _T("Class Info"), layout_.GetTitle(1), layout_.GetImage(1));
	if (SUCCEEDED(hr_)) {
		m_caps[1].Format(This_Res::e_logo_class);
		m_caps[1].Image().Format().Bkgnd().Sectors().Append(shared::Get_Theme().Get(TThemePart::e_caption,  TThemeElement::e_back), TTileSize(80, 30));
		m_caps[1].Image().Format().Bkgnd().Sectors().Append(shared::Get_Format().Panel().Bkgnd().Solid(), TTileSize(80, 50));
		m_caps[1].Image().Layout().Update();
	}

	LPCWSTR lp_sz_class_cap[] = {_T("CLSID"), _T("Desc"), _T("Name")};

	for (UINT i_ = 0; i_ < _countof(m_class) && i_ < _countof(lp_sz_class_cap); i_++) {
		shared::Get_Format().Control().Edit() >> m_class[i_].Edit().Borders();
		shared::Get_Format().Control().Edit() >> m_class[i_].Edit().Format();
		shared::Get_Format().Control().Edit() >> m_class[i_].Label().Format();

		m_class[i_].Create(*this, layout_.GetLab_Class(i_), layout_.GetEdt_Class(i_)); m_class[i_].Label().Text(lp_sz_class_cap[i_]);
	}
	const SIZE sz_btn = { 150, 34 };
	shared::Get_Format().Control() >> m_browse.Format() ; 
	
	m_browse.Create(*this, layout_.GetBtn_Browse(sz_btn), This_Ctl::e_btn_browse);
	m_browse.Borders().Thickness(1);
	m_browse.Borders().Color(ex_ui::draw::CColour(shared::Get_Theme().Get(TThemePart::e_button,  TThemeElement::e_border)));
	m_browse.Layout().Layers().Text().String() = _T("Browse Driver...");
	m_browse.Layout().Update();
	m_browse.Refresh();

	CStringW cs_msg; cs_msg.LoadStringW(IDS_EBO_SHA_BLK_PAN_INF_E);
	TInfoErrorFmt() >> m_info ;
	m_info.Create(*this, layout_.InfArea(), cs_msg.GetString(), 0);

	return 0;
}

LRESULT   CFake_Dev::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_b_hand = FALSE;
	for (UINT i_ = 0; i_ < _countof(m_class); i_++) m_class[i_].Destroy();
	for (UINT i_ = 0; i_ < _countof(m_iface); i_++) m_iface[i_].Destroy();

	m_caps[0].Destroy();
	m_caps[1].Destroy();  m_browse.Destroy(); m_info.Destroy();
	
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CFake_Dev::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {

	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CFake_Dev_Layout layout_(m_view); layout_.Update(m_caps, _countof(m_caps));

	if (m_info.Is()) {
		m_info.Layout ().Update(layout_.InfArea());
		m_info.Refresh();
	}

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CFake_Dev:: CFake_Dev (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CFake_Dev::~CFake_Dev (void) {}