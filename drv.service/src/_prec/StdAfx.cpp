/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 at 8:08:27a, UTC+7, Phuket, Rawai, Friday;
	This is generic shared library precompiled headers implementation file.
	-----------------------------------------------------------------------------
	Adopted to SSH-Tunnel project on 1-Aug-2020 at 10:37:03a, UTC+7, Novosibirsk, Saturday;
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 7:42:17.225 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)