#ifndef _EBOSHAVEINSCAN_H_8C13DFB8_2853_4BC4_A678_D8093FF1419D_INCLUDED
#define _EBOSHAVEINSCAN_H_8C13DFB8_2853_4BC4_A678_D8093FF1419D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Nov-2020 at 5:16:08a, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha optima tool vein scan sub-view interface declaration file;
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.view.empty.h"
#include "ebo.sha.gui.cmp.cap.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CVein_Scan : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CVein_Scan;
		private:
			CVein_Scan&   m_view ;
			TCaptionComponent  m_caps[1];

			TEmptyView    m_empty;

			CLabel_Ex     m_scan_labs[1];
			CLabel_Ex     m_scan_edts[1];

		private:
			 CViewWnd (CVein_Scan&);
			~CViewWnd (void);

		private:   // TWindow
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CVein_Scan (void);
		~CVein_Scan (void);

	public: // CView_Base
		HRESULT    Refresh (void) override;
	};

}}}}

typedef ebo::sha::gui::view::CVein_Scan TVeinScan;

#endif/*_EBOSHAVEINSCAN_H_8C13DFB8_2853_4BC4_A678_D8093FF1419D_INCLUDED*/