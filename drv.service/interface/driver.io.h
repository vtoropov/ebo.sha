#ifndef _DRIVERIO_H_851A5CCF_97A1_4E3D_93A9_0D88AD27B661_INCLUDED
#define _DRIVERIO_H_851A5CCF_97A1_4E3D_93A9_0D88AD27B661_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Aug-2020 at 1:22:33a, UTC+7, Novosibirsk, Saturday;
	This is SSH-Tunnel WFP driver-to-client input/output interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 7:42:16.893 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "shared.gen.sys.err.h"
#include "driver.dev.loc.h"

namespace shared { namespace driver { namespace client {
	using shared::sys_core::CError;
	// https://docs.microsoft.com/en-us/windows/win32/api/ioapiset/nf-ioapiset-deviceiocontrol
	// https://docs.microsoft.com/en-us/windows/win32/DevIO/calling-deviceiocontrol
	class CDriverIO {
	private:
		HANDLE   m_device;
		CError   m_error ;

	public:
		 CDriverIO (void);
		~CDriverIO (void);

	public:
		TErrorRef   Error(void) const;
		const bool  Is   (void) const;       // checks the device handle for valid value;
		HRESULT     Turn (const bool _b_on); // connects/disconnects to/from driver     ;

	public:
		DWORD       Read  (const DWORD _io_ctl, PVOID _p_out, const DWORD _sz_out, LPOVERLAPPED);
		DWORD       RawIO (const DWORD _io_ctl, PVOID _p_in , const DWORD _sz_in , PVOID _p_out, const DWORD _sz_out, LPOVERLAPPED = NULL);
		DWORD       Write (const DWORD _io_ctl, PVOID _p_in , const DWORD _sz_in , LPOVERLAPPED);
	};

}}}

#endif/*_DRIVERIO_H_851A5CCF_97A1_4E3D_93A9_0D88AD27B661_INCLUDED*/