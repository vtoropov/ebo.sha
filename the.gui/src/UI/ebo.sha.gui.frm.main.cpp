/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jul-2018 on 10:31:47a, UTC+7, Novosibirsk, Friday;
	This is Ebo Bot HTTP/NET test desktop application main form interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 7:19:46a, UTC+7, Novosibirsk, Friday;
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 5:04:47a, UTC+7, Novosibirsk, Thursday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 3:56:42p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool on 21-May-2020 at 7:18:17p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Shared Optimization Tool project on 26-Aug-2020 at 6:28:59a, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "ebo.sha.gui.frm.main.h"
#include "ebo.sha.gui.frm.lay.h"

#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui;
using namespace ebo::sha::gui::view;

#include "shared.gen.app.res.h"
#include "shared.gen.app.obj.h"

using namespace shared::user32;

#include "shared.uix.gen.clrs.h"
#include "shared.uix.gdi.renderer.h"

using namespace ex_ui::draw;
using namespace ex_ui::controls;
/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CMainForm_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_ban   = IDC_EBO_SHA_BAN_CTL  ,
			e_ctl_sta   = IDC_EBO_SHA_STA_CTL  ,
		};
	}; typedef CMainForm_Ctl This_Ctl;

	class CMainForm_Res {
	public:
		enum _res : WORD {
			e_res_ban   = IDR_EBO_SHA_BAN_IMG  ,
			e_res_sta   = IDR_EBO_SHA_STA_IMG  ,
		};
	}; typedef CMainForm_Res This_Res;

	class CMainForm_Init {
	private:
		CMainForm&  m_main ;
		CError      m_error;

	public:
		CMainForm_Init(CMainForm& _main_frm) : m_main(_main_frm) { m_error << __MODULE__ << S_OK;}

	public:
		TErrorRef   Error (void) const { return m_error; }

		HRESULT     OnCreate(void){
			m_error << __MODULE__ << S_OK;
#pragma region _status
#pragma endregion
			return m_error;
		}

		HRESULT    SetTitle (LPCWSTR _lp_sz_view_desc) { m_error << __MODULE__ << S_OK;
			_lp_sz_view_desc;
			const CApplication& the_app = GetAppObjectRef();

			CStringW cs_title;
			if (NULL == _lp_sz_view_desc || 0 == lstrlenW(_lp_sz_view_desc)) {
				cs_title.Format (
					_T("%s [ver.%s]"),
					(LPCWSTR)the_app.Version().CustomValue(_T("Comments"   )),
					(LPCWSTR)the_app.Version().CustomValue(_T("FileVersion"))
				);
			}
			else {
				cs_title.Format (
					_T("%s :: %s"),
					(LPCWSTR)the_app.Version().CustomValue(_T("Comments")), _lp_sz_view_desc
				);
			}
			m_main.Window().SetWindowTextW(cs_title.GetString());
			return m_error;
		}
	};
}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CMainForm::CMainFormWnd:: CMainFormWnd(CMainForm& _frm) :
	m_form(_frm), m_banner(This_Res::e_res_ban), m_strip(*this), m_status(m_v_man) { m_error << __MODULE__ << S_OK;
	m_banner.Renderer(this);
}
CMainForm::CMainFormWnd::~CMainFormWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CMainForm::CMainFormWnd::OnCreate (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	CIconLoader loader_(IDR_EBO_SHA_ICO);
	{
		TWindow::SetIcon(loader_.DetachLarge(), TRUE );
		TWindow::SetIcon(loader_.DetachSmall(), FALSE);
	}

	CMainForm_Init init_(m_form);
	HRESULT hr_  = init_.OnCreate();
	if (FAILED(hr_)) {
		m_error  = init_.Error(); m_error.Show();
	}
	init_.SetTitle(NULL);
	m_status.Create(*this);
#pragma region _str_bar

	m_strbar_wrap >> m_strip;
	TStripBarFmt()>> m_strip;
	m_strip.Create(*this, 1);

#pragma endregion

	CLayout lay_(m_form); lay_.Update();
	m_v_man.Create(*this, lay_.ViewArea());
	m_v_man.Trace().Create(*this, lay_.Trace(true), false);

	if (SUCCEEDED(m_strip.Items().Active(0))) {
		this->IControlEvent_OnClick(IDC_EBO_SHA_STR_CMD_0);
	}

	shared::Get_AppUI().IAppUI_PutTraceInfo(__MODULE__);
	shared::Get_AppUI().IAppUI_PutTraceInfo(_T("The app has successfully started;"));

	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnDestroy(UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;

	m_v_man.Destroy();
	m_status.Destroy();
	if (m_strip.Window  ().IsWindow()) {
		m_strip.Destroy ();
	}
	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnDraw   (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnErase  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	static bool  b_fst_time = false;
	if (false == b_fst_time) {
		HBRUSH brush = ::CreateSolidBrush(shared::Get_Format().Panel().Bkgnd().Solid());
		::SetClassLongPtr(*this, GCLP_HBRBACKGROUND, (LONG_PTR)brush);
		b_fst_time = false;
	}

	static const LRESULT l_is_drawn = 1;
	{
		RECT rc_view = CLayout(m_form).ViewArea();

		CZBuffer buf_((HDC)_w_prm, rc_view);
		buf_.FillSolidRect(&rc_view, shared::Get_Format().Panel().Bkgnd().Solid());
	}
	return l_is_drawn;
}

LRESULT   CMainForm::CMainFormWnd::OnMnuInit(UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnSize   (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	RECT rc_ = {0, 0, LOWORD(_l_prm), HIWORD(_l_prm)}; rc_;

	bool b_req_attention = false;

	switch (_w_prm) {
	case SIZE_RESTORED  : { b_req_attention = true; } break;
	case SIZE_MINIMIZED : {} break;
	case SIZE_MAXIMIZED : { b_req_attention = true;	} break;
	}
	if (b_req_attention) {
		CLayout lay_(m_form, rc_); lay_.Update(); m_v_man.Update(true);
	}
	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnSizing (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_v_man.Update(false);
	return 0;
}

LRESULT   CMainForm::CMainFormWnd::OnSysCmd (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	switch (_w_prm) {
	case SC_CLOSE:  {
		::PostQuitMessage(0);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CMainForm::CMainFormWnd::DrawParentBackground(const HWND hChild, const HDC hSurface, RECT& rcUpdated) {
	hChild;
	if (NULL == hSurface)
		return __DwordToHresult(ERROR_INVALID_HANDLE);
	if (::IsRectEmpty(&rcUpdated))
		return OLE_E_INVALIDRECT;
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getsyscolor
	CZBuffer dc_(hSurface, rcUpdated);
	dc_.FillSolidRect(
		&rcUpdated, shared::Get_Format().Panel().Bkgnd().Solid()
	);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CMainForm::CMainFormWnd::OnCommand(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled) {
	bHandled = TRUE; wNotifyCode; hWndCtl; wID;
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CMainForm::CMainFormWnd::IStripEvt_OnAppend (const ST_Ctrls::strip::CItem& _added)  { _added; return S_OK; }
HRESULT   CMainForm::CMainFormWnd::IStripEvt_OnFormat (const TStripFmt&)  { return S_OK; }
HRESULT   CMainForm::CMainFormWnd::IStripEvt_OnRemove (const DWORD _panel_ndx)  { _panel_ndx; return S_OK; }

/////////////////////////////////////////////////////////////////////////////

HRESULT   CMainForm::CMainFormWnd::IControlEvent_OnClick(const UINT ctrlId) {
	switch (ctrlId) {
	case IDC_EBO_SHA_STR_CMD_0: { m_v_man.Active(view::TViewType::e_home ); } break;
	case IDC_EBO_SHA_STR_CMD_1: { m_v_man.Active(view::TViewType::e_sets ); } break;
	case IDC_EBO_SHA_STR_CMD_2: { m_v_man.Active(view::TViewType::e_help ); } break;
	case IDC_EBO_SHA_STR_CMD_3: { m_v_man.Active(view::TViewType::e_logon); } break;
	case IDC_EBO_SHA_STR_CMD_4: { m_v_man.Active(view::TViewType::e_fake ); } break;
	case IDC_EBO_SHA_STR_CMD_5: { m_v_man.Active(view::TViewType::e_block); } break;
	case IDC_EBO_SHA_STR_CMD_6: { m_v_man.Active(view::TViewType::e_media); } break;
	}
	CMainForm_Init init_(m_form);
	CView_Base* const p_active = m_v_man.Active_Ptr();
	if (p_active) {
		CStringW cs_title;
		p_active->Window().GetWindowTextW(cs_title);
		init_.SetTitle(cs_title.GetString());
	}
	else {
		init_.SetTitle(NULL);
	}

	return S_OK;
}
HRESULT   CMainForm::CMainFormWnd::IControlEvent_OnClick(const UINT ctrlId, const LONG_PTR nData) { ctrlId; nData; return E_NOTIMPL; }
HRESULT   CMainForm::CMainFormWnd::IControlEvent_OnHover(const UINT ctrlId) { HRESULT hr_ = S_OK;
	ctrlId;
	const INT n_found_ndx = m_strbar_wrap.Commands().ID_To_Ndx(ctrlId);
	if (TStripCmds::not_found == n_found_ndx) {
		shared::Get_AppUI().IAppUI_PutStatusInfo(_T("Ready"));
	}
	else {
		shared::Get_AppUI().IAppUI_PutStatusInfo(m_strbar_wrap.Commands().Raw().at(static_cast<size_t>(n_found_ndx)).Tips());
	}
	return hr_;
}
HRESULT   CMainForm::CMainFormWnd::IControlEvent_OnEnter(void) { HRESULT hr_ = S_OK; return hr_; }
HRESULT   CMainForm::CMainFormWnd::IControlEvent_OnLeave(void) { HRESULT hr_ = S_OK;

	m_form.Status().Panels().Panel(0).Text() = _T("Ready");
	m_form.Status().Refresh();
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

CMainForm:: CMainForm(void) : m_wnd(*this) { shared::Set_AppUI() = this; }
CMainForm::~CMainForm(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT   CMainForm::Create  (const HWND hParent, const RECT& rcArea)
{
	if (m_wnd.IsWindow())
		return __DwordToHresult(ERROR_OBJECT_ALREADY_EXISTS);
	if (::IsRectEmpty(&rcArea))
		return OLE_E_INVALIDRECT;

	RECT rc_ = CLayout::CenterArea(rcArea);
	HWND hView = m_wnd.Create(hParent, &rc_, NULL, WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS);
	if (!hView)
		return __DwordToHresult(::GetLastError());
	else
		return S_OK;
}

HRESULT   CMainForm::Destroy (void) {
	if (m_wnd.IsWindow())
		m_wnd.SendMessage(WM_CLOSE);
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////
const
TBanner&  CMainForm::Banner  (void) const { return m_wnd.m_banner; }
TBanner&  CMainForm::Banner  (void)       { return m_wnd.m_banner; }
const
TStatusCtrl& CMainForm::Status  (void) const { return m_wnd.m_status.Ref(); }
TStatusCtrl& CMainForm::Status  (void)       { return m_wnd.m_status.Ref(); }
const
TStrip&   CMainForm::Strip   (void) const { return m_wnd.m_strip ; }
TStrip&   CMainForm::Strip   (void)       { return m_wnd.m_strip ; }
const
TViewMan& CMainForm::Views   (void) const { return m_wnd.m_v_man ; }
TViewMan& CMainForm::Views   (void)       { return m_wnd.m_v_man ; }

HRESULT   CMainForm::Visible (const bool _vis) {

	HRESULT hr_ = S_OK;
	if (m_wnd.IsWindow())
		m_wnd.ShowWindow(_vis ? SW_SHOW : SW_HIDE);
	else
		hr_ = OLE_E_BLANK;

	return hr_;
}
const
CWindow&  CMainForm::Window  (void) const { return m_wnd; }
CWindow&  CMainForm::Window  (void)       { return m_wnd; }

/////////////////////////////////////////////////////////////////////////////

CMainForm::operator  CWindow&(void) { return this->Window(); }

/////////////////////////////////////////////////////////////////////////////

HRESULT   CMainForm::IAppUI_ExecCommand   ( const DWORD   _cmd_id ) {
	_cmd_id;
	m_wnd.m_error << __MODULE__ << S_OK;
	INT n_str_ndx = -1;
	// TODO: strip menu index must be associated with command identifier;
	switch (_cmd_id) {
	case IDC_EBO_SHA_STR_CMD_3: { n_str_ndx = 6; } break;
	case IDC_EBO_SHA_STR_CMD_4: { n_str_ndx = 2; } break;
	case IDC_EBO_SHA_STR_CMD_5: { n_str_ndx = 1; } break;
	case IDC_EBO_SHA_STR_CMD_6: { n_str_ndx = 3; } break;
	case IDC_EBO_SHA_UPDATE_LAYOUT: {

			m_wnd.m_v_man.Update(false);
			TMainLayout layout(*this); layout.Update();

			m_wnd.m_status.Update(m_wnd.m_v_man.Trace().IsVisible());

		} break;
	}
	if ((n_str_ndx != -1) && SUCCEEDED(this->Strip().Items().Active(n_str_ndx))) {
		this->Strip().Refresh();
		this->m_wnd.IControlEvent_OnClick(_cmd_id);
	}
	return m_wnd.m_error;
}

HRESULT   CMainForm::IAppUI_PutStatusInfo ( LPCWSTR _lp_sz_message) { HRESULT hr_ = S_OK;
	_lp_sz_message;
	if (NULL == _lp_sz_message || 0 == ::lstrlenW(_lp_sz_message))
		return (hr_ = E_INVALIDARG);
	this->Status().Panels().Panel(0).Text() = _lp_sz_message;
	this->Status().Refresh();
	return hr_;
}

HRESULT   CMainForm::IAppUI_PutTraceError ( LPCWSTR _lp_sz_message) { HRESULT hr_ = S_OK;
	_lp_sz_message;
	if (NULL == _lp_sz_message || 0 == ::lstrlenW(_lp_sz_message))
		return (hr_ = E_INVALIDARG);

	m_wnd.m_v_man.Trace().Output().Entries() += CEntry() << TEntryType::e_error << _lp_sz_message;

	return hr_;
}

HRESULT   CMainForm::IAppUI_PutTraceInfo  ( LPCWSTR _lp_sz_message) { HRESULT hr_ = S_OK;
	_lp_sz_message;
	if (NULL == _lp_sz_message || 0 == ::lstrlenW(_lp_sz_message))
		return (hr_ = E_INVALIDARG);

	m_wnd.m_v_man.Trace().Output().Entries() += CEntry() << TEntryType::e_info << _lp_sz_message;

	return hr_;
}

HRESULT   CMainForm::IAppUI_PutTraceWarn  ( LPCWSTR _lp_sz_message) { HRESULT hr_ = S_OK;
	_lp_sz_message;
	if (NULL == _lp_sz_message || 0 == ::lstrlenW(_lp_sz_message))
		return (hr_ = E_INVALIDARG);

	m_wnd.m_v_man.Trace().Output().Entries() += CEntry() << TEntryType::e_warn << _lp_sz_message;

	return hr_;
}

HRESULT   CMainForm::IAppUI_PutTraceError ( TErrorRef _error) { HRESULT hr_ = S_OK;
	_error;
	CStringW cs_fmt;
	cs_fmt.Format(_T("Code = 0x%x (%d)\nDescription = %s;\nSource: = %s"), _error.Result(), _error.Code(), _error.Desc(), _error.Source());

	m_wnd.m_v_man.Trace().Output().Entries() += CEntry() << TEntryType::e_error << cs_fmt;
	return hr_;
}