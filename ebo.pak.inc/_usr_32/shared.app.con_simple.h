#ifndef _SHAREDAPPCON_SIMPLE_H_705A1F81_2731_467F_AA1A_BAFE09F36C5E_INCLUDED
#define _SHAREDAPPCON_SIMPLE_H_705A1F81_2731_467F_AA1A_BAFE09F36C5E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Sep-2020 at 6:54:34p, UTC+7, Novosibirsk, Thursday;
	This is FakeGPS driver version 2 project desktop client console interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 4-Dec-2020 at 3:28:07.316 pm, UTC+7, Novosibirsk, Friday;
*/
#include <atlwin.h>
#include "shared.gen.sys.err.h"
namespace shared { namespace user32 {

	using shared::sys_core::CError;

	class CConsole_simplified {
	public:
		class CFrame {
		public:
			 CFrame (void);
			~CFrame (void);

		public:
			HRESULT  MoveToCenter (const LONG _l_width = 0) const; // if length is zero or less than zero, console window side is not affected;
			CWindow  Window       (void) const;
		};
	private:
		CFrame   m_frame;

	public:
		 CConsole_simplified (void);
		~CConsole_simplified (void);

	public:
		HRESULT  OnCreate (LPCWSTR _lp_sz_cap , LPCWSTR _lp_sz_title);
		HRESULT  OnWait   (LPCWSTR _lp_sz_msg);

	public:
		CFrame&  GetFrame (void);
		HRESULT  SetIcon  (const WORD _w_res_id);
		HRESULT  SetTitle (LPCWSTR _lp_sz_title);

	public:
		CConsole_simplified& operator <<(TErrorRef _err_obj ); // prints out an error details;
		CConsole_simplified& operator <<(LPCWSTR _lp_sz_info); // prints out an information;
		CConsole_simplified& operator >>(LPCWSTR _lp_sz_err ); // prints out an error details;
		CConsole_simplified& operator <=(LPCWSTR _lp_sz_warn); // prints out an warning;
	};

}}

typedef shared::user32::CConsole_simplified          TConsole_Simple;
typedef shared::user32::CConsole_simplified::CFrame  TConsole_Frame ;

#endif/*_SHAREDAPPCON_SIMPLE_H_705A1F81_2731_467F_AA1A_BAFE09F36C5E_INCLUDED*/