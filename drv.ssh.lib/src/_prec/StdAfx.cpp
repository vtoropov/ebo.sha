/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-May-2018 at 12:19:32p, GMT+7, Phuket, Rawai, Tuesday;
	This is File Guardian driver common library precompiled headers implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo virtual audio device/connector project on 20-Jun-2020 at 9:02:21p, UTC+7, Novosibirsk, Saturday;
	Adopted to SSH Tunnel driver WFP filter project on 2-Aug-2020 at 11:30:03p, UTC+7, Novosibirsk, Sunday;
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 8:09:37.545 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"