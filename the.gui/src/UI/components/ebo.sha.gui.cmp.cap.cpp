/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Dec-2020 at 9:32:29.017 pm, UTC+7, Novosibirsk, Wednesday;
	This is Ebo Sha Optima Tool generic caption GUI component interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:01:04.551 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "StdAfx.h"
#include "ebo.sha.gui.cmp.cap.h"

using namespace ebo::sha::gui;

/////////////////////////////////////////////////////////////////////////////

TCaptionComponent:: CCaption (void) {}
TCaptionComponent::~CCaption (void) {}

/////////////////////////////////////////////////////////////////////////////
const
TImageCtrl&   TCaptionComponent::Image(void) const { return m_image; }
TImageCtrl&   TCaptionComponent::Image(void)       { return m_image; }
const
TLabelCtrl&   TCaptionComponent::Title(void) const { return m_title; }
TLabelCtrl&   TCaptionComponent::Title(void)       { return m_title; }

/////////////////////////////////////////////////////////////////////////////

HRESULT   TCaptionComponent::Create (const HWND _parent, LPCWSTR _lp_sz_text, const RECT& _rc_title, const RECT& _rc_image) { HRESULT hr_ = S_OK;
	_parent; _rc_title; _rc_image;
	if (NULL == _parent || FALSE == ::IsWindow(_parent)) return (hr_ = OLE_E_INVALIDHWND);
	if (::IsRectEmpty(&_rc_title) || ::IsRectEmpty(&_rc_image)) return (hr_ = OLE_E_INVALIDRECT);

	hr_ = m_title.Create(_parent, _rc_title, _lp_sz_text);
	hr_ = m_image.Create(_parent, _rc_image, ::GetTickCount());
	hr_ = m_image.Layout().Update();

	return hr_;
}

HRESULT   TCaptionComponent::Destroy(void) { HRESULT hr_ = S_OK;

	hr_ = m_image.Destroy();
	hr_ = m_title.Destroy();
	return hr_;
}

HRESULT   TCaptionComponent::Format (const WORD _img_res_id, const ex_ui::controls::CMargins _margins) { HRESULT hr_ = S_OK;
	_img_res_id; _margins;

	TColourEx clr_back(shared::Get_Format().Panel().Bkgnd().Solid());
#if defined(_DEBUG)
	const CStringW cs_back = clr_back.Print();
#endif
	TLabelFmt& fmt_ = m_title.Format(); fmt_ = shared::Get_Format().Control().Label(); fmt_.Margins() = _margins;

	TImageCtrl& pic_ = m_image;
	pic_.Format().Bkgnd().Gradient().c0() << clr_back;
	pic_.Format().Bkgnd().Gradient().c1() << clr_back;

	shared::Get_Format().Control().Caption() >> m_title.Format();
	shared::Get_Format().Control().Caption() >> m_image.Format();

	hr_ = pic_.Image().Create(_img_res_id);

	return hr_;
}