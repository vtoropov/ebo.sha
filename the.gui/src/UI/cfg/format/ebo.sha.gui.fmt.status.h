#ifndef _EBOSHAGUIFMTSTATUS_H_782EE415_B962_41E1_817B_97106DBB93A7_INCLUDED
#define _EBOSHAGUIFMTSTATUS_H_782EE415_B962_41E1_817B_97106DBB93A7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Jan-2021 at 11:00:07.228 pm, UTC+7, Novosibirsk, Sunday;
	This is Ebo Sha Optima Tool app main form status bar format interface declaration file.
*/
#include "sfx.status.fmt.h"
#include "sfx.status.ctrl.h"
#include "shared.uix.ctrl.border.h"

namespace ebo { namespace sha { namespace gui { namespace format {

	class CStatusBar_Format {
	public:
		 CStatusBar_Format (void);
		~CStatusBar_Format (void);

	public:
		HRESULT   ApplyTo (ex_ui::controls::format::CBorder_Ex&) const;
		HRESULT   ApplyTo (ST_Ctrls::CStatusBar&) const;

	public:
		const CStatusBar_Format& operator >> (ST_Ctrls::CStatusBar&) const;
	};

}}}}

typedef ebo::sha::gui::format::CStatusBar_Format  TStatusBarFmt;

#endif/*_EBOSHAGUIFMTSTATUS_H_782EE415_B962_41E1_817B_97106DBB93A7_INCLUDED*/