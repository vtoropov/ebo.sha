/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:19:45p, UTC+7, Phuket, Rawai, Tuesday;
	This is communication data exchange receiver desktop console app precompiled header creation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 3:45:34a, UTC+7, Novosibirsk, Tuesday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 2:57:55p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool project on 21-May-2020 at 5:31:55p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Shared optimization tool project on 26-Aug-2020 at 6:09:07a, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"

#if (_ATL_VER < 0x0700)
#include <atlimpl.cpp>
#endif //(_ATL_VER < 0x0700)

#include "shared.gen.syn.obj.h"

::WTL::CMessageLoop* get_MessageLoop(){ return _Module.GetMessageLoop(); }
HINSTANCE get_HINSTANCE() { return _Module.GetResourceInstance(); }

namespace shared {
	class CAppUI : public IAppUI {
	public:
		 CAppUI (void) {}
		~CAppUI (void) {}

	private:
		HRESULT  IAppUI_ExecCommand   ( const DWORD   _cmd_id ) override { _cmd_id; return E_NOTIMPL; }
		HRESULT  IAppUI_PutStatusInfo ( LPCWSTR _lp_sz_message) override { _lp_sz_message; return E_NOTIMPL; }
		HRESULT  IAppUI_PutTraceError ( LPCWSTR _lp_sz_message) override { _lp_sz_message; return E_NOTIMPL; }
		HRESULT  IAppUI_PutTraceInfo  ( LPCWSTR _lp_sz_message) override { _lp_sz_message; return E_NOTIMPL; }
		HRESULT  IAppUI_PutTraceWarn  ( LPCWSTR _lp_sz_message) override { _lp_sz_message; return E_NOTIMPL; }
		HRESULT  IAppUI_PutTraceError ( TErrorRef) override { return E_NOTIMPL; }
	};

	IAppUI&    Get_AppUIRef (void) {
		static CAppUI c_UI;
		return c_UI;
	}

	IAppUI*&   Get_AppUIPtr (void) {
		static IAppUI* p_UI = NULL;
		return p_UI;
	}

	IAppUI &   Get_AppUI  (void) { return (NULL == Get_AppUIPtr() ? Get_AppUIRef() : *Get_AppUIPtr()); }
	IAppUI*&   Set_AppUI  (void) { return Get_AppUIPtr(); }

	TFormat&   Get_Format (void) { static TFormat format_; return format_; }
	TLayout&   Get_Layout (void) { static TLayout layout_; return layout_; }
}