#ifndef _EBOSHAGUIFORMAT_H_78D9F279_402C_4925_8870_2E9D4C1D0E43_INCLUDED
#define _EBOSHAGUIFORMAT_H_78D9F279_402C_4925_8870_2E9D4C1D0E43_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Dec-2020 at 2:49:57.227 am, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha optima tool generic GUI parts format interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:03:27.620 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.uix.ctrl.base.fmt.h"
#include "shared.uix.ctrl.defs.h"
#include "sfx.tabs.fmt.h"
#include "sfx.select.fmt.h"
#include "sfx.label.fmt.h"
#include "sfx.image.fmt.h"
#include "sfx.button.fmt.h"

#include "ebo.sha.gui.fmt.cap.h"
#include "ebo.sha.gui.fmt.edit.h"
#include "ebo.sha.gui.fmt.status.h"

namespace ebo { namespace sha { namespace gui { namespace format {

	class CControl {
	private:
		TCaptionFmt m_caption;
		TEditFmt    m_edit   ;
		TLabelFmt   m_label  ;
		TButtonFmt  m_button ;

	public:
		 CControl (void) ;
		 CControl (const CControl&);
		~CControl (void) ;

	public:
		const
		TButtonFmt&   Button  (void) const;
		TButtonFmt&   Button  (void)      ;
		const
		TCaptionFmt&  Caption (void) const;
		TCaptionFmt&  Caption (void)      ;
		const
		TEditFmt&  Edit  (void) const;
		TEditFmt&  Edit  (void)      ;
		const
		TLabelFmt& Label (void) const;
		TLabelFmt& Label (void)      ;

	public:
		CControl&  operator = (const CControl&  );
		CControl&  operator <<(const CCaption&  );
		CControl&  operator <<(const TButtonFmt&);
		CControl&  operator <<(const TEditFmt&  );
		CControl&  operator <<(const TLabelFmt& );

	public:
		const CControl& operator >>(CCaption&  ) const;
		const CControl& operator >>(TButtonFmt&) const;
		const CControl& operator >>(TEditFmt&  ) const;
		const CControl& operator >>(TLabelFmt& ) const;
	};

	class CPanel {
	private:
		TFmtBkg      m_bkg;
		TBorderFmt   m_borders;

	public:
		 CPanel (void);
		 CPanel (const CPanel&);
		~CPanel (void);

	public:
		const
		TFmtBkg&      Bkgnd  (void) const;
		TFmtBkg&      Bkgnd  (void)      ;
		const
		TBorderFmt&   Borders(void) const;
		TBorderFmt&   Borders(void)      ;

	public:
		CPanel&  operator = (const CPanel&);
		CPanel&  operator <<(const TBorderFmt&);
	};

	class CSelect : public TSelectFmt { typedef TSelectFmt TBase;
	public:
		 CSelect (void);
		 CSelect (const CSelect&);
		~CSelect (void);

	public:
		const
		CSelect& operator >>(TSelectFmt&) const;    // applies format to control;
		CSelect& operator = (const CSelect&);
	};

	class CTabbed : public TTabbedFmt { typedef TTabbedFmt TBase;
	public:
		 CTabbed (void);
		 CTabbed (const CTabbed&);
		~CTabbed (void);

	public:
		const
		CTabbed&  operator >>(TTabbedFmt&) const;   // applies format to control;
		CTabbed&  operator = (const CTabbed&);
	};

	class CFormat {
	private:
		CControl  m_ctrls  ;
		CPanel    m_panel  ;
		CSelect   m_select ;
		CTabbed   m_tabbed ;

	public:
		 CFormat (void);
		 CFormat (const CFormat&);
		~CFormat (void);

	public:
		const
		CControl& Control(void) const;
		CControl& Control(void)      ;
		const
		CPanel&   Panel  (void) const;
		CPanel&   Panel  (void)      ;
		const
		CSelect&  Select (void) const;
		CSelect&  Select (void)      ;
		const
		CTabbed&  Tabbed (void) const;
		CTabbed&  Tabbed (void)      ;

	public:
		CFormat&  operator = (const CFormat&);
	};

}}}}

typedef ebo::sha::gui::format::CControl  TControlFmt;

typedef ebo::sha::gui::format::CPanel    TPanelFmt;
typedef ebo::sha::gui::format::CSelect   TSelectorFmt;
typedef ebo::sha::gui::format::CTabbed   TTabsFmt ;

typedef ebo::sha::gui::format::CFormat   TFormat  ;

#endif/*_EBOSHAGUIFORMAT_H_78D9F279_402C_4925_8870_2E9D4C1D0E43_INCLUDED*/