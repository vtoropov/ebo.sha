#ifndef _SHAREDUIXTRACEEXT_H_2E78558A_D067_4901_A327_1C4B8C4E4284_INCLUDED
#define _SHAREDUIXTRACEEXT_H_2E78558A_D067_4901_A327_1C4B8C4E4284_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 26-Apr-2020 at 4:40:18a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack UIX trace view extension interface declration file.
*/
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.border.h"
#include "shared.uix.gen.clrs.h"
#include "shared.uix.gen.font.h"
#include "shared.uix.trace.data.h"

namespace ex_ui { namespace trace {

	interface IFormat_Events {
		virtual HRESULT   IFmt_BkgChanged (void) PURE;                // backgroung myust be redrawn;
		virtual HRESULT   IFmt_FontChanged(void) PURE;                // font is changed; content must be redrawn;
		virtual HRESULT   IFmt_ImgChanged (const UINT _u_res_id) PURE;// entry type associated images are changed;
	};

	using ex_ui::controls::CBorders;
	using ex_ui::draw::CColour;
	typedef ::std::map<TEntryType, COLORREF>  TColorMap;
	typedef ::std::map<TEntryType, INT     >  TImageMap;  // a type is connected to image list by index;

	class CBorders_Ex : public CBorders {
	private:
		bool     m_b_exclude_indents;

	public:
		 CBorders_Ex (void);
		~CBorders_Ex (void);

	public:
		const
		bool&    ExcludeIndents(void) const;
		bool&    ExcludeIndents(void)      ;
	};

	class CFont_Format {
	private:
		IFormat_Events&m_view     ;
		TColorMap      m_fore     ;
		DWORD          m_size     ;
		CStringW       m_family   ;
		TFontOpts      m_options  ;

	public:
		 CFont_Format (IFormat_Events& _view);
		~CFont_Format (void);

	public:
		COLORREF       Colour (const TEntryType) const ; // by defailt is forecolor of current theme;
		HRESULT        Colour (const TEntryType, const COLORREF);
		LPCWSTR        Family (void) const;              // font name, by default is "Verdana"
		HRESULT        Family (LPCWSTR)   ;
		const
		TFontOpts&     Options(void) const;
		TFontOpts&     Options(void)      ;
		DWORD          Size   (void) const;              // by default is 10pt;
		HRESULT        Size   (const DWORD);
	};

	class CTrace_Format {
	private:
		IFormat_Events&m_view     ;
		COLORREF       m_back     ;        // background solid color;
		TImageMap      m_img_ndx  ;        // an image index in image list for specific entry type;
		THorzAlign     m_hz_align ;
		UINT           m_bkg_id   ;        // background image resource identifier; 0 - no background image;
		UINT           m_img_id   ;        // identifier of resource for dreawing item images; 0 - no item images;
		CBorders_Ex    m_borders  ;
		CFont_Format   m_font     ;
		bool           m_timestamp;   // by default is true;

	public:
		 CTrace_Format(IFormat_Events& _view);
		~CTrace_Format(void);

	public:
		COLORREF       BackColour(void) const;
		HRESULT        BackColour(const COLORREF);
		UINT           BkgImage  (void) const;          // gets background image resource identifier;
		HRESULT        BkgImage  (const UINT u_res_id); // sets background image resource identifier; zero is acceptable;
		const
		CBorders_Ex&   Borders   (void) const;
		CBorders_Ex&   Borders   (void)      ;
		const
		CFont_Format&  Font      (void) const;
		CFont_Format&  Font      (void)      ;
		THorzAlign     HorzAlign (void) const;          // by default is left;
		VOID           HorzAlign (const THorzAlign);
		const  INT     ImageNdx  (const TEntryType) const;
		HRESULT        ImageNdx  (const TEntryType, const INT _ndx);
		const UINT     ImageRes  (void) const;          // gets item image list resource identifier;
		HRESULT        ImageRes  (const UINT u_res_id); // sets item image list resource identifier; zero is acceptable;
		bool           Timestamp (void) const;
		bool&          Timestamp (void)      ;
	};
}}

typedef ex_ui::trace::CTrace_Format   TTraceFmt;

#endif/*_SHAREDUIXTRACEEXT_H_2E78558A_D067_4901_A327_1C4B8C4E4284_INCLUDED*/