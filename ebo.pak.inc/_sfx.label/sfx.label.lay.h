#ifndef __PULSEPAYSHAREDLABELCONTROLDEFAULTRENDERER_H_95A7D14C_B227_4d2e_8CB5_33E42E27A579_INCLUDED
#define __PULSEPAYSHAREDLABELCONTROLDEFAULTRENDERER_H_95A7D14C_B227_4d2e_8CB5_33E42E27A579_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Nov-2012 at 7:56:58pm, GMT+3, Rostov-on-Don, Thursday;
	This is Pulsepay Shared Label Control Default Renderer class declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Sfx Label control project on 24-Nov-2020 at 6:36:22.990 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.base.lay.h"

namespace ST_Ctrls { class CLabel; namespace layout {

	using shared::sys_core::CError;

	using ex_ui::controls::layout::CLayersEx;

	class CLabel_Layout {
	protected:
		ST_Ctrls::CLabel&
		            m_ctrl ;
		CLayersEx   m_lays ;
		SIZE        m_size ; // predefined label size that can be used for specifiying a label control hight end width;

	public:
		 CLabel_Layout (ST_Ctrls::CLabel&);
		~CLabel_Layout (void);

	public:
		const
		CLayersEx&  Layers (void) const;
		CLayersEx&  Layers (void)      ;
		const SIZE& Size   (void) const;           // gets a label pre-defined or set size;
		HRESULT     Size   (const DWORD _width, const DWORD _hight);   // sets label predefined size;
		HRESULT     Update (void)      ;           // recalculates position/rectangle of all elements;
		HRESULT     Update (const RECT& _rc_area); // updates control window position into an area provided;

	public:
		CLabel_Layout&  operator <<(const RECT& _rc_area);        // updates control window position in accordance with area  ;
		const RECT      operator = (const RECT& _rc_area) const;  // returns calculated rectangle of control window for area provided;

	private: // non-copyable;
		CLabel_Layout (const CLabel_Layout&);
		CLabel_Layout& operator = (const CLabel_Layout&);
	};

}}

typedef ST_Ctrls::layout::CLabel_Layout TLabelLay;

#endif/*__PULSEPAYSHAREDLABELCONTROLDEFAULTRENDERER_H_95A7D14C_B227_4d2e_8CB5_33E42E27A579_INCLUDED*/