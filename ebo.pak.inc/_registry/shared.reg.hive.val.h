#ifndef _SHAREDREGHIVEVAL_H_BCA29446_710C_4DC9_845A_28597CA5C991_INCLUDED
#define _SHAREDREGHIVEVAL_H_BCA29446_710C_4DC9_845A_28597CA5C991_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Oct-2019 at 8:06:02p, UTC+7, Novosibirsk, Wednesday;
	This is Ebo Pack shared registry wrapper library value interface declaration file.
*/
#include "shared.reg.hive.defs.h"
namespace shared { namespace registry {

	class CType {
		// https://docs.microsoft.com/en-us/windows/win32/sysinfo/registry-value-types
	public:
		enum _e_type : DWORD {
			e_none     = REG_NONE  ,
			e_bin_dat  = REG_BINARY,
			e_dword    = REG_DWORD ,
			e_expand   = REG_EXPAND_SZ,
			e_link     = REG_LINK  ,
			e_text     = REG_SZ    ,
			e_text_ln  = REG_MULTI_SZ ,
		};
	private:
		_e_type   m_val;
	public:
		 CType(const _e_type = e_none);
		 CType(const CType&);
		~CType(void);
	public:
		bool    Is (const _e_type) const;
		bool    IsDword     (void) const;
		bool    IsMultiline (void) const;
		bool    IsNone      (void) const; // it looks like a value is not found and a type cannot be determined;
		bool    IsText      (void) const;
		_e_type Type        (void) const;
		void    Type        (const _e_type);

	public:
		operator _e_type    (void) const;

	public:
		CType&   operator = (const _e_type);
	};

	bool operator==(const CType::_e_type _lhs, const CType& _rhs);
	bool operator!=(const CType::_e_type _lhs, const CType& _rhs);
	bool operator==(const CType& _lhs, const CType::_e_type _rhs);
	bool operator!=(const CType& _lhs, const CType::_e_type _rhs);
	bool operator==(const CType& _lhs, const CType& _rhs);
	bool operator!=(const CType& _lhs, const CType& _rhs);
#if (0)
	typedef ::std::vector<::ATL::CAtlString>  TMultiString;
#endif
	class CValue
	{
	public:
		CAtlString        m_name;
		CType             m_type;
		_variant_t        m_val ;

	public:
		 CValue (void) ;
		~CValue (void) ;

	public:
		CAtlString        AsText (void) const;
		DWORD             AsDword(void) const;
		const
		CAtlString&       Name(void) const;
		CAtlString&       Name(void)      ;
		const
		_variant_t&       Ref (void) const;
		_variant_t&       Ref (void)      ;
		const
		CType&            Type(void) const;
		CType&            Type(void)      ;

	public:
		operator const _variant_t& (void) const;
		operator       _variant_t& (void)      ;

	public:
		CValue&  operator<< (const CType&);
		CValue&  operator<< (LPCWSTR)     ;  // sets a name ;
		CValue&  operator = (const _variant_t&  ) ;
		CValue&  operator = (const DWORD) ;  // sets a value;
		CValue&  operator = (const TMultiString&) ;
		CValue&  operator = (LPCWSTR)     ;  // sets a value;
	};

	typedef ::std::vector<CValue>   TValues;
}}

#endif/*_SHAREDREGHIVEVAL_H_BCA29446_710C_4DC9_845A_28597CA5C991_INCLUDED*/