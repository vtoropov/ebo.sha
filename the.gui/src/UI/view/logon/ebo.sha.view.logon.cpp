/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Dec-2019 at 5:47:05p, UTC+7, Novosibirsk, Monday;
	This is Ebo Pack personal account app accountant view interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 3-Mar-2020 at 6:44:26p, UTC+7, Novosibirsk, Tuesday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 3:42:36p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool on 24-May-2020 at 10:50:33p, UTC+7, Novosibirsk, Sunday;
	Adopted to Ebo Sha optima tool on 23-Sep-2020 at 10:55:00p, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "ebo.sha.view.logon.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui;
using namespace ebo::sha::gui::view;

#include "shared.gen.sys.date.h"
using namespace shared::common;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CLogon_Cmd {
	public:
		enum _cmd : DWORD {
			e_cmd_sel_auth     = 0x01,
			e_cmd_sel_sign     = 0x02,
		};
	}; typedef CLogon_Cmd This_Cmd;

	class CLogon_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_sel  = IDC_EBO_SHA_LGN_SEL,
		};
	}; typedef CLogon_Ctl This_Ctl;

	class CLogon_Res {
	public:
		enum _res : WORD {
			e_res_sel_overlay  = IDR_EBO_SHA_SEL_OVR_7,
			e_res_sel_img_lst  = IDR_EBO_SHA_SEL_IMG_L,
			e_res_sel_itm_1_a  = IDR_EBO_SHA_SEL_FNG_01_a,
			e_res_sel_itm_1_n  = IDR_EBO_SHA_SEL_FNG_01_n,
			e_res_sel_itm_2_a  = IDR_EBO_SHA_SEL_FNG_02_a,
			e_res_sel_itm_2_n  = IDR_EBO_SHA_SEL_FNG_02_n,
		};
	}; typedef CLogon_Res This_Res;

	class CLogon_Layout {
	private:
		CLogon&  m_view;
		RECT     m_rect;

	public:
		 CLogon_Layout (CLogon& _view) : m_view(_view) {
			 if (m_view.Window().IsWindow())
				 m_view.Window().GetClientRect(&m_rect);
			 else
				 ::SetRectEmpty(&m_rect);
		 }
		~CLogon_Layout (void) {}

	public:
		RECT   GetSelectRect (void) const {
			RECT rc_sel = m_rect; rc_sel.right = rc_sel.left + m_view.Select().Layout().Width();
			return rc_sel;
		}

		RECT   GetSubViewRect(void) const {
			RECT rc_sub = m_rect; rc_sub.left = this->GetSelectRect().right + 0x0;
			::InflateRect(&rc_sub, -0x0, -0x0);
			return rc_sub;
		}

		VOID   Update (void) {

			const RECT rc_sub_view = this->GetSubViewRect();

			TSelect& sel_ctrl = m_view.Select(); sel_ctrl.Layout() << m_rect;
			TSignIn& sign_in  = m_view.SignIn(); sign_in.Update (rc_sub_view);
		}
	};

	class CLogon_Init {
	private:
		CLogon&  m_view ;
		CError   m_error;

	public:
		 CLogon_Init (CLogon& _view) : m_view(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }
		~CLogon_Init (void) {}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;
#pragma region __sel_create
			TSelect& sel_ctrl = m_view.Select();

			sel_ctrl.Format() = shared::Get_Format().Select();
			sel_ctrl.Format().Overlay().Image() = This_Res::e_res_sel_overlay;
			
			shared::Get_Layout().Select() >> sel_ctrl.Layout();
			shared::Get_Layout().Select() >> sel_ctrl.Borders();

			sel_ctrl.Format().Images() = This_Res::e_res_sel_img_lst;

			TSelItems& items = sel_ctrl.Items();

			TSelItem itm_0;
			itm_0.Id() = This_Cmd::e_cmd_sel_auth;
			itm_0.Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_sel_itm_1_n);
			itm_0.Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_sel_itm_1_a);
			itm_0.Image () = 2;
			items.Append(itm_0);

			TSelItem itm_1;
			itm_1.Id() = This_Cmd::e_cmd_sel_sign;
			itm_1.Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_sel_itm_2_n);
			itm_1.Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_sel_itm_2_a);
			itm_1.Image () = 0;
			items.Append(itm_1);

			items.Selected(1);

			RECT rc_ = CLogon_Layout(m_view).GetSelectRect();
			HRESULT hr_ = sel_ctrl.Create(m_view.Window(), rc_, _T("ebo_pack::controls::selector"), This_Ctl::e_ctl_sel);

			if (FAILED(hr_)) {
				m_error = sel_ctrl.Error();
				m_error.Show();
			}
			else {
				sel_ctrl.Window().SetWindowPos(HWND_TOP, &rc_, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE);
				sel_ctrl.Layout().Update(rc_);
			}
#pragma endregion
			return m_error;
		}
	};
}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CLogon::CLogonWnd:: CLogonWnd(CLogon& _view) : TViewWnd(_view), m_view(_view), m_select(_view) {}
CLogon::CLogonWnd::~CLogonWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CLogon::CLogonWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	const RECT rc_ = Lay_(*this); rc_; HRESULT hr_ = S_OK; TViewWnd::SetWindowTextW(_T("Logon"));

	CLogon_Init init_(m_view); init_.OnCreate();
	hr_ = m_sign.Create(*this, CLogon_Layout(m_view).GetSubViewRect(), true);
	if (SUCCEEDED(hr_)) {
		m_sign.Window().SetWindowTextW(_T("logon::sign-in::subview"));
	}
	hr_ = m_auth.Create(*this, CLogon_Layout(m_view).GetSubViewRect(), false);
	if (SUCCEEDED(hr_)) {
		m_auth.Window().SetWindowTextW(_T("logon::auth-prov::subview"));
	}

	return 0;
}

LRESULT   CLogon::CLogonWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_auth.Destroy();
	m_sign.Destroy();
	m_select.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CLogon::CLogonWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)}; rc_; // it cannot be applied to selector control layout directly;
	CLogon_Layout lay_(m_view);
	// TODO: select control applies border thickness format when updates the layout; must be reviewed;
	m_select.Layout() << lay_.GetSelectRect();
	m_select.Refresh();
	m_sign.Update(lay_.GetSubViewRect());
	m_auth.Update(lay_.GetSubViewRect());

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CLogon:: CLogon(void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CLogon::~CLogon(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CLogon::ISelector_OnItemClick  (const UINT _u_itm_id) { HRESULT hr_ = S_OK;
	_u_itm_id;
	switch (_u_itm_id) {
	case This_Cmd::e_cmd_sel_auth: {
		if (m_wnd.m_auth.IsValid()) hr_ = m_wnd.m_auth.IsVisible(true );
		if (m_wnd.m_sign.IsValid()) hr_ = m_wnd.m_sign.IsVisible(false);
	} break;
	case This_Cmd::e_cmd_sel_sign: {
		if (m_wnd.m_sign.IsValid()) hr_ = m_wnd.m_sign.IsVisible(true );
		if (m_wnd.m_auth.IsValid()) hr_ = m_wnd.m_auth.IsVisible(false);
	} break;
	}
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

TAuthProv& CLogon::AuthProv(void) { return m_wnd.m_auth  ; }
TSelect&   CLogon::Select  (void) { return m_wnd.m_select; }
TSignIn&   CLogon::SignIn  (void) { return m_wnd.m_sign  ; }