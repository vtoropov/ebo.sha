#ifndef __EBOBOOFRMMAIN_H_DFF7313A_9C01_46B4_8BD1_E8DCED919AB7_INCLUDED
#define __EBOBOOFRMMAIN_H_DFF7313A_9C01_46B4_8BD1_E8DCED919AB7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Jul-2018 at 10:07:10a, UTC+7, Novosibirsk, Friday;
	This is Ebo Bot HTTP/NET test desktop application main form interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 2:16:31a, UTC+7, Novosibirsk, Friday;
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 5:02:38a, UTC+7, Novosibirsk, Thursday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 3:55:41p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool on 21-May-2020 at 7:12:57p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Shared optimization tool project on 26-Aug-2020 at 6:26:58a, UTC+7, Novosibirsk, Wednesday;
*/
#include "shared.gen.sys.err.h"

#include "shared.uix.gdi.renderer.h"
#include "shared.uix.frms.img.ban.h"

#include "ebo.sha.gui.str.bar.h"
#include "ebo.sha.gui.sta.bar.h"

#include "ebo.sha.gui.view.man.h"

namespace ebo { namespace sha { namespace gui {

	using shared::sys_core::CError;
	using ex_ui::draw::defs::IRenderer;

	using ST_Ctrls::CStripBar    ; typedef CStripBar  TStrip  ;
	using ST_Ctrls::IStripEvents ;

	class CMainForm : public shared::IAppUI {
	private:
		class CMainFormWnd :
			public  ::ATL::CWindowImpl<CMainFormWnd>, IRenderer, IStripEvents {
			typedef ::ATL::CWindowImpl<CMainFormWnd>  TWindow; friend class CMainForm;

		private:
			CMainForm&    m_form  ;
			CError        m_error ;
			TRenderer     m_render;
			TBanner       m_banner;  // not used yet in UI;
			CStripBar     m_strip ;
			TStripBarWrap m_strbar_wrap;

		private:
			TViewMan      m_v_man ;
			TStatusWrap   m_status;

		public:
			#define WM_MENUPOPUP  WM_INITMENUPOPUP
			#define WM_ERASE      WM_ERASEBKGND

			DECLARE_WND_CLASS(_T("ebo::sha::optima::MainForm"));
			BEGIN_MSG_MAP(CMainFormWnd)
				MESSAGE_HANDLER(WM_CREATE       , OnCreate )
				MESSAGE_HANDLER(WM_DESTROY      , OnDestroy)
				MESSAGE_HANDLER(WM_DRAWITEM     , OnDraw   )
				MESSAGE_HANDLER(WM_MENUPOPUP    , OnMnuInit)
				MESSAGE_HANDLER(WM_ERASE        , OnErase  )
				MESSAGE_HANDLER(WM_SIZE         , OnSize   )
				MESSAGE_HANDLER(WM_SIZING       , OnSizing ) // it is useful for locking sub-view(s) update;
				MESSAGE_HANDLER(WM_SYSCOMMAND   , OnSysCmd )
			END_MSG_MAP()
		private:
			LRESULT   OnCreate (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnDestroy(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnDraw   (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnErase  (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnMnuInit(UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnSize   (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnSizing (UINT, WPARAM, LPARAM, BOOL&);
			LRESULT   OnSysCmd (UINT, WPARAM, LPARAM, BOOL&);

		public:
			 CMainFormWnd(CMainForm&);
			~CMainFormWnd(void);

		private:
			LRESULT   OnCommand(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
#pragma warning(disable: 4481)
		private: // IRenderer
			HRESULT   DrawParentBackground(const HWND hChild, const HDC hSurface, RECT& rcUpdated) override sealed;
#pragma warning(default: 4481)
		private: // IStripEvents
#pragma warning(disable:4481)
			virtual   HRESULT  IStripEvt_OnAppend (const ST_Ctrls::strip::CItem& _added)  override sealed;
			virtual   HRESULT  IStripEvt_OnFormat (const TStripFmt&) override sealed;
			virtual   HRESULT  IStripEvt_OnRemove (const DWORD _panel_ndx) override sealed;
		private: // IControlEvent
			virtual   HRESULT  IControlEvent_OnClick(const UINT ctrlId) override sealed;
			virtual   HRESULT  IControlEvent_OnClick(const UINT ctrlId, const LONG_PTR nData) override sealed;
			virtual   HRESULT  IControlEvent_OnHover(const UINT ctrlId) override sealed;
			virtual   HRESULT  IControlEvent_OnEnter(void) override sealed;
			virtual   HRESULT  IControlEvent_OnLeave(void) override sealed;
#pragma warning(default:4481)
		};
	private:
		CMainFormWnd   m_wnd;
	public:
		 CMainForm(void);
		~CMainForm(void);

	public: // life-cycle;
		HRESULT    Create   (const HWND hParent, const RECT& rcArea);
		HRESULT    Destroy  (void);

	public: // accessors;
		const
		TBanner&   Banner   (void) const;
		TBanner&   Banner   (void)      ;
		const
		TStatusCtrl&  Status(void) const;
		TStatusCtrl&  Status(void)      ;
		const
		TStrip&    Strip    (void) const;
		TStrip&    Strip    (void)      ;
		const
		TViewMan&  Views    (void) const;
		TViewMan&  Views    (void)      ;
		HRESULT    Visible  (const bool);
		const
		CWindow&   Window   (void) const;   // main form window handle reference;
		CWindow&   Window   (void)      ;
	public:
		operator  CWindow&  (void)      ;

	private: // shared::IAppUI
#pragma warning (disable: 4481)
		HRESULT   IAppUI_ExecCommand   ( const DWORD   _cmd_id ) override sealed;
		HRESULT   IAppUI_PutStatusInfo ( LPCWSTR _lp_sz_message) override sealed;
		HRESULT   IAppUI_PutTraceError ( LPCWSTR _lp_sz_message) override sealed;
		HRESULT   IAppUI_PutTraceInfo  ( LPCWSTR _lp_sz_message) override sealed;
		HRESULT   IAppUI_PutTraceWarn  ( LPCWSTR _lp_sz_message) override sealed;
		HRESULT   IAppUI_PutTraceError ( TErrorRef) override sealed;
#pragma warning (default: 4481)
	};

}}}

#endif/*__EBOBOOFRMMAIN_H_DFF7313A_9C01_46B4_8BD1_E8DCED919AB7_INCLUDED*/