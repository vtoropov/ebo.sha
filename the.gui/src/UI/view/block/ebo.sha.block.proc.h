#ifndef _EBOSHABLOCKPROC_H_081BBDD2_45B4_4D9B_B8CD_73BBB23F0894_INCLUDED
#define _EBOSHABLOCKPROC_H_081BBDD2_45B4_4D9B_B8CD_73BBB23F0894_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Dec-2020 at 5:46:01.272 pm, UTC+7, Novosibirsk, Sunday;
	This is Ebo Sha optima tool running process control view interface declaration file.
*/
#include "shared.gen.sys.err.h"

#include "sfx.tabs.ctrl.h"

#include "ebo.sha.view.bas.h"
#include "ebo.sha.view.empty.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	using shared::sys_core::CError;

	using ST_Ctrls::CTab;
	using ST_Ctrls::CTabbed    ; typedef CTabbed TTabCtrl;
	using ST_Ctrls::ITabEvents ;

	class CBlock_Proc : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CBlock_Proc;
		private:
			CBlock_Proc&  m_view  ;
			TTabCtrl      m_tabbed;
			TEmptyView    m_empty ;

		public:
			 CViewWnd (CBlock_Proc&);
			~CViewWnd (void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CBlock_Proc (void);
		~CBlock_Proc (void);

#pragma warning(disable:4481)
	private:    // ITabEvents
		HRESULT    ITabEvent_OnSelect (const DWORD _tab_ndx) override sealed;
#pragma warning(default:4481)
	
	public:
		TTabCtrl&    Tabbed (void);
	};

}}}}

typedef ebo::sha::gui::view::CBlock_Proc   TProcView;

#endif/*_EBOSHABLOCKPROC_H_081BBDD2_45B4_4D9B_B8CD_73BBB23F0894_INCLUDED*/