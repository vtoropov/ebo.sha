/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Aug-2018 at 4:23:18p, UTC+7, Novosibirsk, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop application main form layout interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 27-Oct-2019 at 3:09:59p, UTC+7, Novosibirsk, Sunday;
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 5:08:22a, UTC+7, Novosibirsk, Thursday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 4:00:18p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool on 21-May-2020 at 7:28:32p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Shared optimization tool project on 26-Aug-2020 at 6:25:00a, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "ebo.sha.gui.frm.lay.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui;
using namespace ebo::sha::gui::_impl;

#include "shared.uix.gdi.provider.h"
#include "shared.uix.frms.img.ban.h"

using namespace ex_ui::draw;
using namespace ex_ui::frames;

#include "shared.gui.page.layout.h"

using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

CLayout:: CLayout (CMainForm& _main) : m_main(_main), m_area {0} {
	if (m_main.Window().IsWindow())
		m_main.Window().GetClientRect(&m_area);
}
CLayout:: CLayout (CMainForm& _main , const RECT& _rc_client_area) : m_main(_main), m_area {0} { ::CopyRect(&m_area, &_rc_client_area); }
CLayout::~CLayout (void) {}

/////////////////////////////////////////////////////////////////////////////

RECT    CLayout::Statusbar(const bool _b_default) const {
	RECT rc_sta = m_area;
	// (1) gets status bar height;
	const TStatusCtrl& sta_ctrl = m_main.Status ();
	if (sta_ctrl.Window().IsWindow() && sta_ctrl.Window().IsWindowVisible()) {
		rc_sta.top = rc_sta.bottom - sta_ctrl.Layout().Height();
	}
	else if (_b_default)
		rc_sta.top = rc_sta.bottom - sta_ctrl.Layout().Height();
	else
		rc_sta.top = rc_sta.bottom;
	return rc_sta;
}

RECT    CLayout::Stripbar (const bool _b_default) const {
	RECT rc_str = m_area;
	const TStrip& str_ctrl = m_main.Strip();

	if (str_ctrl.Window().IsWindow() && str_ctrl.Window().IsWindowVisible()) {
		rc_str.bottom = rc_str.top + str_ctrl.Layout().Height();
	}
	else if (_b_default)
		rc_str.bottom = rc_str.top + str_ctrl.Layout().Height();
	else
		rc_str.top = rc_str.bottom;
	return rc_str;
}

RECT    CLayout::Trace    (const bool _b_default) const {
	RECT rc_trace = m_area;
	RECT rc_status = this->Statusbar(false);

	rc_trace.bottom = rc_status.top; // it necessary anyway;

	TViewMan& view_man = m_main.Views();
	TTraceView& v_trace = view_man.Trace();

	const bool b_visible = v_trace.IsVisible();
	const LONG l_def_height = 150;

	if (_b_default) {
		rc_trace.top = rc_status.top - l_def_height;
	}
	else if (v_trace.IsValid() && b_visible) {
		rc_trace.top = rc_status.top - l_def_height;
	}
	else
		rc_trace.top = rc_trace.bottom;

	return rc_trace;
}

VOID    CLayout::Update   (void) {

	CWindow main_frm = m_main.Window();

	if (main_frm.IsWindow() == FALSE)
		return;

	RECT rc_area = m_area;
#if(1)
	if (FALSE == main_frm.GetClientRect(&rc_area)) // is already assigned via class constructor;
		return;
#endif
	if (::IsRectEmpty(&rc_area))
		return;
	TStrip&   str_ctrl = m_main.Strip  (); str_ctrl.Layout() << rc_area;
	TStatusCtrl& sta_ctrl = m_main.Status (); sta_ctrl.Layout() << rc_area;

	// TODO : it's better to use batch updating windows position;
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-begindeferwindowpos
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-deferwindowpos
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-enddeferwindowpos

	const RECT& rc_view = this->ViewArea();

	TViewMan& v_man = m_main.Views();

	for (DWORD i_ = 0; i_ < v_man.Views().Count(); i_++) {
		if (v_man.Views().View(i_) &&
			v_man.Views().View(i_)->Window().IsWindow())
			v_man.Views().View(i_)->Update(rc_view);
	}

	if (v_man.Trace().IsVisible()) {
		v_man.Trace().Update(this->Trace(true));
	}
}

RECT    CLayout::ViewArea (void) const {

	RECT rc_view = m_area;
	rc_view.top    += __H(this->Stripbar ());
	rc_view.bottom -= __H(this->Statusbar());
	rc_view.bottom -= __H(this->Trace(false));

	return rc_view;
}

/////////////////////////////////////////////////////////////////////////////

RECT   CLayout::AdjustRect  (const RECT& _rc) {

	RECT rc_adjusted = _rc;
	const RECT rc_area = CLayout::GetAvailableArea();

	if (_rc.left < rc_area.left)
		::OffsetRect(&rc_adjusted, rc_area.left - _rc.left, 0);

	if (_rc.top  < rc_area.top )
		::OffsetRect(&rc_adjusted, 0, rc_area.top - _rc.top);

	const LONG x_delta = rc_adjusted.right  - rc_area.right ;  if (0 < x_delta) rc_adjusted.right  = rc_area.right ;
	const LONG y_delta = rc_adjusted.bottom - rc_area.bottom;  if (0 < y_delta) rc_adjusted.bottom = rc_area.bottom;

	return rc_adjusted;
}

RECT   CLayout::CenterArea  (const RECT& _rc) {

	const RECT  screen_ = CLayout::GetAvailableArea();
	const POINT left_top = {
		(screen_.right - screen_.left) / 2  - (_rc.right - _rc.left) / 2,
		(screen_.bottom - screen_.top) / 2  - (_rc.bottom - _rc.top) / 2,
	};
	RECT center_ = {
		left_top.x,
		left_top.y,
		left_top.x + (_rc.right - _rc.left),
		left_top.y + (_rc.bottom - _rc.top)
	};

	return center_;
}

RECT   CLayout::FormDefPlace(void) {

	RECT rc_ = {0, 0, 700, 700};

	return CLayout::CenterArea(rc_);
}

/////////////////////////////////////////////////////////////////////////////

RECT   CLayout::GetAvailableArea(void)
{
	const POINT ptZero = {0};
	const HMONITOR hMonitor = ::MonitorFromPoint(ptZero, MONITOR_DEFAULTTOPRIMARY);
	MONITORINFO mInfo  = {0};
	mInfo.cbSize = sizeof(MONITORINFO);
	::GetMonitorInfo(hMonitor, &mInfo);
	return mInfo.rcWork;
}