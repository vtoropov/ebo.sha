#ifndef _SHAREDUIXGDIDRAWBKG_H_079105A7_B853_4C4B_92B8_9CD3AF2A0D63_INCLUDED
#define _SHAREDUIXGDIDRAWBKG_H_079105A7_B853_4C4B_92B8_9CD3AF2A0D63_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Jan-2019 at 8:10:28a, UTC+7, Novosibirsk, Wednesday;
	This is Ebo Pack shared UIX draw library rendering tile surface interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.gdi.object.h"
#include "shared.uix.gen.clrs.h"
#include "shared.uix.gdi.z_buf.h"
//
// TODO: this file must be renamed to somthing similar to this - mapped images' renderer;
//       such renderer may be used for background surface draw also;
//      (done on 10-Dec-2020 at 2:32:57.110 pm, UTC+7, Novosibirsk);
namespace ex_ui { namespace draw {

	using shared::sys_core::CError;

	class CTile_Bind {
	public: enum e_bound : INT { e_none = -1 };
	public: enum e_side : DWORD { e_left = 0, e_top = 1, e_right = 2, e_bottom = 3 };
	private:
		e_side   m_side;
		INT      m_bound_ndx;  // a tile index in collection to with this bind object refers to;

	public:
		 CTile_Bind (void);
		 CTile_Bind (const e_side, const INT _bound_ndx);
		 CTile_Bind (const CTile_Bind&);
		~CTile_Bind (void);

	public:
		bool     Is   (void) const;      // return true if bound index is not less zero;
		e_side   Side (void) const;
		VOID     Side (const e_side);
		INT      To   (void) const  ;    // gets bound tile index; if less than zero, no bound tile;
		VOID     To   (const INT _ndx);  // sets bound tile index; if less than zero, no bound tile;

	public:
		CTile_Bind& operator = (const CTile_Bind&);
		CTile_Bind& operator <<(const e_side);
		CTile_Bind& operator <<(const INT _bound_ndx);
	};

	class CTile_Mode {
	public:
		enum _mode { e_any = 0x0, e_horz = 0x1, e_vert = 0x2}; // {both:horz&vert|horz|vert};
	private:
		_mode  m_value;
	public:
		 CTile_Mode (void) ;
		 CTile_Mode (const CTile_Mode&);
		 CTile_Mode (const CTile_Mode::_mode);
		~CTile_Mode (void) ;

	public:
		bool   Is    (void) const;   // returns true if any direction mode is acceptable; default;
		bool   IsHorz(void) const;   // returns true if horizontal direction is acceptable only;
		bool   IsVert(void) const;   // returns true if vertical direction is acceptable only;
		_mode  Value (void) const;
		_mode& Value (void)      ;

	public:
		CTile_Mode& operator = (const CTile_Mode&);
		CTile_Mode& operator <<(const CTile_Mode::_mode);
	};

	class CTile_Size : public SIZE { typedef SIZE TSize;
	public:
		enum _values : DWORD {
			e_auto   =(DWORD) -1,
		};
	public:
		 CTile_Size (void);
		 CTile_Size (const DWORD _dx, const DWORD _dy);
		~CTile_Size (void);

	public:
		explicit CTile_Size (const CTile_Size&);
		explicit CTile_Size (const SIZE&);

	public:
		const bool IsAutoHeight (void) const;
		const bool IsAutoWidth  (void) const;
		const bool Is  (void) const;             // returns true if both dimentions of size do not equal to zero;

	public:
		operator const SIZE& (void) const;
		operator       SIZE& (void)      ;

	public:
		CTile_Size& operator = (const CTile_Size&);
		CTile_Size& operator <<(const SIZE&);
	};

	// tile may contain both color tile background and an image specified via resource identifier;
	class CTile {
	public:
#if (0)
		// tile position or cell is not required: it is defined by index in vector container;
		// the following tile draw order is assumed from 2-dimension space: columns - from left to right, rows from top to bottom;
		enum _pos : UINT {
			e_none     = 0x0000,   // no segment;
			e_left     = 0x0001,   // left          side;
			e_top      = 0x0002,   // top           side;
			e_right    = 0x0004,   // right         side;
			e_bottom   = 0x0008,   // bottom        side;
			e_middle   = 0x0010,   // middle|center;
			e_top_lft  = 0x0020,   // top   |left   corner;
			e_top_rht  = 0x0040,   // top   |right  corner;
			e_btm_lft  = 0x0080,   // bottom|left   corner;
			e_btm_rht  = 0x0100,   // bottom|right  corner;
		};
#endif
	protected:
		WORD       m_res_id;
		CColour    m_color ;  // transparent by default;
		CGradient  m_grad  ;  // a gradient of colours;
		CTile_Size m_size  ;  // is specified at creation time or later;
		RECT       m_rect  ;  // used by layout for storing current place of this tile in window client area;
		CTile_Bind m_bind  ;  // information to tile of binding;

	public:
		 CTile (void) ;
		 CTile (const CColour&);
		 CTile (const CGradient&);
		 CTile (const CTile_Size& );
		 CTile (const CTile& )     ;
		 CTile (const WORD _res_id);
		~CTile (void);

	public:
		const
		CTile_Bind& Bind  (void) const;
		CTile_Bind& Bind  (void)      ;
		const
		CColour&    Color (void) const;
		CColour&    Color (void)      ;
		const
		CGradient&  Gradient (void) const;
		CGradient&  Gradient (void)      ;
		const bool  Is    (void) const; // returns true if size and (color or resource id) are valid;
		const RECT& Rect  (void) const;
		/*_*/ RECT& Rect  (void)      ;
		const WORD  ResId (void) const;
		const
		CTile_Size& Size  (void) const;
		CTile_Size& Size  (void)      ;

	public:
		CTile&   operator = (const CTile&);
		CTile&   operator <<(const CTile_Bind& );
		CTile&   operator <<(const CColour&);
		CTile&   operator <<(const CGradient&  );
		CTile&   operator <<(const CTile_Size& );
		CTile&   operator <<(const RECT  _rect );
		CTile&   operator <<(const WORD _res_id);
	};

	typedef ::std::vector<CTile> TTiles;

	class CTiledSurface {
	private:
		CError    m_error;
		TTiles    m_tiles;

	public:
		 CTiledSurface (void);
		 CTiledSurface (const CTiledSurface&);
		~CTiledSurface (void);

	public:
		HRESULT     Append (const CTile&);
		HRESULT     Append (const CColour&, const SIZE&);  // for adding colored tile;
		HRESULT     Append (const CColour&, const WORD&);  // for adding picture tile;
		HRESULT     Clear  (void)      ;  // clears tiles and sets error to uninitialized state (OLE_E_BLANK);
		TErrorRef   Error  (void) const;
		const bool  Is     (void) const;  // returns true if tile vector is not empty;
		const
		TTiles&     Raw    (void) const;
		TTiles&     Raw    (void)      ;

	public:
		CTiledSurface& operator = (const CTiledSurface&);
		CTiledSurface& operator+= (const CTile&);
	};

	class CTiledImages : public CImageCache {
	                    typedef CImageCache TCache;
	private:
		CError      m_error;

	public:
		 CTiledImages (void);
		 CTiledImages (const CTiledImages&);
		~CTiledImages (void);

	public:
		HRESULT     Create (const CTiledSurface&);   // creates image cache from tile resource identifier(s);
		TErrorRef   Error  (void) const;             // gets last error object;
		const bool  Is     (void) const;             // checks image cache count, if the count is zero, it returns true;

	public:
		CTiledImages& operator = (const CTiledImages&);
	};

	//
	// this class is for helping in calculation of given tile collection layout in accordance with area rectangle provided;
	//
	class CTile_Layout {
	public:
		 CTile_Layout (void);
		~CTile_Layout (void);

	public:
		HRESULT  Recalc (ex_ui::draw::TTiles&, const RECT& _rc_area); // recalcs color section(s) in accordance with rect provided;

	};
}}

typedef ex_ui::draw::CTile_Bind    TTileBind; typedef TTileBind::e_side  TBoundSide;
typedef ex_ui::draw::CTile_Mode    TTileMode;
typedef ex_ui::draw::CTile_Size    TTileSize;
typedef ex_ui::draw::CTile         TTile    ;
typedef ex_ui::draw::CTiledImages  TTilesImages ;
typedef ex_ui::draw::CTiledSurface TTiledSurface;
typedef ex_ui::draw::CTile_Layout  TTileLayout  ;

#endif/*_SHAREDUIXGDIDRAWBKG_H_079105A7_B853_4C4B_92B8_9CD3AF2A0D63_INCLUDED*/