#ifndef __SFXSOACLIENTDRAWINGSERVICECOLOURQUAD_H_
#define __SFXSOACLIENTDRAWINGSERVICECOLOURQUAD_H_
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Mar-2012 at 9:59:04pm, GMT+3, Rostov-on-Don, Thursday;
	This is SfxSOA Client Drawing Service Colour Quad class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 2-May-2020 at 8:53:42p, UTC+7, Novosibirsk, Saturday;
*/
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw { 

	class CColourQuad
	{
	private:
		BYTE    m__r;
		BYTE    m__g;
		BYTE    m__b;
		BYTE    m__a;
	public:
		 CColourQuad(void);
		 CColourQuad(const COLORREF rgb, const BYTE a__);
		~CColourQuad(void);
	public:
		CColourQuad& operator =(const COLORREF);
		CColourQuad& operator =(const CColourQuad&);
		CColourQuad& operator+=(const COLORREF);
		CColourQuad& operator+=(const CColourQuad& rh);
		CColourQuad& operator/=(const BYTE  b__);
	public:
		operator COLORREF(void)const;
	public:
		void     Darken (const BYTE b__);  // darkens a color by specified percent value (b__ is in [0...100])
		void     Lighten(const BYTE b__);  // lightens a color by specified percent value (b__ is in [0...100])
		COLORREF ToColor(void)const;
	public:
		__inline  BYTE  GetB(void) const {return m__b;}
		__inline  BYTE  GetG(void) const {return m__g;}
		__inline  BYTE  GetR(void) const {return m__r;}
	};

}}
#endif/*__SFXSOACLIENTDRAWINGSERVICECOLOURQUAD_H_*/