#ifndef _EBOSHAGUISTRBAR_H_E11E044D_C71A_481B_9190_822E02369D8A_INCLUDED
#define _EBOSHAGUISTRBAR_H_E11E044D_C71A_481B_9190_822E02369D8A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Jan-2021 at 11:29:48.198 am, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha Optima Tool app main form menu strip bar wrapper interface declaration file.
*/
#include "sfx.strip.fmt.h"
#include "sfx.strip.ctrl.h"

#include "ebo.sha.gui.fmt.strip.h"
#include "ebo.sha.gui.view.man.h"

namespace ebo { namespace sha { namespace gui {

	using ebo::sha::gui::view::TViewType;

	class CStripBar_Cmd {
	private:
		DWORD     m_cmd_id;
		CStringW    m_tips;
		TViewType   m_view;   // associated view type;

	public:
		 CStripBar_Cmd (void);
		 CStripBar_Cmd (const CStripBar_Cmd&);
		 CStripBar_Cmd (const DWORD _cmd_id, const WORD _tips_res_id);
		 CStripBar_Cmd (const DWORD _cmd_id, LPCWSTR _lp_sz_tips);
		~CStripBar_Cmd (void);

	public:
		DWORD      CmdID(void) const;
		DWORD&     CmdID(void)      ;
		LPCWSTR    Tips (void) const;
		HRESULT    Tips (const WORD _w_res_id);
		HRESULT    Tips (LPCWSTR)   ;
		const
		TViewType  View (void) const;
		TViewType& View (void)      ;

	public:
		CStripBar_Cmd&  operator = (const CStripBar_Cmd& );
		CStripBar_Cmd&  operator <<(const DWORD  _cmd_id );
		CStripBar_Cmd&  operator <<(const TViewType _view);
		CStripBar_Cmd&  operator <<(LPCWSTR  _lp_sz_tips );
	};

	typedef ::std::vector<CStripBar_Cmd> TCmdEnum;

	class CStripBar_Cmds {
	public :
		enum _ndx { not_found = -1 };
	private:
		TCmdEnum    m_cmds;

	public:
		 CStripBar_Cmds (void);
		 CStripBar_Cmds (const CStripBar_Cmds&);
		~CStripBar_Cmds (void);

	public:
		const INT      ID_To_Ndx (const DWORD _cmd_id) const;  // if command does not found, -1 is returned;
		const
		CStripBar_Cmd& ID_To_Ref (const DWORD _cmd_id) const;  // if command does not exist, fake reference is returned;
		TViewType      ID_To_View(const DWORD _cmd_id) const;  // if command does not exist, fake view type is returned;
		
	public:
		HRESULT     Init(void);
		const
		TCmdEnum&   Raw (void) const;

	public:
		 CStripBar_Cmds& operator = (const CStripBar_Cmds&);
		 CStripBar_Cmds& operator <<(const TCmdEnum&);
	};

	class CStripBar_Wrap {
	private:
		CStripBar_Cmds  m_cmds;

	public:
		 CStripBar_Wrap (void);
		 CStripBar_Wrap (const CStripBar_Wrap&);
		~CStripBar_Wrap (void);

	public:
		HRESULT   ApplyTo (ST_Ctrls::CStripBar&) const;
		const
		CStripBar_Cmds& Commands (void) const;

	public:
		const
		CStripBar_Wrap& operator >>(ST_Ctrls::CStripBar&) const;
		CStripBar_Wrap& operator = (const CStripBar_Wrap&);
	};

}}}

typedef ebo::sha::gui::CStripBar_Cmd    TStripCommand;
typedef ebo::sha::gui::CStripBar_Cmds   TStripCmds;
typedef ebo::sha::gui::CStripBar_Wrap   TStripBarWrap;

#endif/*_EBOSHAGUISTRBAR_H_E11E044D_C71A_481B_9190_822E02369D8A_INCLUDED*/