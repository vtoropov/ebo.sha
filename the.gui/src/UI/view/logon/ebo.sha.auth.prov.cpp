/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2021 at 1:11:50.797 pm, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool logon screen authenticate provider sub-view interface implementation file;
*/
#include "StdAfx.h"
#include "ebo.sha.auth.prov.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CLogon_Auth_Layout {
	private:
		CLogon_Auth& m_view;
		RECT         m_rect;

	public:
		 CLogon_Auth_Layout (CLogon_Auth& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CLogon_Auth_Layout (void) {}

	public:
		RECT   GetEmpty(void) { RECT rc_empty = m_rect; ::InflateRect(&rc_empty, -0x5, -0x5); return rc_empty; }
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CLogon_Auth::CViewWnd:: CViewWnd(CLogon_Auth& _view) : TViewWnd(_view), m_view(_view), m_tabbed(_view) {}
CLogon_Auth::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CLogon_Auth::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	CLogon_Auth_Layout layout_(m_view);

	HRESULT hr_ = m_empty.Create(*this, layout_.GetEmpty(), true); hr_;
	m_empty.DrawBorder(true);

	return 0;
}

LRESULT   CLogon_Auth::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_empty.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CLogon_Auth::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CLogon_Auth_Layout layout_(m_view);
	m_empty.Update(layout_.GetEmpty());

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CLogon_Auth:: CLogon_Auth (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CLogon_Auth::~CLogon_Auth (void) {}

/////////////////////////////////////////////////////////////////////////////

TTabCtrl&  CLogon_Auth::Tabbed (void) { return m_wnd.m_tabbed; }