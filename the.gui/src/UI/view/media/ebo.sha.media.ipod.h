#ifndef _EBOSHAMEDIAIPOD_H_329034A9_E948_4D72_87BF_7756599E3987_INCLUDED
#define _EBOSHAMEDIAIPOD_H_329034A9_E948_4D72_87BF_7756599E3987_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Nov-2020 at 10:10:19.379 am, UTC+7, Novosibirsk, Friday;
	This is Ebo Sha optima tool media iPod sub-view interface declaration file;
*/
#include "shared.gen.sys.err.h"

#include "sfx.tabs.ctrl.h"

#include "ebo.sha.view.bas.h"
#include "ebo.sha.media.conn.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CMedia_iPod : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CMedia_iPod;
		private:
			CMedia_iPod&  m_view  ;
			TTabCtrl      m_tabbed;
			TiPodDevice   m_device;

		public:
			 CViewWnd (CMedia_iPod&);
			~CViewWnd (void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CMedia_iPod (void);
		~CMedia_iPod (void);

#pragma warning(disable:4481)
	private:    // ITabEvents
		HRESULT    ITabEvent_OnSelect (const DWORD _tab_ndx) override sealed;
#pragma warning(default:4481)
	
	public:
		TiPodDevice& Device (void);
		TTabCtrl&    Tabbed (void);
	};
}}}}

typedef ebo::sha::gui::view::CMedia_iPod   TiPodView;

#endif/*_EBOSHAMEDIAIPOD_H_329034A9_E948_4D72_87BF_7756599E3987_INCLUDED*/