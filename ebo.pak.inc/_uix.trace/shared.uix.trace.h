#ifndef _SHAREDUIXTRACE_H_E330B738_1B74_4EA8_9553_C5D78DBCEF79_INCLUDED
#define _SHAREDUIXTRACE_H_E330B738_1B74_4EA8_9553_C5D78DBCEF79_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Apr-2020 at 4:04:27p, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack UIX trace view interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.run.event.h"
#include "shared.log.file.h"

#include "shared.uix.trace.data.h"
#include "shared.uix.trace.fmt.h"
#include "shared.uix.trace.lay.h"
#include "shared.uix.trace.ind.h"

#include "shared.uix.ctrl.label.h"
#include "shared.uix.ctrl.label.ext.h"
#include "shared.uix.ctrl.button.h"

namespace ex_ui { namespace trace {

	using shared::sys_core::CError ;
	using shared::runnable::IGenericEventNotify;

	interface ITrace_Events {
		virtual HRESULT ITrace_OnEntryClick(const DWORD _ent_id) PURE;
	};

	interface IHeader_Events : public ex_ui::controls::ILabel_Events {};

	using ex_ui::controls::CLabel_Ex;       typedef CLabel_Ex     THeader;
	using ex_ui::controls::CLab_Format;     typedef CLab_Format   THeaderFmt;

	using ex_ui::controls::CButton_Ex;      typedef CButton_Ex    TScroll;
	using ex_ui::controls::CState;   typedef CState TState ;

	class CScrolls {
	protected:
		HANDLE&     m_wnd_ptr;

	public:
		 CScrolls (HANDLE& _parent_ref);
		~CScrolls (void);

	public:
		const
		TScroll&    Down(void) const;
		TScroll&    Down(void)      ;
		const
		TScroll&    Up  (void) const;
		TScroll&    Up  (void)      ;
	};
	class CTraceEx;
	class CVisible_Range {
	protected:
		CTraceEx&    m_view ;
		DWORD        m_n_top;
		DWORD        m_n_cnt;

	public:
		 CVisible_Range (CTraceEx&) ;
		~CVisible_Range (void) ;

	public:
		const
		DWORD&       Count(void) const;
		DWORD&       Count(void)      ;
		const bool   IsScrollable(const RECT& _rc_area) const;
		const
		DWORD&       Top(void) const;
		DWORD&       Top(void)      ;
	};

	typedef ex_ui::trace::CVisible_Range     TVisRange    ;

	class CTraceEx : public IFormat_Events, public IGenericEventNotify, public IHeader_Events {
	protected:
		ITrace_Events& m_evt_snk;   // trace view owner event sink reference;
		HANDLE         m_wnd_ptr;
		CError         m_error  ;
		CEntries       m_entries;
		TTraceLayout   m_layout ;
		TTraceFmt      m_format ;
		CIndents       m_indents;
		CScrolls       m_scrolls;
		TVisRange      m_range  ;

	public:
		 CTraceEx (ITrace_Events&);
		~CTraceEx (void);

	public:
		HRESULT        Create (const HWND _h_parent, const RECT& _rc_area, const bool _b_visible = true);
		HRESULT        Destroy(void);
		const	       
		CEntries&      Entries(void) const;
		CEntries&      Entries(void)      ;
		TErrorRef      Error  (void) const;
		const	       
		TTraceFmt&     Format (void) const;
		TTraceFmt&     Format (void)      ;
		const	       
		THeader&       Header (void) const;
		THeader&       Header (void)      ;
		TIndentsRef    Indents(void) const;
		CIndents&      Indents(void)      ;
		const bool     Is     (void) const;
		const	       
		TTraceLayout&  Layout (void) const;
		TTraceLayout&  Layout (void)      ;
		const	       
		TVisRange&     Range  (void) const;
		TVisRange&     Range  (void)      ;
		HRESULT        Refresh(void)      ;
		const	       
		CScrolls&      Scrolls(void) const;
		CScrolls&      Scrolls(void)      ;
		CWindow        Window (void) const;

	public:
#if (0)
		VOID        Error(const CError&);
		VOID        Error(LPCTSTR pszFormat, ...);
		VOID        Info (LPCTSTR pszFormat, ...);
		VOID        Warn (LPCTSTR pszFormat, ...);
		VOID        InfoFromResource(const UINT nResId, ...);
#endif
	public:
		VOID        Put  (const TEntryType, LPCTSTR pszFormat, ...); // generic method for appending new entry;

#if (0)
		VOID        Put  (const TEntryType, LPCTSTR _lp_sz_timestamp, LPCTSTR pszFormat, ...);
		VOID        Put  (const TEntryType, const INT  _w_img_ndx   , LPCTSTR pszFormat, ...);
		VOID        Put  (const TEntryType, const WORD _w_img_res   , LPCTSTR pszFormat, ...);
		VOID        Put  (const TEntryType, const INT  _w_img_ndx   , LPCTSTR _lp_sz_timestamp, LPCTSTR pszFormat, ...);
		VOID        Put  (const TEntryType, const WORD _w_img_res   , LPCTSTR _lp_sz_timestamp, LPCTSTR pszFormat, ...);
#endif
	private:   // IFormat_Events
#pragma warning(disable:4481)
		HRESULT   IFmt_BkgChanged (void) override sealed;
		HRESULT   IFmt_FontChanged(void) override sealed;
		HRESULT   IFmt_ImgChanged (const UINT _u_res_id) override sealed;
#pragma warning(default:4481)
	private:   // IGenericEventNotify
#pragma warning(disable:4481)
		HRESULT   GenericEvent_OnNotify(const LONG n_evt_id) override sealed;
#pragma warning(default:4481)
	private:   // IHeader_Events
#pragma warning(disable:4481)
		HRESULT   ILabel_OnBkgChanged (void) override sealed;
		HRESULT   ILabel_OnFontChanged(void) override sealed;
#pragma warning(default:4481)

	private:
		CTraceEx(const CTraceEx&);
		CTraceEx& operator= (const CTraceEx&);
	};

}}
typedef ex_ui::trace::CTraceEx TTrace;

#endif/*_SHAREDUIXTRACE_H_E330B738_1B74_4EA8_9553_C5D78DBCEF79_INCLUDED*/