/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Dec-2020 at 8:25:52.675 pm, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha optima tool generic GUI parts layout interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:03:27.634 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "StdAfx.h"
#include "ebo.sha.gui.layout.h"

using namespace ebo::sha::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace layout { namespace _impl {

	class CSelect_Def {
	public:
		 CSelect_Def (void) {}
		~CSelect_Def (void) {}

	public:
		HRESULT  Do (TBorders& _borders) const {  HRESULT hr_ = S_OK;
			_borders;
			_borders.Right().Thickness() = 1;
			_borders.Right().Color () << RGB(0, 0, 0); // TODO: this is the bug; if no color is set, border is nor drawn;
			return hr_;
		}

		HRESULT  Do (TSelectLay& _layout) const {  HRESULT hr_ = S_OK;
			_layout;
			_layout.Width(225);
			_layout.HAlign() = THorzAlign::eLeft;
			_layout.Margins().Left() = 5;

			_layout.Images().Size().cx = 48;
			_layout.Images().Size().cy = 48;

			_layout.Images().Margins().Right() = 5; _layout.Images().Margins().Left() = 3;
			_layout.Images().Margins().Top  () = _layout.Images().Margins().Bottom() = 5;

			return hr_;
		}
	};

}}}}}
using namespace ebo::sha::gui::layout::_impl;
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

TSelectLayout:: CSelect (void) {}
TSelectLayout:: CSelect (const TSelectLayout& _lay) : TSelectLayout() { *this = _lay; }
TSelectLayout:: CSelect (TSelectLay& _target) : TSelectLayout() { *this >> _target; }
TSelectLayout::~CSelect (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   TSelectLayout::ApplyTo(TBorders& _ctrl_borders) const { HRESULT hr_ = S_OK;
	_ctrl_borders;
	hr_ = CSelect_Def().Do(_ctrl_borders);
	return hr_;
}

HRESULT   TSelectLayout::ApplyTo(TSelectLay& _ctrl_layout) const { HRESULT hr_ = S_OK;
	_ctrl_layout;
	hr_ = CSelect_Def().Do(_ctrl_layout);
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////
const
TSelectLayout&  TSelectLayout::operator >>(TBorders& _borders) const { this->ApplyTo(_borders); return *this; }
const
TSelectLayout&  TSelectLayout::operator >>(TSelectLay& _lay) const { this->ApplyTo(_lay); return *this; }
TSelectLayout&  TSelectLayout::operator = (const TSelectLayout& _lay) { _lay; return *this; }

/////////////////////////////////////////////////////////////////////////////

TLayout:: CLayout (void) {}
TLayout:: CLayout (const TLayout& _ref) : TLayout() { *this = _ref; }
TLayout::~CLayout (void) {}

/////////////////////////////////////////////////////////////////////////////
const
TSelectLayout&   TLayout::Select  (void) const { return m_select; }
TSelectLayout&   TLayout::Select  (void)       { return m_select; }

/////////////////////////////////////////////////////////////////////////////

TLayout&  TLayout::operator = (const TLayout& _ref) { this->Select() = _ref.Select(); return *this; }