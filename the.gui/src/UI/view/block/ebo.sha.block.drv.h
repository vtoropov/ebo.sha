#ifndef _EBOSHABLOCKDRV_H_06E49384_EF42_46E7_8438_C0FA7B4D2312_INCLUDED
#define _EBOSHABLOCKDRV_H_06E49384_EF42_46E7_8438_C0FA7B4D2312_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 31-Dec-2020 at 5:24:29.075 am, UTC+7, Novosibirsk, Thursday;
	This is Ebo Sha Optima Tool HTTP connection control driver mode sub-view interface declaration file.
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.gui.cmp.cap.h"
#include "ebo.sha.gui.cmp.edit.h"
#include "ebo.sha.gui.fmt.edit.h"

#include "ebo.sha.cmd.hand.ssh.h"
#include "ebo.sha.svc.cmd.hand.h"

#include "driver.dev.loc.gui.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CHttp_Driver : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CHttp_Driver;
		private:
			CHttp_Driver&     m_view   ;
			TCaptionComponent m_caps[1];
			TInfoPanel        m_info   ;
			TEditRa           m_ctrl[6];
			TButtonCtrl       m_btns[2];
			TSvcLastState     m_svc_sta;

		private:
			 CViewWnd (CHttp_Driver&);
			~CViewWnd (void);

		private:   // TWindow
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd          m_wnd;
		TSshDrvCmdHandler m_handler;

	public:
		 CHttp_Driver (void);
		~CHttp_Driver (void);
#pragma warning (disable: 4481)
	private:
		HRESULT   IControlEvent_OnClick(const UINT ctrlId) override sealed; // looks like it must be implemented in window object due to a lot of refs required;
#pragma warning (default: 4481)
	public:
		HRESULT   IsVisible(const bool) override;
		
	public:
		TSshDrvCmdHandler&   Handler(void);
	};

}}}}

typedef ebo::sha::gui::view::CHttp_Driver   THttpDrvView;

#endif/*_EBOSHABLOCKDRV_H_06E49384_EF42_46E7_8438_C0FA7B4D2312_INCLUDED*/