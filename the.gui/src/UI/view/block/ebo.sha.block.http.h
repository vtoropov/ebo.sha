#ifndef _EBOSHABLOCK_H_39DDD107_6748_467F_A011_2911F35B03C6_INCLUDED
#define _EBOSHABLOCK_H_39DDD107_6748_467F_A011_2911F35B03C6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Dec-2020 at 07:52:11.171 am, UTC+7, Novosibirsk, Sunday;
	This is Ebo Sha optima tool HTTP connection control view interface declaration file.
*/
#include "shared.gen.sys.err.h"

#include "sfx.tabs.ctrl.h"

#include "ebo.sha.view.bas.h"
#include "ebo.sha.block.drv.h"
#include "ebo.sha.view.empty.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CBlock_Http : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CBlock_Http;
		private:
			CBlock_Http&  m_view  ;
			TTabCtrl      m_tabbed;
			THttpDrvView  m_driver;
			TEmptyView    m_empty ;

		public:
			 CViewWnd (CBlock_Http&);
			~CViewWnd (void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CBlock_Http (void);
		~CBlock_Http (void);

#pragma warning(disable:4481)
	private:    // ITabEvents
		HRESULT    ITabEvent_OnSelect (const DWORD _tab_ndx) override sealed;
#pragma warning(default:4481)

	public:
		THttpDrvView& Driver (void);
		TTabCtrl&     Tabbed (void);
	};
}}}}

typedef ebo::sha::gui::view::CBlock_Http   THttpView;

#endif/*_EBOSHABLOCK_H_39DDD107_6748_467F_A011_2911F35B03C6_INCLUDED*/