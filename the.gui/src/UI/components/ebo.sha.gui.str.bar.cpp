/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Jan-2021 at 11:38:52.963 am, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha Optima Tool app main form menu strip bar wrapper interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.gui.str.bar.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui;

/////////////////////////////////////////////////////////////////////////////

TStripCommand:: CStripBar_Cmd  (void) : m_cmd_id (0), m_view(TViewType::e_none) {}
TStripCommand:: CStripBar_Cmd  (const TStripCommand& _ref) : TStripCommand() { *this = _ref; }
TStripCommand:: CStripBar_Cmd  (const DWORD _cmd_id, const WORD _tips_res_id) : TStripCommand() { *this << _cmd_id; this->Tips(_tips_res_id); }
TStripCommand:: CStripBar_Cmd  (const DWORD _cmd_id, LPCWSTR _lp_sz_tips) : TStripCommand() { *this << _cmd_id; this->Tips(_lp_sz_tips); }
TStripCommand::~CStripBar_Cmd  (void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD     TStripCommand::CmdID (void) const { return m_cmd_id; }
DWORD&    TStripCommand::CmdID (void)       { return m_cmd_id; }
LPCWSTR   TStripCommand::Tips  (void) const { return m_tips.GetString(); }
HRESULT   TStripCommand::Tips  (const WORD _w_res_id) { HRESULT hr_ = S_OK;
	_w_res_id;
	if (0 == _w_res_id)
		return (hr_ = E_INVALIDARG);

	m_tips.LoadStringW(_w_res_id);

	return hr_;
}
HRESULT   TStripCommand::Tips  (LPCWSTR _lp_sz_tips) { HRESULT hr_ = S_OK;
	_lp_sz_tips;
	m_tips = _lp_sz_tips;
	return hr_;
}

const
TViewType  TStripCommand::View (void) const { return m_view; }
TViewType& TStripCommand::View (void)       { return m_view; }

/////////////////////////////////////////////////////////////////////////////

TStripCommand&  TStripCommand::operator = (const TStripCommand& _ref) { *this << _ref.CmdID() << _ref.Tips(); return *this; }
TStripCommand&  TStripCommand::operator <<(const DWORD _cmd_id)   { this->CmdID() = _cmd_id; return *this; }
TStripCommand&  TStripCommand::operator <<(const TViewType _view) { this->View() = _view; return *this; }
TStripCommand&  TStripCommand::operator <<(LPCWSTR _lp_sz_tips)   { this->Tips(_lp_sz_tips); return *this; }

/////////////////////////////////////////////////////////////////////////////

TStripCmds:: CStripBar_Cmds (void) { this->Init(); }
TStripCmds:: CStripBar_Cmds (const TStripCmds& _ref) : TStripCmds() { *this = _ref; }
TStripCmds::~CStripBar_Cmds (void) {}

/////////////////////////////////////////////////////////////////////////////
const INT      TStripCmds::ID_To_Ndx (const DWORD _cmd_id) const {
	_cmd_id;
	INT ndx_found = TStripCmds::not_found;
	if (0 == _cmd_id)
		return ndx_found;

	for (UINT i_ = 0; i_ < m_cmds.size(); i_++)
		if (_cmd_id == m_cmds[i_].CmdID()) {
			ndx_found = static_cast<INT>(i_); break;
		}

	return ndx_found;
}
const
CStripBar_Cmd& TStripCmds::ID_To_Ref (const DWORD _cmd_id) const {
	_cmd_id;
	const INT n_found_ndx = this->ID_To_Ndx(_cmd_id); 
	if (TStripCmds::not_found == n_found_ndx) {
		static CStripBar_Cmd na_cmd;
		return na_cmd;
	}
	else
		return this->Raw().at(static_cast<size_t>(n_found_ndx));
}
TViewType      TStripCmds::ID_To_View(const DWORD _cmd_id) const {
	_cmd_id;
	const INT n_found_ndx = this->ID_To_Ndx(_cmd_id);
	if (TStripCmds::not_found == n_found_ndx)
		return TViewType::e_none;
	else
		return this->Raw().at(static_cast<size_t>(n_found_ndx)).View();
}

HRESULT      TStripCmds::Init(void) { HRESULT hr_ = S_OK;

	if (m_cmds.empty() == false)
		m_cmds.clear();

	static UINT m_cmd_ids[] = { // this is the order of the strip items; from left to right, i.g. from begin to end for this array;
		IDC_EBO_SHA_STR_CMD_0,  // sweet home;
		IDC_EBO_SHA_STR_CMD_5,  // TCP/IP/process blocker;
		IDC_EBO_SHA_STR_CMD_4,  // fake GPS;
		IDC_EBO_SHA_STR_CMD_6,  // media;
		IDC_EBO_SHA_STR_CMD_1,  // cool settings;
		IDC_EBO_SHA_STR_CMD_2,  // desired help ;
		IDC_EBO_SHA_STR_CMD_3,  // fkn identity ;
	};

	for (UINT i_ = 0; i_ < _countof(m_cmd_ids); i_++) {
		CStripBar_Cmd str_cmd(m_cmd_ids[i_], static_cast<WORD>(m_cmd_ids[i_]));
		TViewType    view_type = TViewType::e_none ;
		if (0 == i_) view_type = TViewType::e_home ;
		if (1 == i_) view_type = TViewType::e_block;
		if (2 == i_) view_type = TViewType::e_fake ;
		if (3 == i_) view_type = TViewType::e_media;
		if (4 == i_) view_type = TViewType::e_sets ;
		if (5 == i_) view_type = TViewType::e_help ;
		if (6 == i_) view_type = TViewType::e_logon;

		str_cmd.View() = view_type;
		try {
			m_cmds.push_back(str_cmd);
		}
		catch (const ::std::bad_alloc&) {
			hr_ = E_OUTOFMEMORY; break;
		}
	}

	return hr_;
}
const
TCmdEnum&    TStripCmds::Raw (void) const { return m_cmds; }

/////////////////////////////////////////////////////////////////////////////

TStripCmds&  TStripCmds::operator = (const TStripCmds& _ref) { *this << _ref.Raw(); return *this; }
TStripCmds&  TStripCmds::operator <<(const TCmdEnum& _cmds) {
	_cmds;
	try {
		this->m_cmds.insert(this->m_cmds.end(), _cmds.begin(), _cmds.end());
	}
	catch (const ::std::bad_alloc&) {}
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

TStripBarWrap:: CStripBar_Wrap (void) {}
TStripBarWrap:: CStripBar_Wrap (const TStripBarWrap& _ref) : TStripBarWrap() { *this = _ref; }
TStripBarWrap::~CStripBar_Wrap (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   TStripBarWrap::ApplyTo (ST_Ctrls::CStripBar& _strip) const { HRESULT hr_ = S_OK;
	_strip;
	if (_strip.Items().Count())
		return (hr_ = __DwordToHresult(ERROR_ALREADY_INITIALIZED));

	for (INT i_ = 0; i_ < this->Commands().Raw().size(); i_++) {
		const TStripCommand& str_cmd = this->Commands().Raw()[i_];
		hr_ = _strip.Items().Add(str_cmd.CmdID(), NULL);
		if (FAILED(hr_))
			break;
	}

	return hr_;
}
const
TStripCmds&     TStripBarWrap::Commands (void) const { return m_cmds; }

/////////////////////////////////////////////////////////////////////////////
const
TStripBarWrap&  TStripBarWrap::operator >>(ST_Ctrls::CStripBar& _strip) const {
	this->ApplyTo(_strip); return *this;
}
TStripBarWrap&  TStripBarWrap::operator = (const TStripBarWrap& _ref) { this->m_cmds = _ref.Commands(); return *this; }

/////////////////////////////////////////////////////////////////////////////