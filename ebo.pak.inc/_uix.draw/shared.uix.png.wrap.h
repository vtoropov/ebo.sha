#ifndef _UIXDRAWPNGLOADER_H_E1B2893F_B891_4e86_9457_A6C667A0EA5F_INCLUDED
#define _UIXDRAWPNGLOADER_H_E1B2893F_B891_4e86_9457_A6C667A0EA5F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Feb-2015 at 1:16:22am, GMT+3, Taganrog, Friday;
	This is UIX draw library PNG wrapper common class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 28-May-2018 at 11:17:10.932 pm, UTC+7, Phuket, Rawai, Monday;
*/
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw {
	// https://en.wikipedia.org/wiki/Portable_Network_Graphics#File_format
	typedef Gdiplus::Bitmap TBitmap;
	//
	// TODO: cloning by using gdiplus GetHBITMAP() method does not guarantee you have new independant object; it must be checked yet;
	//       supporting APNG is just in a plan for the nearest future of Ebo Pack;
	//
	class CPngBitmap {
	public:
		TBitmap*   m_pBitmap; // actually, it is not necessary to use pointer here, regular instance is also fine;

	public:
		virtual
		~CPngBitmap (void);
		 CPngBitmap (void);
		 CPngBitmap (const CPngBitmap&);
		 CPngBitmap (const INT nWidth, const INT nHeight, const DWORD _fmt = PixelFormat32bppARGB);
		 CPngBitmap (const SIZE&, const DWORD _fmt = PixelFormat32bppARGB);
		 CPngBitmap (LPCWSTR lp_sz_path);

	public:
		HRESULT    Attach (TBitmap*);
		TBitmap*   Detach (void);

	public:
		// https://docs.microsoft.com/en-us/windows/win32/gdi/alpha-blending-a-bitmap is an example;
		// https://docs.microsoft.com/en-us/windows/win32/gdi/alpha-blending
		// must be reviewed otherwise it is useless;
		HRESULT    Alpha  (const TAlpha, const bool _b_increase);   // sets a value for alpha/transparency byte;
		HBITMAP    Clone  (void) const  ;                           // returns a copy of internal bitmap object;
		HRESULT    Create (const HBITMAP ); // https://docs.microsoft.com/en-us/windows/win32/api/gdiplusheaders/nf-gdiplusheaders-bitmap-frombitmapinfo
		HRESULT    Create (const INT nWidth, const INT nHeight, const DWORD _fmt = PixelFormat32bppARGB); // creates an empty bitmap;
		HRESULT    Create (const SIZE&, const DWORD _fmt = PixelFormat32bppARGB);                         // creates an empty bitmap;
		HRESULT    Create (const UINT resId, const HMODULE hResourceModule = NULL);                       // loads a PNG from resource of executable;
		HRESULT    Destroy(void);           // https://docs.microsoft.com/en-us/windows/win32/api/gdiplusbase/nf-gdiplusbase-gdiplusbase-operatordelete
		HRESULT    Load   (LPCWSTR lp_sz_path);                  // loads a PNG image file;

	public:
		bool       Is  (void) const;        // https://docs.microsoft.com/en-us/windows/win32/api/gdiplusheaders/nf-gdiplusheaders-image-getlaststatus
		HRESULT    Handle (HBITMAP& _out) const;                 // assigns a bitmap handle that is a copy of this class instance bitmap handle;
		const	   
		TBitmap&   Ref (void) const;
		TBitmap&   Ref (void)      ;
		TBitmap*   Ptr (void) const;
		SIZE       Size(void) const;

	public:
		CPngBitmap& operator = (const CPngBitmap&);   // creates a clone;
		CPngBitmap& operator <<(const HBITMAP&);      // creates new bitmap object;
		CPngBitmap& operator <<(const SIZE&);         // creates an empty bitmap (transparent) of specified size;
		CPngBitmap& operator <<(LPCWSTR lp_sz_path);  // creates a bitmap from an image file;

	public:
		operator   const HBITMAP  (void) const;
		operator   const TBitmap* (void) const;

	public:
		static HRESULT IsValid (const TBitmap*); // returns S_OK if valid, otherwise it returns an error that is based on gdiplus status;
	};

	class CPngBitmapPtr
	{
	private:
		CPngBitmap*   m_pBitmap;
		HRESULT       m_hResult;
	public:
		 CPngBitmapPtr(const bool bCreateObject = false);
		~CPngBitmapPtr(void);

	public:
		HRESULT       Attach (CPngBitmap*);
		HRESULT       Attach (Gdiplus::Bitmap*);
		HRESULT       Create (const INT nWidth, const INT nHeight);
		HRESULT       Create (const UINT resId, const HMODULE hResourceModule = NULL);
		HRESULT       Destroy(void);
		CPngBitmap*   Detach (void);
		HRESULT       GetLastResult(void) const;
		CPngBitmap*   GetObject    (void) const;
		const
		CPngBitmap&   GetObjectRef (void) const;
		CPngBitmap&   GetObjectRef (void);
		bool          IsValidObject(void) const;
	public:
		operator CPngBitmap*() const { return m_pBitmap; }
	public:
		static HRESULT   CreateObject(CPngBitmap*&);
		static HRESULT   CreateObject(const INT nWidth, const INT nHeight, CPngBitmap*&);
		static HRESULT   DestroyObject_Safe(CPngBitmap*&);
	};
}}

#endif/*_UIXDRAWPNGLOADER_H_E1B2893F_B891_4e86_9457_A6C667A0EA5F_INCLUDED*/