/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Aug-2020 at 3:17:17a, UTC+7, Novosibirsk, Friday;
	This is SSH-Tunnel WFP driver locator interface implementation file; 
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 7:42:17.113 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "driver.dev.loc.h"

using namespace shared::driver::client;

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace driver { namespace client { namespace _impl {

	CStringW CLocate_AppDir (void) {

		static const DWORD dw_max_len = _MAX_DRIVE + _MAX_DIR + _MAX_FNAME + _MAX_EXT;
		static WCHAR w_buffer[dw_max_len] = {0};

		if (0 == ::wcslen(w_buffer)) {
			::GetModuleFileNameW(NULL, w_buffer, _countof(w_buffer));
		}

		CStringW cs_dir(w_buffer);

		const INT n_pos = cs_dir.ReverseFind('\\');
		if (-1 != n_pos) {
			cs_dir = cs_dir.Left(n_pos + 1);
		}

		return cs_dir;
	}

	CStringW CLocate_SvcAlias (LPCWSTR _lp_sz_path) {
		_lp_sz_path;
		CStringW cs_alias;
		if (NULL == _lp_sz_path || 0 == ::lstrlenW(_lp_sz_path))
			return cs_alias;

		cs_alias = _lp_sz_path;
		INT n_pos = cs_alias.ReverseFind('\\');
		if (-1 != n_pos) {
			cs_alias = cs_alias.Right(cs_alias.GetLength() - n_pos - 1);

			n_pos = cs_alias.ReverseFind('.');
			if (-1 != n_pos) {
				cs_alias = cs_alias.Left(n_pos);
			}
		}

		return cs_alias;
	}

}}}}

using namespace shared::driver::client::_impl;
/////////////////////////////////////////////////////////////////////////////

TDrvLocate:: CDrvLocator(void) { m_error << __MODULE__ << S_OK >> __MODULE__;
	m_error = this->Alias(_T("ssh.tnl.drv_v15"));
	m_path.Format(_T("%s%s.sys"), CLocate_AppDir().GetString(), this->Alias());
}
TDrvLocate:: CDrvLocator (const TDrvLocate& _ref) : TDrvLocate() { *this = _ref; }
TDrvLocate::~CDrvLocator(void) {}

/////////////////////////////////////////////////////////////////////////////

LPCWSTR      TDrvLocate::Alias  (void) const { return m_alias.GetString(); }
HRESULT      TDrvLocate::Alias  (LPCWSTR _lp_sz_val) {
	m_error << __MODULE__ << S_OK;

	if (NULL == _lp_sz_val || 0 == ::lstrlenW(_lp_sz_val))
		return (m_error << E_INVALIDARG);
	else
		m_alias = _lp_sz_val;

	return m_error;
}

CStringW     TDrvLocate::Device (void) const { CStringW cs_dev_name(DEVICE_NAME_WIN32); return cs_dev_name; }
CStringW     TDrvLocate::Folder (void) const { return CLocate_AppDir(); }
TErrorRef    TDrvLocate::Error  (void) const { return m_error; }

LPCWSTR      TDrvLocate::Path   (void) const { return m_path.GetString() ; }
HRESULT      TDrvLocate::Path   (LPCWSTR _lp_sz_path) {
	m_error << __MODULE__ << S_OK;
	_lp_sz_path;
	if (NULL == _lp_sz_path || 0 == ::lstrlenW(_lp_sz_path))
		return (m_error << E_INVALIDARG);

	m_path  = _lp_sz_path;
	m_alias = CLocate_SvcAlias(_lp_sz_path);

	return m_error;
}

CStringW     TDrvLocate::Service(void) const { return CStringW(this->Alias()); }

/////////////////////////////////////////////////////////////////////////////

TDrvLocate&  TDrvLocate::operator = (const TDrvLocate& _ref) { this->Alias(_ref.Alias()); return *this; }

/////////////////////////////////////////////////////////////////////////////

TDrvLocatePers:: CDrvLocator_Pers (void) : TLocator() { TLocator::m_error >> __MODULE__; }
TDrvLocatePers:: CDrvLocator_Pers (const TDrvLocatePers& _ref) : TDrvLocatePers() { *this = _ref; }
TDrvLocatePers::~CDrvLocator_Pers (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   TDrvLocatePers::Load (void) {
	TLocator::m_error << __MODULE__ << S_OK;

	TRegStg reg_stg(HKEY_CURRENT_USER);

	CStringW cs_value;
	reg_stg.Load(_T("ebo::sha\\driver\\http"), _T("path"), cs_value);
	if (cs_value.IsEmpty() == false) {
		TLocator::m_path = cs_value;
		TLocator::m_alias = CLocate_SvcAlias(cs_value);
	}

	return TLocator::m_error;
}

HRESULT   TDrvLocatePers::Save (void) {
	TLocator::m_error << __MODULE__ << S_OK; HRESULT hr_ = TLocator::m_error;

	TRegStg reg_stg(HKEY_CURRENT_USER);

	hr_ = reg_stg.Save(_T("ebo::sha\\driver\\http"), _T("path"), m_path); if (FAILED(hr_)) TLocator::m_error = reg_stg.Error();

	return TLocator::m_error;
}

/////////////////////////////////////////////////////////////////////////////

TDrvLocatePers&  TDrvLocatePers::operator = (const TDrvLocatePers& _ref) { (TLocator&)*this = (const TLocator&)_ref; return *this; }