#ifndef _SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED
#define _SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Apr-2012 at 07:46:04pm, GMT+3, Rostov-on-Don, Sunday;
	This is Pulsepay project server application generic command line interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Platinum Clocking project on 19-Mar-2014 at 8:17:25am, GMT+4, Taganrog, Wednesday;
	Adopted to BotRevolt project on 21-Aug-2014 at 6:06:46am, GMT+4, Taganrog, Thursday;
	Adopted to FG (thefileguardian.com) project on 11-Jun-2016 at 1:28:28p, GMT+7, Phuket, Rawai, Saturday;
	Adopted to FakeGPS driver project on 13-Dec-2019 at 9:43:27a, UTC+7, Novosibirsk, Friday;
*/
#include <vector>
#include <shellapi.h>
#include <map>
#include "shared.gen.sys.err.h"
namespace shared { namespace user32 {

	typedef ::std::map<::ATL::CStringW, ::ATL::CStringW> TCmdLineArgs;

	class CCommandLine {
	private:
		CAtlString   m_module_path;
		TCmdLineArgs m_args;
	public:
		 CCommandLine(void);
		~CCommandLine(void);
	public:
		HRESULT      Append(LPCWSTR _lp_sz_nm, LPCWSTR _lp_sz_val);
		CAtlString   Arg   (LPCWSTR _lp_sz_nm) const;
		LONG         Arg   (LPCWSTR _lp_sz_nm, const LONG _def_val)const;
		TCmdLineArgs Args  (void) const;                                  // returns a copy of command line argument collection
		VOID         Clear (void)      ;
		INT          Count (void) const;
		bool         Has   (LPCWSTR pszArgName)const;
		CAtlString   Path  (void) const;                                  // returns executable absolute path;
	public:
		CAtlString   ToString(LPCWSTR _lp_sz_sep = NULL) const;

	public:
		operator  LPCWSTR (void) const;               // returns command line object as a string;

	public:
		bool operator==(LPCWSTR pszArgName) const;    // finds an argument by name provided;
	};

	class CArgument {
	protected:
		DWORD     m_type;
		CStringW  m_name;
		CStringW  m_verb;

	public:
		 CArgument (void);
		 CArgument (const CArgument&);
		 CArgument (const WORD _w_res_name, const WORD _w_res_verb, const DWORD _dw_type);
		 CArgument (LPCWSTR _lp_sz_name, LPCWSTR _lp_sz_verb, const DWORD _dw_type);
		~CArgument (void);

	public:
		bool      Is  (void) const;
		LPCWSTR   Name(void) const;
		HRESULT   Name(const WORD _w_res_id);
		HRESULT   Name(LPCWSTR _lp_sz_name );
		DWORD     Type(void) const;
		bool      Type(const DWORD);
		LPCWSTR   Verb(void) const;
		HRESULT   Verb(const WORD _w_res_id);
		HRESULT   Verb(LPCWSTR _lp_sz_desc );

	public:
		CArgument& operator = (const CArgument&);

	public:
		bool operator == (const CArgument&) const;
		bool operator == (LPCWSTR _lp_sz_name) const;
	};
}}
typedef ::std::vector<shared::user32::CArgument> TArguments;
typedef shared::user32::CCommandLine TCmdLine;

#endif/*_SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED*/