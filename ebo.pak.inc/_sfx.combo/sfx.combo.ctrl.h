#ifndef _SFXCOMBOCTRL_H_4D58DEF9_86FE_4544_9A28_771F47B6E27F_INCLUDED
#define _SFXCOMBOCTRL_H_4D58DEF9_86FE_4544_9A28_771F47B6E27F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 02-Aug-2010 at 08:54:41pm, GMT+3, Rostov-on-Don, Monday;
	This is ST Combo Box Control class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Sfx combo project on 22-Nov-2020 at 6:53:21.133 am, UTC+7, Novosibirsk, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"

#include "sfx.button.ctrl.h"
#include "sfx.combo.fmt.h"
#include "sfx.combo.lay.h"

#include "sfx.label.ctrl.h"

namespace ST_Ctrls {
	using shared::sys_core::CError ;

	using ex_ui::controls::CBorders;
	using ex_ui::controls::IControlEvent;
	using ex_ui::controls::IRenderer_Ex;

	using ex_ui::draw::defs::IRenderer;

	class CCombo {
	protected:
		IControlEvent&
		             m_evt_snk;
		HANDLE       m_wnd_ptr;
		CError       m_error  ;
		UINT         m_ctrl_id;
		CBorders     m_borders;
		TComboFmt    m_format ;
		TComboLay    m_layout ;
		TButtonCtrl  m_button ;
		TLabelCtrl   m_label  ; // used for disable mode as read-only text component of combo control;

	public:
		 CCombo (IControlEvent& _evt_snk);
		~CCombo (void);

	public:
		HRESULT        Create  (const HWND hParent, const RECT& _rc_area, const UINT _ctrl_id);
		HRESULT        Destroy (void)      ;

	public:
		const
		CBorders&      Borders (void) const;
		CBorders&      Borders (void)      ;
		const
		TButtonCtrl&   Button  (void) const;
		TButtonCtrl&   Button  (void)      ;
		TErrorRef      Error   (void) const;
		IControlEvent& Events  (void) const;
		IControlEvent& Events  (void)      ;
		const
		TComboFmt&     Format  (void) const;
		TComboFmt&     Format  (void)      ;
		const UINT     ID      (void) const;
		const
		TLabelCtrl&    Label   (void) const;
		TLabelCtrl&    Label   (void)      ;
		const
		TComboLay&     Layout  (void) const;
		TComboLay&     Layout  (void)      ;
		HRESULT        Renderer(IRenderer_Ex*  const );
		IRenderer_Ex*  Renderer(void) const;
		HRESULT        Refresh (void)      ;
		CWindow        Window  (void) const;

	private: // non-copyable;
		CCombo (const CCombo&);
		CCombo& operator = (const CCombo&);
	};
}

typedef ST_Ctrls::CCombo  TComboCtrl;

#endif/*_SFXCOMBOCTRL_H_4D58DEF9_86FE_4544_9A28_771F47B6E27F_INCLUDED*/