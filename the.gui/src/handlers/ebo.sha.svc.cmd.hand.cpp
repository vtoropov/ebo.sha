/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jan-2021 at 8:23:24.319 pm, UTC+7, Novosibirsk, Friday;
	This is Ebo Pack UM test driver control service command handler interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.svc.cmd.hand.h"

using namespace ebo::sha::handlers;

/////////////////////////////////////////////////////////////////////////////

CSvcCmd_Base:: CSvcCmd_Base (void) : m_type(e_type::e_undef) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CSvcCmd_Base:: CSvcCmd_Base (const CSvcCmd_Base& _ref) : CSvcCmd_Base() { *this = _ref; }
CSvcCmd_Base::~CSvcCmd_Base (void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CSvcCmd_Base::Error  (void) const { return m_error; }
TCmdSvcType   CSvcCmd_Base::Type   (void) const { return m_type ; }

/////////////////////////////////////////////////////////////////////////////
const
TDrvService&  CSvcCmd_Base::Service(void) const { return m_service; }
TDrvService&  CSvcCmd_Base::Service(void)       { return m_service; }

/////////////////////////////////////////////////////////////////////////////

CSvcCmd_Base& CSvcCmd_Base::operator = (const CSvcCmd_Base& _ref) {
	this->m_type = _ref.Type(); this->Service() = _ref.Service(); this->m_error = _ref.Error(); return *this;
}

/////////////////////////////////////////////////////////////////////////////

TCmdSvcCreate:: CSvcCmd_Create (void) : TBase() { TBase::m_error >> __MODULE__; }
TCmdSvcCreate:: CSvcCmd_Create (const TCmdSvcCreate& _ref) : TCmdSvcCreate() { *this = _ref; }
TCmdSvcCreate::~CSvcCmd_Create (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT  TCmdSvcCreate::Execute (void) {
	m_error << __MODULE__ << S_OK;

	CServiceMan drv_man;
	HRESULT hr_ = drv_man.Create(CKmdfDrv_Crt_SSH(), TBase::m_service);
	if (SUCCEEDED(hr_)) {
		TBase::Service().Properties() << TBase::Service();
	}
	else {
		m_error = drv_man.Error();
		shared::Get_AppUI().IAppUI_PutTraceError(m_error);
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

TCmdSvcCreate&  TCmdSvcCreate::operator = (const TCmdSvcCreate& _ref) { (TBase&)*this = (const TBase&)_ref; return *this; }

/////////////////////////////////////////////////////////////////////////////

TCmdSvcDelete:: CSvcCmd_Delete (void) : TBase() { TBase::m_error >> __MODULE__; }
TCmdSvcDelete:: CSvcCmd_Delete (const TCmdSvcDelete& _ref) : TCmdSvcDelete() { *this = _ref; }
TCmdSvcDelete::~CSvcCmd_Delete (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT  TCmdSvcDelete::Execute (void) {
	m_error << __MODULE__ << S_OK;

	CServiceMan drv_man;
	HRESULT hr_ = drv_man.Delete(CKmdfDrv_Crt_SSH());
	if (SUCCEEDED(hr_)) {
		TBase::Service().Properties().Property(TDrvSvcPropSet::e_state, _T("#DELETED"));
	}
	else {
		TBase::m_error = drv_man.Error();
		shared::Get_AppUI().IAppUI_PutTraceError(m_error);
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

TCmdSvcDelete&  TCmdSvcDelete::operator = (const TCmdSvcDelete& _ref) { (TBase&)*this = (const TBase&)_ref; return *this; }

/////////////////////////////////////////////////////////////////////////////

TCmdSvcStart:: CSvcCmd_Start (void) : TBase() { TBase::m_error >> __MODULE__; }
TCmdSvcStart:: CSvcCmd_Start (const TCmdSvcStart& _ref) : TCmdSvcStart() { *this = _ref; }
TCmdSvcStart::~CSvcCmd_Start (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT TCmdSvcStart::Execute(void) {
	m_error << __MODULE__ << S_OK;

	TDrvLocatePers loc_pers;
	HRESULT hr_ =  loc_pers.Load();
	if (FAILED(hr_))
		return (m_error = loc_pers.Error());

	CKmdfDrv_Crt_SSH crt_ssh; crt_ssh << loc_pers;

	CServiceMan drv_man;
	hr_ = drv_man.Start(crt_ssh);
	if (SUCCEEDED(hr_)) {
		TBase::Service().Properties().Property(TDrvSvcPropSet::e_state, _T("#STARTED"));
	}
	else {
		TBase::m_error = drv_man.Error();
		shared::Get_AppUI().IAppUI_PutTraceError(m_error);
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

TCmdSvcStart&  TCmdSvcStart::operator = (const TCmdSvcStart& _ref) { (TBase&)*this = (const TBase&)_ref; return *this; }

/////////////////////////////////////////////////////////////////////////////

TCmdSvcState:: CSvcCmd_State (void) : TBase() { TBase::m_error >> __MODULE__; }
TCmdSvcState:: CSvcCmd_State (const TCmdSvcState& _ref) : TCmdSvcState() { *this = _ref; }
TCmdSvcState::~CSvcCmd_State (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT TCmdSvcState::Execute(void) {
	m_error << __MODULE__ << S_OK;

	CServiceMan drv_man;

	TDrvLocatePers loc_pers;
	HRESULT hr_ =  loc_pers.Load();
	if (FAILED(hr_))
		return (m_error = loc_pers.Error());

	CKmdfDrv_Crt_SSH crt_ssh; crt_ssh << loc_pers;

	hr_ = drv_man.Status(crt_ssh, TBase::Service());
	if (SUCCEEDED(hr_)) {
		TBase::Service().Properties() << TBase::Service();
	}
	else {
		TBase::Service().Properties() << crt_ssh;
		TBase::m_error = drv_man.Error();
		shared::Get_AppUI().IAppUI_PutTraceError(m_error);
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

TCmdSvcState&  TCmdSvcState::operator = (const TCmdSvcState& _ref) { (TBase&)*this = (const TBase&)_ref; return *this; }

/////////////////////////////////////////////////////////////////////////////

TCmdSvcStop:: CSvcCmd_Stop (void) : TBase() { TBase::m_error >> __MODULE__; }
TCmdSvcStop:: CSvcCmd_Stop (const TCmdSvcStop& _ref) : TCmdSvcStop() { *this = _ref; }
TCmdSvcStop::~CSvcCmd_Stop (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT TCmdSvcStop::Execute(void) {
	m_error << __MODULE__ << S_OK;

	CServiceMan drv_man;
	HRESULT hr_ = drv_man.Stop(CKmdfDrv_Crt_SSH());
	if (SUCCEEDED(hr_)) {
		TBase::Service().Properties().Property(TDrvSvcPropSet::e_state, _T("#STOPPED"));
	}
	else {
		TBase::m_error = drv_man.Error();
		shared::Get_AppUI().IAppUI_PutTraceError(m_error);
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

TCmdSvcStop& TCmdSvcStop::operator = (const TCmdSvcStop& _ref) { (TBase&)*this = (const TBase&)_ref; return *this; }

/////////////////////////////////////////////////////////////////////////////

TSvcLastState:: CTheLastState_Svc (void) : m_current(e_state::e_undefined) { m_error << __MODULE__ << S_OK >> __MODULE__; }
TSvcLastState:: CTheLastState_Svc (const TSvcLastState& _ref) : TSvcLastState() { *this = _ref; }
TSvcLastState::~CTheLastState_Svc (void) {}

/////////////////////////////////////////////////////////////////////////////

CStringW      TSvcLastState::Desc (void) const {

	static LPCWSTR lp_sz_pat = _T("%s (WinAPI error=%d)");

	CStringW cs_formatted;
	if (577 == this->m_error.Code()) {
		shared::common::CStdString std_str(this->m_error.Desc());
		shared::common::TStdStrVec std_phrases = std_str.Split(_T("."), true);
		if (std_phrases.empty() == false) {
			if (std_phrases.size() > 0) { cs_formatted.Format(lp_sz_pat, std_phrases[0].GetString(), this->m_error.Code()); }
			if (std_phrases.size() > 1) { cs_formatted += _T("\n"); cs_formatted += std_phrases[1]; }
			if (std_phrases.size() > 2) { cs_formatted += std_phrases[2]; }
		}
	}
	else if (ERROR_FILE_NOT_FOUND == this->m_error.Code()) {
		cs_formatted.Format(lp_sz_pat, this->m_error.Desc(), this->m_error.Code());
		cs_formatted+= _T("\nService cannot be neither created nor started.");
	}
	else {
		cs_formatted.Format(lp_sz_pat, this->m_error.Desc(), this->m_error.Code());
	}

	if (this->m_current == e_state::e_to_install) {
		cs_formatted += _T("\nPlease specify driver location and press [Create] button in order to continue.");
	}
	else if (this->m_current == e_state::e_to_start) {
		cs_formatted  = _T("The service is stopped;\n Press [Start] button in order to proceed with monitoring HTTP connections;"); 
	}
	else {
	}

	return cs_formatted;
}
TErrorRef     TSvcLastState::Error(void) const { return m_error; }
CSvcError&    TSvcLastState::Error(void)       { return m_error; }
TSvcStateEnum TSvcLastState::State(void) const { return m_current; }
bool          TSvcLastState::State(const e_state _state) { const bool b_changed = (m_current != _state); m_current = _state; return b_changed; }

bool          TSvcLastState::State (const TCmdSvcCreate& _cmd) {
	_cmd;
	this->Error() = _cmd.Error();

	e_state new_state = this->m_current;

	if (this->Error().Is() == false) {
		new_state = e_state::e_to_start;
	}
	else {
		new_state = e_state::e_to_install;
	}
	return this->State(new_state);
}

bool          TSvcLastState::State(const TCmdSvcStart& _cmd) {
	_cmd;
	this->Error() = _cmd.Error();

	e_state new_state = this->m_current;

	if (1060 == this->Error().Code()) {
		new_state = e_state::e_to_install;
	}
	else {
		new_state = e_state::e_undefined ;
	}
	return this->State(new_state);
}

bool          TSvcLastState::State(const TCmdSvcState& _cmd) {
	_cmd;
	this->Error() = _cmd.Error();

	e_state new_state = this->m_current;

	if (1060 == this->Error().Code()) {
		new_state = e_state::e_to_install;
	}
	else if (_cmd.Service().State().Pending()) { new_state = e_state::e_to_wait ; }
	else if (_cmd.Service().State().Running()) { new_state = e_state::e_to_stop ; }
	else if (_cmd.Service().State().Stopped()) { new_state = e_state::e_to_start; }
	else {
	//	new_state = e_state::e_undefined; // does not change the state in case of error;
	}
	return this->State(new_state);
}

/////////////////////////////////////////////////////////////////////////////

TSvcLastState& TSvcLastState::operator = (const TSvcLastState& _ref) { *this << _ref.Error() << _ref.State(); return *this; }
TSvcLastState& TSvcLastState::operator <<(TErrorRef _error) { this->Error() = _error; return *this; }
TSvcLastState& TSvcLastState::operator <<(const TCmdSvcCreate& _cmd) { this->State(_cmd); return *this; }
TSvcLastState& TSvcLastState::operator <<(const TCmdSvcStart& _cmd) { this->State(_cmd); return *this; }
TSvcLastState& TSvcLastState::operator <<(const TCmdSvcState& _cmd) { this->State(_cmd); return *this; }
TSvcLastState& TSvcLastState::operator <<(const TSvcStateEnum _state) { this->State(_state); return *this; }