#ifndef _SFXSTRIPLAY_H_4B393BB4_36DD_4D7C_B258_EA4DD332BA87_INCLUDED
#define _SFXSTRIPLAY_H_4B393BB4_36DD_4D7C_B258_EA4DD332BA87_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Aug-2020 at 11:36:16p, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack shared status bar control layout interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Sfx Strip project on 6-Sep-2020 at 3:22:55p, UTC+7, Novosibirsk, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.base.lay.h"

namespace ST_Ctrls {
	class CStripBar;
namespace layout {

	using shared::sys_core::CError ;

	using ex_ui::controls::CSides  ;
	using ex_ui::controls::TSide   ;
	using ex_ui::controls::CMargins;
	using ex_ui::controls::layout::CImage;

	class CStrip {
	protected:
		mutable
		CError       m_error  ;
		CStripBar&   m_control;   // the control reference for accessing item rectangles;
		CMargins     m_margins;
		DWORD        m_height ;
#if (0)
		CImage       m_image  ;   // is not used in this version; items' layers came into the game;
#endif
	public:
		 CStrip (CStripBar&) ;
		~CStrip (void);

	public:
		TErrorRef    Error  (void) const;
		DWORD        Height (void) const;
		DWORD&       Height (void)      ;
#if (0)
		const
		CImage&      Image  (void) const;
		CImage&      Image  (void)      ;
#endif;
		const bool   IsValid(void) const;                     // returns true if height > 0;
		const
		CMargins&    Margins(void) const;
		CMargins&    Margins(void)      ;
		HRESULT      Update (void)      ;
		HRESULT      Update (const RECT& _rc_area);           // updates status bar control window position into an area provided;
	public:
		CStrip&      operator<<(const RECT& _rc_area);        // updates status bar control window position in accordance with area  ;
		const RECT   operator =(const RECT& _rc_area) const;  // returns calculated rectangle of status bar control for area provided;
	};

}}

typedef ST_Ctrls::layout::CStrip  TStripLay ;

#endif/*_SFXSTRIPLAY_H_4B393BB4_36DD_4D7C_B258_EA4DD332BA87_INCLUDED*/