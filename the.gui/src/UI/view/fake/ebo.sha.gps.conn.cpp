/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2021 at 8:15:08.186 pm, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool fake GPS driver connection sub-view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.gps.conn.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;


/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CFake_Conn_Layout {
	public:
		typedef TCtrlDefLayout::_pos _pos;

	private:
		TFakeConnect& m_view;
		RECT          m_rect;

	public:
		 CFake_Conn_Layout (TFakeConnect& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CFake_Conn_Layout (void) {}

	public:
		RECT   GetEmpty(void) { RECT rc_empty = m_rect; return rc_empty; }
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CFake_Connect::CViewWnd:: CViewWnd(CFake_Connect& _view) : TViewWnd(_view), m_view(_view) {}
CFake_Connect::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CFake_Connect::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	CFake_Conn_Layout layout_(m_view);

	HRESULT hr_ = m_empty.Create(*this, layout_.GetEmpty(), true); hr_;
	m_empty.DrawBorder(false);

	return 0;
}

LRESULT   CFake_Connect::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_empty.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CFake_Connect::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CFake_Conn_Layout layout_(m_view);
	m_empty.Update(layout_.GetEmpty());

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CFake_Connect:: CFake_Connect (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CFake_Connect::~CFake_Connect (void) {}