#ifndef _DRVSETUP_H_1C8A1B14_8850_46EB_996E_5E4F566F7564_INCLUDED
#define _DRVSETUP_H_1C8A1B14_8850_46EB_996E_5E4F566F7564_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Sep-2020 at 6:35:14p, UTC+7, Novosibirsk, Thursday;
	This is Ebo Pack generic driver installation interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 12-Jan-2021 at 7:43:51.490 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "drv.setup.defs.h"
#include "drv.setup.iface.h"
#include "drv.setup.instance.h"

#include <hidsdi.h>
#pragma comment(lib, "Hid.lib")

namespace shared { namespace driver { namespace setup {

	using shared::sys_core::CError;

	class CSetup_Base {
	protected:
		CError m_error;

	protected:
		 CSetup_Base (void);
		 CSetup_Base (const CSetup_Base&);
		~CSetup_Base (void);

	public:
		TErrorRef       Error  (void) const;

	public:
		CSetup_Base& operator = (const CSetup_Base&);
	};

	class CSetup_Ifaces : public CSetup_Base { typedef CSetup_Base TBase;
	protected:
		TDevIfaceDetais   m_details;

	public:
		 CSetup_Ifaces (void);
		 CSetup_Ifaces (const CSetup_Ifaces&);
		~CSetup_Ifaces (void);

	public:
		const
		TDevIfaceDetais& Details(void) const;
		HRESULT          Get    (const GUID& _dev_iface_cls); // gets all devices which support interface provided;
		const bool       Is     (void) const;                 // returns true if device setup information details is not empty;
		HRESULT          Reset  (void);

	public:
		CSetup_Ifaces&   operator = (const CSetup_Ifaces&);   // original object that is being copied is erased by this operator for proper memory deallocation;
	};

	class CSetup_Present : public CSetup_Base { typedef CSetup_Base TBase;
	protected:
		TDevInstances    m_instances;
	public:
		 CSetup_Present (void);
		 CSetup_Present (const CSetup_Present&);
		~CSetup_Present (void);

	public:
		HRESULT          Get (const GUID& _dev_iface_cls, const DWORD _flags, LPCWSTR _lp_sz_flt); // gets devices that support device class provided and run at request time;
		const
		TDevInstances&   Instances(void) const;
		const bool       Is  (void) const;

	public:
		CSetup_Present&  operator = (const CSetup_Present&);
	};

}}}

#endif/*_DRVSETUP_H_1C8A1B14_8850_46EB_996E_5E4F566F7564_INCLUDED*/