/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Jan-2021 at 1:05:23.405 pm, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha Optima Tool app main form menu strip bar wrapper interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.gui.fmt.strip.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::format;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace format { namespace _impl {

	class CStripBar_Res {
	public:
		enum _res : WORD {
			e_str_drk_0_n = IDR_EBO_SHA_STR_DRK_0_n, e_str_drk_0_a = IDR_EBO_SHA_STR_DRK_0_g, e_str_drk_0_h = IDR_EBO_SHA_STR_DRK_0_a,
			e_str_drk_1_n = IDR_EBO_SHA_STR_DRK_1_n, e_str_drk_1_a = IDR_EBO_SHA_STR_DRK_1_g, e_str_drk_1_h = IDR_EBO_SHA_STR_DRK_1_a,
			e_str_drk_2_n = IDR_EBO_SHA_STR_DRK_2_n, e_str_drk_2_a = IDR_EBO_SHA_STR_DRK_2_g, e_str_drk_2_h = IDR_EBO_SHA_STR_DRK_2_a,
			e_str_drk_3_n = IDR_EBO_SHA_STR_DRK_3_n, e_str_drk_3_a = IDR_EBO_SHA_STR_DRK_3_a, e_str_drk_3_h = IDR_EBO_SHA_STR_DRK_3_a,
			e_str_drk_4_n = IDR_EBO_SHA_STR_DRK_4_n, e_str_drk_4_a = IDR_EBO_SHA_STR_DRK_4_g, e_str_drk_4_h = IDR_EBO_SHA_STR_DRK_4_a,
			e_str_drk_5_n = IDR_EBO_SHA_STR_DRK_5_n, e_str_drk_5_a = IDR_EBO_SHA_STR_DRK_5_g, e_str_drk_5_h = IDR_EBO_SHA_STR_DRK_5_a,
			e_str_drk_6_n = IDR_EBO_SHA_STR_DRK_6_n, e_str_drk_6_a = IDR_EBO_SHA_STR_DRK_6_g, e_str_drk_6_h = IDR_EBO_SHA_STR_DRK_6_a,
		};
	}; typedef CStripBar_Res This_Res;

}}}}}
using namespace ebo::sha::gui::format::_impl;
/////////////////////////////////////////////////////////////////////////////

TStripBarFmt:: CStripBar_Format (void) {}
TStripBarFmt::~CStripBar_Format (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   TStripBarFmt::ApplyTo (ST_Ctrls::CStripBar& _strip) const { HRESULT hr_ = S_OK;
	_strip;
	TStripFmt& str_fmt = _strip.Format(); str_fmt = ST_Ctrls::format::CSymantec_NA_Str();
//	str_fmt.Bkgnd().Image() = IDR_EBO_SHA_STR_DRK_B;
	str_fmt.Items().Width() = 85;
	str_fmt.Bkgnd().Solid() << shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_back);

	static UINT n_width[] = {
		85, 90, 65, 75, 130, 75, 100     // Home|Block|GPS|Media|Settings|Help|Logon << expected order;
	};
	static UINT n_images_a[] = { // active state of item;
		This_Res::e_str_drk_0_a, This_Res::e_str_drk_5_a, This_Res::e_str_drk_4_a, This_Res::e_str_drk_6_a, This_Res::e_str_drk_1_a,
		This_Res::e_str_drk_2_a, This_Res::e_str_drk_3_a
	};
	static UINT n_images_n[] = { // normal state of item;
		This_Res::e_str_drk_0_n, This_Res::e_str_drk_5_n, This_Res::e_str_drk_4_n, This_Res::e_str_drk_6_n, This_Res::e_str_drk_1_n,
		This_Res::e_str_drk_2_n, This_Res::e_str_drk_3_n
	};
	static UINT n_images_h[] = { // hovered state of item;
		This_Res::e_str_drk_0_h, This_Res::e_str_drk_5_h, This_Res::e_str_drk_4_h, This_Res::e_str_drk_6_h, This_Res::e_str_drk_1_h,
		This_Res::e_str_drk_2_h, This_Res::e_str_drk_3_h
	};

	for (UINT i_ = 0; i_ < _strip.Items().Count() &&
	          i_ < _countof(n_width) && i_ < _countof(n_images_n) && i_ < _countof(n_images_a) && i_ < _countof(n_images_h); i_ ++) {
		_strip.Items ().Item (i_).Layers().Width(n_width[i_]);
		_strip.Items ().Item (i_).Layers().Images().Cache().Add(CState::eNormal  , n_images_n[i_]);
		_strip.Items ().Item (i_).Layers().Images().Cache().Add(CState::eSelected, n_images_a[i_]);
		_strip.Items ().Item (i_).Layers().Images().Cache().Add(CState::eHovered , n_images_h[i_]);
	}

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

const TStripBarFmt&   TStripBarFmt::operator >> (ST_Ctrls::CStripBar& _strip) const { this->ApplyTo(_strip); return *this; }