/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Jan-2021 at 5:30:22.034 pm, UTC+7, Novosibirsk, Thursday;
	This is generic driver executable file locator GUI extension interface implementation file;
*/
#include "StdAfx.h"
#include "driver.dev.loc.gui.h"

using namespace shared::driver::client;

/////////////////////////////////////////////////////////////////////////////

TDrvLocateExt:: CDrvLocator_Ext (void) : TBase() { TBase::m_error << __MODULE__ << S_OK >> __MODULE__; }
TDrvLocateExt:: CDrvLocator_Ext (const TDrvLocateExt& _ref) : TDrvLocateExt() { *this = _ref; }
TDrvLocateExt::~CDrvLocator_Ext (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   TDrvLocateExt::Select (const HWND _h_parent) {
	_h_parent;
	TBase::m_error << __MODULE__ << S_OK; HRESULT hr_ = m_error; hr_;
#if (0)
	WCHAR szFilter[_MAX_PATH] = _T("Driver executable file (*.sys)\0*.sys");
	const DWORD dwStyle = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT | OFN_EXPLORER;

	WTL::CFileDialog dlg_(
		TRUE, _T("sys"), NULL, dwStyle, szFilter, _h_parent
	);
#else
	// https://docs.microsoft.com/en-us/windows/win32/learnwin32/example--the-open-dialog-box
	// https://docs.microsoft.com/en-us/windows/win32/api/shobjidl_core/nn-shobjidl_core-ifileopendialog
	// https://docs.microsoft.com/en-us/windows/win32/api/shobjidl_core/nn-shobjidl_core-ifiledialog
	CComPtr<IFileOpenDialog> p_dialog;
	m_error = p_dialog.CoCreateInstance(
			CLSID_FileOpenDialog, NULL, CLSCTX_ALL
	);
	if (m_error.Is() == false) {
		COMDLG_FILTERSPEC flt_spec[] = {
			{ _T("Driver executable file (*.sys)"), _T("*.sys") }
		};
		hr_ = p_dialog->SetFileTypes(_countof(flt_spec), flt_spec);
		hr_ = p_dialog->SetTitle(_T("Specifying Driver Path"));
		hr_ = p_dialog->Show(_h_parent);
		if (SUCCEEDED(hr_)) {
			CComPtr<IShellItem> p_item;
			hr_ = p_dialog->GetResult(&p_item);
			if (SUCCEEDED(hr_)) {
				PWSTR lp_path = NULL;

				hr_ = p_item->GetDisplayName(SIGDN_FILESYSPATH, &lp_path);
				if (SUCCEEDED(hr_)) {
					TBase::Path(lp_path);
					if (TBase::m_error.Is() == false){
						TBase::Save();
					}
					CStringW cs_out;
					cs_out += _T("The driver path chosen: ");
					cs_out += lp_path;
				//	shared::Get_AppUI().IAppUI_PutTraceInfo(cs_out);
				}
				if (lp_path) {
					::CoTaskMemFree(lp_path); lp_path = NULL;
				}
			}
		}
		else {
			shared::Get_AppUI().IAppUI_PutStatusInfo((m_error << hr_).Desc());
		//	shared::Get_AppUI().IAppUI_PutTraceWarn (_T("Selecting drive path is canceled;"));
		}
	}
#endif

#if (0)
	if (IDOK != dlg_.DoModal())
		TBase::m_error = S_FALSE;
	else {
		TBase::Path(dlg_.m_ofn.lpstrFile);
		if (TBase::m_error.Is() == false){
			TBase::Save();
		}
	}
#endif
	return TBase::m_error;
}

/////////////////////////////////////////////////////////////////////////////

TDrvLocateExt&  TDrvLocateExt::operator = (const TDrvLocateExt& _ref) { (TBase&)*this = (const TBase&)_ref; return *this; }