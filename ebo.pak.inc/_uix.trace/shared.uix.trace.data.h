#ifndef _SHAREDUIXTRACEDATA_H_7B1EBEBA_0F84_4AC6_8622_C4A43E46E2E0_INCLUDED
#define _SHAREDUIXTRACEDATA_H_7B1EBEBA_0F84_4AC6_8622_C4A43E46E2E0_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Apr-2020 at 11:29:55p, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack UIX trace view data interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.syn.obj.h"
#include "shared.gen.sys.date.h"
#include "shared.run.event.h"
#include "shared.log.file.h"

namespace ex_ui { namespace trace {

	using shared::common::CDateTime;

	using shared::sys_core::CSyncObject; typedef TComCritSection TDataGuard;
	using shared::sys_core::CError;

	using shared::log::CFileLogger;

	using shared::runnable::IGenericEventNotify;

	class CEntryType {
	public:
		enum _type {
			e_info = 0x0, e_error, e_warn, e_dont_care
		};
	protected:
		_type   m_value;
	public:
		 CEntryType (const _type = _type::e_dont_care) ;
		 CEntryType (const CEntryType&);
		~CEntryType (void) ;

	public:
		virtual CAtlString ToString(void) const;

	public:
		const bool   Is_err (void) const;
		const bool   Is_info(void) const;
		const bool   Is_warn(void) const;
		const
		_type& Value   (void) const;
		_type& Value   (void)      ;

	public:
		CEntryType&
		operator     = (const _type);
		operator _type (void) const ;
		CEntryType&
		operator     = (const CEntryType&);
	};

	typedef CEntryType::_type TEntryType;

	class CEntry {
	protected:
		CEntryType  m_type     ;
		CDateTime   m_timestamp;
		CAtlString  m_text     ;

	public:
		 CEntry (void) ;
		 CEntry (const CEntry&);
		~CEntry (void) ;

	public:
		const
		CAtlString& Text(void) const;
		CAtlString& Text(void)      ;
		const
		CDateTime&  Timestamp(void) const;
		CDateTime&  Timestamp(void)      ;
		const
		CEntryType& Type(void) const;
		CEntryType& Type(void)      ;

	public:
		CEntry& operator << (const CAtlString& );
		CEntry& operator << (const CDateTime & );
		CEntry& operator << (const CEntryType& );
		CEntry& operator << (const TEntryType& );
		CEntry& operator << (LPCTSTR _lp_sz_txt);
		CEntry& operator  = (const CEntry&)     ;
	};
#if (1)
	typedef ::std::vector<CEntry> TEntries;
#else
	typedef ::std::map<LONG, CEntry> TEntries;
#endif
	typedef const CEntry&         TEntryRef  ;
	typedef const TEntries&       TEntriesRef;

	// for this version entries are displayed in descending order: the last entry on the top of a content;
	class CEntries {
	protected:
		IGenericEventNotify&
		           m_evt_snk;   // event sync to UI for notifying about data changes;
		mutable
		TDataGuard m_guard  ;   // entries are protected in multi-thread environment;
		DWORD      m_limit  ;   // 0 - default: no limit of entries;
		TEntries   m_entries;   // entries being displayed (in visible range);
		CError     m_error  ;
		bool       m_use_log;
		CFileLogger m_logger;

	public:
		 CEntries (IGenericEventNotify&);
		~CEntries (void);

	public:
		HRESULT    Append(const CEntry&);
		HRESULT    Clear (void)      ;
		size_t     Count (void) const;
		TEntryRef  Entry (const size_t _ndx) const;    // is not thread safe; unlock must be invoked after calling this method;
		TErrorRef  Error (void) const;
		DWORD      Limit (void) const;
		HRESULT    Limit (const DWORD);                // if zero is provided, no limit is applied;
		HRESULT    Remove(const size_t _ndx);          // removes entry by index; usual, reacjing limit value will control entries count;
		TEntriesRef
		           Raw   (void);                       // calling this method will block entries from changes; Unlock must be invoked after using data;
	public:
		HRESULT    Lock  (void) const;                 // locks the guard for blocking entries vector change;
		HRESULT    UnLock(void) const;                 // unlocks entries vector for change;
	public:
		CEntries&  operator += (const CEntry&)    ;    // appends new entry;
		CEntries&  operator -= (const size_t _ndx);    // removes an entry by index provided;
	public:
		const bool Logged(void) const;                 // not thread safe;
		bool&      Logged(void)      ;                 // not thread safe;
	};
}}
typedef ex_ui::trace::CEntryType::_type TEntryType;

#endif/*_SHAREDUIXTRACEDATA_H_7B1EBEBA_0F84_4AC6_8622_C4A43E46E2E0_INCLUDED*/