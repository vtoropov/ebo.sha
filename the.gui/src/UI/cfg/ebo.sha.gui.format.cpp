/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Dec-2020 at 3:03:01.698 am, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha optima tool generic GUI parts format interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:03:27.582 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "StdAfx.h"
#include "ebo.sha.gui.format.h"

using namespace ebo::sha::gui::format;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace format { namespace _impl {

	class CButton_Default {
	public:
		 CButton_Default (void){}
		~CButton_Default (void){}

	public:
		HRESULT    Do(TButtonFmt& _fmt) const { HRESULT hr_ = S_OK;
			_fmt;
			TBtnStateFmt& sta_fmt = _fmt.State();
			TFmtBase& disable_fmt = sta_fmt.Disable();
			TFmtBase& hovered_fmt = sta_fmt.Hovered();
			TFmtBase& pressed_fmt = sta_fmt.Pressed();

			TFmtBase& normal_fmt = sta_fmt.Normal ();
			TFmtBase& select_fmt = sta_fmt.Default();

			disable_fmt.Font().Family(_T("Verdana")); disable_fmt.Font().Fore() = shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_fore, TThemeState::e_disable);
			hovered_fmt.Font().Family(_T("Verdana")); hovered_fmt.Font().Fore() = shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_fore, TThemeState::e_hovered);
			pressed_fmt.Font().Family(_T("Verdana")); pressed_fmt.Font().Fore() = shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_fore, TThemeState::e_hovered);
			normal_fmt .Font().Family(_T("Verdana")); normal_fmt .Font().Fore() = shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_fore);
			select_fmt .Font().Family(_T("Verdana")); select_fmt .Font().Fore() = shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_fore/*, TThemeState::e_selected*/);

			disable_fmt.Font().Size() = hovered_fmt.Font().Size() = pressed_fmt.Font().Size() = normal_fmt.Font().Size() = select_fmt.Font().Size() = 18;

			disable_fmt.Font().Align().Horz() = hovered_fmt.Font().Align().Horz() = 
			pressed_fmt.Font().Align().Horz() = normal_fmt.Font().Align().Horz() = select_fmt.Font().Align().Horz() = THorzAlign::eCenter;

			hovered_fmt.Bkgnd().Solid()  = ex_ui::draw::CColour(shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_back, TThemeState::e_hovered));
			disable_fmt.Bkgnd().Solid()  = ex_ui::draw::CColour(shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_back));
			pressed_fmt.Bkgnd().Solid()  = ex_ui::draw::CColour(shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_back));
			normal_fmt .Bkgnd().Solid()  = ex_ui::draw::CColour(shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_back));
			select_fmt .Bkgnd().Solid()  = ex_ui::draw::CColour(shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_back));

			const COLORREF clr_borders_n = shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_border, TThemeState::e_default ); CStringW cs_border_n = TDbgPrint().Print(clr_borders_n);
			const COLORREF clr_borders_h = shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_border, TThemeState::e_hovered ); CStringW cs_border_h = TDbgPrint().Print(clr_borders_h);
			const COLORREF clr_borders_s = shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_border, TThemeState::e_selected); CStringW cs_border_s = TDbgPrint().Print(clr_borders_s);

		//	CStringW cs_borders_n = TDbgPrint() << clr_borders_n;

			disable_fmt.Borders() >> clr_borders_n;
			hovered_fmt.Borders() >> clr_borders_h;
			pressed_fmt.Borders() >> clr_borders_h; normal_fmt.Borders() >> clr_borders_n; select_fmt.Borders() >> clr_borders_s;

			return hr_;
		}
	};

	class CPanel_Default  {
	public:
		 CPanel_Default (void) {}
		~CPanel_Default (void) {}

	public:
		HRESULT    Do(TPanelFmt& _fmt) const { HRESULT hr_ = S_OK;
			_fmt;
			_fmt.Borders() >> shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_border);
			_fmt.Bkgnd().Solid() << shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_back);
			return hr_;
		}
	};

	class CSelect_Default {
	public:
		 CSelect_Default (void) {}
		~CSelect_Default (void) {}

	public:
		HRESULT   Do(TSelectorFmt& _fmt) const { HRESULT hr_ = S_OK;

			(TSelectFmt&)_fmt = ST_Ctrls::format::CSymantec_NA_Sel();

			_fmt.Font ().Fore() = ST_Ctrls::format::CSymantec_NA_Sel().Font ().Fore();
			_fmt.Font ().Size() = 23;
			_fmt.Font().Family(_T("Trebuchet MS"));

			_fmt.Borders().Right().Shadow().Create(0, TAlpha::eOpaque);

			_fmt.Items().Active().Font().Align().Vert() = TVertAlign::eMiddle;
			_fmt.Items().Active().Font().Size () = 23;
			_fmt.Items().Active().Font().Family(_T("Trebuchet MS"));

			_fmt.Items().Active().Bkgnd().Solid().Alpha(TAlpha::eTransparent30);

			_fmt.Overlay().Size().cx = 220;
			_fmt.Overlay().Size().cy = 220;

			return hr_;
		}
	};

	class CTabs_Default {
	public:
		 CTabs_Default (void) {}
		~CTabs_Default (void) {}

	public:
		HRESULT   Do(TTabsFmt& _fmt) const { HRESULT hr_ = S_OK;

			(TTabbedFmt&)_fmt = ST_Ctrls::format::CSymantec_NA_Tab();
			// TODO: Tabbed control bug: if gradient is not specified, solid color must be used;
			_fmt.Bkgnd().Gradient().c0() << shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_back);;
			_fmt.Bkgnd().Gradient().c1() << shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_back);;
			_fmt.Bkgnd().Solid() << shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_back);

			_fmt.Borders().Thickness(1);
			_fmt.Borders().Color(shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_border));
			// TODO: Tabbed control bug: if gradient is not specified, solid color must be used;
			_fmt.Tabs().Active().Bkgnd().Solid()         << shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_back);
			_fmt.Tabs().Active().Bkgnd().Gradient().c0() << shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_back);
			_fmt.Tabs().Active().Bkgnd().Gradient().c1() << shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_back);

			_fmt.Tabs().Active().Font ().Fore() = shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_fore);
			_fmt.Font().Size() = 9;

			_fmt.Tabs().Active().Borders().Thickness(1);
			_fmt.Tabs().Active().Borders().Color(shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_border));

			return hr_;
		}
	};

}}}}}
using namespace ebo::sha::gui::format::_impl;
/////////////////////////////////////////////////////////////////////////////

TControlFmt:: CControl (void) { CButton_Default().Do(m_button); }
TControlFmt:: CControl (const TControlFmt& _ref) : TControlFmt() { *this = _ref; }
TControlFmt::~CControl (void) {}

/////////////////////////////////////////////////////////////////////////////
const
TButtonFmt&   TControlFmt::Button  (void) const { return m_button ; }
TButtonFmt&   TControlFmt::Button  (void)       { return m_button ; }
const
TCaptionFmt&  TControlFmt::Caption (void) const { return m_caption; }
TCaptionFmt&  TControlFmt::Caption (void)       { return m_caption; }
const
TEditFmt&  TControlFmt::Edit  (void) const { return m_edit ; }
TEditFmt&  TControlFmt::Edit  (void)       { return m_edit ; }
const
TLabelFmt& TControlFmt::Label (void) const { return m_label; }
TLabelFmt& TControlFmt::Label (void)       { return m_label; }

/////////////////////////////////////////////////////////////////////////////

TControlFmt&  TControlFmt::operator = (const TControlFmt& _ref) { *this << _ref.Caption() << _ref.Label() << _ref.Button(); return *this; }
TControlFmt&  TControlFmt::operator <<(const TButtonFmt&  _btn) {  this->Button()  = _btn;      return *this; }
TControlFmt&  TControlFmt::operator <<(const TCaptionFmt& _cap) {  this->Caption() = _cap;      return *this; }
TControlFmt&  TControlFmt::operator <<(const TEditFmt& _edt_ra) {  this->Edit ()   = _edt_ra;   return *this; }
TControlFmt&  TControlFmt::operator <<(const TLabelFmt& _label) {  this->Label()   = _label ;   return *this; }

/////////////////////////////////////////////////////////////////////////////

const TControlFmt&  TControlFmt::operator >>(TButtonFmt&  _btn) const { _btn = this->Button() ; return *this; }
const TControlFmt&  TControlFmt::operator >>(TCaptionFmt& _cap) const { _cap = this->Caption(); return *this; }
const TControlFmt&  TControlFmt::operator >>(TEditFmt& _edt_ra) const { _edt_ra = this->Edit(); return *this; }
const TControlFmt&  TControlFmt::operator >>(TLabelFmt& _label) const { _label = this->Label(); return *this; }

/////////////////////////////////////////////////////////////////////////////

TPanelFmt:: CPanel (void) { CPanel_Default().Do(*this); }
TPanelFmt:: CPanel (const TPanelFmt& _ref) : TPanelFmt() { *this = _ref; }
TPanelFmt::~CPanel (void) {}

/////////////////////////////////////////////////////////////////////////////
const
TFmtBkg&      TPanelFmt::Bkgnd  (void) const { return m_bkg; }
TFmtBkg&      TPanelFmt::Bkgnd  (void)       { return m_bkg; }
const
TBorderFmt&   TPanelFmt::Borders(void) const { return m_borders; }
TBorderFmt&   TPanelFmt::Borders(void)       { return m_borders; }

/////////////////////////////////////////////////////////////////////////////

TPanelFmt&  TPanelFmt::operator = (const TPanelFmt& _ref) { *this << _ref.Borders(); return *this; }
TPanelFmt&  TPanelFmt::operator <<(const TBorderFmt& _fmt) { this->Borders() = _fmt; return *this; }

/////////////////////////////////////////////////////////////////////////////

TSelectorFmt:: CSelect (void) : TBase() { CSelect_Default().Do(*this); }
TSelectorFmt:: CSelect (const TSelectorFmt& _ref) : TSelectorFmt() { *this = _ref; }
TSelectorFmt::~CSelect (void) {}

/////////////////////////////////////////////////////////////////////////////
const
TSelectorFmt&  TSelectorFmt::operator >>(TSelectFmt& _fmt) const { _fmt = (const TBase&)*this; return *this; }
TSelectorFmt&  TSelectorFmt::operator = (const TSelectorFmt& _ref) { (TBase&)*this = (const TBase&)_ref; return *this; }

/////////////////////////////////////////////////////////////////////////////

TTabsFmt:: CTabbed (void) : TBase() { CTabs_Default().Do(*this); }
TTabsFmt:: CTabbed (const TTabsFmt& _ref) : TTabsFmt() { *this = _ref; }
TTabsFmt::~CTabbed (void) {}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
const
TTabsFmt&  TTabsFmt::operator >>(TTabbedFmt& _fmt) const { _fmt = (const TBase&)*this; return *this; }
TTabsFmt&  TTabsFmt::operator = (const TTabsFmt& _ref ) { (TBase&)*this = (const TBase&)_ref; return *this; }

/////////////////////////////////////////////////////////////////////////////

TFormat:: CFormat (void) {}
TFormat:: CFormat (const TFormat& _ref) : TFormat() { *this = _ref; }
TFormat::~CFormat (void) {}

/////////////////////////////////////////////////////////////////////////////
const
TControlFmt&  TFormat::Control(void) const { return m_ctrls; }
TControlFmt&  TFormat::Control(void)       { return m_ctrls; }
const
TPanelFmt&    TFormat::Panel  (void) const { return m_panel; }
TPanelFmt&    TFormat::Panel  (void)       { return m_panel; }
const
TSelectorFmt& TFormat::Select (void) const { return m_select; }
TSelectorFmt& TFormat::Select (void)       { return m_select; }
const
TTabsFmt&     TFormat::Tabbed (void) const { return m_tabbed; }
TTabsFmt&     TFormat::Tabbed (void)       { return m_tabbed; }

/////////////////////////////////////////////////////////////////////////////

TFormat&  TFormat::operator = (const TFormat& _ref) { _ref; return *this; }