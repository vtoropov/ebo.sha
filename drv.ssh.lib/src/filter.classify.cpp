/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Aug-2020 at 9:13:30p, UTC+7, Novosibirsk, Monday;
	This is SSH Tunnel WFP filter device classifying functions interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 8:09:37.487 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "filter.classify.h"

using namespace ssh::driver::classify;

/////////////////////////////////////////////////////////////////////////////

NTSTATUS CConvert::v4::To_accept  (const FWPS_INCOMING_VALUES0* _p_from, ULONG& protocol, ADDR_THUNK& _remote, ADDR_THUNK& _local ) {
	__unused(_p_from);
	__unused(protocol);
	__unused(_remote);
	__unused(_local);
	if (NULL == _p_from)
		return (STATUS_INVALID_PARAMETER);
//	PAGED_CODE();
	//if (PASSIVE_LEVEL == KeGetCurrentIrql()) {
	//	DbgPrint( "__ssh.tnl.drv: [INFO] CConvert::v4::To_accept()::fn_begin();\n" );
	//}
#if (0)
	_local._addr   = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_RECV_ACCEPT_V4_IP_LOCAL_ADDRESS ].value.uint32;
	_local._port   = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_RECV_ACCEPT_V4_IP_LOCAL_PORT    ].value.uint16;
	_local._addr6  = NULL;

	_remote._addr  = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_RECV_ACCEPT_V4_IP_REMOTE_ADDRESS].value.uint32;
	_remote._port  = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_RECV_ACCEPT_V4_IP_REMOTE_PORT   ].value.uint16;
	_remote._addr6 = NULL;

	protocol = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_RECV_ACCEPT_V4_IP_PROTOCOL].value.uint16;
#endif
//	DbgPrint( "__ssh.tnl.drv: [INFO] CConvert::v4::To_accept()::fn_end();\n" );
	return S_OK;
}

NTSTATUS CConvert::v6::To_accept  (const FWPS_INCOMING_VALUES0* _p_from, ULONG& protocol, ADDR_THUNK& _remote, ADDR_THUNK& _local ) {
	__unused(_p_from);
	__unused(protocol);
	__unused(_remote);
	__unused(_local);
	if (NULL == _p_from)
		return (STATUS_INVALID_PARAMETER);
//	DbgPrint( "__ssh.tnl.drv: [INFO] CConvert::v6::To_accept()::fn_begin();\n" );
#if (0)
	_local._port   = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_RECV_ACCEPT_V6_IP_LOCAL_PORT    ].value.uint16;
	_local._addr6  = (IN6_ADDR *) // TODO: possibly it would be better to make a copy;
	                 _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_RECV_ACCEPT_V6_IP_LOCAL_ADDRESS ].value.byteArray16->byteArray16;
	_local._addr   = CAddress::Fake(_local._addr6);

	_remote._port  = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_RECV_ACCEPT_V6_IP_REMOTE_PORT   ].value.uint16;
	_remote._addr6 = (IN6_ADDR *) // TODO: possibly it would be better to make a copy;
	                 _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_RECV_ACCEPT_V6_IP_REMOTE_ADDRESS].value.byteArray16->byteArray16;
	_remote._addr  = CAddress::Fake(_remote._addr6);

	protocol = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_RECV_ACCEPT_V6_IP_PROTOCOL].value.uint16;
#endif
//	DbgPrint( "__ssh.tnl.drv: [INFO] CConvert::v6::To_accept()::fn_end();\n" );
	return S_OK;
}

NTSTATUS CConvert::v4::To_connect (const FWPS_INCOMING_VALUES0* _p_from, ULONG& protocol, ADDR_THUNK& _local , ADDR_THUNK& _remote) {
	__unused(_p_from);
	__unused(protocol);
	__unused(_remote);
	__unused(_local);
	if (NULL == _p_from)
		return (STATUS_INVALID_PARAMETER);
//	DbgPrint( "__ssh.tnl.drv: [INFO] CConvert::v4::To_connect()::fn_begin();\n" );
#if (0)
	_local._addr   = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_CONNECT_V4_IP_LOCAL_ADDRESS ].value.uint32;
	_local._port   = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_CONNECT_V4_IP_LOCAL_PORT    ].value.uint16;
	_local._addr6  = NULL;

	_remote._addr  = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_CONNECT_V4_IP_REMOTE_ADDRESS].value.uint32;
	_remote._port  = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_CONNECT_V4_IP_REMOTE_PORT   ].value.uint16;
	_remote._addr6 = NULL;

	protocol = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_CONNECT_V4_IP_PROTOCOL].value.uint16;
#endif
//	DbgPrint( "__ssh.tnl.drv: [INFO] CConvert::v4::To_connect()::fn_end();\n" );
	return S_OK;
}

NTSTATUS CConvert::v6::To_connect (const FWPS_INCOMING_VALUES0* _p_from, ULONG& protocol, ADDR_THUNK& _local , ADDR_THUNK& _remote) {
	__unused(_p_from);
	__unused(protocol);
	__unused(_remote);
	__unused(_local);
	if (NULL == _p_from)
		return (STATUS_INVALID_PARAMETER);
//	DbgPrint( "__ssh.tnl.drv: [INFO] CConvert::v6::To_connect()::fn_begin();\n" );
#if(0)
	_local._port   = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_CONNECT_V6_IP_LOCAL_PORT    ].value.uint16;
	_local._addr6  = (IN6_ADDR *) // TODO: possibly it would be better to make a copy;
	                 _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_CONNECT_V6_IP_LOCAL_ADDRESS ].value.byteArray16->byteArray16;
	_local._addr   = CAddress::Fake(_local._addr6);

	_remote._port  = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_CONNECT_V6_IP_REMOTE_PORT   ].value.uint16;
	_remote._addr6 = (IN6_ADDR *) // TODO: possibly it would be better to make a copy;
	                 _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_CONNECT_V6_IP_REMOTE_ADDRESS].value.byteArray16->byteArray16;
	_remote._addr  = CAddress::Fake(_remote._addr6);

	protocol = _p_from->incomingValue[FWPS_FIELD_ALE_AUTH_CONNECT_V6_IP_PROTOCOL].value.uint16;
#endif
//	DbgPrint( "__ssh.tnl.drv: [INFO] CConvert::v6::To_connect()::fn_end();\n" );
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

ULONG CThunk::v4::Accept ( const ULONG protocol, const ADDR_THUNK& _remote, const ADDR_THUNK& _local ) {
	__unused(protocol);
	__unused(_local  );
	__unused(_remote );
//	DbgPrint( "__ssh.tnl.drv: [INFO] CThunk::v4::Accept()::fn_begin();\n" );
#if (0)
	t_raw_notify message = {0};
	message.action = CThunk::GetAction(protocol, _local, _remote);
	// source is remote IP, destination is local one;
	CAddress::Fill(_remote, _local, &message);

	if (g_dev_ext)
		Notification_Send(&g_dev_ext->queue, &message);

	return message.action;
#endif
//	DbgPrint( "__ssh.tnl.drv: [INFO] CThunk::v4::Accept()::fn_end();\n" );
	return (ULONG)CAction::allow;
}

ULONG CThunk::v6::Accept ( const ULONG protocol, const ADDR_THUNK& _remote, const ADDR_THUNK& _local ) {
	__unused(protocol);
	__unused(_local  );
	__unused(_remote );
//	DbgPrint( "__ssh.tnl.drv: [INFO] CThunk::v6::Accept()::fn_begin();\n" );
#if (0)
	t_raw_notify message = {0};

	if (_remote._addr == 0) // it looks like there is no fake address;
		message.action = (ULONG)CAction::allow;
	else
		message.action = CThunk::GetAction(protocol, _local, _remote);

	// source is remote IP, destination is local one;
	CAddress::Fill(_remote, _local, &message);

	if (g_dev_ext)
		Notification_Send(&g_dev_ext->queue, &message);

	return message.action;
#endif
//	DbgPrint( "__ssh.tnl.drv: [INFO] CThunk::v6::Accept()::fn_end();\n" );
	return (ULONG)CAction::allow;
}

ULONG CThunk::v4::Connect( const ULONG protocol, const ADDR_THUNK& _local , const ADDR_THUNK& _remote) {
	__unused(protocol);
	__unused(_local  );
	__unused(_remote );
//	DbgPrint( "__ssh.tnl.drv: [INFO] CThunk::v4::Connect()::fn_begin();\n" );
#if (0)
	t_raw_notify message = {0};
	message.action = CThunk::GetAction(protocol, _local, _remote);
	// source is local IP, destination is remote one;
	CAddress::Fill(_local, _remote, &message);

	if (g_dev_ext)
		Notification_Send(&g_dev_ext->queue, &message);

	return message.action;
#endif
//	DbgPrint( "__ssh.tnl.drv: [INFO] CThunk::v4::Connect()::fn_end();\n" );
	return (ULONG)CAction::allow;
}

ULONG CThunk::v6::Connect( const ULONG protocol, const ADDR_THUNK& _local , const ADDR_THUNK& _remote) {
	__unused(protocol);
	__unused(_local  );
	__unused(_remote );
//	DbgPrint( "__ssh.tnl.drv: [INFO] CThunk::v6::Connect()::fn_begin();\n" );
#if (0)
	t_raw_notify message = {0};

	if (_remote._addr == 0) // it looks like there is no fake address;
		message.action = (ULONG)CAction::allow;
	else
		message.action = CThunk::GetAction(protocol, _local, _remote);

	// source is local IP, destination is remote one;
	CAddress::Fill(_local, _remote, &message);

	if (g_dev_ext)
		Notification_Send(&g_dev_ext->queue, &message);

	return message.action;
#endif
//	DbgPrint( "__ssh.tnl.drv: [INFO] CThunk::v6::Connect()::fn_end();\n" );
	return (ULONG)CAction::allow;
}

/////////////////////////////////////////////////////////////////////////////

ULONG CThunk::GetAction ( const ULONG protocol, const ADDR_THUNK& _local, const ADDR_THUNK& _remote) {
	__unused(protocol);
	__unused(_local  );
	__unused(_remote );
//	DbgPrint( "__ssh.tnl.drv: [INFO] CThunk::GetAction()::fn_begin();\n" );
#if (0)
	if(protocol == IPPROTO_TCP && (CPorts::CDestination::IsAllowed(_remote._port) || CPorts::CSource::IsAllowed(_local._port))) {
		return (ULONG)CAction::allow;
	}
	else
		return (ULONG)CRanges::Get(_remote._addr);
#endif
//	DbgPrint( "__ssh.tnl.drv: [INFO] CThunk::GetAction()::fn_end();\n" );
	return (ULONG)CAction::allow;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS CAction_Accept::v4::Do (
	const FWPS_INCOMING_VALUES0* _p_in, const FWPS_INCOMING_METADATA_VALUES0* _p_meta, VOID* _p_pkt, const FWPS_FILTER0* _p_flt, UINT64 _u_ctx, FWPS_CLASSIFY_OUT0* _p_out
	) {
	__unused(_p_in);
	__unused(_p_meta);
	__unused(_p_pkt );
	__unused(_u_ctx );
	__unused(_p_flt );
	__unused(_p_out );
	DbgPrint( "__ssh.tnl.drv: [INFO] CAction_Accept::v4::Do()::fn_begin();\n" );
	NTSTATUS nt_status = S_OK;
#if (0)
	if (NULL == g_dev_ext || 0 == g_dev_ext->block || NULL == _p_out)
		return nt_status;

	ADDR_THUNK thk_local ; CAddress::Reset(thk_local );
	ADDR_THUNK thk_remote; CAddress::Reset(thk_remote);

	ULONG protocol = 0;

	nt_status = CConvert::v4::To_accept (_p_in, protocol, thk_remote, thk_local);
	if (FAILED(nt_status)) {
		return(nt_status = S_OK);
	}

	if (0 == TBase::v4::Accept(protocol, thk_remote, thk_local)) {

		// action is block, clear the write flag of the rights member of the FWPS_CLASSIFY_OUT0 structure.
		_p_out->rights    &= ~FWPS_RIGHT_ACTION_WRITE;
		_p_out->actionType =  FWP_ACTION_BLOCK;

		return nt_status;
	}

	// Action is allow, clear the write flag of the rights member of the FWPS_CLASSIFY_OUT0 structure,
	// If the FWPS_FILTER_FLAG_CLEAR_ACTION_RIGHT of the flags member in the FWPS_FILTER0 structure is set.
	if (NULL != _p_flt && (_p_flt->flags & FWPS_FILTER_FLAG_CLEAR_ACTION_RIGHT)) {
		_p_out->rights &= ~FWPS_RIGHT_ACTION_WRITE;
	}
	_p_out->actionType = FWP_ACTION_CONTINUE;
#endif
	DbgPrint( "__ssh.tnl.drv: [INFO] CAction_Accept::v4::Do()::fn_end();\n" );
	return nt_status;
}

NTSTATUS CAction_Accept::v6::Do (
	const FWPS_INCOMING_VALUES0* _p_in, const FWPS_INCOMING_METADATA_VALUES0* _p_meta, VOID* _p_pkt, const FWPS_FILTER0* _p_flt, UINT64 _u_ctx, FWPS_CLASSIFY_OUT0* _p_out
	) {
	__unused(_p_in);
	__unused(_p_meta);
	__unused(_p_pkt );
	__unused(_p_flt );
	__unused(_u_ctx );
	__unused(_p_out );
	DbgPrint( "__ssh.tnl.drv: [INFO] CAction_Accept::v6::Do()::fn_begin();\n" );
	NTSTATUS nt_status = S_OK;
#if (0)
	if (NULL == g_dev_ext || 0 == g_dev_ext->block || NULL == _p_out)
		return nt_status;

	ADDR_THUNK thk_local ; CAddress::Reset(thk_local );
	ADDR_THUNK thk_remote; CAddress::Reset(thk_remote);

	ULONG protocol = 0;

	nt_status = CConvert::v6::To_accept (_p_in, protocol, thk_remote, thk_local);
	if (FAILED(nt_status)) {
		return(nt_status = S_OK);
	}

	if (0 == TBase::v6::Accept(protocol, thk_remote, thk_local)) {

		// action is block, clear the write flag of the rights member of the FWPS_CLASSIFY_OUT0 structure.
		_p_out->rights    &= ~FWPS_RIGHT_ACTION_WRITE;
		_p_out->actionType =  FWP_ACTION_BLOCK;

		return nt_status;
	}

	// Action is allow, clear the write flag of the rights member of the FWPS_CLASSIFY_OUT0 structure,
	// If the FWPS_FILTER_FLAG_CLEAR_ACTION_RIGHT of the flags member in the FWPS_FILTER0 structure is set.
	if (NULL != _p_flt && (_p_flt->flags & FWPS_FILTER_FLAG_CLEAR_ACTION_RIGHT)) {
		_p_out->rights &= ~FWPS_RIGHT_ACTION_WRITE;
	}
	_p_out->actionType = FWP_ACTION_CONTINUE;
#endif
	DbgPrint( "__ssh.tnl.drv: [INFO] CAction_Accept::v6::Do()::fn_end();\n" );
	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS CAction_Connect::v4::Do (
	const FWPS_INCOMING_VALUES0* _p_in, const FWPS_INCOMING_METADATA_VALUES0* _p_meta, VOID* _p_pkt, const FWPS_FILTER0* _p_flt, UINT64 _u_ctx, FWPS_CLASSIFY_OUT0* _p_out
	) {
	__unused(_p_in);
	__unused(_p_meta);
	__unused(_p_pkt );
	__unused(_p_flt );
	__unused(_u_ctx );
	__unused(_p_out );
	DbgPrint( "__ssh.tnl.drv: [INFO] CAction_Connect::v4::Do()::fn_begin();\n" );
	NTSTATUS nt_status = S_OK;
#if (0)
	if (NULL == g_dev_ext || 0 == g_dev_ext->block || NULL == _p_out)
		return nt_status;

	ADDR_THUNK thk_local ; CAddress::Reset(thk_local );
	ADDR_THUNK thk_remote; CAddress::Reset(thk_remote);

	ULONG protocol = 0;

	nt_status = CConvert::v4::To_connect(_p_in, protocol, thk_local, thk_remote);
	if (FAILED(nt_status)) {
		return(nt_status = S_OK);
	}

	if (0 == TBase::v4::Connect(protocol, thk_local, thk_remote)) {

		// action is block, clear the write flag of the rights member of the FWPS_CLASSIFY_OUT0 structure.
		_p_out->rights    &= ~FWPS_RIGHT_ACTION_WRITE;
		_p_out->actionType =  FWP_ACTION_BLOCK;

		return nt_status;
	}

	// Action is allow, clear the write flag of the rights member of the FWPS_CLASSIFY_OUT0 structure,
	// If the FWPS_FILTER_FLAG_CLEAR_ACTION_RIGHT of the flags member in the FWPS_FILTER0 structure is set.
	if (NULL != _p_flt && (_p_flt->flags & FWPS_FILTER_FLAG_CLEAR_ACTION_RIGHT)) {
		_p_out->rights &= ~FWPS_RIGHT_ACTION_WRITE;
	}
	_p_out->actionType = FWP_ACTION_CONTINUE;
#endif
	DbgPrint( "__ssh.tnl.drv: [INFO] CAction_Connect::v4::Do()::fn_end();\n" );
	return nt_status;
}

NTSTATUS CAction_Connect::v6::Do (
	const FWPS_INCOMING_VALUES0* _p_in, const FWPS_INCOMING_METADATA_VALUES0* _p_meta, VOID* _p_pkt, const FWPS_FILTER0* _p_flt, UINT64 _u_ctx, FWPS_CLASSIFY_OUT0* _p_out
	) {
	__unused(_p_in);
	__unused(_p_meta);
	__unused(_p_pkt );
	__unused(_p_flt );
	__unused(_u_ctx );
	__unused(_p_out );
	DbgPrint( "__ssh.tnl.drv: [INFO] CAction_Connect::v6::Do()::fn_begin();\n" );
	NTSTATUS nt_status = S_OK;
#if (0)
	if (NULL == g_dev_ext || 0 == g_dev_ext->block || NULL == _p_out)
		return nt_status;

	ADDR_THUNK thk_local ; CAddress::Reset(thk_local );
	ADDR_THUNK thk_remote; CAddress::Reset(thk_remote);

	ULONG protocol = 0;

	nt_status = CConvert::v6::To_connect(_p_in, protocol, thk_local, thk_remote);
	if (FAILED(nt_status)) {
		return(nt_status = S_OK);
	}

	if (0 == TBase::v6::Connect(protocol, thk_local, thk_remote)) {

		// action is block, clear the write flag of the rights member of the FWPS_CLASSIFY_OUT0 structure.
		_p_out->rights    &= ~FWPS_RIGHT_ACTION_WRITE;
		_p_out->actionType =  FWP_ACTION_BLOCK;

		return nt_status;
	}

	// Action is allow, clear the write flag of the rights member of the FWPS_CLASSIFY_OUT0 structure,
	// If the FWPS_FILTER_FLAG_CLEAR_ACTION_RIGHT of the flags member in the FWPS_FILTER0 structure is set.
	if (NULL != _p_flt && (_p_flt->flags & FWPS_FILTER_FLAG_CLEAR_ACTION_RIGHT)) {
		_p_out->rights &= ~FWPS_RIGHT_ACTION_WRITE;
	}
	_p_out->actionType = FWP_ACTION_CONTINUE;
#endif
	DbgPrint( "__ssh.tnl.drv: [INFO] CAction_Connect::v6::Do()::fn_end();\n" );
	return nt_status;
}