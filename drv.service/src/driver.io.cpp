/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Aug-2020 at 2:03:16a, UTC+7, Novosibirsk, Saturday;
	This is SSH-Tunnel WFP driver-to-client input/output interface implementation file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 7:42:17.127 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "driver.io.h"

using namespace shared::driver::client;

/////////////////////////////////////////////////////////////////////////////

CDriverIO:: CDriverIO(void) : m_device(INVALID_HANDLE_VALUE) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CDriverIO::~CDriverIO(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CDriverIO::Error (void) const { return m_error; }
const bool  CDriverIO::Is    (void) const { return (NULL != m_device && INVALID_HANDLE_VALUE != m_device); }
HRESULT     CDriverIO::Turn  (const bool _b_on) {
	m_error << __MODULE__ << S_OK;

	if (true == _b_on && this->Is() == true) return (m_error << __DwordToHresult(ERROR_INVALID_STATE)) = _T("Already initialized;");
	if (false == _b_on && this->Is() == false) return (m_error << __DwordToHresult(ERROR_INVALID_STATE)) = _T("Already destroyed;");

	if (_b_on) {
		m_device = ::CreateFile(
			CDrvLocator().Device().GetString(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL
		);
		if (INVALID_HANDLE_VALUE == m_device)
			return (m_error = ::GetLastError());
	}
	else {
		// https://docs.microsoft.com/en-us/windows/win32/api/handleapi/nf-handleapi-closehandle
		const BOOL b_result = ::CloseHandle(m_device);
		if (FALSE==b_result)
			m_error.Last();
		else
			m_device = INVALID_HANDLE_VALUE;
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

DWORD       CDriverIO::Read  (const DWORD _io_ctl, PVOID _p_out, const DWORD _sz_out, LPOVERLAPPED _overlap) {
	return this->RawIO(_io_ctl, NULL, 0, _p_out, _sz_out, _overlap);
}

DWORD       CDriverIO::RawIO (const DWORD _io_ctl, PVOID _p_in , const DWORD _sz_in , PVOID _p_out, const DWORD _sz_out, LPOVERLAPPED _p_over) {
	m_error << __MODULE__ << S_OK;

	if (NULL == _p_in  || 0 == _sz_in ) {(m_error = E_INVALIDARG) = _T("Input buffer is not valid;"); return 0; }
	if (NULL == _p_out || 0 == _sz_out) {(m_error = E_INVALIDARG) = _T("Output buffer is not valid;"); return 0; }

	if (this->Is() == false) {
		m_error << OLE_E_BLANK; return 0;
	}
	// https://docs.microsoft.com/en-us/windows/win32/api/ioapiset/nf-ioapiset-deviceiocontrol
	DWORD dw_bytes = 0;
	const BOOL b_result = ::DeviceIoControl(m_device, _io_ctl, _p_in, _sz_in, _p_out, _sz_out, &dw_bytes, _p_over);

	if (FALSE == b_result) {
		const DWORD dw_err_code = ::GetLastError();
		if (ERROR_IO_PENDING != dw_err_code)
			m_error = __DwordToHresult(dw_err_code);
	}

	return 0;
}

DWORD       CDriverIO::Write (const DWORD _io_ctl, PVOID _p_in , const DWORD _sz_in , LPOVERLAPPED _overlap) {
	return this->RawIO(_io_ctl, _p_in, _sz_in, NULL, 0, _overlap);
}

/////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////