/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Aug-2020 at 5:17:33a, UTC+7, Novosibirsk, Friday;
	This is SSH-Tunnel WFP driver service create data interface implementation file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 7:42:17.193 pm, UTC+7, Novosibirsk, Wednesday;
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 6:44:04.778 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "StdAfx.h"
#include "driver.svc.crt.h"

using namespace shared::driver::client;

/////////////////////////////////////////////////////////////////////////////

CKmdfDrv_Crt:: CKmdfDrv_Crt (void) {
	TBase::Account()           = _T("NT AUTHORITY\\LocalSystem");
	TBase::Options().Control() = CCrtOption::eStoppable | CCrtOption::eCanShutdown;
	TBase::Options().Start  () = CCrtOption::eOnDemand  ;
	TBase::Options().Type   () = CCrtOption::eKernelDrv ;
}

CKmdfDrv_Crt::~CKmdfDrv_Crt(void) {}

/////////////////////////////////////////////////////////////////////////////

CKmdfDrv_Crt&   CKmdfDrv_Crt::operator << (const TDrvLocate& _locator) {
	this->Path() = _locator.Path ();
	this->Name() = _locator.Alias();
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CKmdfDrv_Crt_SSH::CKmdfDrv_Crt_SSH (void) {

	const CDrvLocator drv_loc; 

	TBase::Description() = _T("SSH-Tunnel WFP driver for TCP/IP redirection;");     // TODO: executable file resources must be used;
	TBase::DisplayName() = _T("SSH-Tunnel WFP driver;");

	*this << drv_loc;
}

CKmdfDrv_Crt_SSH::~CKmdfDrv_Crt_SSH(void) {}

/////////////////////////////////////////////////////////////////////////////

CKmdfDrv_Crt_SSH::operator LPCWSTR (void) const { return TBase::Name(); }