#ifndef _DRVSETUPINSTANCE_H_3615AC89_B5D2_4418_8122_4ACAEEB67174_INCLUDED
#define _DRVSETUPINSTANCE_H_3615AC89_B5D2_4418_8122_4ACAEEB67174_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Sep-2020 at 7:35:14p, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack generic driver device instance setup information interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 12-Jan-2021 at 7:28:26.702 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "drv.setup.defs.h"
#include "drv.setup.info.h"
#include "drv.setup.registry.h"

namespace shared { namespace driver { namespace setup {

	using shared::sys_core::CError;

	// https://docs.microsoft.com/en-us/windows/win32/api/setupapi/ns-setupapi-sp_devinfo_data | used for getting device instance info;

	class CDevInstInfo {
	public:
		enum _att : DWORD {
			e_att_id = 0, e_att_desc, e_att_name /*friendly name*/, e__count
		};
	protected:
		CStringW   m_atts[_att::e__count];
		CError     m_error;

	public:
		 CDevInstInfo (void);
		 CDevInstInfo (const CDevInstInfo&);
		~CDevInstInfo (void);

	public:
		LPCWSTR    Att_Of(const _att) const;
		CStringW&  Att_Of(const _att)      ;
		HRESULT    Erase (void) ;
		TErrorRef  Error (void) const;
		HRESULT    Get   (const CDevInfoSet&, const CDevInfo&);
		CStringW   ToStr (LPCWSTR _lp_sz_sep = _T(";")) const ;

	public:
		CDevInstInfo& operator = (const CDevInstInfo&);
	};

	typedef CDevInstInfo::_att TProperty;
	typedef ::std::vector<CDevInstInfo>  TDevInstances;

}}}

#endif/*_DRVSETUPINSTANCE_H_3615AC89_B5D2_4418_8122_4ACAEEB67174_INCLUDED*/