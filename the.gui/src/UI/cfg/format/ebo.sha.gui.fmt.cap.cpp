/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2020 at 6:18:31.190 am, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Sha Optima Tool common caption control format interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:03:27.409 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "StdAfx.h"
#include "ebo.sha.gui.fmt.cap.h"

using namespace ebo::sha::gui::format;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace format { namespace _impl {

	class CCaption_Cfg {
	public:
		 CCaption_Cfg (void) {}
		~CCaption_Cfg (void) {}

	public:
		HRESULT    Do(TLabelFmt& _fmt) const { HRESULT hr_ = S_OK;
			_fmt;
			_fmt.Bkgnd().Solid() << shared::Get_Theme().Get(TThemePart::e_caption, TThemeElement::e_back);
			_fmt.Font ().Fore ()  = shared::Get_Theme().Get(TThemePart::e_caption, TThemeElement::e_fore);
			_fmt.Font ().Family(_T("Segoe UI"));
			_fmt.Font ().Size ()  =(23);

			static const ex_ui::controls::CMargins margins_(95,0,0,3);
			_fmt.Margins() = margins_;

			return hr_;
		}

		HRESULT    Do(TPictureFormat& _fmt) const { HRESULT hr_ = S_OK;
			_fmt;
			_fmt.Bkgnd().Solid() << shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_back);
			return hr_;
		}
	};

}}}}}
using namespace ebo::sha::gui::format::_impl;

/////////////////////////////////////////////////////////////////////////////

TCaptionFmt:: CCaption (void) { CCaption_Cfg cfg_; cfg_.Do(m_logo); cfg_.Do(m_title);}
TCaptionFmt:: CCaption (const TCaptionFmt& _ref) : TCaptionFmt() { *this = _ref; }
TCaptionFmt::~CCaption (void) {}

/////////////////////////////////////////////////////////////////////////////
const
TPictureFormat&  TCaptionFmt::Logo (void) const { return m_logo; }
TPictureFormat&  TCaptionFmt::Logo (void)       { return m_logo; }
const
TLabelFmt& TCaptionFmt::Title (void) const { return m_title; }
TLabelFmt& TCaptionFmt::Title (void)       { return m_title; }

/////////////////////////////////////////////////////////////////////////////

TCaptionFmt&  TCaptionFmt::operator = (const TCaptionFmt& _ref) { *this << _ref.Logo() << _ref.Title(); return *this; }
TCaptionFmt&  TCaptionFmt::operator <<(const TLabelFmt& _title) {  this->Title() = _title; return *this; }
TCaptionFmt&  TCaptionFmt::operator <<(const TPictureFormat& _logo) { this->Logo() = _logo; return*this; }

/////////////////////////////////////////////////////////////////////////////

const TCaptionFmt&  TCaptionFmt::operator >>(TLabelFmt& _title) const { _title = this->Title(); return *this; }
const TCaptionFmt&  TCaptionFmt::operator >>(TPictureFormat& _logo) const { _logo = this->Logo(); return *this; }