#ifndef _EBOSHAGUIFMTSTRIP_H_75311D42_687E_48B8_9346_FEDBD673D1B7_INCLUDED
#define _EBOSHAGUIFMTSTRIP_H_75311D42_687E_48B8_9346_FEDBD673D1B7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Jan-2021 at 12:51:30.640 pm, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha Optima Tool app main form menu strip bar wrapper interface declaration file.
*/
#include "sfx.strip.fmt.h"
#include "sfx.strip.ctrl.h"

namespace ebo { namespace sha { namespace gui { namespace format {

	class CStripBar_Format {
	public:
		 CStripBar_Format (void);
		~CStripBar_Format (void);

	public:
		HRESULT   ApplyTo (ST_Ctrls::CStripBar&) const;

	public:
		const CStripBar_Format& operator >> (ST_Ctrls::CStripBar&) const;
	};

}}}}

typedef ebo::sha::gui::format::CStripBar_Format  TStripBarFmt;

#endif/*_EBOSHAGUIFMTSTRIP_H_75311D42_687E_48B8_9346_FEDBD673D1B7_INCLUDED*/