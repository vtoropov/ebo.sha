#ifndef _SFXSELECTLAY_H_E752F34E_47EA_4709_98E3_EB22BAB80C00_INCLUDED
#define _SFXSELECTLAY_H_E752F34E_47EA_4709_98E3_EB22BAB80C00_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Aug-2020 at 8:45:33a, UTC+7, Novosibirsk, Thursday;
	This is shared Sfx library selector control layout interface declaration file;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.base.lay.h"

namespace ST_Ctrls { class CSelector; namespace layout {

	using shared::sys_core::CError  ;

	using ex_ui::controls::CSides   ;
	using ex_ui::controls::TSide    ;
	using ex_ui::controls::CMargins ;
	using ex_ui::controls::layout::CImage;

	class CSel_Layout {
	protected:
		CSelector&   m_control ;
		THorzAlign   m_hz_align; // horizontal alignment in parent window;
		CMargins     m_margins ;
		UINT         m_width   ; // control width; it has a default value on startup, can be re-written;
		CImage       m_image   ;
		RECT         m_overlay ; // this is background overlay rectangle; TODO: to review a place/class where this data must be kept;

	protected:
		mutable
		CError       m_error   ;

	public:
		 CSel_Layout(CSelector&);
		~CSel_Layout(void);

	public:
		TErrorRef    Error  (void) const ;
		const
		THorzAlign&  HAlign (void) const ;
		THorzAlign&  HAlign (void)       ;
		const
		CImage&      Images (void) const ;          // if defined, it is used for all items regardless having an image;
		CImage&      Images (void)       ;
		const
		CMargins&    Margins(void) const ;          // selector control margins between frame border(s) and control's content;
		CMargins&    Margins(void)       ;
		const RECT&  Overlay(void) const ;          // access is read-only; layout class calculates this rectangle internally, no outer help is required;
		HRESULT      Update (void)       ;          // assumes that control position and size are already set;
		HRESULT      Update (const RECT& _rc_area); // updates selector window position into an area provided;
		UINT         Width  (void) const ;
		HRESULT      Width  (const UINT );          // TODO: input value is checked on 0-value only; minimal value must be defined;

	public:
		CSel_Layout& operator <<(const RECT& _rc_area);        // updates selector window position in accordance with area  ;
		const RECT   operator = (const RECT& _rc_area) const;  // returns calculated rectangle of selector for area provided;
	};
}}

typedef ST_Ctrls::layout::CSel_Layout TSelectLay;

#endif/*_SFXSELECTLAY_H_E752F34E_47EA_4709_98E3_EB22BAB80C00_INCLUDED*/