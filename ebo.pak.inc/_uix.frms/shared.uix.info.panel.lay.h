#ifndef _SHAREDUIXINFOPANELLAY_H_5B911303_56E1_47DC_9629_58A567DC89A5_INCLUDED
#define _SHAREDUIXINFOPANELLAY_H_5B911303_56E1_47DC_9629_58A567DC89A5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Jan-2021 at 7:45:37.291 pm, UTC+7, Novosibirsk, Friday;
	This is Ebo Pack shared information panel layout interface declaration file;
*/
#include "shared.uix.ctrl.base.lay.h"
#include "shared.uix.gen.text.h"
#include "shared.uix.info.panel.ext.h"

#include <vector>

namespace ex_ui { namespace frames {
	//
	// very simple z-order class; it is enough for current version; surely, it must be improved/extended;
	//
	class CInfoZorder {
	public :
		enum _part { e_caption, e_image };
		enum _index { e_not_found = -1 };  // TODO: if element/part is not found in z-order vector, the toppest index is returned; it's not valid;

	private:
		typedef ::std::vector<_part> TOrder;
		TOrder  m_z_order ;

	public:
		 CInfoZorder (void) ;
		 CInfoZorder (const CInfoZorder&) ;
		~CInfoZorder (void) ;

	public:
		INT    Index (const _part) const;
		_part  Top   (const _part)      ;  // sets given part of content on top; previous top part is returned;

	public:
		CInfoZorder&  operator = (const CInfoZorder&);
	};

	class CInfoPanel  ;
	class CInfoLayout : public TLayoutBase { typedef TLayoutBase TBase;
	private:
		CInfoPanel&    m_panel  ;  // reference for panel object;
		TImageCache_Ex m_cache  ;  // image collection for message avatars;
		TInfoCaption   m_caption;
		CInfoZorder    m_z_order;

	public:
		 CInfoLayout (CInfoPanel&);
		~CInfoLayout (void);

	public:
		const
		TImageCache_Ex& Cache   (void) const;
		TImageCache_Ex& Cache   (void)      ;
		const
		TInfoCaption&   Caption (void) const;
		TInfoCaption&   Caption (void)      ;
		HRESULT     Update (void)    ;             // recalculates position/rectangle of all elements;
		HRESULT     Update (const RECT& _rc_area); // updates panel window position into an area provided;
		const
		CInfoZorder&    ZOrder  (void) const;
		CInfoZorder&    ZOrder  (void)      ;

	public:
		CInfoLayout& operator <<(const RECT& _rc_area);        // updates info panel window position in accordance with given area;
		const RECT   operator = (const RECT& _rc_area) const;  // returns calculated rectangle of panrl window for area provided;

	private: // non-copyable;
		CInfoLayout (const CInfoLayout&);
		CInfoLayout& operator = (const CInfoLayout&);
	};

}}

typedef ex_ui::frames::CInfoLayout          TInfoLayout;
typedef ex_ui::frames::CInfoZorder          TInfoZOrder;
typedef ex_ui::frames::CInfoZorder::_part   TInfoZElement;

#endif/*_SHAREDUIXINFOPANELLAY_H_5B911303_56E1_47DC_9629_58A567DC89A5_INCLUDED*/