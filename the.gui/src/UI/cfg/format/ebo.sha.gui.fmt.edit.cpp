/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Dec-2020 at 9:25:15.743 pm, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool common edit control format interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:03:27.477 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "StdAfx.h"
#include "ebo.sha.gui.fmt.edit.h"

using namespace ebo::sha::gui::format;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace format { namespace _impl {

	class CDisabled_Borders {
	public:
		 CDisabled_Borders (void) {}
		~CDisabled_Borders (void) {}

	public:
		COLORREF Color (void) const { return shared::Get_Theme().Get(TThemePart::e_edit, TThemeElement::e_border); }
		DWORD    Thick (void) const { return 1; }
	};

	class CDisabled_Default {
	public:
		 CDisabled_Default (void) {}
		~CDisabled_Default (void) {}

	public:
		HRESULT    Do(TBorders& _fmt) const { HRESULT hr_ = S_OK;
			_fmt;
			_fmt.Color(TColour(CDisabled_Borders().Color(), TAlpha::eOpaque));
			_fmt.Thickness(CDisabled_Borders().Thick());
			return hr_;
		}

		HRESULT    Do(TLabelFmt& _fmt) const { HRESULT hr_ = S_OK;
			_fmt;
			_fmt.Bkgnd().Solid() << shared::Get_Theme().Get(TThemePart::e_edit, TThemeElement::e_back);
			_fmt.Font ().Fore ()  = shared::Get_Theme().Get(TThemePart::e_edit, TThemeElement::e_fore);
			_fmt.Font ().Family(_T("Verdana"));
			_fmt.Font ().Size ()  =(18);

			static const ex_ui::controls::CMargins margins_(5,0,0,3);
			_fmt.Margins() = margins_;

			return hr_;
		}
	};


}}}}}
using namespace ebo::sha::gui::format::_impl;
/////////////////////////////////////////////////////////////////////////////

TEditFmt:: CEdit (void) {
	CDisabled_Default().Do(m_box_ra);
	CDisabled_Default().Do(m_borders);
}
TEditFmt:: CEdit (const TEditFmt& _ref) : TEditFmt() { *this = _ref; }
TEditFmt::~CEdit (void) {}

/////////////////////////////////////////////////////////////////////////////
const
TBorders&  TEditFmt::Borders (void) const { return m_borders; }
TBorders&  TEditFmt::Borders (void)       { return m_borders; }
const
TLabelFmt& TEditFmt::Box (void) const { return m_box_ra; }
TLabelFmt& TEditFmt::Box (void)       { return m_box_ra; }
const
CState&    TEditFmt::State (void) const { return m_state; }
CState&    TEditFmt::State (void)       { return m_state; }

/////////////////////////////////////////////////////////////////////////////

TEditFmt&  TEditFmt::operator = (const TEditFmt& _ref) { *this << _ref.Borders() << _ref.Box(); return *this; }
TEditFmt&  TEditFmt::operator <<(const TBorders& _borders) { this->Borders() = _borders; return *this; }
TEditFmt&  TEditFmt::operator <<(const TLabelFmt& _edit_ra) { this->Box() = _edit_ra; return *this; }

/////////////////////////////////////////////////////////////////////////////

const TEditFmt& TEditFmt::operator >>(TBorders& _borders) const {
	_borders = this->Borders();
	return *this; }
const TEditFmt& TEditFmt::operator >>(TLabelFmt& _edit_ra) const { _edit_ra = this->Box(); return *this; }