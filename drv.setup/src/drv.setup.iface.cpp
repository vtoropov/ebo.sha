/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Sep-2020 at 8:24:45p, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack generic driver device interface setup information interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 12-Jan-2021 at 7:39:58.421 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "drv.setup.iface.h"

using namespace shared::driver::setup;

/////////////////////////////////////////////////////////////////////////////

CDevIfaceData:: CDevIfaceData (void) { this->Reset(); }
CDevIfaceData:: CDevIfaceData (const CDevIfaceData& _ref) : CDevIfaceData() { *this = _ref; }
CDevIfaceData::~CDevIfaceData (void) {}

/////////////////////////////////////////////////////////////////////////////
const GUID&     CDevIfaceData::Class(void) const { return m_data.InterfaceClassGuid; }
HRESULT         CDevIfaceData::Class(const GUID& _guid) {
	HRESULT hr_ = S_OK;
	const errno_t err_t = ::memcpy_s(&m_data.InterfaceClassGuid, sizeof(GUID), &_guid, sizeof(GUID));
	if (err_t)
		hr_ = E_OUTOFMEMORY;
	return  hr_;
}
DWORD           CDevIfaceData::Flags(void) const { return m_data.Flags; }
DWORD&          CDevIfaceData::Flags(void)       { return m_data.Flags; }
const bool      CDevIfaceData::Is   (void) const {
	// https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/memcmp-wmemcmp
	const INT n_result = ::IsEqualGUID(GUID_NULL, m_data.InterfaceClassGuid);
	return (0 == n_result);
}
const
TDevIfaceData&  CDevIfaceData::Ref  (void) const { return m_data; }
TDevIfaceData&  CDevIfaceData::Ref  (void)       { return m_data; }
VOID            CDevIfaceData::Reset(void)       { RtlZeroMemory(&m_data, sizeof(TDevIfaceData)); m_data.cbSize = sizeof(TDevIfaceData); }
CStringW        CDevIfaceData::ToStr(LPCWSTR _lp_sz_sep) const {
	static LPCWSTR lp_sz_pat = _T(
		"Class = %s"
		"%s"
		"Flags = %s"
	);

	CStringW cs_guid;
	LPOLESTR lp_ole_guid = NULL;

	HRESULT  hr_ = ::StringFromCLSID(m_data.InterfaceClassGuid, &lp_ole_guid);
	if (SUCCEEDED(hr_)) {
		cs_guid = lp_ole_guid;
		::CoTaskMemFree((LPVOID)lp_ole_guid); lp_ole_guid = NULL;
	}
	else
		cs_guid = _T("#error::no_guid");

	CStringW cs_flags;
	if (m_data.Flags & SPINT_ACTIVE ) { if (cs_flags.IsEmpty() == false) cs_flags += _T("|"); cs_flags += _T("SPINT_ACTIVE" ); }
	if (m_data.Flags & SPINT_DEFAULT) { if (cs_flags.IsEmpty() == false) cs_flags += _T("|"); cs_flags += _T("SPINT_DEFAULT"); }
	if (m_data.Flags & SPINT_REMOVED) { if (cs_flags.IsEmpty() == false) cs_flags += _T("|"); cs_flags += _T("SPINT_REMOVED"); }
	if (cs_flags.IsEmpty())
		cs_flags = _T("#undef");

	CStringW cs_data; cs_data.Format(
		lp_sz_pat, cs_guid.GetString(), _lp_sz_sep, cs_flags.GetString()
	);
	return cs_data;
}

/////////////////////////////////////////////////////////////////////////////

CDevIfaceData&  CDevIfaceData::operator = (const CDevIfaceData& _ref) {
	// https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/memcpy-s-wmemcpy-s
	const errno_t err_t = ::memcpy_s(&this->Ref(), sizeof(TDevIfaceData), &_ref.Ref(), sizeof(TDevIfaceData));
	if (err_t != NO_ERROR){}
	return *this;
}

CDevIfaceData&  CDevIfaceData::operator >>(CStringW& _ref) { _ref = this->ToStr(); return *this; }
CDevIfaceData&  CDevIfaceData::operator <<(const GUID& _guid) { this->Class(_guid); return *this; }
CDevIfaceData&  CDevIfaceData::operator <<(const DWORD _flags) { this->Flags() = _flags; return *this; }
CDevIfaceData&  CDevIfaceData::operator +=(const DWORD _flags) { this->Flags()|= _flags; return *this; }
CDevIfaceData&  CDevIfaceData::operator -=(const DWORD _flags) { this->Flags()&=~_flags; return *this; }

/////////////////////////////////////////////////////////////////////////////

CDevIfaceData::operator const CStringW (void) const { return this->ToStr(); }
CDevIfaceData::operator const TDevIfaceData& (void) const { return m_data; }
CDevIfaceData::operator       TDevIfaceData& (void)       { return m_data; }

/////////////////////////////////////////////////////////////////////////////

CDevIfaceDetails:: CDevIfaceDetails (void) : TBase() {}
CDevIfaceDetails:: CDevIfaceDetails (const CDevIfaceDetails& _ref) : CDevIfaceDetails() { *this = _ref; }
CDevIfaceDetails::~CDevIfaceDetails (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT       CDevIfaceDetails::Get (const CDevInfoSet& _set, const CDevIfaceData& _data) {
	m_error << __MODULE__ << S_OK;

	if (_set.Is() == false) return (m_error << E_INVALIDARG);
	if (_data.Is() == false) return (m_error << E_INVALIDARG);
	if (this->Is())
		return (m_error << __DwordToHresult(ERROR_ALREADY_INITIALIZED));

	CDevIfaceData& data_ = const_cast<CDevIfaceData&>(_data); // input argument is constant, due to it is not changed here;
	DWORD dw_reqired = 0;
	// (1) gets required size for memory allocation;
	BOOL b_result = ::SetupDiGetDeviceInterfaceDetail( _set, &data_.Ref(), NULL, 0, &dw_reqired, NULL );
	DWORD the_lst_err = ::GetLastError();

	if (ERROR_INSUFFICIENT_BUFFER != the_lst_err) {
		return (m_error << __DwordToHresult(the_lst_err));
	}

	this->Create(dw_reqired);

	if (m_error == true)
		return m_error;

	TDevIfaceDetailsPtr ptr_ = reinterpret_cast<TDevIfaceDetailsPtr>(this->Ptr());
	if (NULL == ptr_)
		return (m_error << __DwordToHresult(ERROR_INVALID_DATATYPE));
	else
		ptr_->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA); // if not specified: ERROR_INVALID_USER_BUFFER error occurs;

	b_result = SetupDiGetDeviceInterfaceDetail(
		_set, &data_.Ref(), ptr_, TBase::Size(), &dw_reqired, NULL
	);
	if (FALSE == b_result)
		m_error = __LastErrToHresult();

	return m_error;
}

CStringW      CDevIfaceDetails::Path (void) const {
	CStringW cs_path;
	if (TBase::Is()) {
		TDevIfaceDetailsPtr ptr_ = reinterpret_cast<TDevIfaceDetailsPtr>(this->Ptr());
		if (NULL != ptr_) {
			cs_path += ptr_->DevicePath;
		}
	}
	if (cs_path.IsEmpty())
		cs_path = _T("#error:ole_e_blank;");

	return cs_path;
}

/////////////////////////////////////////////////////////////////////////////

CDevIfaceDetails& CDevIfaceDetails::operator = (const CDevIfaceDetails& _ref) { (TBase&)*this = (const TBase&)_ref; return *this; }

/////////////////////////////////////////////////////////////////////////////

CDevIfaceInfo:: CDevIfaceInfo (void) { m_error << __MODULE__ << S_OK; }
CDevIfaceInfo:: CDevIfaceInfo (const CDevIfaceInfo& _ref) : CDevIfaceInfo() { *this = _ref; }
CDevIfaceInfo::~CDevIfaceInfo (void) { this->Erase(); }

/////////////////////////////////////////////////////////////////////////////
const
CDevIfaceData&  CDevIfaceInfo::Data  (void) const { return m_data; }
CDevIfaceData&  CDevIfaceInfo::Data  (void)       { return m_data; }
const
CDevIfaceDetails& CDevIfaceInfo::Details(void) const { return m_details; }

/////////////////////////////////////////////////////////////////////////////

HRESULT         CDevIfaceInfo::Erase (void) {
	m_error << __MODULE__ << S_OK; HRESULT hr_ = m_error;

	if (m_data.Is()) m_data.Reset();
	if (m_details.Is()) {
		hr_ = m_details.Destroy(); if (FAILED(hr_)) m_error = m_details.Error();
	}
	return m_error;
}
TErrorRef       CDevIfaceInfo::Error (void) const { return m_error; }

HRESULT         CDevIfaceInfo::Get   (const CDevInfoSet& _dev_set ) { return this->Get(_dev_set, this->Data()); }
HRESULT         CDevIfaceInfo::Get   (const CDevInfoSet& _dev_set , const CDevIfaceData& _iface_data) {
	_dev_set; _iface_data;
	m_error << __MODULE__ << S_OK;

	if (_dev_set.Is() == false)
		return (m_error << E_INVALIDARG);
	if (_iface_data.Is() == false)
		return (m_error << E_INVALIDARG);
	if (m_details.Is())
		return (m_error << __DwordToHresult(ERROR_ALREADY_INITIALIZED));

	HRESULT  hr_ = m_details.Get(_dev_set, _iface_data);
	if (FAILED(hr_))
		m_error = m_details.Error();

	*this << _iface_data;

	return  m_error;
}
const bool      CDevIfaceInfo::Is    (void) const { return (m_data.Is() && m_details.Is()); }
CStringW        CDevIfaceInfo::ToStr (LPCWSTR _lp_sz_sep ) const {

	static LPCWSTR lp_sz_pat = _T(
		"%s"
		"%s"
		"Path  = %s"
	);

	CStringW cs_info; cs_info.Format(
		lp_sz_pat, m_data.ToStr(_lp_sz_sep).GetString(), _lp_sz_sep, m_details.Path().GetString()
	); 

	return cs_info;
}

/////////////////////////////////////////////////////////////////////////////

CDevIfaceInfo&  CDevIfaceInfo::operator = (const CDevIfaceInfo& _ref ) { *this << _ref.Data() << _ref.Details(); return *this; }
CDevIfaceInfo&  CDevIfaceInfo::operator <<(const CDevIfaceData& _data) {
	m_error << __MODULE__ << S_OK;
	if (&this->m_data != &_data)   // TODO: it's possible to copy data into itself; must be re-viewed;
	     this->m_data  =  _data;
#if (0)
	// https://docs.microsoft.com/en-us/cpp/c-runtime-library/reference/memcpy-s-wmemcpy-s
	const errno_t err_t = ::memcpy_s(&this->m_data, sizeof(CDevIfaceData), &_data, sizeof(CDevIfaceData));
	if (err_t != NO_ERROR)
		m_error << E_OUTOFMEMORY;
#endif
	return *this;
}

CDevIfaceInfo&  CDevIfaceInfo::operator <<(const CDevIfaceDetails& _det) {
	m_error <<__MODULE__ << S_OK;
	this->m_details = _det;
	if (m_details.Error())
		this->m_error = m_details.Error();
	return *this;
}