#ifndef _EBOSHAVIEWBLOCK_H_552F2E1F_D14D_49A0_8F14_0C81065D9DCC_INCLUDED
#define _EBOSHAVIEWBLOCK_H_552F2E1F_D14D_49A0_8F14_0C81065D9DCC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 26-Dec-2020 at 5:45:46.734 am, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha optima tool TCP/IP/Process blocker view interface declaration file.
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.block.http.h"
#include "ebo.sha.block.proc.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CBlock : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CBlock;
		private:
			CBlock&   m_view  ;
			TSelect   m_select;
			THttpView m_http  ;
			TProcView m_proc  ;

		public:
			 CViewWnd (CBlock&);
			~CViewWnd (void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CBlock (void);
		~CBlock (void);
#pragma warning(disable:4481)
	private:    // ISelector_Events
		HRESULT    ISelector_OnItemClick  (const UINT _u_itm_id) override sealed;
#pragma warning(default:4481)

	public:
		THttpView& Http    (void) ;
		TProcView& Proc    (void) ;
		TSelect&   Select  (void) ;
	};

}}}}

#endif/*_EBOSHAVIEWBLOCK_H_552F2E1F_D14D_49A0_8F14_0C81065D9DCC_INCLUDED*/