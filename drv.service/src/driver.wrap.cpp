/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Aug-2020 at 0:51:41a, UTC+7, Novosibirsk, Wednesday;
	This is SSH-Tunnel WFP driver wrapper interface implementation file; 
*/
#include "StdAfx.h"
#include "driver.wrap.h"

using namespace ssh::client;

/////////////////////////////////////////////////////////////////////////////

namespace ssh { namespace client { namespace _impl {

	class CService_Man {
	private:
		SC_HANDLE  m_svc_man;
		CError     m_error;

	public:
		 CService_Man(void) : m_svc_man(NULL) { m_error << __MODULE__ <<  S_OK; }
		~CService_Man(void) { if (this->Is()) this->Close(); }

	public:
		HRESULT    Close(void) {
			m_error << __MODULE__ << S_OK;

			if (this->Is() == false)
				return (m_error = __DwordToHresult(ERROR_INVALID_STATE));

			const BOOL b_result = ::CloseServiceHandle(m_svc_man);
			if (FALSE == b_result)
				m_error = ::GetLastError();
			else
				m_svc_man = NULL;

			return m_error;
		}
		TErrorRef  Error(void) const { return m_error; }
		const bool Is   (void) const { return (NULL != m_svc_man); }
		HRESULT    Open (void) {
			m_error << __MODULE__ << S_OK;

			if (this->Is())
				return (m_error = __DwordToHresult(ERROR_ALREADY_INITIALIZED));

			m_svc_man = ::OpenSCManagerW(NULL, NULL, SC_MANAGER_ALL_ACCESS);
			if (NULL == m_svc_man)
				m_error = ::GetLastError();

			return m_error;
		}

	public:
		operator const SC_HANDLE (void) const { return m_svc_man; }

	private:
		CService_Man (const CService_Man&) {}
		CService_Man& operator = (const CService_Man&) { return *this; }
	};

	class CService {
	private:
		SC_HANDLE   m_handle;
		CError      m_error ;

	public:
		 CService (void) : m_handle(NULL) { m_error << __MODULE__ << S_OK; }
		~CService (void) { if (this->Is()) this->Close(); }

	public:
		HRESULT     Create(const CService_Man& _man, const TDrvLocate& _loc) {
			m_error << __MODULE__ << S_OK;

			if (this->Is())
				return (m_error = __DwordToHresult(ERROR_INVALID_STATE));

			m_handle = ::CreateServiceW(
						_man, _loc.Service().GetString(), _loc.Service().GetString(),
						SERVICE_ALL_ACCESS, SERVICE_KERNEL_DRIVER, SERVICE_DEMAND_START, SERVICE_ERROR_NORMAL,
						_loc.Path().GetString(),
						NULL, NULL, NULL, NULL, NULL
					);

			return m_error;
		}
		HRESULT     Close (void) {
			m_error << __MODULE__ << S_OK;

			if (this->Is() == false)
				return (m_error = __DwordToHresult(ERROR_INVALID_STATE));

			const BOOL b_result = ::CloseServiceHandle(m_handle);
			if (FALSE == b_result)
				m_error = ::GetLastError();
			else
				m_handle = NULL;

			return m_error;
		}
		TErrorRef   Error (void) const { return m_error; }
		const bool  Is    (void) const { return (NULL != m_handle); }
		HRESULT     Open  (const CService_Man& _man, const TDrvLocate& _loc) {
			m_error << __MODULE__ << S_OK;

			if (this->Is())
				return (m_error = __DwordToHresult(ERROR_ALREADY_EXISTS));

			m_handle = ::OpenServiceW(_man, _loc.Service().GetString(), SERVICE_ALL_ACCESS);
			if (NULL == m_handle)
				m_error = ::GetLastError();

			return m_error;
		}

	public:
		operator const SC_HANDLE (void) const { return m_handle; }
	};

}}}
using namespace ssh::client::_impl;
/////////////////////////////////////////////////////////////////////////////

CDriver:: CDriver(void) : m_dev(INVALID_HANDLE_VALUE) { m_error << __MODULE__ << S_OK; }
CDriver::~CDriver(void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CDriver::Error   (void) const { return m_error; }

bool        CDriver::IsLoaded(void) const {
	m_error << __MODULE__ << S_OK;

	bool b_loaded = false;

	CService_Man svc_man;
	HRESULT hr_ = svc_man.Open();

	if (FAILED(hr_) || svc_man.Is() == false) {
		m_error = svc_man.Error(); return b_loaded;
	}

	CService svc;
	svc.Open(svc_man, m_locate);
	b_loaded = svc.Is();

	return b_loaded;
}

HRESULT     CDriver::Load    (void) {
	m_error << __MODULE__ << S_OK;

	if (this->IsLoaded())
		return (m_error = __DwordToHresult(ERROR_ALREADY_INITIALIZED));

	CService_Man svc_man;
	HRESULT hr_ = svc_man.Open();

	if (FAILED(hr_) || svc_man.Is() == false) {
		m_error = svc_man.Error(); return m_error;
	}


	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////