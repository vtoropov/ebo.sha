#ifndef _EBOFRMMAINVIEWLOG_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
#define _EBOFRMMAINVIEWLOG_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Dec-2019 at 5:44:43p, UTC+7, Novosibirsk, Monday;
	This is Ebo Pack personal account app accountant view interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 3-Mar-2020 at 6:37:23p, UTC+7, Novosibirsk, Tuesday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 3:41:40p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool on 24-May-2020 at 10:37:23p, UTC+7, Novosibirsk, Sunday;
	Adopted to Ebo Sha optima tool on 23-Sep-2020 at 10:45:28p, UTC+7, Novosibirsk, Wednesday;
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.user.sign.h"
#include "ebo.sha.auth.prov.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CLogon : public CView_Base { typedef CView_Base TView;
	private:
		class CLogonWnd : public CViewWnd_Base {
		                 typedef CViewWnd_Base TViewWnd; friend class CLogon;
		private:
			CLogon&   m_view  ;
			TSelect   m_select;
			TSignIn   m_sign  ;
			TAuthProv m_auth  ;

		public:
			 CLogonWnd(CLogon&);
			~CLogonWnd(void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CLogonWnd   m_wnd  ;

	public:
		 CLogon(void);
		~CLogon(void);
#pragma warning(disable:4481)
	private:    // ISelector_Events
		HRESULT    ISelector_OnItemClick  (const UINT _u_itm_id) override sealed;
#pragma warning(default:4481)
	
	public:
		TAuthProv& AuthProv(void) ;
		TSelect&   Select  (void) ;
		TSignIn&   SignIn  (void) ;
	};

}}}}

typedef ebo::sha::gui::view::CLogon  TLogonView;

#endif/*_EBOFRMMAINVIEWLOG_H_8B61F3CD_05E4_4987_A691_7DA7951B472E_INCLUDED*/