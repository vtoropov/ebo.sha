#ifndef __STCTRLSINFOPANELINTERFACE_H_F4397D0D_B36E_46f5_A69F_E8298FE5BF3C_INCLUDED
#define __STCTRLSINFOPANELINTERFACE_H_F4397D0D_B36E_46f5_A69F_E8298FE5BF3C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Oct-2010 at 4:42:43pm, GMT+3, Rostov-on-Don, Saturday;
	This is ST Info Panel control interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 7-Mar-2020 at 4:56:30a, UTC+7, Novosibirsk, Saturday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.border.h"
#include "shared.uix.info.panel.ext.h"
#include "shared.uix.info.panel.fmt.h"
#include "shared.uix.info.panel.lay.h"

namespace ex_ui { namespace frames {

	using shared::sys_core::CError;

	interface IInfoPanelCallback
	{
		virtual void      OnButtonClick   (const UINT UID) PURE;
		virtual HRESULT   OnInfoPanelClose(void)           PURE;
	};

	class CInfoPanel : public TIInfo_Events { friend class ex_ui::frames::CInfoLayout;
	protected:
		TIInfo_Events& m_evt_snk;   // panel owner event sink reference;
		HANDLE         m_wnd_ptr;
		CError         m_error  ;
		UINT           m_ctrl_id;
		TBorders       m_borders;
		TInfoFormat    m_format ;  
		TInfoLayout    m_layout ;

	public:
		 CInfoPanel(TIInfo_Events&);
		~CInfoPanel(void);

	public:
		HRESULT        Create  (const HWND hParent, const RECT& _rc_area, LPCWSTR _lp_sz_cap, const UINT _ctrl_id);
		HRESULT        Destroy (void)      ;
		TErrorRef      Error   (void) const;
		const bool     Is      (void) const;                                // returns true, if the panel window is valid;
		HRESULT        Show    (const ULONG _u_img_id, LPCWSTR _lp_sz_cap); // sets image identifier and message text for displaying by the panel;

	public:
		const
		TBorders&      Borders (void) const;
		TBorders&      Borders (void)      ;
		const
		TInfoFormat&   Format  (void) const;
		TInfoFormat&   Format  (void)      ;
		const
		TInfoLayout&   Layout  (void) const;
		TInfoLayout&   Layout  (void)      ;
		HRESULT        Refresh (void)      ;

	private: // IInfoPanel_Events
		virtual HRESULT   IInfo_OnBkgChanged (void) override;
		virtual HRESULT   IInfo_OnFontChanged(void) override;
		virtual HRESULT   IInfo_OnItemClick  (const UINT _u_itm_id) override;
		virtual HRESULT   IInfo_OnItemImages (const UINT _u_res_id) override;

	private: // non-copyable;
		CInfoPanel (const CInfoPanel&);
		CInfoPanel& operator= (const CInfoPanel&);
	};
}}

typedef ex_ui::frames::CInfoPanel  TInfoPanel;

#endif/*__STCTRLSINFOPANELINTERFACE_H_F4397D0D_B36E_46f5_A69F_E8298FE5BF3C_INCLUDED*/