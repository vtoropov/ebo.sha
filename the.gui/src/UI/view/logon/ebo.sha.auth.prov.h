#ifndef _EBOSHAAUTHPROV_H_ADFA8312_BF57_42CF_999A_A8C124B58004_INCLUDED
#define _EBOSHAAUTHPROV_H_ADFA8312_BF57_42CF_999A_A8C124B58004_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2021 at 12:41:39.071 pm, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool logon screen authenticate provider sub-view interface declaration file;
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.view.empty.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CLogon_Auth : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CLogon_Auth;
		private:
			CLogon_Auth&  m_view  ;
			TTabCtrl      m_tabbed;
			TEmptyView    m_empty ;

		public:
			 CViewWnd (CLogon_Auth&);
			~CViewWnd (void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CLogon_Auth (void);
		~CLogon_Auth (void);

	public:
		TTabCtrl&  Tabbed (void);
	};

}}}}

typedef ebo::sha::gui::view::CLogon_Auth TAuthProv;

#endif/*_EBOSHAAUTHPROV_H_ADFA8312_BF57_42CF_999A_A8C124B58004_INCLUDED*/