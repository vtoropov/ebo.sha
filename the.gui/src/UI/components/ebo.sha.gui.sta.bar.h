#ifndef _EBOSHAGUISTABAR_H_1CBE780F_6585_491C_8029_52E7945E7EFA_INCLUDED
#define _EBOSHAGUISTABAR_H_1CBE780F_6585_491C_8029_52E7945E7EFA_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jan-2021 at 00:44:13.550 am, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool app main form status bar wrapper interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "sfx.button.ctrl.h"
#include "sfx.status.ctrl.h"
#include "ebo.sha.gui.fmt.status.h"

namespace ebo { namespace sha { namespace gui {

	using shared::sys_core::CError;
	using ex_ui::controls::IControlEvent;

	class CStatusBar_Wrap : public ST_Ctrls::IStatusEvents {
	private:
		IControlEvent&
		            m_evt_snk;
		CError      m_error  ;
		TButtonCtrl m_button ;   // child control of status bar for managing trace sub-view visibility;
		TStatusCtrl m_status ;

	public:
		 CStatusBar_Wrap (IControlEvent&);
		~CStatusBar_Wrap (void);

	public:
		TErrorRef   Error(void) const;
		const
		TStatusCtrl&  Ref(void) const;
		TStatusCtrl&  Ref(void) ;

	public:  // life-cycle;
		HRESULT     Create (const HWND _h_parent);
		HRESULT     Destroy(void) ;
		HRESULT     Update (const bool _b_trace) ; // updates control state;

	private: // IStatusEvents
#pragma warning(disable: 4481)
		virtual HRESULT  IStatusEvt_OnAppend (const ST_Ctrls::CPanel& _added) override sealed;
		virtual HRESULT  IStatusEvt_OnFormat (const TStatusFmt&) override sealed;
		virtual HRESULT  IStatusEvt_OnRemove (const DWORD _panel_ndx) override sealed;
#pragma warning(default: 4481)

	private: // non-copyable;
		CStatusBar_Wrap& operator = (const CStatusBar_Wrap&);
		CStatusBar_Wrap (const CStatusBar_Wrap&);
	};

}}}

typedef ebo::sha::gui::CStatusBar_Wrap   TStatusWrap;

#endif/*_EBOSHAGUISTABAR_H_1CBE780F_6585_491C_8029_52E7945E7EFA_INCLUDED*/