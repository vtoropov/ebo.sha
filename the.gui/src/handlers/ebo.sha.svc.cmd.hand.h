#ifndef _EBOSHASVCCMDHAND_H_63558A02_2271_4B2E_9120_6A142F9097C6_INCLUDED
#define _EBOSHASVCCMDHAND_H_63558A02_2271_4B2E_9120_6A142F9097C6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Jan-2021 at 7:36:03.774 pm, UTC+7, Novosibirsk, Friday;
	This is Ebo Pack UM test driver control service command handler interface declaration file.
*/
#include "sys.svc.service.h"
#include "sys.svc.man.h"

#include "driver.svc.h"
#include "driver.svc.crt.h"
#include "driver.svc.props.h"

#include "shared.gen.str.h"

namespace ebo { namespace sha { namespace handlers {

	using namespace shared::service;
	using namespace shared::driver::client;

	class CSvcCmd_Base {
	public:
		enum e_type {
			e_create    = 0, // creates service;
			e_delete    = 1, // deletes service;
			e_start     = 2, // starts  service;
			e_state     = 3, // gets  service state;
			e_stop      = 4, // stops service;
			e_undef     = 5, // command type is undefined;
		};
	protected:
		e_type      m_type   ;
		CSvcError   m_error  ;
		TDrvService m_service;

	protected:
		 CSvcCmd_Base (void);
		 CSvcCmd_Base (const CSvcCmd_Base&);
		~CSvcCmd_Base (void);

	public:
		TErrorRef   Error (void) const;
		e_type      Type  (void) const;

	public:
		const
		TDrvService&   Service (void) const;
		TDrvService&   Service (void)      ;
		
	public:
		CSvcCmd_Base&  operator = (const CSvcCmd_Base&);
	};

	class CSvcCmd_Create : public CSvcCmd_Base { typedef CSvcCmd_Base TBase;
	public:
		 CSvcCmd_Create (void);
		 CSvcCmd_Create (const CSvcCmd_Create&);
		~CSvcCmd_Create (void);

	public:
		HRESULT  Execute(void);
	public:
		 CSvcCmd_Create& operator = (const CSvcCmd_Create&);
	};

	class CSvcCmd_Delete : public CSvcCmd_Base { typedef CSvcCmd_Base TBase;
	public:
		 CSvcCmd_Delete (void);
		 CSvcCmd_Delete (const CSvcCmd_Delete&);
		~CSvcCmd_Delete (void);

	public:
		HRESULT  Execute(void);
	public:
		CSvcCmd_Delete& operator = (const CSvcCmd_Delete&);
	};

	class CSvcCmd_Start : public CSvcCmd_Base { typedef CSvcCmd_Base TBase;
	public:
		 CSvcCmd_Start (void);
		 CSvcCmd_Start (const CSvcCmd_Start&);
		~CSvcCmd_Start (void);

	public:
		HRESULT  Execute(void);
	public:
		CSvcCmd_Start& operator = (const CSvcCmd_Start&);
	};

	class CSvcCmd_State : public CSvcCmd_Base { typedef CSvcCmd_Base TBase;
	public:
		 CSvcCmd_State (void);
		 CSvcCmd_State (const CSvcCmd_State&);
		~CSvcCmd_State (void);

	public:
		HRESULT  Execute(void);
	public:
		CSvcCmd_State& operator = (const CSvcCmd_State&);
	};

	class CSvcCmd_Stop : public CSvcCmd_Base { typedef CSvcCmd_Base TBase;
	public:
		 CSvcCmd_Stop (void);
		 CSvcCmd_Stop (const CSvcCmd_Stop&);
		~CSvcCmd_Stop (void);

	public:
		HRESULT  Execute(void);
	public:
		CSvcCmd_Stop& operator = (const CSvcCmd_Stop&);
	};

	class CTheLastState_Svc {
	public:
		enum e_state {
			e_undefined, e_to_install, e_to_delete, e_to_start, e_to_stop,
			e_to_wait // an operation is in progress; it requires to wait; cannot assign undefined state;
		};
	private:
		CSvcError     m_error;
		e_state       m_current;

	public:
		 CTheLastState_Svc (void);
		 CTheLastState_Svc (const CTheLastState_Svc&);
		~CTheLastState_Svc (void);

	public:
		CStringW      Desc  (void) const; // gets formatted string such as {err_code=%d|err_Desc|preferable action(s)};
		TErrorRef     Error (void) const;
		CSvcError&    Error (void)      ;
		e_state       State (void) const;
		bool          State (const e_state);        //  returns true if changed;
		bool          State (const CSvcCmd_Create&);
		bool          State (const CSvcCmd_Start&);
		bool          State (const CSvcCmd_State&);

	public:
		CTheLastState_Svc& operator = (const CTheLastState_Svc&);
		CTheLastState_Svc& operator <<(const TErrorRef&);
		CTheLastState_Svc& operator <<(const e_state);
		CTheLastState_Svc& operator <<(const CSvcCmd_Create&);
		CTheLastState_Svc& operator <<(const CSvcCmd_Start&);
		CTheLastState_Svc& operator <<(const CSvcCmd_State&);
	};
}}}

typedef ebo::sha::handlers::CSvcCmd_Create       TCmdSvcCreate;
typedef ebo::sha::handlers::CSvcCmd_Delete       TCmdSvcDelete;
typedef ebo::sha::handlers::CSvcCmd_Start        TCmdSvcStart ;
typedef ebo::sha::handlers::CSvcCmd_State        TCmdSvcState ;
typedef ebo::sha::handlers::CSvcCmd_Stop         TCmdSvcStop  ;
typedef ebo::sha::handlers::CSvcCmd_Base::e_type TCmdSvcType  ;

typedef ebo::sha::handlers::CTheLastState_Svc    TSvcLastState;
typedef ebo::sha::handlers::CTheLastState_Svc::e_state    TSvcStateEnum;

#endif/*_EBOSHASVCCMDHAND_H_63558A02_2271_4B2E_9120_6A142F9097C6_INCLUDED*/