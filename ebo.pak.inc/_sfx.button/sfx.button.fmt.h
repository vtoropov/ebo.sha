#ifndef _SFXBUTTONFMT_H_42060911_55CF_4D55_A1F1_8BA2D2726792_INCLUDED
#define _SFXBUTTONFMT_H_42060911_55CF_4D55_A1F1_8BA2D2726792_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Nov-2020 at 9:58:21a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack Sfx button format interface declaration file. 
*/
#include "shared.uix.ctrl.base.fmt.h"
#include "shared.uix.ctrl.defs.h"

namespace ST_Ctrls { namespace format {

	using ex_ui::controls::CBorder ;
	using ex_ui::controls::CBorders;

	using ex_ui::draw::shape::CCorner;
	using ex_ui::draw::shape::CCorners;

	using ex_ui::controls::format::CBkgnd;

	class CButton_Format {
	public:
		class CState {
		private:
			TFmtBase   m_disable;
			TFmtBase   m_hovered;
			TFmtBase   m_normal ;
			TFmtBase   m_pressed;
			TFmtBase   m_default;  // i.e. is selected or highlighted;

		public:
			 CState (void);
			 CState (const CState&);
			~CState (void);

		public:
			const TFmtBase& Default (void) const;
			      TFmtBase& Default (void)      ;
			const TFmtBase& Disable (void) const;
			      TFmtBase& Disable (void)      ;
			const TFmtBase& Hovered (void) const;
			      TFmtBase& Hovered (void)      ;
			const TFmtBase& Normal  (void) const;
			      TFmtBase& Normal  (void)      ;
			const TFmtBase& Pressed (void) const;
			      TFmtBase& Pressed (void)      ;

		public:
			CState&   operator = (const CState&);
		};
	private:
		CState   m_state;
	public:
		 CButton_Format (void);
		 CButton_Format (const CButton_Format&);
		~CButton_Format (void);

	public:
		const
		CState&   State (void) const;
		CState&   State (void)      ;

	public:
		const
		TFmtBase& Current(const ex_ui::controls::CState&) const;
		TFmtBase& Current(const ex_ui::controls::CState&)      ;

	public:
		CButton_Format& operator = (const CButton_Format&);
	};

}}

typedef ST_Ctrls::format::CButton_Format         TButtonFmt  ;
typedef ST_Ctrls::format::CButton_Format::CState TBtnStateFmt;

#endif/*_SFXBUTTONFMT_H_42060911_55CF_4D55_A1F1_8BA2D2726792_INCLUDED*/