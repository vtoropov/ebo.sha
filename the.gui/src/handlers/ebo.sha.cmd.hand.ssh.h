#ifndef _EBOSHACMDHANDSSH_H_76A354A4_2FFB_4352_893C_0C04ECF4EE7C_INCLUDED
#define _EBOSHACMDHANDSSH_H_76A354A4_2FFB_4352_893C_0C04ECF4EE7C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Jan-2021 at 11:45:31.904 pm, UTC+7, Novosibirsk, Wednesday;
	This is Ebo Sha Optima Tool SSH tunnel driver service control handler interface declaration file.
*/
#include "driver.svc.h"
#include "driver.svc.aux.h"
#include "driver.svc.crt.h"
#include "driver.io.h"

namespace ebo { namespace sha { namespace handlers {

	using shared::service::CSvcError;
	using shared::service::CCfg     ;
	using shared::service::CCfg_Ex  ;
	using shared::service::CCfgToStr;


	class CSshDrvCmdHandler {
	private:
		CSvcError         m_error;
		TDrvService       m_svc  ;
		TDrvServiceMan    m_man  ;

	public:
		 CSshDrvCmdHandler (void);
		~CSshDrvCmdHandler (void);

	public:
		TErrorRef   Error  (void) const;
		CStringW    State  (void)      ;

	private: // non-copyable;
		CSshDrvCmdHandler (const CSshDrvCmdHandler&);
		CSshDrvCmdHandler& operator = (const CSshDrvCmdHandler&);
	};

}}}

typedef ebo::sha::handlers::CSshDrvCmdHandler  TSshDrvCmdHandler;

#endif/*_EBOSHACMDHANDSSH_H_76A354A4_2FFB_4352_893C_0C04ECF4EE7C_INCLUDED*/