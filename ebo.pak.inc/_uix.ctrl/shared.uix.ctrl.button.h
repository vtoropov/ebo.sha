#ifndef _UIXCTRLBUTTONCTRL_H_81EC4C24_8073_49ef_9939_3D5C401193D4_INCLUDED
#define _UIXCTRLBUTTONCTRL_H_81EC4C24_8073_49ef_9939_3D5C401193D4_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Feb-2015 at 6:22:03pm, GMT+3, Taganrog, Sunday;
	This is UIX library custom button control class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack constrol library on 21-Nov-2019 at 11:18:39a, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.gdi.draw.defs.h"
#include "shared.uix.ctrl.base.rnd.h"
#include "shared.uix.ctrl.base.lay.h"

namespace ex_ui { namespace controls {

	using ex_ui::controls::IControlEvent;
	using ex_ui::controls::CRenderer_Ex; typedef TCtrlRender_Ex TButtonRnd;

	using ex_ui::draw::defs::IRenderer ;

	class CButton_Layout : public TLayoutBase { typedef TLayoutBase TBase;
	public:
		 CButton_Layout (void);
		~CButton_Layout (void);

	public:
		HRESULT  Update (const RECT& _area) override;

	public:
		CButton_Layout& operator << (const RECT& _area);
	};

	typedef CButton_Layout TBtnLayout;
	//
	// TODO: not good API: this class has *two* instances of control runtime - (1) in button itself, (2) in renderer another one;
	//       such case leads to misunderstanding why button draw funtion works incorrect - different formats are used for setting colors;
	//
	class CButton_Ex {
	protected:
		HANDLE     m_wnd_ptr;
		CControlCrt    m_crt;
		TButtonRnd     m_rnd;
		TBtnLayout     m_lay;

	public:
		 CButton_Ex(IControlEvent&);
		 CButton_Ex(IControlEvent&, IRenderer& parent_rnd);
		~CButton_Ex(void);

	public:
		HRESULT        Create  (const HWND hParent, const LPRECT , const bool bHidden);
		HRESULT        Create  (const HWND hParent, const   RECT&, const bool bHidden);
		HRESULT        Destroy (void);
		const bool     Enabled (void) const;
		HRESULT        Enabled (const bool);
		const bool     Is      (void) const;
		const
		TBtnLayout&    Layout  (void) const;
		TBtnLayout&    Layout  (void)      ;
		HRESULT        Refresh (const bool b_async = false); // refreshes content of a button control window;
		const
		TButtonRnd&    Renderer(void) const;
		TButtonRnd&    Renderer(void)      ;
		const SIZE     ReqSize (void) const;
		const
		CControlCrt&   Runtime (void) const;
		CControlCrt&   Runtime (void)      ;
		HRESULT        Subclass(::ATL::CWindow&);
		HRESULT        Tips    (LPCWSTR)   ;   // sets/removes tooltips of the button;
		ATL::CWindow   Window  (void) const;

	private:
		CButton_Ex(const CButton_Ex&);
		CButton_Ex& operator= (const CButton_Ex&);
	};

	using shared::sys_core::CError;

	class CButton_Set : public IControlEvent {
	protected:
		CError     m_error;

	public:
		 CButton_Set (IControlEvent&);
		 CButton_Set (IControlEvent& , IRenderer& parent_rnd);
		~CButton_Set (void);
#if (0)
	private: // IControlEvent
#pragma warning(disable:4581)
		HRESULT  IControlEvent_OnClick(const UINT ctrlId) override;
		HRESULT  IControlEvent_OnClick(const UINT ctrlId, const LONG_PTR nData) override;
#pragma warning(default:4581)
#endif
	};
}}

#endif/*_UIXCTRLBUTTONCTRL_H_81EC4C24_8073_49ef_9939_3D5C401193D4_INCLUDED*/