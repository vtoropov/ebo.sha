/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Jan-2021 at 11:11:14.098 pm, UTC+7, Novosibirsk, Sunday;
	This is Ebo Sha Optima Tool app main form status bar format interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.gui.fmt.status.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::format;

/////////////////////////////////////////////////////////////////////////////

TStatusBarFmt:: CStatusBar_Format (void) {}
TStatusBarFmt::~CStatusBar_Format (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   TStatusBarFmt::ApplyTo (ex_ui::controls::format::CBorder_Ex& _border) const { HRESULT hr_ = S_OK;
	_border;
	// temporary border colors for dark theme are defined here:
	const COLORREF clr_border_light = RGB(42, 45, 47);
	const COLORREF clr_border_dark  = RGB(92, 95, 97);

	_border.Color().Create(clr_border_light);
	_border.Shadow().Create(clr_border_dark);
	_border.Thickness() = 1;

	return hr_;
}

HRESULT   TStatusBarFmt::ApplyTo (ST_Ctrls::CStatusBar& _status) const { HRESULT hr_ = S_OK;
	_status;
	_status.Format() = ST_Ctrls::format::CSymantec_NA_Sta(); // TODO: assigning predefined format schema must be re-viewed or improved;

	// temporary border colors for dark theme are defined here:
	const COLORREF clr_border_light = RGB(42, 45, 47);
	const COLORREF clr_border_dark  = RGB(92, 95, 97);

	ex_ui::controls::layout::CImage temp_img;
	temp_img.Size().cx = temp_img.Size().cy = 24;

	temp_img.Margins().Left() = 7;
	temp_img.Margins().Top ()  = 3;
	temp_img.Margins().Right()  = 5;

	_status.Format().Images() = IDR_EBO_SHA_STA_IMG;
	_status.Panels().Add(1, _T("Ready"));
	_status.Panels().Raw()[0].Image_Ndx() =  0 ;
	_status.Panels().Raw()[0].Layout().Image() = temp_img;
	_status.Panels().Raw()[0].Layout().Style().Width() = TLayStyle::e_width::e_auto;

	_status.Panels().Add(2, NULL); // for managing trace view visibility; a button will be added here;
	_status.Panels().Raw()[1].Image_Ndx() = -1 ;
	_status.Panels().Raw()[1].Layout().Style().Width() = TLayStyle::e_width::e_fixed;
	_status.Panels().Raw()[1].Layout().Fixed() = 180;
	_status.Panels().Raw()[1].Layout().Style().Stick() = TLayStyle::e_stick::e_right;
	_status.Panels().Raw()[1].Layout().Text ().Margins().Left () =
	_status.Panels().Raw()[1].Layout().Text ().Margins().Right() = 7;
	_status.Panels().Raw()[1].Layout().Text ().Align().Horz() = THorzAlign::eCenter;

	this->ApplyTo(_status.Panels().Raw()[1].Borders().Left());
	this->ApplyTo(_status.Panels().Raw()[1].Borders().Right());

	_status.Layout().Height() = 30;
	_status.Layout().Glyph ().Image_Ndx() =  2;
	_status.Layout().Glyph ().Margins().Left() = 15; _status.Layout().Glyph ().Size().cx = _status.Layout().Glyph ().Size().cy = 24;

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

const TStatusBarFmt& TStatusBarFmt::operator >> (ST_Ctrls::CStatusBar& _status) const { this->ApplyTo(_status); return *this; }