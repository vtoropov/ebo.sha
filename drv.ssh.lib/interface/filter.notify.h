#ifndef _FILTERNOTIFY_H_573551B2_B3BF_469D_ABC4_45C19B07839C_INCLUDED
#define _FILTERNOTIFY_H_573551B2_B3BF_469D_ABC4_45C19B07839C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Aug-2020 at 11:10:25p, UTC+7, Novosibirsk, Sunday;
	This is SSH Tunnel WFP filter notification interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 8:09:37.330 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "filter.defs.h"

namespace ssh { namespace driver {

	enum class CAction : ULONG {
		block      = 0,
		allow_exp  = 1, // explicit allow (ie allow list);
		allow      = 2, // allow (not blocked);
	};

	typedef struct tag_notification {
		ULONG        cmd_id;
		SSH_TNL_ADDR source;
		SSH_TNL_ADDR dest  ;
		ULONG        action;
	} NOTIFICATION ;

	typedef struct _irp_queue {
		KSPIN_LOCK lock;
		LIST_ENTRY irp_list;
		LIST_ENTRY ntf_list;
		NPAGED_LOOKASIDE_LIST lookaside;
		UINT queued;
	} NOTIFICATION_QUEUE;

	NTSTATUS InitNotificationQueue   (NOTIFICATION_QUEUE*);
	NTSTATUS DestroyNotificationQueue(NOTIFICATION_QUEUE*);

	NTSTATUS Notification_Send   (NOTIFICATION_QUEUE*, const NOTIFICATION*);
	NTSTATUS Notification_Recieve(NOTIFICATION_QUEUE*, PIRP);


	static VOID  Notification_OnCancel(PDEVICE_OBJECT _p_device, PIRP _p_irp);

}}

typedef ssh::driver::NOTIFICATION        t_raw_notify;
typedef ssh::driver::NOTIFICATION_QUEUE  t_ntf_queue ;

#endif/*_FILTERNOTIFY_H_573551B2_B3BF_469D_ABC4_45C19B07839C_INCLUDED*/