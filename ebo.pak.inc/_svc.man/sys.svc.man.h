#ifndef _SHAREDSYSTEMSERVICEMAN_H_A9A4D670_9050_4562_9D73_9457363B8347_INCLUDED
#define _SHAREDSYSTEMSERVICEMAN_H_A9A4D670_9050_4562_9D73_9457363B8347_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jun-2016 at 2:02:10p, UTC+7, Phuket, Rawai, Monday;
	This is Shared system service manager wrapper class declaration file.
	( project: thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 13-Jul-2020 at 9:46:02a, UTC+7, Novosibirsk, Monday;
*/
#include "sys.svc.error.h"
#include "sys.svc.crt.h"
#include "sys.svc.service.h"

namespace shared { namespace service {
	// https://docs.microsoft.com/en-us/windows/win32/services/service-security-and-access-rights
	class CManagerParams {
	public:
		enum _e : DWORD {
			eOpenForService = SC_MANAGER_ALL_ACCESS,
			eOpenForEnum    = SC_MANAGER_ENUMERATE_SERVICE
		};
	};

	class CManager_Handle {
	private:
		mutable
		CSvcError     m_error  ;
		SC_HANDLE     m_handle ;

	public:
		 CManager_Handle (void);
		 CManager_Handle (const SC_HANDLE);
		~CManager_Handle (void);

	public:
		HRESULT       Attach (const SC_HANDLE);
		HRESULT       Close  (void) ;
		SC_HANDLE     Detach (void) ;
		TErrorRef     Error  (void) const;
		const bool    Is     (void) const;
		HRESULT       Open   (const DWORD _dw_access = SC_MANAGER_ALL_ACCESS) ;

	public:
		CManager_Handle& operator << (const SC_HANDLE);  // attach a handle;
		CManager_Handle& operator >> (SC_HANDLE&);       // detach a handle;

	public:
		operator const bool (void) const;
		operator const SC_HANDLE& (void) const;

	public:
		static bool Is_acceptable(const SC_HANDLE);

	private: // noncopyable;
		CManager_Handle (CManager_Handle&);
		CManager_Handle& operator= (const CManager_Handle&);
	};
	//
	// https://docs.microsoft.com/en-us/windows/win32/services/service-security-and-access-rights
	// GENERIC_READ - to get service current state;
	//
	typedef CManager_Handle TSCHandle;
	//
	// *Note*: sample of deleting a service:
	//         https://docs.microsoft.com/en-us/windows/win32/services/deleting-a-service
	//         lies regarding open service security right - when DELETE is specified, access denied error is returned;
	//         the right way is to specify SERVICE_ALL_ACCESS security level for getting success;
	//
	class CManager {
	private:
		mutable
		CSvcError   m_error ;
		TSCHandle   m_handle;

	public:
		 CManager(void);
		~CManager(void);

	public:
		HRESULT     Create (const CCrtData&, CService&);
		HRESULT     Delete (LPCWSTR _lp_sz_svc_name);
		TErrorRef   Error  (void) const;
		bool        Is     (void) const;
		HRESULT     Open   (const CCrtData&, const DWORD dwAccess, CService&);
		HRESULT     Open   (LPCWSTR lpszServiceName , const DWORD dwAccess, CService&);
		HRESULT     Start  (LPCWSTR lpszServiceName);
		HRESULT     Stop   (LPCWSTR lpszServiceName);
		
	public:
		const
		TSCHandle&  Handle(void) const;
		TSCHandle&  Handle(void)      ;
		operator SC_HANDLE(void) const;

	private: // copy protected;
		CManager(const CManager&);
		CManager&  operator= (const CManager&);

	public:
		CManager&  operator= (const SC_HANDLE);
	};
}}

typedef shared::service::CManager   TServiceMan;

#endif/*_SHAREDSYSTEMSERVICEMAN_H_A9A4D670_9050_4562_9D73_9457363B8347_INCLUDED*/