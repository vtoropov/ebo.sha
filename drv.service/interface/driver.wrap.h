#ifndef _DRIVERWRAP_H_D36984D3_8093_4E07_A7A3_E37AF4ED91E5_INCLUDED
#define _DRIVERWRAP_H_D36984D3_8093_4E07_A7A3_E37AF4ED91E5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Aug-2020 at 0:37:58a, UTC+7, Novosibirsk, Wednesday;
	This is SSH-Tunnel WFP driver wrapper interface declaration file; 
*/
#include "shared.gen.sys.err.h"
#include "driver.dev.loc.h"
namespace ssh { namespace client {

	using shared::sys_core::CError;

	class CDriver {
	private:
		mutable
		CError      m_error;
		HANDLE      m_dev  ;
		CDrvLocator m_locate;

	public:
		 CDriver (void);
		~CDriver (void);

	public:
		TErrorRef   Error    (void) const;
		bool        IsLoaded (void) const;
		HRESULT     Load     (void)      ;
		const
		TDrvLocate& Locate   (void) const;
		TDrvLocate& Locate   (void)      ;
		HRESULT     Unload   (void)      ;

	private: // noncopyable;
		CDriver (const CDriver&);
		CDriver& operator = (const CDriver&);
	};

}}

#endif/*_DRIVERWRAP_H_D36984D3_8093_4E07_A7A3_E37AF4ED91E5_INCLUDED*/