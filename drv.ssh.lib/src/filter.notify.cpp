/*
	Created by Tech_dog (ebontrop@gmail.com) on 2-Aug-2020 at 11:22:50p, UTC+7, Novosibirsk, Sunday;
	This is SSH Tunnel WFP filter notification interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 8:09:37.338 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "filter.notify.h"
#include "filter.dev.ext.h"

using namespace ssh::driver;

/////////////////////////////////////////////////////////////////////////////

typedef struct __ssh_notifynode {
	LIST_ENTRY    entry;
	t_raw_notify* p_notification;
} NOTIFYNODE;

typedef struct __ssh_irpnode {
	LIST_ENTRY entry;
	PIRP p_irp;
} IRPNODE;

typedef union tag_LISTNODE {
	NOTIFYNODE ntf_node;
	IRPNODE    irp_node;
} LISTNODE;

/////////////////////////////////////////////////////////////////////////////

namespace ssh { namespace driver {

NTSTATUS  InitNotificationQueue(NOTIFICATION_QUEUE* _p_queue) {

	NTSTATUS nt_status = S_OK;

	if (NULL == _p_queue)
		return (nt_status = STATUS_INVALID_ADDRESS);

	::InitializeListHead(&_p_queue->irp_list);
	::InitializeListHead(&_p_queue->ntf_list);

	::ExInitializeNPagedLookasideList(&_p_queue->lookaside, NULL, NULL, 0, sizeof(LISTNODE), AUX_LIST_NODE_TAG, 0);
	::KeInitializeSpinLock(&_p_queue->lock);
	_p_queue->queued = 0;

	return nt_status;
}

NTSTATUS  DestroyNotificationQueue(NOTIFICATION_QUEUE* _p_queue) {
	
	NTSTATUS nt_status = S_OK;

	if (NULL == _p_queue)
		return (nt_status = STATUS_INVALID_ADDRESS);

	while(!::IsListEmpty(&_p_queue->ntf_list))
	{
		NOTIFYNODE* p_node = (NOTIFYNODE*)::RemoveHeadList(&_p_queue->ntf_list);
		::ExFreeToNPagedLookasideList(&_p_queue->lookaside, p_node);
	}

	while(!::IsListEmpty(&_p_queue->irp_list))
	{
		IRPNODE* p_node = (IRPNODE*)::RemoveHeadList(&_p_queue->irp_list);

		p_node->p_irp->IoStatus.Status = STATUS_CANCELLED;
		p_node->p_irp->IoStatus.Information = 0;

		IoCompleteRequest(p_node->p_irp, IO_NO_INCREMENT);

		::ExFreeToNPagedLookasideList(&_p_queue->lookaside, p_node);
	}

	::ExDeleteNPagedLookasideList(&_p_queue->lookaside);

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS  Notification_Recieve(NOTIFICATION_QUEUE* _p_queue, PIRP _p_irp) {

	NTSTATUS nt_status = S_OK;

	if (NULL == _p_queue ) return (nt_status = STATUS_INVALID_PARAMETER_1);
	if (NULL == _p_irp) return (nt_status = STATUS_INVALID_PARAMETER_2);

	PIO_STACK_LOCATION irpstack = IoGetCurrentIrpStackLocation(_p_irp);
	KIRQL irq = 0;

	if (irpstack->Parameters.DeviceIoControl.OutputBufferLength != sizeof(t_raw_notify)) {

		_p_irp->IoStatus.Status = STATUS_BUFFER_TOO_SMALL;
		_p_irp->IoStatus.Information = 0;
		IoCompleteRequest(_p_irp, IO_NO_INCREMENT);

		return (nt_status = STATUS_BUFFER_TOO_SMALL);
	}

	KeAcquireSpinLock(&_p_queue->lock, &irq);

	if (::IsListEmpty(&_p_queue->ntf_list)) {

		IRPNODE* p_node = NULL;
		KIRQL crirq = 0;

		p_node = (IRPNODE*)::ExAllocateFromNPagedLookasideList(&_p_queue->lookaside);

		InitializeListHead(&p_node->entry);

		p_node->p_irp = _p_irp;

		::InsertTailList(&_p_queue->irp_list, &p_node->entry);
		::IoMarkIrpPending(_p_irp);
		::IoAcquireCancelSpinLock(&crirq);

#pragma warning(push)
#pragma warning(disable:4311 4312)
		//IoSetCancelRoutine generates warnings in 32-bit due to silly macroisms;
		::IoSetCancelRoutine(_p_irp, Notification_OnCancel);
#pragma warning(pop)

		::IoReleaseCancelSpinLock(crirq);
		::KeReleaseSpinLock(&_p_queue->lock, irq);

		nt_status = STATUS_PENDING;
	}
	else {
		NOTIFYNODE* p_node = (NOTIFYNODE*)::RemoveHeadList(&_p_queue->ntf_list);
		t_raw_notify* p_buffer = (t_raw_notify*)_p_irp->AssociatedIrp.SystemBuffer;

		RtlCopyMemory(p_buffer, &p_node->p_notification, sizeof(t_raw_notify));
		::ExFreeToNPagedLookasideList(&_p_queue->lookaside, p_node);
		--_p_queue->queued;

		::KeReleaseSpinLock(&_p_queue->lock, irq);

		_p_irp->IoStatus.Status = STATUS_SUCCESS;
		_p_irp->IoStatus.Information = sizeof(t_raw_notify);
		IoCompleteRequest(_p_irp, IO_NO_INCREMENT);

		nt_status = STATUS_SUCCESS;
	}
	return nt_status;
}

NTSTATUS  Notification_Send(NOTIFICATION_QUEUE* _p_queue, const t_raw_notify* _p_notify) {

	NTSTATUS nt_status = S_OK;

	if (NULL == _p_queue ) return (nt_status = STATUS_INVALID_PARAMETER_1);
	if (NULL == _p_notify) return (nt_status = STATUS_INVALID_PARAMETER_2);

	KIRQL irq = 0;

	KeAcquireSpinLock(&_p_queue->lock, &irq);

	if (::IsListEmpty(&_p_queue->irp_list)) {

		if (_p_queue->queued < 64) {

			NOTIFYNODE* p_node = (NOTIFYNODE*)::ExAllocateFromNPagedLookasideList(&_p_queue->lookaside);

			::InitializeListHead(&p_node->entry);
			RtlCopyMemory(&p_node->p_notification, _p_notify, sizeof(t_raw_notify));

			InsertTailList(&_p_queue->ntf_list, &p_node->entry);
			++_p_queue->queued;
		}

		::KeReleaseSpinLock(&_p_queue->lock, irq);
	}
	else {

		IRPNODE* p_node = (IRPNODE*)::RemoveHeadList(&_p_queue->irp_list);

		t_raw_notify* p_notify = (t_raw_notify*)p_node->p_irp->AssociatedIrp.SystemBuffer;
		PIRP p_irp = NULL;

		RtlCopyMemory(p_notify, _p_notify, sizeof(t_raw_notify));

		p_irp = p_node->p_irp;

		::ExFreeToNPagedLookasideList(&_p_queue->lookaside, p_node);

		::KeReleaseSpinLock(&_p_queue->lock, irq);

		p_irp->IoStatus.Status = STATUS_SUCCESS;
		p_irp->IoStatus.Information = sizeof(t_raw_notify);

		IoCompleteRequest(p_irp, IO_NO_INCREMENT);
	}

	return nt_status;
}

/////////////////////////////////////////////////////////////////////////////

VOID Notification_OnCancel(PDEVICE_OBJECT _p_device, PIRP _p_irp) {

	if (NULL == _p_device || NULL == _p_irp) return;

	KIRQL irq = 0;
	NOTIFICATION_QUEUE* p_queue = &((t_dev_ext*)_p_device->DeviceExtension)->queue;
	LIST_ENTRY* iter = NULL;
	INT found = 0;

	KeAcquireSpinLock(&p_queue->lock, &irq);

	for(iter = p_queue->irp_list.Flink; iter != &p_queue->irp_list; iter = iter->Flink)
	{
		IRPNODE* p_node = (IRPNODE*)iter;
		if (p_node->p_irp == _p_irp)
		{
			::RemoveEntryList(iter);
			::ExFreeToNPagedLookasideList(&p_queue->lookaside, p_node);
			found = 1;
			break;
		}
	}
	::KeReleaseSpinLock(&p_queue->lock, irq);

	// if it wasn't found, it has already been dequeued and handled.
	if (found) {
		
		_p_irp->IoStatus.Status = STATUS_CANCELLED;
		_p_irp->IoStatus.Information = 0;
		IoCompleteRequest(_p_irp, IO_NO_INCREMENT);
	}

	::IoReleaseCancelSpinLock(_p_irp->CancelIrql);
}

}}