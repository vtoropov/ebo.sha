/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Dec-2018 at 12:15:54p, UTC+7, Novosibirsk, Sunday;
	This is USB Drive Detective desktop (bitsphereinc.com) app main form view manager interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 29-Oct-2019 at 9:56:38p, UTC+7, Novosibirsk, Tuesday;
	Adopted to Ebo Sha optima tool on 21-Sep-2020 at 0:13:53a, UTC+7, Novosibirsk, Monday;
*/
#include "StdAfx.h"
#include "ebo.sha.gui.view.man.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

#include "ebo.sha.gui.frm.lay.h"

/////////////////////////////////////////////////////////////////////////////

CViews:: CViews (void) : m_active(NULL) { ::memset(&m_views, 0, sizeof(m_views)); m_error << __MODULE__ << S_OK >> __MODULE__; this->Create(); }
CViews::~CViews (void) { this->Destroy(); }

/////////////////////////////////////////////////////////////////////////////

HRESULT   CViews::Create  (void) { m_error << __MODULE__ << S_OK;
	for (INT i_ = 0; i_ < _countof(m_views); i_++) {
		try {
			if (TViewType::e_fake  == i_) m_views[i_] = new TFakeView();
			if (TViewType::e_home  == i_) m_views[i_] = new TMainView();
			if (TViewType::e_logon == i_) m_views[i_] = new CLogon();
			if (TViewType::e_media == i_) m_views[i_] = new CMedia();
			if (TViewType::e_block == i_) m_views[i_] = new CBlock();
			if (TViewType::e_sets  == i_) m_views[i_] = new TSettings();
			if (TViewType::e_help  == i_) m_views[i_] = new THelp();
		}
		catch (const ::std::bad_alloc&) {
			m_error << E_OUTOFMEMORY;
		} 
	}
	return m_error;
}
HRESULT   CViews::Destroy (void) { m_error << __MODULE__ << S_OK;
	for (INT i_ = 0; i_ < _countof(m_views); i_++) {
		if (NULL ==  m_views[i_])
			continue;
		if (TViewType::e_fake  == i_) { delete dynamic_cast<TFakeView*>(m_views[i_]); }
		if (TViewType::e_home  == i_) { delete dynamic_cast<TMainView*>(m_views[i_]); }
		if (TViewType::e_logon == i_) { delete dynamic_cast<CLogon*>(m_views[i_]); }
		if (TViewType::e_media == i_) { delete dynamic_cast<CMedia*>(m_views[i_]); }
		if (TViewType::e_block == i_) { delete dynamic_cast<CBlock*>(m_views[i_]); }
		if (TViewType::e_sets  == i_) { delete dynamic_cast<TSettings*>(m_views[i_]); }
		if (TViewType::e_help  == i_) { delete dynamic_cast<THelp*>(m_views[i_]); }
		
		m_views[i_] = NULL;
	}

	return m_error;
}

TErrorRef  CViews::Error   (void) const { return m_error; }

/////////////////////////////////////////////////////////////////////////////

DWORD      CViews::Count   (void) const { return _countof(m_views); }
CView_Base* const CViews::View (const DWORD _ndx) const { if (_ndx < this->Count()) return m_views[_ndx]; else return NULL; }

/////////////////////////////////////////////////////////////////////////////

CHandler:: CHandler(CManager& _man) : m_man_ref(_man) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CHandler::~CHandler(void) {}

/////////////////////////////////////////////////////////////////////////////

bool       CHandler::Accept(const WORD _cmd_id) const {

	bool b_handled = false;

	for (DWORD i_ = 0; i_ < m_man_ref.Views().Count(); i_++) {
		if (m_man_ref.Views().View(i_))
			b_handled = m_man_ref.Views().View(i_)->CanAccept(_cmd_id);
		if (b_handled) break;
	}
	return  b_handled;
}

HRESULT    CHandler::Do    (const WORD _cmd_id) {
	m_error << __MODULE__ << S_OK;

	if (this->Accept(_cmd_id) == false)
		return (m_error = (DWORD)ERROR_INVALID_OPERATION);

	for (DWORD i_ = 0; i_ < m_man_ref.Views().Count(); i_++) {
		if (m_man_ref.Views().View(i_) &&
			m_man_ref.Views().View(i_)->CanAccept(_cmd_id)) {
			if (FAILED(m_man_ref.Views().View(i_)->OnCommand(_cmd_id))) {
				m_error = m_man_ref.Views().View(i_)->Error(); break;
			}
		}
	}
	
	return  m_error;
}

TErrorRef  CHandler::Error (void) const { return m_error; }

/////////////////////////////////////////////////////////////////////////////

CManager:: CManager(void) : m_hand(*this) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CManager::~CManager(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CManager::Create (const HWND hParent, const RECT& _area) {
	m_error << __MODULE__ << S_OK;

	if (NULL == hParent || !::IsWindow(hParent)) return (m_error = OLE_E_INVALIDHWND);
	if (::IsRectEmpty(&_area)) return (m_error = OLE_E_INVALIDRECT);

	HRESULT hr_ = m_error;

	for (DWORD i_ = 0; i_ < m_views.Count(); i_++) {
		if (NULL ==  m_views.View(i_))
			continue;
		hr_ = m_views.View(i_)->Create(hParent, _area, false); if (FAILED(hr_)) { m_error = m_views.View(i_)->Error(); break; }
	}

	return m_error;
}

HRESULT    CManager::Destroy(void) {
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = m_error;

	for (DWORD i_ = 0; i_ < m_views.Count(); i_++) {
		if (NULL ==  m_views.View(i_))
			continue;
		hr_ = m_views.View(i_)->Destroy(); if (FAILED(hr_)) m_views.View(i_)->Error().Show();
	}

	m_trace.Destroy();

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

TViewType  CManager::Active (void) const  {
	for (DWORD i_ = 0; i_ < m_views.Count(); i_++)
		if (NULL != m_views.View(i_) && m_views.View(i_)->IsVisible())
			return (CViewType::_tp)i_;
	return CViewType::e_none;
}
HRESULT    CManager::Active (const TViewType _tp){
	m_error << __MODULE__ << S_OK;

	m_views.m_active = NULL;
	if (_tp != CViewType::e_none)
		m_views.m_active = m_views.View(_tp);
	

	if (m_views.m_active)
		m_views.m_active->IsVisible(true);
	for (DWORD i_ = 0; i_ < m_views.Count(); i_++) {
		if (m_views.View(i_) == NULL) continue;
		if (m_views.View(i_) != m_views.m_active)
			m_views.View(i_)->IsVisible(false);
	}

	return m_error;
}

CView_Base* const CManager::Active_Ptr (void) const { return m_views.m_active; }

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CManager::Error   (void) const { return m_error; }
const
THandler&   CManager::Handler (void) const { return m_hand ; }
THandler&   CManager::Handler (void)       { return m_hand ; }
TTraceView& CManager::Trace   (void)       { return m_trace; }
const
CViews&     CManager::Views   (void) const { return m_views; }

/////////////////////////////////////////////////////////////////////////////

HRESULT    CManager::Redraw   (void) {
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = m_error;

	for (DWORD i_ = 0; i_ < m_views.Count(); i_++) {
		if (NULL== m_views.View(i_))
			continue;
		hr_ = m_views.View(i_)->Refresh();
		if (FAILED(hr_))
			m_error = m_views.View(i_)->Error();
	}

	return m_error;
}

HRESULT    CManager::Trigger  (const CViewType::_tp _view) {
	m_error << __MODULE__ << S_OK;
	// it is very important to keep a specific sequence in managing views' visibility:
	// a view being made visible must come first, otherwise, main window background can appear at a moment;
	return this->Active(_view);
}

bool       CManager::Visible  (void) const { return (m_views.m_active != NULL); }
HRESULT    CManager::Update   (const bool _b_allow) { HRESULT hr_ = S_OK;
	_b_allow;
	if (NULL == m_views.m_active)
		return (hr_ = E_NOT_VALID_STATE);

	m_views.m_active->Window().LockWindowUpdate(static_cast<BOOL>(!_b_allow));

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CManager::IControlEvent_OnClick(const UINT ctrlId) { HRESULT hr_ = S_OK;
	ctrlId;
	if (m_trace.IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	hr_ = m_trace.IsVisible(!m_trace.IsVisible());
	if (SUCCEEDED(hr_))
		hr_ = shared::Get_AppUI().IAppUI_ExecCommand(IDC_EBO_SHA_UPDATE_LAYOUT);

	return hr_;
}