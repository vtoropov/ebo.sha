#ifndef _SYSSVCSETUP_H_20A95C2A_6F69_48CA_AA52_F61CF5AEE0C5_INCLUDED
#define _SYSSVCSETUP_H_20A95C2A_6F69_48CA_AA52_F61CF5AEE0C5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Jul-2020 at 5:44:36a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack system service control management setup interface declaration file.
*/
#include "sys.svc.error.h"
#include "sys.svc.crt.h"
#include "sys.svc.service.h"
namespace shared { namespace service {
	using shared::sys_core::CError;

	class CSetup {
	public:
		class CState {
		public:
			enum _value {
				eNotInstalled =  0x0,
				eInstalled    =  0x1,
			};

		private:
			_value   m_current;

		public:
			 CState (void);
			 CState (const _value _curr);
			 CState (const CState&);
			~CState (void);

		public:
			_value     Current (void) const;
			_value&    Current (void)      ;

			CAtlStringW AsText (void) const;

		public:
			CState& operator = (const _value  );
			CState& operator = (const CState& );
		};
	private:
		mutable
		CError   m_error;
		CState   m_state;

	public:
		 CSetup(void);
		 CSetup(const CSetup&);
		 CSetup(const CSetup::CState::_value _state);
		~CSetup(void);

	public:
		// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-openservicew
		// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-openscmanagerw
		// https://docs.microsoft.com/en-us/windows/win32/services/service-security-and-access-rights
		TErrorRef Error (void) const;
		HRESULT   Is    (LPCWSTR lpszServiceName) const;
		HRESULT   Is    (const CCrtData&) const;
		HRESULT   Is    (const CCrtData&, CService& _result) const;
		const
		CState&   State (void) const;
		CState&   State (void)      ;

	public:
		CSetup& operator = (const CSetup&);
	};

	typedef ::std::vector<CSetup> TSvcSetups;
	typedef const CSetup&       TSvcSetupRef;
#if (0)
	class CSetupEnum {
	public:
		 CSetupEnum (void);
		~CSetupEnum (void);
	public:
		INT           Count(void) const;
		const
		TSvcSetups&   Raw  (void) const;
		TSvcSetupRef  Setup(const INT nIndex) const;
	};
#endif
}}

typedef shared::service::CSetup::CState TSvcSetupState;

#endif/*_SYSSVCSETUP_H_20A95C2A_6F69_48CA_AA52_F61CF5AEE0C5_INCLUDED*/