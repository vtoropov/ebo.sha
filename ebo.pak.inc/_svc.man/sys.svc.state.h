#ifndef _SYSSVCSTATE_H_0FA4E18F_97DF_4E5F_AC7F_87CA3B0EEAC0_INCLUDED
#define _SYSSVCSTATE_H_0FA4E18F_97DF_4E5F_AC7F_87CA3B0EEAC0_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Jul-2020 at 8:20:33a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack system service control management service's state interface declaration file.
*/
#include "sys.svc.error.h"

namespace shared { namespace service {

	// sets service handle and refreshes service current state data;
	class CState {
	public:
		enum _state : DWORD {
			eUNDEF  = 0x0,
			eSERVICE_STOPPED          = SERVICE_STOPPED         ,
			eSERVICE_START_PENDING    = SERVICE_START_PENDING   ,
			eSERVICE_STOP_PENDING     = SERVICE_STOP_PENDING    ,
			eSERVICE_RUNNING          = SERVICE_RUNNING         ,
			eSERVICE_CONTINUE_PENDING = SERVICE_CONTINUE_PENDING,
			eSERVICE_PAUSE_PENDING    = SERVICE_PAUSE_PENDING   ,
			eSERVICE_PAUSED           = SERVICE_PAUSED          ,
		};
	private:
		_state       m_state;   // current state value;
		CSvcError    m_error;
	public:
		 CState(void);
		 CState(const CState&);
		 CState(const SC_HANDLE _p_svc_handle);
		~CState(void);

	public:
		_state      Current  (void) const ;
		TErrorRef   Error    (void) const ;
		const bool  Is       (void) const ;      // return true if the current state value is defined;
		HRESULT     Query    (const SC_HANDLE _p_svc_handle); // sets service handle and refreshes service current state data;
		HRESULT     Reset    (void);

	public:
		bool        Pending  (void) const ;      // transitive state when service status control operation in pending state;
		bool        Running  (void) const ;
		bool        Stopped  (void) const ;

	public:
		CState&  operator  = (const CState&  );
		CState&  operator << (const SC_HANDLE);  // sets service handle and refreshes service current state data;
	public:
		operator const _state(void) const;
	};

}}

typedef       shared::service::CState::_state TSvcStateValue;
typedef const shared::service::CState&        TSvcStateRef;

#endif/*_SYSSVCSTATE_H_0FA4E18F_97DF_4E5F_AC7F_87CA3B0EEAC0_INCLUDED*/