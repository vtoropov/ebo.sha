#ifndef __OWNERDRAW_GDIPLUS_WRAP_H_
#define __OWNERDRAW_GDIPLUS_WRAP_H_
/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Aug-2009 at 7:03:18p, GMT+3, Rostov-on-Don, Sunday;
	This is Sfx Pack GDI+ library owner draw wrapper class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 3-May-2020 at 0:33:05a, UTC+7, Novosibirsk, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw {

	using shared::sys_core::CError;

	class CFontOptions {
	public:
		enum _opt : ULONG {
			eNone          = 0x0000, // creates font with default (i.e. system height/bold/italic)
			eExactSize     = 0x0001, // creates font with specified size (irrelative)
			eRelativeSize  = 0x0002, // creates font with relative size (reduction/increasing)
			eBold          = 0x0004,
			eItalic        = 0x0008,
			eUnderline     = 0x0010,
		};
	protected:
		DWORD     m_opts;
	public:
		 CFontOptions (void);
		 explicit CFontOptions (const DWORD _opts);
		 explicit CFontOptions (const CFontOptions&);
		~CFontOptions (void);

	public:
		DWORD   Get (void) const;
		bool    Has (const DWORD _opts) const;
		DWORD&  Set (void)      ;

	public:
		CFontOptions&  operator = (const DWORD _opts);        // sets options value;
		CFontOptions&  operator = (const CFontOptions&);      // sets options value;
		CFontOptions&  operator+= (const DWORD _opts);        // adds options;
		CFontOptions&  operator-= (const DWORD _opts);        // excludes options;

	public:
		const bool     operator == (const DWORD _opts) const;          // returns true if options include value provided, otherwise, false;
		const bool     operator != (const DWORD _opts) const;          // returns true if options exclude value provided, otherwise, false;
		operator const DWORD (void) const;
	};
}}

typedef ex_ui::draw::CFontOptions  TFontOpts;

namespace ex_ui { namespace draw {

	class CFont_Base {
	protected:
		HFONT     m_handle;   // font handle

	protected:
		 CFont_Base (void);
		~CFont_Base (void);

	public:
		HRESULT   Destroy(void);
		HFONT     Detach (void);
		HFONT     Handle (void) const;
		bool      Is     (void) const;
	public:
		operator
		const     HFONT  (void) const;
	private:
		CFont_Base(const CFont_Base&);
		CFont_Base& operator= (const CFont_Base&);
	};
	//  https://docs.microsoft.com/en-us/windows/win32/api/wingdi/ns-wingdi-logfontw
	class CFont : public CFont_Base {
	             typedef CFont_Base TBase;
	private:
		bool      m_bManaged; // flag that indicates font handle must be destroyed (it the font is not stock one)
	public:
		 CFont(LPCTSTR pszFamily = NULL, const DWORD dwOptions = CFontOptions::eNone, const LONG lParam = 0);
		~CFont(void);
	public:
		HRESULT   Create (LPCTSTR pszFamily = NULL, const DWORD dwOptions = CFontOptions::eNone, const LONG lParam = 0);
		HFONT     Detach (void);
	};

	class CFontScalable : public CFont_Base {
	                     typedef CFont_Base TBase;
	public:
		 CFontScalable(const HDC, LPCTSTR lpszFontFamily, const INT nSize, const DWORD dwOptions = CFontOptions::eNone);
		~CFontScalable(void);

	public:
		HRESULT   Create (const HDC, LPCTSTR lpszFontFamily, const INT nSize, const DWORD dwOptions = CFontOptions::eNone);
	};

	class CLogFontInfo {
	protected:
		LOGFONT   m_lgf;
		CError    m_error;

	public:
		 CLogFontInfo (void);
		 CLogFontInfo (const LOGFONT&);
		~CLogFontInfo (void);

	public:
		CLogFontInfo& operator= (const LOGFONT& ref);

	public:
		HRESULT     Create(const DWORD dwOptions);
		TErrorRef   Error (void) const;
	public:
		operator const  LOGFONT&(void)
		const; operator LOGFONT&(void);
	};
}}

typedef ex_ui::draw::CFont  TFont;

#endif/*__OWNERDRAW_GDIPLUS_WRAP_H_*/