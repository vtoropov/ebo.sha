/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Sep-2020 at 5:12:46a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack generic driver setup registry property interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 12-Jan-2021 at 7:25:22.956 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "drv.setup.registry.h"

using namespace shared::driver::setup;
using namespace shared::driver::setup::registry;

/////////////////////////////////////////////////////////////////////////////

CProperty:: CProperty (void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CProperty:: CProperty (const CProperty& _ref) : CProperty() { *this = _ref; }
CProperty::~CProperty (void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   CProperty::Error (void) const { return m_error; }
CStringW    CProperty::Get   (const CDevInfoSet& _set, const CDevInfo& _dat, const CPropTypes::_type _type) {
	m_error << __MODULE__ << S_OK;

	CStringW cs_value;

	if (_set.Is() == false) {
		(m_error << E_INVALIDARG) = _T("Device info set is invalid;");
		return cs_value;
	}

	if (_dat.Is() == false) {
		(m_error << E_INVALIDARG) = _T("Device info data is invalid;");
		return cs_value;
	}

	CMemAlloca p_buffer;
	DWORD n_required  = 0;             // size is in bytes; does not matter either char or wchar value;
	DWORD the_lst_err = ERROR_SUCCESS; // yes, that's right: success is always an error;

	DWORD dw_reg_type = 0;
	CDevInfo& dat_ = const_cast<CDevInfo&>(_dat);

	// (1) gets a required buffer size;
	BOOL b_result = ::SetupDiGetDeviceRegistryPropertyW(_set, dat_, _type, NULL, NULL, NULL, &n_required);
	the_lst_err = ::GetLastError();

	if (FALSE == b_result && the_lst_err != ERROR_INSUFFICIENT_BUFFER) {
		m_error = __DwordToHresult(the_lst_err);
		return cs_value;
	}

	HRESULT hr_ = p_buffer.Create(n_required);
	if (FAILED(hr_)) {
		m_error = p_buffer.Error(); return cs_value;
	}
	// (2) gets actual data;
	// https://docs.microsoft.com/en-us/windows/win32/shell/hkey-type
	// https://docs.microsoft.com/en-us/windows/win32/sysinfo/registry-value-types
	b_result = ::SetupDiGetDeviceRegistryPropertyW(_set, dat_, _type, &dw_reg_type, p_buffer.Ptr(), p_buffer.Size(), &n_required);
	if (FALSE == b_result) {
		m_error = __LastErrToHresult();
		return cs_value;
	}
	switch (dw_reg_type) {
	case REG_SZ       : { cs_value = reinterpret_cast<LPCWSTR>(p_buffer.Ptr()); } break;
	case REG_EXPAND_SZ: { cs_value = reinterpret_cast<LPCWSTR>(p_buffer.Ptr());; } break; // no data expension in this version;
	case REG_DWORD    : {
		const DWORD dw_value = *(reinterpret_cast<PDWORD>(p_buffer.Ptr()));
	} break;
	case REG_BINARY   : { cs_value = _T("#binary"); } break;
	case REG_LINK     : { cs_value = _T("#link"); } break;
	case REG_MULTI_SZ : { cs_value = _T("#multiline-string");} break;
	case REG_NONE     : { cs_value = _T("#no_data"); } break;
	default:
		cs_value = _T("#other");
	}

	return cs_value;
}

/////////////////////////////////////////////////////////////////////////////

CProperty&  CProperty::operator = (const CProperty& _ref) { _ref; return *this; }

/////////////////////////////////////////////////////////////////////////////