#ifndef _STDAFX_H_A2AD1503_5555_ABCD_9B38_341E6E697B7E_INCLUDED
#define _STDAFX_H_A2AD1503_5555_ABCD_9B38_341E6E697B7E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:17:57p, UTC+7, Phuket, Rawai, Tuesday;
	This is communication data exchange receiver desktop console app precompiled header declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 3:24:20a, UTC+7, Novosibirsk, Tuesday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 2:56:24a, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool project on 21-May-2020 at 5:31:55p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Shared optimization tool project on 26-Aug-2020 at 6:07:06a, UTC+7, Novosibirsk, Wednesday;
*/
#include "ebo.sha.gui.ver.h"

#ifndef STRICT
#define STRICT
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe
#pragma warning(disable: 4458)  // declaration of '{func_name}' hides class member (GDI+)

#include <atlbase.h>
#include <atlstr.h>             // important order, this file must be included before any includes of the WTL headers

#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>
#include <atlctrls.h>
#include <atldlgs.h>

#include <comdef.h>
#include <comutil.h>

using namespace ATL;

#ifdef _DEBUG
	#define _ATL_DEBUG_INTERFACES
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif

#include <vector>
#include <map>
#include <time.h>
#include <typeinfo>

#if (0)
#if defined WIN64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='amd64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined WIN32
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='x86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#elif defined _M_IA64
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='ia64' publicKeyToken='6595b64144ccf1df' language='*'\"")
#else
  #pragma comment(linker, "/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif
#endif

#if (0)
#else
#define __nothing
#define __empty_ln
#define __no_args
#endif

#pragma comment(lib, "__shared.lite_v15.lib")

#pragma comment(lib, "_ntfs_v15.lib"     )
#pragma comment(lib, "_registry_v15.lib" )
#pragma comment(lib, "_runnable_v15.lib" )
#pragma comment(lib, "_svc.man_v15.lib"  )

#pragma comment(lib, "_uix.ctrl_v15.lib" )
#pragma comment(lib, "_uix.draw_v15.lib" )
#pragma comment(lib, "_uix.frms_v15.lib" )

#pragma comment(lib, "_user.32_v15.lib"  )
#pragma comment(lib, "_web_v15.lib")

#pragma comment(lib, "sfx.select_v15.lib")
#pragma comment(lib, "sfx.status_v15.lib")
#pragma comment(lib, "sfx.strip_v15.lib" )
#pragma comment(lib, "sfx.tabs_v15.lib"  )
#pragma comment(lib, "sfx.tips_v15.lib"  )
#pragma comment(lib, "sfx.image_v15.lib" )
#pragma comment(lib, "sfx.label_v15.lib" )
#pragma comment(lib, "sfx.combo_v15.lib" )
#pragma comment(lib, "sfx.button_v15.lib")

#pragma comment(lib, "drv.service_v15.lib")
#pragma comment(lib, "drv.setup_v15.lib"  )
#pragma comment(lib, "ssh.tnl.drv.lib_v15.lib")

#include "ebo.sha.gui.format.h"
#include "ebo.sha.gui.layout.h"
#include "ebo.sha.gui.theme.h"

namespace shared {
	interface IAppUI {
		virtual HRESULT  IAppUI_ExecCommand   ( const DWORD   _cmd_id ) PURE;
		virtual HRESULT  IAppUI_PutStatusInfo ( LPCWSTR _lp_sz_message) PURE;
		virtual HRESULT  IAppUI_PutTraceError ( LPCWSTR _lp_sz_message) PURE;
		virtual HRESULT  IAppUI_PutTraceInfo  ( LPCWSTR _lp_sz_message) PURE;
		virtual HRESULT  IAppUI_PutTraceWarn  ( LPCWSTR _lp_sz_message) PURE;
		virtual HRESULT  IAppUI_PutTraceError ( TErrorRef) PURE;
	};
	// not safe due to everything is supposed to be running in main thread, i.e. UI thread of the app;
	IAppUI &  Get_AppUI (void); // used by any window/module; if no implementation exists, fake reference is returned;
	IAppUI*&  Set_AppUI (void); // controlled by main frame;

	TFormat&  Get_Format(void);
	TLayout&  Get_Layout(void);
}

#endif/*_STDAFX_H_A2AD1503_5555_ABCD_9B38_341 E6E697B7E_INCLUDED*/