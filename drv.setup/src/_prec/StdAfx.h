#ifndef _STDAFX_4D66287F_C16B_43e5_919D_DADFB8E00745_INCLUDED
#define _STDAFX_4D66287F_C16B_43e5_919D_DADFB8E00745_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Aug-2016 at 5:56:22a, GMT+7, Phuket, Rawai, Tuesday;
	This is Evalu8 Outlook add-in precompiled header include file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack generic driver setup project on 12-Jan-2021 at 6:26:55.996 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "drv.setup.ver.h"

#ifndef STRICT
#define STRICT
#endif

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#define _ATL_APARTMENT_THREADED
#define _ATL_NO_AUTOMATIC_NAMESPACE

#include <atlbase.h>
#include <atlcom.h>
#include <comdef.h>
#include <atlstr.h>
#include <atlsafe.h>

#ifdef _DEBUG
	#define _ATL_DEBUG_INTERFACES
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
#endif

#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <vector>
#include <map>
namespace std {
	#include <math.h>
	#include <time.h>
}

#include <Sddl.h>

using namespace ::ATL;

#ifndef __DwordToHresult
#define __DwordToHresult(err_code)  HRESULT_FROM_WIN32(err_code)
#endif

#endif/*_STDAFX_4D66287F_C16B_43e5_919D_DADFB8E00745_INCLUDED*/