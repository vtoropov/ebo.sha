#ifndef _SHAREDUIXCTRLBASECRT_H_F4EBCB21_16E3_4EB2_8AAF_B073B2931167_INCLUDED
#define _SHAREDUIXCTRLBASECRT_H_F4EBCB21_16E3_4EB2_8AAF_B073B2931167_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-May-2012 at 10:06:20am, GMT+3, Rostov-on-Don, Friday;
	This is Pulsepay Shared Skinned Control Create Data class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 18-Aug-2020 at 7:46:10a, UTC+7, Novosibirsk, Tuesday;
*/
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.base.fmt.h"

namespace ex_ui { namespace controls {

	class CControlCrt // useless class; must be re-viewed or deprecated;
	{
	protected:
		UINT        m_ctrlId;
		TCtrlState  m_state ;
		TCtrlStyle  m_style ;
		TFmtBase    m_format;

	public:
		 CControlCrt (void);
		 CControlCrt (const CControlCrt&);
		~CControlCrt (void);

	public:
		UINT     CtrlId(void) const;
		HRESULT  CtrlId(const UINT);

	public:
		const TFmtBase&      Format(void) const; TFmtBase&      Format(void)      ;
		const TCtrlState&    State (void) const; TCtrlState&    State (void)      ;
		const TCtrlStyle&    Style (void) const; TCtrlStyle&    Style (void)      ;

	public:
		CControlCrt&   operator = (const CControlCrt&);
		CControlCrt&   operator <<(const DWORD _dw_id);
		CControlCrt&   operator <<(const TCtrlState& );
		CControlCrt&   operator <<(const TCtrlStyle& );
		CControlCrt&   operator <<(const TFmtBase& );
	};
}}

typedef ex_ui::controls::CControlCrt  TCtrlCrt;

#endif/*_SHAREDUIXCTRLBASECRT_H_F4EBCB21_16E3_4EB2_8AAF_B073B2931167_INCLUDED*/