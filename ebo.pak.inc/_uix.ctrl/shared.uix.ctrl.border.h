#ifndef _SHAREDUIXCTRLBORDER_H_EC3F1FA6_CC16_444D_B57F_4339805FF652_INCLUDED
#define _SHAREDUIXCTRLBORDER_H_EC3F1FA6_CC16_444D_B57F_4339805FF652_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-May-2012 at 9:36:22am, GMT+3, Rostov-on-Don, Friday;
	This is Pulsepay Project Shared Skinned Control Base class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack project on 25-Nov-2020 at 6:46:50.364 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.gdi.z_buf.h"

namespace ex_ui { namespace controls {

	class CBorder {
	protected:
		CColour_Ex  m_color;
		DWORD       m_thickness;
		CPosition   m_position ;

	public:
		 CBorder(void);
		 CBorder(const CBorder&) ;
		 CBorder(const CColour&) ;
		 CBorder(const COLORREF, const DWORD _thick, const BYTE alpha = TAlpha::eOpaque);
		~CBorder(void);

	public:
		const
		CColour_Ex& Color(void) const;
		CColour_Ex& Color(void)      ;
		const
		CPosition&  Position (void) const;
		CPosition&  Position (void)      ;
		const
		DWORD&      Thickness(void) const;
		DWORD&      Thickness(void)      ;

	public:
		const bool  Is(void)        const;
		const bool  IsClear  (void) const;
		const bool  IsLimpid (void) const;

	public:
		CBorder&    operator << (const CColour&    );   // sets a color for a border;
		CBorder&    operator << (const DWORD _thick);   // sets a thickness of a border;
		CBorder&    operator << (const CPosition&  );
		CBorder&    operator >> (const COLORREF _clr_border_color);
		CBorder&    operator  = (const CBorder&    );

	public:
		CBorder& operator <<(LPCWSTR _lp_sz_style); // creates this class object from XML fragment text; for example: "width:1px;color:{0};"
	};

	class CBorders {
	public:
		CBorder   m_borders[CSides::e_bottom + 1];
	public:
		 CBorders (void);
		 CBorders (const CBorders&);
		 CBorders (const COLORREF clrAll);
		~CBorders (void);

	public:
		const
		CBorder&   Side(const TSide) const;
		CBorder&   Side(const TSide)      ;

	public:
		const
		CBorder&   Border(const DWORD _ndx) const;
		CBorder&   Border(const DWORD _ndx)      ;
		DWORD      Count (void) const;

	public:
		const CBorder&   Bottom(void) const; CBorder&   Bottom(void)      ;
		const CBorder&   Left  (void) const; CBorder&   Left  (void)      ;
		const CBorder&   Right (void) const; CBorder&   Right (void)      ;
		const CBorder&   Top   (void) const; CBorder&   Top   (void)      ;

	public:
		VOID       Color (const CColour&);  // sets color to all borders;
		VOID       Thickness(const DWORD);  // sets thickness to all borders;

	public:
		/*
			***ATTENTION***
			operators are not reliable in cases when they are mixed or accidantly / implicitly replaced by another ones;
			for example, CBorders::operator = (DWORD) is not defined, but code like this: ($object).borders() = (DWORD)(1) produces interesting things;
		*/
		CBorders&  operator  = (const CBorders&)  ;
		CBorders&  operator << (const CColour&)   ; // sets a color to all borders;
		CBorders&  operator << (const DWORD _d_thickness);
		CBorders&  operator >> (const COLORREF _clr_rgb ); // sets a color to all borders via RGB;
		CBorders&  operator << (const RECT&)      ; // calculates border positions in accordance with rectangle side values and applied thickness;
		const
		CBorder &  operator << (const TSide) const; // returns a border of specified side; (ro);
		CBorder &  operator << (const TSide)      ; // returns a border of specified side; (rw);
		const
		CBorders&  operator >> (RECT& _out ) const; // extracts border thickness from given rectangle;

	public:
		CBorders&  operator <<(LPCWSTR _lp_sz_style); // creates this class object from XML fragment text;
	};

namespace format {

	class CBorder_Ex : public CBorder { typedef CBorder TBase; // this format class looks like deprecated; not is used so much;
	protected:
		CColour_Ex  m_shadow;

	public:
		 CBorder_Ex (void);
		 CBorder_Ex (const CBorder_Ex&);
		~CBorder_Ex (void);

	public:
		const
		CColour_Ex& Shadow (void) const;
		CColour_Ex& Shadow (void)      ;

	public:
		CBorder_Ex& operator = (const CBorder_Ex&);
	};

	class CBorders_Ex {
	public:
		CBorder_Ex   m_borders[CSides::e_bottom + 1];
	public:
		 CBorders_Ex (void);
		 CBorders_Ex (const CBorders_Ex&);
		~CBorders_Ex (void);

	public:
		const
		CBorder_Ex&   Side(const TSide) const;
		CBorder_Ex&   Side(const TSide)      ;

	public:
		const CBorder_Ex&   Bottom(void) const; CBorder_Ex&   Bottom(void)      ;
		const CBorder_Ex&   Left  (void) const; CBorder_Ex&   Left  (void)      ;
		const CBorder_Ex&   Right (void) const; CBorder_Ex&   Right (void)      ;
		const CBorder_Ex&   Top   (void) const; CBorder_Ex&   Top   (void)      ;

	public:
		CBorders_Ex&  operator = (const CBorders_Ex&);
		CBorders_Ex&  operator <<(const CColour&);         // sets colour to all borders;
		CBorders_Ex&  operator <<(const DWORD _thickness); // sets a thickness to all borders;
		CBorders_Ex&  operator >>(const COLORREF _rgb);    // sets colour to all borders via RGB value;
	};
}
	using ex_ui::draw::CZBuffer;
	using ex_ui::controls::format::CBorder_Ex;

	class CBorders_Helper {
	private:
		const
		CBorders& m_borders;

	public:
		 CBorders_Helper (const CBorders&);
		~CBorders_Helper (void);

	public:
		HRESULT   Draw (CZBuffer&, const RECT& _rc_draw) const;
		HRESULT   Draw (CZBuffer&, const RECT& _rc_draw, const COLORREF) const; // draws all borders by overlap color; useful for control state;

	public:
		static
		HRESULT   Draw (CZBuffer&, const CBorder_Ex&);
	};
}}

typedef ex_ui::controls::CBorders                 TBorders   ;
typedef ex_ui::controls::format::CBorder_Ex       TBorderFmt ;
typedef ex_ui::controls::CBorders_Helper          TBorderRnd ;

#endif/*_SHAREDUIXCTRLBORDER_H_EC3F1FA6_CC16_444D_B57F_4339805FF652_INCLUDED*/