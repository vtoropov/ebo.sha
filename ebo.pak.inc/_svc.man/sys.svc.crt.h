#ifndef _SYSSVCCREATE_H_F6FED3CC_D476_4d88_82AC_06C9BC8745C9_INCLUDED
#define _SYSSVCCREATE_H_F6FED3CC_D476_4d88_82AC_06C9BC8745C9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Jun-2016 at 11:25:42a, UTC+7, Phuket, Rawai, Monday;
	This is Shared system service create data interface declaration file.
	( project: thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 11-Jul-2020 at 6:27:42p, UTC+7, Novosibirsk, Saturday;
*/
#include <AccCtrl.h>
#include <Aclapi.h>
#include <winsvc.h>

#include "sys.svc.error.h"

namespace shared { namespace service {

	using shared::sys_core::CError;
	// https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/sc-create
	class CCrtOption { // service create option enumeration;
	public:
		enum _ctrl : DWORD {
			eStoppable   = SERVICE_ACCEPT_STOP          ,   // accepts a stop of a service;
			eCanContinue = SERVICE_ACCEPT_PAUSE_CONTINUE,   // pause and continue control are available through properties of SC console;
			eCanShutdown = SERVICE_ACCEPT_SHUTDOWN      ,   // unloading is acceptable;
		};
		enum _start : DWORD {
			eOnAuto      = SERVICE_AUTO_START           ,   // starts on operating system loading;
			eOnDemand    = SERVICE_DEMAND_START         ,   // manual start is acceptable;
		};
		enum _type : DWORD {
			eKernelDrv   = SERVICE_KERNEL_DRIVER        ,   // runs as kernel mode driver;
			eServece     = SERVICE_WIN32_OWN_PROCESS    ,   // runs as user mode service;
		};

	protected:
		DWORD   m_ctrl_mask;
		DWORD   m_load_mask;
		DWORD   m_type_mask;

	public:
		 CCrtOption (void);
		 CCrtOption (const CCrtOption&);
		~CCrtOption (void);

	public: // modifiers
		DWORD   Control (void) const;
		DWORD&  Control (void)      ;
		DWORD   Start   (void) const;
		DWORD&  Start   (void)      ;
		DWORD   Type    (void) const;
		DWORD&  Type    (void)      ;

	public: // accessors
		bool    CanPause(void) const;
		bool    CanStop (void) const;
		bool    IsDriver(void) const;
		bool    OnDemand(void) const;
		bool    Shutdown(void) const;

	public:
		CCrtOption& operator = (const CCrtOption&);
	};

	class CCrtData {
	public:
		enum _att_ndx {
			e_account  = 0x0,
			e_desc     = 0x1, // service description;
			e_name     = 0x2,
			e_path     = 0x3, // path to a service binary;
			e_title    = 0x4, // aka display name;
		};
	private:
		mutable
		CSvcError   m_error;
	private:
		CStringW    m_atts[_att_ndx::e_title + 1];
		CCrtOption  m_opts;
	public:
		 CCrtData (void);
		 CCrtData (const CCrtData&);
		~CCrtData (void);
	public:
		LPCWSTR     Account(void)    const;
		CStringW&   Account(void)         ;
		LPCWSTR     Description(void)const;
		CStringW&   Description(void)     ;
		LPCWSTR     DisplayName(void)const;
		CStringW&   DisplayName(void)     ;
		TErrorRef   Error  (void)    const;
		bool        IsValid(void)    const;
		LPCWSTR     Name(void)       const;
		CStringW&   Name(void)            ;
		const
		CCrtOption& Options(void)    const;
		CCrtOption& Options(void)         ;
		LPCWSTR     Path(void)       const;
		CStringW&   Path(void)            ;

	public:
		CStringW    ToString(void)   const;

	public:
		CCrtData&   operator = (const CCrtData&);
	};
}}

#endif/*_SYSSVCCREATE_H_F6FED3CC_D476_4d88_82AC_06C9BC8745C9_INCLUDED*/