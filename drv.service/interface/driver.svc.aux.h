#ifndef _DRIVERSVCAUX_H_A84257A3_9A8F_453E_A51C_2BE15A926B1E_INCLUDED
#define _DRIVERSVCAUX_H_A84257A3_9A8F_453E_A51C_2BE15A926B1E_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Aug-2020 at 5:29:45a, UTC+7, Novosibirsk, Friday;
	This is SSH-Tunnel WFP driver service auxiliary interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 7:42:16.893 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "sys.svc.service.h"
#include "driver.dev.loc.h"

namespace shared { namespace driver { namespace client {

	using shared::service::CService;
	using shared::service::CService_Handle;

	class CObjectToStr {
	public:
		 CObjectToStr (void);
		~CObjectToStr (void);

	public:
		CStringW   Print(const CDrvLocator&);
		CStringW   Print(const CService&);
	};
}}}

#endif/*_DRIVERSVCAUX_H_A84257A3_9A8F_453E_A51C_2BE15A926B1E_INCLUDED*/