#ifndef _EBOSHAGPSPOS_H_4F3EEDE4_BB43_4FB4_BC6B_159E3EFF0CF4_INCLUDED
#define _EBOSHAGPSPOS_H_4F3EEDE4_BB43_4FB4_BC6B_159E3EFF0CF4_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2021 at 7:47:57.448 pm, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool fake GPS position sub-view interface declaration file.
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.view.empty.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CFake_Pos : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CFake_Pos;
		private:
			CFake_Pos&    m_view ;
			TEmptyView    m_empty;

		private:
			 CViewWnd (CFake_Pos&);
			~CViewWnd (void);

		private:   // TWindow
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CFake_Pos (void);
		~CFake_Pos (void);
	};

}}}}

typedef ebo::sha::gui::view::CFake_Pos TFakePos;

#endif/*_EBOSHAGPSPOS_H_4F3EEDE4_BB43_4FB4_BC6B_159E3EFF0CF4_INCLUDED*/