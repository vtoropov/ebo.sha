#ifndef _SHAREDUIXCTRLSELECTOREXT_H_49A0BB5A_25DB_493B_A79C_C9DACF94AE2F_INCLUDED
#define _SHAREDUIXCTRLSELECTOREXT_H_49A0BB5A_25DB_493B_A79C_C9DACF94AE2F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Nov-2019 at 9:16:09p, UTC+7, Novosibirsk, Monday;
	This is shared UIX library task/view selector control format interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to shared Sfx library on 27-Aug-2020 at 0:11:56a, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.uix.ctrl.base.fmt.h"

namespace ST_Ctrls { interface ISelector_Events; namespace format {

	using ex_ui::controls::CBorder ;
	using ex_ui::controls::CBorders;
	using ex_ui::controls::CAlign  ;

	using ex_ui::controls::format::CBase;

	// https://stackoverflow.com/questions/31427117/difference-between-element-item-and-entry-when-it-comes-to-programming
	class CItem : public TFmtBase { typedef TFmtBase TItem;
	public:
		 CItem (void);
		 CItem (const CItem&);
		~CItem (void);

	public:
		CItem& operator = (const CItem&);
	};

	class CItems {
	protected:
		CItem   m_active  ;  // i.e. selected;
		CItem   m_disabled;
		CItem   m_normal  ;

	public:
		 CItems (void) ;
		 CItems (const CItems&);
		~CItems (void);

	public:
		const
		CItem&   Active   (void) const;
		CItem&   Active   (void)      ;
		const
		CItem&   Disabled (void) const;
		CItem&   Disabled (void)      ;
		const
		CItem&   Normal   (void) const;
		CItem&   Normal   (void)      ;

	public:
		CItems&  operator = (const CItems&);
	};

	class COverlay {
	protected:
		DWORD    m_res_id;
		CAlign   m_align ;
		SIZE     m_size  ; // a size of overlay image; of course, it can be calculated dynamically, but for simplicity, direct specifying is okay; 

	public:
		 COverlay (void);
		 COverlay (const COverlay&);
		~COverlay (void);

	public:
		const
		CAlign&  Align (void) const;
		CAlign&  Align (void)      ;
		DWORD    Image (void) const;
		DWORD&   Image (void)      ;
		bool     Is    (void) const;  // returns false in case when image resource identifier is less than 1; used in drawing procedure;
		const
		SIZE&    Size  (void) const;
		SIZE&    Size  (void)      ;

	public:
		COverlay& operator = (const COverlay&);
		COverlay& operator <<(const CAlign& );
		COverlay& operator <<(const DWORD _dw_res_id);
		COverlay& operator <<(const SIZE &_size );
	};

	class CSelector : public TFmtBase { typedef TFmtBase TFormat;
	protected:
		DWORD      m_img_res;
		CItems     m_items  ;
		COverlay   m_overlay;

	public:
		 CSelector (void);
		 CSelector (const CSelector&);
		~CSelector (void);

	public:
		DWORD      Images  (void) const;
		DWORD&     Images  (void)      ;
		const
		CItems&    Items   (void) const;
		CItems&    Items   (void)      ;
		const
		COverlay&  Overlay (void) const;   // used for drawing an image over background color/image at specified point, i.e. an anchor;
		COverlay&  Overlay (void)      ;

	public:
		CSelector& operator = (const CSelector&);
	};

	class CSymantec_NA_Sel : public CSelector {
	                        typedef CSelector TBase;
	public:
		 CSymantec_NA_Sel (void);
		~CSymantec_NA_Sel (void);
	};

	class CWhitespread_Sel : public CSelector {
	                        typedef CSelector TBase;
	public:
		 CWhitespread_Sel (void);
		~CWhitespread_Sel (void);
	};
}}

typedef ST_Ctrls::format::CSelector  TSelectFmt;

#endif/*_SHAREDUIXCTRLSELECTOREXT_H_49A0BB5A_25DB_493B_A79C_C9DACF94AE2F_INCLUDED*/