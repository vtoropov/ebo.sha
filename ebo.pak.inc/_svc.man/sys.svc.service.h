#ifndef _SYSSVCOBJECT_H_54EA6A52_EE05_4685_8E50_AB7EDCF70B94_INCLUDED
#define _SYSSVCOBJECT_H_54EA6A52_EE05_4685_8E50_AB7EDCF70B94_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Jan-2015 on 8:45:58p, UTC+3, Taganrog, Wednesday;
	This is shared system service manager library class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 12-Jul-2020 at 10:00:43a, UTC+7, Novosibirsk, Sunday;
*/
#include "sys.svc.error.h"
#include "sys.svc.cfg.h"
#include "sys.svc.state.h"
namespace shared { namespace service {

	// this class is for closing handle automatically when going out of scope;
	// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-closeservicehandle
	class CService_Handle {
	private:
		SC_HANDLE   m_handle;
		CSvcError   m_error ;

	public:
		 CService_Handle (void);
	explicit CService_Handle (const CService_Handle&);
	explicit CService_Handle (const SC_HANDLE);
		~CService_Handle (void);

	public:
		HRESULT     Attach (const SC_HANDLE); // NULL is acceptable;
		HRESULT     Close  (void)      ;
		SC_HANDLE   Detach (void)      ;
		TErrorRef   Error  (void) const;
		const bool  Is     (void) const;

	public:
		operator SC_HANDLE (void) const;

	public:
		CService_Handle& operator = (const CService_Handle&);
		CService_Handle& operator <<(const SC_HANDLE); // attach;
		CService_Handle& operator >>(SC_HANDLE&)     ; // detach;
	};

	class CService
	{
	private:
		mutable
		CSvcError   m_error ;
		SC_HANDLE   m_handle;
		CCfg        m_cfg   ;
		CCfg_Ex     m_cfg_ex;
		CStatus     m_status;
		CState      m_state ; // current state of the service;

	public:
		 CService (void);
	explicit CService (const CService& );
	explicit CService (const SC_HANDLE );
		~CService (void);

	public:
		HRESULT     Attach (const SC_HANDLE);
		HRESULT     Close  (void)      ;
		const
		CCfg&       Cfg    (void) const;
		CCfg&       Cfg    (void)      ;
		const
		CCfg_Ex&    Cfg_Ex (void) const;
		CCfg_Ex&    Cfg_Ex (void)      ;
		SC_HANDLE   Detach (void)      ;
		TErrorRef   Error  (void) const;
		const bool  Is     (void) const;
		HRESULT     Reset  (void)      ; // resets attributes to empty|none state;
		const
		CState&     State  (void) const;
		CState&     State  (void)      ;
		const
		CStatus&    Status (void) const;
		CStatus&    Status (void)      ;
		HRESULT     Update (void)      ; // updates all internal properties: cfg|cfg_ex|status;

	public:
		CService&   operator = (const CService&);
		CService&   operator = (const SC_HANDLE);

	public:
		operator SC_HANDLE (void) const;
	};
}}

#endif/*_SYSSVCOBJECT_H_54EA6A52_EE05_4685_8E50_AB7EDCF70B94_INCLUDED*/