/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Oct-2020 at 11:07:57p, UTC+7, Novosibirsk, Wednesday;
	This is Ebo Sha optima tool fake GPS view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.view.fake.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

#include "shared.uix.gdi.draw.defs.h"
#include "shared.uix.gdi.renderer.h"
using namespace ex_ui::draw;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CFake_Cmd {
	public:
		enum _cmd : DWORD {
			e_cmd_sel_trace    = 0x01,
			e_cmd_sel_driver   = 0x02,
		};
	}; typedef CFake_Cmd This_Cmd;

	class CFake_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_sel  = IDC_EBO_SHA_FAK_SEL,
		};
	}; typedef CFake_Ctl This_Ctl;

	class CFake_Res {
	public:
		enum _res : WORD {
			e_res_sel_ovr_d    = IDR_EBO_SHA_SEL_OVR_3,
			e_res_sel_itm_1_a  = IDR_EBO_SHA_SEL_ITM_01_a,
			e_res_sel_itm_1_n  = IDR_EBO_SHA_SEL_ITM_01_n,
			e_res_sel_itm_2_a  = IDR_EBO_SHA_SEL_ITM_02_a,
			e_res_sel_itm_2_n  = IDR_EBO_SHA_SEL_ITM_02_n,
		};
	}; typedef CFake_Res This_Res;

	class CFake_Layout {
	private:
		CFake&   m_view;
		RECT     m_rect;

	public:
		CFake_Layout (CFake& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CFake_Layout (void) {}

	public:
		RECT   GetSelectRect(void) const {
			RECT rc_sel = m_rect; rc_sel.right = rc_sel.left + m_view.Select().Layout().Width();
			return rc_sel;
		}

		RECT   GetSubViewRect(void) const {
			RECT rc_sub = m_rect; rc_sub.left = this->GetSelectRect().right + 0x0;
			::InflateRect(&rc_sub, -0x0, -0x0);
			return rc_sub;
		}

		VOID   Update (void) {

			const RECT rc_sub_view = this->GetSubViewRect();

			TSelect& sel_ctrl = m_view.Select(); sel_ctrl.Layout() << m_rect;
			TFakeDrv&  driver = m_view.Driver(); driver.Update(rc_sub_view);
			TFakeTrace& trace = m_view.Trace (); trace.Update(rc_sub_view);
		}
	};

	class CFake_Init {
	private:
		CFake&   m_view ;
		CError   m_error;

	public:
		 CFake_Init (CFake& _view) : m_view(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }
		~CFake_Init (void) {}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;
#pragma region __sel_create
			TSelect& sel_ctrl = m_view.Select();

			sel_ctrl.Format() = shared::Get_Format().Select();
			sel_ctrl.Format().Overlay().Image() = IDR_EBO_SHA_FAK_SEL_OVR_0;

			shared::Get_Layout().Select() >> sel_ctrl.Layout();
			shared::Get_Layout().Select() >> sel_ctrl.Borders();

			sel_ctrl.Format().Images() = IDR_EBO_SHA_SEL_LST_01;

			TSelItems& items = sel_ctrl.Items();

			TSelItem itm_0;
			itm_0.Id() = CFake_Cmd::e_cmd_sel_trace;
			itm_0.Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_sel_itm_1_n);
			itm_0.Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_sel_itm_1_a);
			itm_0.Image () = 1;
			items.Append(itm_0);

			TSelItem itm_1;
			itm_1.Id() = CFake_Cmd::e_cmd_sel_driver;
			itm_1.Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_sel_itm_2_n);
			itm_1.Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_sel_itm_2_a);
			itm_1.Image () = 0;
			items.Append(itm_1);
			items.Selected(1);

			RECT rc_ = CFake_Layout(m_view).GetSelectRect();
			HRESULT hr_ = sel_ctrl.Create(m_view.Window(), rc_, _T("ebo_pack::controls::selector"), This_Ctl::e_ctl_sel);

			if (FAILED(hr_)) {
				m_error = sel_ctrl.Error();
				m_error.Show();
			}
			else {
				sel_ctrl.Window().SetWindowPos(HWND_TOP, &rc_, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE);
				sel_ctrl.Layout().Update(rc_);
			}
#pragma endregion
			return m_error;
		}
	};
}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CFake::CViewWnd:: CViewWnd(CFake& _view) : TViewWnd(_view), m_view(_view), m_select(_view) {}
CFake::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CFake::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	const RECT rc_ = Lay_(*this); rc_; HRESULT hr_ = S_OK; TViewWnd::SetWindowTextW(_T("FakeGPS"));

	CFake_Layout layout_(m_view);
	CFake_Init init_(m_view);
	hr_ = init_.OnCreate();
	hr_ = m_driver.Create(*this, layout_.GetSubViewRect(), true);
	if (SUCCEEDED(hr_)) {
		m_driver.Window().SetWindowTextW(_T("fake::gps::driver::subview"));
	}
	hr_ = m_trace.Create(*this, layout_.GetSubViewRect(), false);
	if (SUCCEEDED(hr_)) {
		m_trace.Window().SetWindowTextW(_T("fake::tcp::trace::subview"));
	}

	return 0;
}

LRESULT   CFake::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_b_hand = FALSE;
	m_trace.Destroy();
	m_driver.Destroy();
	m_select.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CFake::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {

	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};
	CFake_Layout lay_(m_view);
	// TODO: select control applies border thickness format when updates the layout; must be reviewed;
	m_select.Layout() << lay_.GetSelectRect();
	m_select.Refresh();
	m_driver.Update(lay_.GetSubViewRect());
	m_trace .Update(lay_.GetSubViewRect());

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CFake:: CFake (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CFake::~CFake (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CFake::ISelector_OnItemClick  (const UINT _u_itm_id) { HRESULT hr_ = S_OK;
	_u_itm_id;
	switch (_u_itm_id) {
	case This_Cmd::e_cmd_sel_driver: {
		if (m_wnd.m_driver.IsValid()) hr_ = m_wnd.m_driver.IsVisible(true);
		if (m_wnd.m_trace.IsValid()) hr_ = m_wnd.m_trace.IsVisible(false);
	} break;
	case This_Cmd::e_cmd_sel_trace: {
		if (m_wnd.m_trace.IsValid()) hr_ = m_wnd.m_trace.IsVisible(true);
		if (m_wnd.m_driver.IsValid()) hr_ = m_wnd.m_driver.IsVisible(false);
	} break;
	}

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

TFakeDrv&   CFake::Driver  (void) { return m_wnd.m_driver; }
TSelect&    CFake::Select  (void) { return m_wnd.m_select; }
TFakeTrace& CFake::Trace   (void) { return m_wnd.m_trace ; }