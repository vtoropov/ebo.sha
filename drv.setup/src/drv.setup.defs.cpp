/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Sep-2020 at 4:21:30a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack generic driver common setup interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 12-Jan-2021 at 7:08:10.486 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "drv.setup.defs.h"

using namespace shared::driver::setup;

/////////////////////////////////////////////////////////////////////////////

CMemAlloca:: CMemAlloca (void) : m_p_raw_data(NULL), m_size(0) { m_error << __MODULE__ << OLE_E_BLANK >> __MODULE__; }
CMemAlloca:: CMemAlloca (const CMemAlloca& _ref) : CMemAlloca() { *this = _ref; }
CMemAlloca::~CMemAlloca (void) { if (this->Is()) this->Destroy(); }

/////////////////////////////////////////////////////////////////////////////

HRESULT      CMemAlloca::Create (const DWORD _dw_size) {
	m_error << __MODULE__ << S_OK;

	if (0 == _dw_size)
		return (m_error << E_INVALIDARG) = _T("Memory buffer cannot be zero-length;");

	if (this->Is())
		return (m_error = __DwordToHresult(ERROR_ALREADY_INITIALIZED));

	try { // it very looks like try block is not required here, just remains for following standard memory allocation error catch; nothing more;
		  // https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapalloc
	      // https://docs.microsoft.com/en-us/windows/win32/memory/awe-example  | just a sample of virtual memory allocation;
		m_p_raw_data = reinterpret_cast<PBYTE>(::HeapAlloc(::GetProcessHeap(), HEAP_ZERO_MEMORY, _dw_size));
	}
	catch (const ::std::bad_alloc&) { // looks like never been called;
		m_error << E_OUTOFMEMORY;
	}
	// https://docs.microsoft.com/en-us/windows/win32/debug/using-an-exception-handler
	// https://docs.microsoft.com/en-us/windows/win32/debug/getexceptioncode | this one is required for getting error information; no GetLastError();
	if (m_p_raw_data == NULL) {
		m_error << E_OUTOFMEMORY;
	}
	else
		m_size = _dw_size;

	return m_error;
}

HRESULT      CMemAlloca::Destroy(void) {
	m_error << __MODULE__ << S_OK;

	if (this->Is() == false)
		return (m_error = __DwordToHresult(ERROR_INVALID_STATE));

	// https://docs.microsoft.com/en-us/windows/win32/api/heapapi/nf-heapapi-heapfree
	const BOOL b_result = ::HeapFree(::GetProcessHeap(), 0, this->m_p_raw_data);
	if (FALSE == b_result) {
		m_error = __LastErrToHresult();
	}
	else {
		m_p_raw_data = NULL; m_size = 0;
	}
	return m_error;
}

TErrorRef    CMemAlloca::Error (void) const { return m_error; }

/////////////////////////////////////////////////////////////////////////////

const bool   CMemAlloca::Is  (void) const { return (m_p_raw_data != NULL); }
PBYTE const  CMemAlloca::Ptr (void) const { return (m_p_raw_data) ; }
PBYTE        CMemAlloca::Ptr (void)       { return (m_p_raw_data) ; }
DWORD        CMemAlloca::Size(void) const { return  m_size; }

/////////////////////////////////////////////////////////////////////////////

CMemAlloca&  CMemAlloca::operator = (const CMemAlloca& _ref) {
	m_error << __MODULE__ << S_OK;

	if (_ref.Is() == false) {
	   (m_error << E_INVALIDARG) = _T("Source memory block is invalid;");
	   return *this;
	}
	
	this->Create(_ref.Size());

	if (m_error == false) {
		const errno_t error_ = ::memcpy_s(this->Ptr(), this->Size(), _ref.Ptr(), _ref.Size());
		if (NO_ERROR != error_)
			m_error << E_OUTOFMEMORY;
	}
	return *this;
}

CMemAlloca&  CMemAlloca::operator <<(const DWORD _size) {
	m_error << __MODULE__ << S_OK;
	if (this->Is())
		this->Destroy();
	if (this->Error() == false)
		this->Create(_size);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CTypes:: CTypes (void) : m_types(_type::e_unspecified) {}
CTypes:: CTypes (const CTypes& _ref) : CTypes() { *this = _ref; }
CTypes:: CTypes (const DWORD _dw_types_reqs ) : CTypes() { *this << _dw_types_reqs; }
CTypes::~CTypes (void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD      CTypes::Get   (void) const { return m_types; }
const bool CTypes::Has   (const _type _e) const { return (0 != (_e & m_types)); }
const bool CTypes::Is    (void) const { return (_type::e_unspecified !=  m_types); }
const bool CTypes::Not   (void) const { return (_type::e_unspecified ==  m_types); }
DWORD&     CTypes::Set   (void)       { return m_types; }

/////////////////////////////////////////////////////////////////////////////

CTypes& CTypes::operator = (const CTypes& _ref) { *this << _ref.Get(); return *this; }
CTypes& CTypes::operator+= (const DWORD _dw_types_added) { this->m_types |= _dw_types_added; return *this; }
CTypes& CTypes::operator-= (const DWORD _dw_types_extra) { this->m_types &=~_dw_types_extra; return *this; }
CTypes& CTypes::operator <<(const DWORD _dw_types_apply) { this->Set() = _dw_types_apply; return *this; }

/////////////////////////////////////////////////////////////////////////////

const bool  CTypes::operator == (const DWORD _dw_type_of_interest) { return (0 != (_dw_type_of_interest & this->Get())); }
CTypes::operator const DWORD (void) const { return this->Get(); }