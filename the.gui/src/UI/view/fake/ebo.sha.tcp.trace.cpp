/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2021 at 5:08:09.574 pm, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool GPS TCP/IP trace  sub-view interface implementation file;
*/
#include "StdAfx.h"
#include "ebo.sha.tcp.trace.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CFake_Trace_Layout {
	private:
		CFake_Trace& m_view;
		RECT         m_rect;

	public:
		 CFake_Trace_Layout (CFake_Trace& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CFake_Trace_Layout (void) {}

	public:
		RECT   GetEmpty(void) { RECT rc_empty = m_rect; ::InflateRect(&rc_empty, -0x5, -0x5); rc_empty.top += 30; return rc_empty; }
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CFake_Trace::CViewWnd:: CViewWnd(CFake_Trace& _view) : TViewWnd(_view), m_view(_view), m_tabbed(_view) {}
CFake_Trace::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CFake_Trace::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	CFake_Trace_Layout layout_(m_view);

	HRESULT hr_ = m_empty.Create(*this, layout_.GetEmpty(), true); hr_;
	m_empty.DrawBorder(true);

	return 0;
}

LRESULT   CFake_Trace::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_empty.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CFake_Trace::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CFake_Trace_Layout layout_(m_view);
	m_empty.Update(layout_.GetEmpty());

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CFake_Trace:: CFake_Trace (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CFake_Trace::~CFake_Trace (void) {}

/////////////////////////////////////////////////////////////////////////////

TTabCtrl&  CFake_Trace::Tabbed (void) { return m_wnd.m_tabbed; }