#ifndef _SHAREDUIXCTRLINFOPANELEXT_H_49A0BB5A_25DB_493B_A79C_C9DACF94AE2F_INCLUDED
#define _SHAREDUIXCTRLINFOPANELEXT_H_49A0BB5A_25DB_493B_A79C_C9DACF94AE2F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 7-Mar-2019 at 12:42:43p, UTC+7, Novosibirsk, Saturday;
	This is shared UIX library information panel control extension interface declaration file.
*/
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.base.lay.h"
#include "shared.uix.ctrl.base.fmt.h"
#include "shared.uix.gen.text.h"

namespace ex_ui { namespace frames {

	interface IInfoPanel_Events {
		virtual HRESULT   IInfo_OnBkgChanged (void) PURE;
		virtual HRESULT   IInfo_OnFontChanged(void) PURE;
		virtual HRESULT   IInfo_OnItemClick  (const UINT _u_itm_id) PURE;
		virtual HRESULT   IInfo_OnItemImages (const UINT _u_res_id) PURE;
	};
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getsystemmetrics
	class CInfoCaption : public TTextLay { typedef TTextLay TBase;
	private:
		ULONG       m_height ;   // by default: window caption height of system metrics; i.g. ::GetSystemMetrics(SM_CYCAPTION);
		bool        m_visible;
		TFmtBase    m_format ;
		TOverlay    m_overlay;

	public :
		 CInfoCaption (void);
		 CInfoCaption (const CInfoCaption&);
		~CInfoCaption (void);

	public:
		const
		TFmtBase&   Format (void) const;
		TFmtBase&   Format (void)      ;
		ULONG       Height (void) const;
		ULONG&      Height (void)      ;
		const
		TOverlay&   Overlay(void) const;
		TOverlay&   Overlay(void)      ;
		bool        Visible(void) const;
		bool&       Visible(void)      ;

	public :
		CInfoCaption& operator = (const CInfoCaption& );
		CInfoCaption& operator <<(const TFmtBase& _fmt);
		CInfoCaption& operator <<(const TOverlay& _ovr);
		CInfoCaption& operator <<(const bool _visible );
		CInfoCaption& operator <<(const ULONG _height );
	};
}}

typedef ex_ui::frames::IInfoPanel_Events  TIInfo_Events;
typedef ex_ui::frames::CInfoCaption       TInfoCaption ;

#endif/*_SHAREDUIXCTRLINFOPANELEXT_H_49A0BB5A_25DB_493B_A79C_C9DACF94AE2F_INCLUDED*/