#ifndef _SYSSVCCFG_H_0BF9BD9E_6B9F_4B18_B485_D3A45730FD56_INCLUDED
#define _SYSSVCCFG_H_0BF9BD9E_6B9F_4B18_B485_D3A45730FD56_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Aug-2020 at 3:23:27p, UTC+7, Novosibirsk, Thursday;
	This is Ebo Pack system service configuration information interface declaration file.
*/
#include "sys.svc.error.h"
namespace shared { namespace service {

	// it very looks like INVALID_HANDLE_VALUE is not acceptable for SC_HANDLE in some cases and NULL must be used;
	class CCfg_Base {
	protected:
		SC_HANDLE   m_owner;   // it is generic handle of a service; it is not managed internally by this class;
		                       // *note*: a service must be opened with the access right: SERVICE_QUERY_CONFIG;
		CSvcError   m_error;

	protected:
		 CCfg_Base (void);
		 CCfg_Base (const CCfg_Base&);
		~CCfg_Base (void);

	public:
		TErrorRef   Error (void) const;        // gets the last error object;
		SC_HANDLE   Owner (void) const;        // gets service handle; the service is an owner of configuration information;
		HRESULT     Owner (const SC_HANDLE);   // sets service handle;
		bool        Valid (void) const;        // checks service/owner handle;

	public:
		operator const SC_HANDLE&(void) const; // used in calling win api functions;

	public:
		CCfg_Base& operator  = (const CCfg_Base&);
		CCfg_Base& operator << (const SC_HANDLE ); // sets owner/service handle; INVALID_HANDLE_VALUE|NULL are acceptable for making it invalid;
	};

	// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-queryserviceconfigw
	// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-changeserviceconfigw
	class CCfg : public CCfg_Base {
	            typedef CCfg_Base TBase;
	public:
		typedef struct __qry_svc_cfg {
			DWORD    dwServiceType ;
			DWORD    dwStartType   ;
			CStringW szBinaryPathName;
			CStringW szLoadOrderGroup;
			CStringW szDisplayName ;
			CStringW szSericeName  ;
		} qry_svc_cfg;
	private:
	// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/ns-winsvc-query_service_configw
	// LPQUERY_SERVICE_CONFIGW  m_p_data;
		qry_svc_cfg  m_data;
	public:
		 CCfg (void);
		 CCfg (const CCfg&);
		~CCfg (void);

	public:
		const
		qry_svc_cfg& Data (void) const;
		qry_svc_cfg& Data (void)      ;
		bool         Is   (void) const;                    // checks a data pointer;
		HRESULT      Query(const SC_HANDLE _p_svc_handle); // sets owner service handle and refreshes configuration data;
		HRESULT      Reset(void);                          // resets configuration data buffer; owner handle is not affected;

	public:
		CCfg&        operator  = (const CCfg&);
		CCfg&        operator << (const SC_HANDLE);        // sets owner service handle and refreshes configuration data;
	};

	// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-queryserviceconfig2w
	// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-changeserviceconfig2w
	class CCfg_Ex : public CCfg_Base {
	               typedef CCfg_Base TBase;
	private:
	// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/ns-winsvc-service_descriptionw
	// LPSERVICE_DESCRIPTIONW  m_p_desc;
		CStringW  m_szDesc;

	public:
		 CCfg_Ex (void);
		 CCfg_Ex (const CCfg_Ex&);
		~CCfg_Ex (void);

	public:
		LPCWSTR   Description(void) const;              // gets a service extended description;
		HRESULT   Description(LPCWSTR)   ;              // sets a service extended description: NULL - no change|empty string - removal data;
		bool      Is   (void) const;                    // checks a data pointer;
		HRESULT   Query(const SC_HANDLE _p_svc_handle); // sets owner service handle and refreshes description data;
		HRESULT   Reset(void);                          // resets configuration data buffer;

	public:
		CCfg_Ex&  operator = (const CCfg_Ex& );
		CCfg_Ex&  operator <<(const SC_HANDLE);
	};

	// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-enumservicesstatusw
	// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-queryservicestatus
	class CStatus : public CCfg_Base {
	               typedef CCfg_Base TBase;
	// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/ns-winsvc-service_status
	private:
		SERVICE_STATUS m_data;
	public:
		 CStatus (void);
		 CStatus (const CStatus&);
		~CStatus (void);

	public:
		SERVICE_STATUS  Data (void) const;
		SERVICE_STATUS& Data (void)      ;              // TODO: might be superfluous reference; it's kept for now;
		bool      Is   (void) const;                    // returns true, if status data is set, otherwise, false;
		HRESULT   Query(const SC_HANDLE _p_svc_handle); // sets owner service handle and refreshes status data;
		HRESULT   Reset(void)      ;

	public:
		CStatus&  operator  = (const CStatus& );
		CStatus&  operator << (const SC_HANDLE);        // sets owner service handle and refreshes status data;
	};

	class CCfgToStr {
	private:
		CStringW  m_sepa;  // separator;
	public:
		 CCfgToStr (LPCWSTR _lp_sz_separator = _T(";"));
		~CCfgToStr (void);
	public:
		CStringW  PrintError(const DWORD _svc_ctrl );
		CStringW  PrintStart(const DWORD _svc_start); // start type can have a one value only; no enumeration;
		CStringW  PrintState(const DWORD _svc_state); // service state value is not a mask, but just number sequence;
		CStringW  PrintType (const DWORD _svc_type );
	};
}}

typedef       shared::service::CCfg     TSvcCfg   ;
typedef const shared::service::CCfg&    TSvcCfgRef;
typedef       shared::service::CCfg_Ex  TSvcCfg_Ex;
typedef const shared::service::CCfg_Ex& TSvcCfg_ExRef;
typedef       shared::service::CStatus  TSvcStatus   ;
typedef const shared::service::CStatus& TSvcStatusRef;

#endif/*_SYSSVCCFG_H_0BF9BD9E_6B9F_4B18_B485_D3A45730FD56_INCLUDED*/