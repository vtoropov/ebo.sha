/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Dec-2020 at 1:55:59a, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha optima tool media tools' view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.view.media.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CMedia_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_sel  = IDC_EBO_SHA_MED_SEL,
		};
	}; typedef CMedia_Ctl This_Ctl;

	class CMedia_Res {
	public:
		enum _res : WORD {
			e_res_sel_overlay  = IDR_EBO_SHA_SEL_MED_OVR ,
			e_res_sel_img_lst  = IDR_EBO_SHA_SEL_MED_LST ,
			e_res_sel_itm_1_a  = IDR_EBO_SHA_SEL_MED_01_a,
			e_res_sel_itm_1_n  = IDR_EBO_SHA_SEL_MED_01_n,
		};
	}; typedef CMedia_Res This_Res;

	class CMedia_Layout {
	private:
		CMedia&  m_view;
		RECT     m_rect;

	public:
		 CMedia_Layout (CMedia& _view) : m_view(_view) {
			 if (m_view.Window().IsWindow())
				 m_view.Window().GetClientRect(&m_rect);
			 else
				 ::SetRectEmpty(&m_rect);
		 }
		~CMedia_Layout (void) {}

	public:
		RECT   GetSelectRect (void) const {
			RECT rc_sel = m_rect; rc_sel.right = rc_sel.left + m_view.Select().Layout().Width();
			return rc_sel;
		}

		RECT   GetSubViewRect(void) const {
			RECT rc_sub = m_rect; rc_sub.left = this->GetSelectRect().right + 0x0;
			::InflateRect(&rc_sub, -0x0, -0x0);
			return rc_sub;
		}

		VOID   Update (void) {

			const RECT rc_sub_view = this->GetSubViewRect();

			TSelect& sel_ctrl = m_view.Select(); sel_ctrl.Layout() << m_rect;
			TiPodView& ipod  = m_view.iPod(); ipod.Update (rc_sub_view);
		}
	};

	class CMedia_Init {
	private:
		CMedia&  m_view ;
		CError   m_error;

	public:
		 CMedia_Init (CMedia& _view) : m_view(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }
		~CMedia_Init (void) {}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;
#pragma region __sel_create
			TSelect& sel_ctrl = m_view.Select();

			sel_ctrl.Format() = shared::Get_Format().Select();
			sel_ctrl.Format().Overlay().Image() = This_Res::e_res_sel_overlay;
			sel_ctrl.Format().Overlay().Size().cx = 128;
			sel_ctrl.Format().Overlay().Size().cy = 128;

			shared::Get_Layout().Select() >> sel_ctrl.Layout();
			shared::Get_Layout().Select() >> sel_ctrl.Borders();

			sel_ctrl.Format().Images() = This_Res::e_res_sel_img_lst;

			RECT rc_ = CMedia_Layout(m_view).GetSelectRect();
			HRESULT hr_ = sel_ctrl.Create(m_view.Window(), rc_, _T("ebo_pack::controls::selector"), This_Ctl::e_ctl_sel);

			TSelItems& items = sel_ctrl.Items();

			TSelItem itm_0;
			itm_0.Id() = 1;
			itm_0.Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_sel_itm_1_n);
			itm_0.Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_sel_itm_1_a);
			itm_0.Image () = 0 ;
			items.Append(itm_0);

			items.Selected(0);

			if (FAILED(hr_)) {
				m_error = sel_ctrl.Error();
				m_error.Show();
			}
			else {
				sel_ctrl.Window().SetWindowPos(HWND_TOP, &rc_, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE);
				sel_ctrl.Layout().Update(rc_);
			}
#pragma endregion
			return m_error;
		}
	};
}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CMedia::CViewWnd:: CViewWnd(CMedia& _view) : TViewWnd(_view), m_view(_view), m_select(_view) {}
CMedia::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CMedia::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	const RECT rc_ = Lay_(*this); rc_; HRESULT hr_ = S_OK; hr_; TViewWnd::SetWindowTextW(_T("Media"));
	CMedia_Init init_(m_view); init_.OnCreate();
	hr_ = m_ipod.Create(*this, CMedia_Layout(m_view).GetSubViewRect(), true);
	if (SUCCEEDED(hr_)) {
		m_ipod.Window().SetWindowTextW(_T("media::ipod::subview"));
	}

	return 0;
}

LRESULT   CMedia::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_b_hand = FALSE;
	m_ipod.Destroy();
	m_select.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CMedia::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {

	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};
	CMedia_Layout lay_(m_view);
	// TODO: select control applies border thickness format when updates the layout; must be reviewed;
	m_select.Layout() << lay_.GetSelectRect();
	m_select.Refresh();
	m_ipod.Update(lay_.GetSubViewRect());

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CMedia:: CMedia (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CMedia::~CMedia (void) {}

/////////////////////////////////////////////////////////////////////////////

TiPodView& CMedia::iPod    (void) { return m_wnd.m_ipod; }
TSelect&   CMedia::Select  (void) { return m_wnd.m_select; }