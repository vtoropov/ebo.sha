#ifndef _EBOSHAGUIFMTINFO_H_CC3EAEB5_DB08_48C0_B135_4207ACE4C292_INCLUDED
#define _EBOSHAGUIFMTINFO_H_CC3EAEB5_DB08_48C0_B135_4207ACE4C292_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Jan-2021 at 12:44:16.201 pm, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Sha Optima Tool info panel GUI format interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:03:27.555 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.uix.info.panel.h"

namespace ebo { namespace sha { namespace gui { namespace format {

	class CInfoPanel {
	public:
		 CInfoPanel (void);
		~CInfoPanel (void);

	public:
		HRESULT   ApplyTo (TInfoPanel&) const;
	public:
		const CInfoPanel& operator >> (TInfoPanel&) const;
	};

	class CInfoAsterisk : private CInfoPanel { typedef CInfoPanel TBase;
	public:
		 CInfoAsterisk (void);
		~CInfoAsterisk (void);

	public:
		HRESULT   ApplyTo (TInfoPanel&) const;
	public:
		const CInfoAsterisk& operator >> (TInfoPanel&) const;
	};

	class CInfoError : private CInfoPanel { typedef CInfoPanel TBase;
	public:
		 CInfoError (void);
		~CInfoError (void);

	public:
		HRESULT   ApplyTo (TInfoPanel&) const;
	public:
		const CInfoError& operator >> (TInfoPanel&) const;
	};

	class CInfoWarn : private CInfoPanel { typedef CInfoPanel TBase;
	public:
		 CInfoWarn (void);
		~CInfoWarn (void);

	public:
		HRESULT   ApplyTo (TInfoPanel&) const;
	public:
		const CInfoWarn& operator >> (TInfoPanel&) const;
	};

}}}}

typedef ebo::sha::gui::format::CInfoPanel    TInfoPanelFmt;
typedef ebo::sha::gui::format::CInfoAsterisk TInfoAsterisk;
typedef ebo::sha::gui::format::CInfoError    TInfoErrorFmt;
typedef ebo::sha::gui::format::CInfoWarn     TInfoWarnFmt ;

#endif/*_EBOSHAGUIFMTINFO_H_CC3EAEB5_DB08_48C0_B135_4207ACE4C292_INCLUDED*/