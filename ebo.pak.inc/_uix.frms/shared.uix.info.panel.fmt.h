#ifndef _SHAREDUIXINFOPANELFMT_H_8A558F90_8319_4385_9816_B12A965BB224_INCLUDED
#define _SHAREDUIXINFOPANELFMT_H_8A558F90_8319_4385_9816_B12A965BB224_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Jan-2021 at 11:20:38.649 pm, UTC+7, Novosibirsk, Friday;
	This is Ebo Pack shared information panel format interface declaration file;
*/
#include "shared.uix.ctrl.base.fmt.h"

namespace ex_ui { namespace frames {

	typedef ::std::vector<CStringW> TPhraseSeparators;

	class CInfoText_Seps {
	private:
		TPhraseSeparators  m_seps; // is empty by default;

	public:
		 CInfoText_Seps (void);
		 CInfoText_Seps (const CInfoText_Seps&);
		~CInfoText_Seps (void);

	public:
		HRESULT    Append (LPCWSTR _lp_sz_sep);   // is protected by try() section, but is not necessary because std::vector takes care;
		HRESULT    Clear  (void) ;
		bool       Is     (void) const;
		bool       Has    (LPCWSTR _lp_sz_sep) const; // is just for kidding, maybe;
		const
		TPhraseSeparators& Get (void) const;
		TPhraseSeparators& Set (void)      ;

	public:
		CInfoText_Seps&  operator = (const CInfoText_Seps&);
		CInfoText_Seps&  operator+= (LPCWSTR _lp_sz_sep);     // removing/excluding particular line separator is not implemented yet;
	};

	class CInfoText_Fromat {
	private:
		bool  m_phrases;  // if true, separates message to different phrases if possible; useful for multiline text output; by default is false;
		bool  m_oneline;  // if true, text output is made in one line; ellipsis are added automatically at the end; by default is false;

		CInfoText_Seps m_seps;

	public:
		 CInfoText_Fromat (void);
		 CInfoText_Fromat (const CInfoText_Fromat&);
		~CInfoText_Fromat (void);

	public:
		bool  OneLine (void) const;
		bool& OneLine (void)      ;
		bool  Phrases (void) const;
		bool& Phrases (void)      ;

		const
		CInfoText_Seps& Separators (void) const;
		CInfoText_Seps& Separators (void)      ;

	public:
		 CInfoText_Fromat& operator = (const CInfoText_Fromat&);
		 CInfoText_Fromat& operator <<(const CInfoText_Seps&);
	};

	class CInfoPanel_Format : public TFmtBase { typedef TFmtBase TBase;
	private:
		  CInfoText_Fromat   m_text_options;

	public:
		 CInfoPanel_Format (void);
		 CInfoPanel_Format (const CInfoPanel_Format&);
		~CInfoPanel_Format (void);

	public:
		const
		CInfoText_Fromat&   TextOutput(void) const;
		CInfoText_Fromat&   TextOutput(void)      ;

	public:
		 CInfoPanel_Format& operator = (const CInfoPanel_Format&);
		 CInfoPanel_Format& operator <<(const CInfoText_Fromat&);
	};

}}

typedef ex_ui::frames::CInfoText_Fromat   TInfoTxtFmt ;
typedef ex_ui::frames::CInfoPanel_Format  TInfoFormat ;
typedef ex_ui::frames::CInfoText_Seps     TInfoTxtSeps;

#endif/*_SHAREDUIXINFOPANELFMT_H_8A558F90_8319_4385_9816_B12A965BB224_INCLUDED*/