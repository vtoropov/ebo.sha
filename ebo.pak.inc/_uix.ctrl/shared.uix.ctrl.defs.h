#ifndef _UIXCTRLCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED
#define _UIXCTRLCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-May-2012 at 9:36:22am, GMT+3, Rostov-on-Don, Friday;
	This is Pulsepay Shared Skinned Control Base class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 9-Feb-2015 at 7:23:30pm, GMT+3, Taganrog, Monday;
	Adopted to FakeGPS project on 24-Apr-2020 at 7:50:15p, UTC+7, Novosibirsk, Friday;
*/
#include "shared.uix.gdi.provider.h"
#include "shared.uix.gdi.object.h"
#include "shared.uix.gdi.draw.shape.h"

#include "shared.gen.sys.err.h"

namespace ex_ui { namespace controls { 

	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::CColour;
	using ex_ui::draw::CColour_Ex;
	using ex_ui::draw::CGradient ;

	using shared::sys_core::CError;

	class CAlign {
	public:
		enum class eHorz { eLeft   = DT_LEFT   , eCenter = DT_CENTER, eRight  = DT_RIGHT  };
		enum class eVert { eMiddle = DT_VCENTER, eTop    = DT_TOP   , eBottom = DT_BOTTOM };

	protected:
		eHorz   m_horz;
		eVert   m_vert;
	public:
		 CAlign (void);
		 CAlign (const CAlign&);
		 CAlign (const eHorz, const eVert);
		~CAlign (void);

	public:
		eHorz   Horz(void) const;
		eHorz&  Horz(void)      ;
		eVert   Vert(void) const;
		eVert&  Vert(void)      ;

	public:
		CAlign& operator = (const CAlign&);
		CAlign& operator <<(const eHorz &);
		CAlign& operator <<(const eVert &);

	public:
		CAlign& operator <<(LPCWSTR _lp_sz_style); // creates this class object from XML fragment text; for example: {left|middle};
	};

	class CSides {
	public:
		enum _side : UINT {
			e_left   = 0x0,
			e_top    = 0x1, // by default;
			e_right  = 0x2,
			e_bottom = 0x3,
		};
	protected:
		_side    m_selected;

	public:
		 CSides (void);
		 CSides (const _side _selected);
		 CSides (const CSides&);
		~CSides (void);

	public:
		_side   Selected(void) const;
		_side&  Selected(void)      ;

	public:
		CSides& operator = (const CSides&);
		CSides& operator <<(const _side _selected) ;
	}; typedef CSides::_side TSide;

	class CPosition : public ex_ui::draw::shape::CPosition {
	                 typedef ex_ui::draw::shape::CPosition TBase;
	};

	class CProperties {
	protected:
		DWORD    m_value;

	protected:
		CProperties (void) ;
	explicit
		CProperties (const DWORD dwValue);
		CProperties (const CProperties& );
		virtual ~CProperties(void);
	public:
		bool     Has   (const DWORD) const;
		VOID     Modify(const DWORD, const bool _bApply);
	public:
		const
		DWORD&   Value (void) const;
		DWORD&   Value (void)      ;

	public:
		operator const DWORD(void) const;
		CProperties&   operator  = (const CProperties&);
		CProperties&   operator << (const DWORD);
		CProperties&   operator += (const DWORD);
		CProperties&   operator -= (const DWORD);
	};

	class CState : public CProperties { typedef CProperties TBase;
	public:
		// TODO : unfortunately, mixing values is possible, there is no sense if disabled is mixed with selected value;
		enum _e : ULONG {
			eNormal   = 0x00, // default value that indicates no action, which is affected a state, if applied; 
			eDisabled = 0x01, eSelected = 0x02, eHovered = 0x04, ePressed = 0x08,
		};
	public:
		 CState (void);
	explicit
		 CState (const DWORD dwValue);
		 CState (const CState&);
		~CState (void);

	public:
		bool  IsDisabled (void) const;
		bool  IsHovered  (void) const;
		bool  IsPressed  (void) const;
		bool  IsSelected (void) const;          // not sure that it is right name of method; it can be used by button control in default state;
#if defined(_DEBUG)
		CStringW  Print  (void) const;
#endif
	public:
		DWORD   Get (void) const;
		DWORD   Set (const CState::_e);         // returns previous value;

	public:
		CState& operator = (const CState&);     // makes a copy;
		CState& operator <<(const CState::_e);  // sets the current state;
	}; typedef CState TCtrlState;

	class CStyle : public CProperties {
	public:
		enum _e : ULONG {
			eNone   = 0x00,  // default, no style is applied;
			eBorder = 0x01,
		};
	private:
		DWORD  m_value;

	public:
	explicit
		 CStyle(const DWORD dwStyle = _e::eNone);
		~CStyle(void);

	public:
		bool   IsBordered(void) const;
		VOID   IsBordered(const bool);
	}; typedef CStyle TCtrlStyle;

	interface IControlEvent
	{
		virtual  HRESULT  IControlEvent_OnClick(const UINT ctrlId) { ctrlId; return E_NOTIMPL; }
		virtual  HRESULT  IControlEvent_OnClick(const UINT ctrlId, const LONG_PTR nData) { ctrlId; nData; return E_NOTIMPL; }
		virtual  HRESULT  IControlEvent_OnHover(const UINT ctrlId) { ctrlId; return E_NOTIMPL; }  // mouse hovers over an element of the control;
		virtual  HRESULT  IControlEvent_OnEnter(void) { return E_NOTIMPL; }  // mouse enters to control area;
		virtual  HRESULT  IControlEvent_OnLeave(void) { return E_NOTIMPL; }  // mouse leaves the control area;
	};

	class CMargins {
	protected:
		INT    m_sides[TSide::e_bottom + 1];

	public:
	explicit
		 CMargins(const INT _lft = 0, const INT _top = 0, const INT _rht = 0, const INT _btm = 0);
		 CMargins(const CMargins&);
		~CMargins(void);

	public: // acceessor(s);
		const INT&   Bottom (void)  const; INT&   Bottom (void)       ;
		const INT&   Left   (void)  const; INT&   Left   (void)       ;
		const INT&   Right  (void)  const; INT&   Right  (void)       ;
		const INT&   Top    (void)  const; INT&   Top    (void)       ;

	public:
		const void   ApplyTo(RECT&) const;
		const RECT   Convert(const RECT&) const; // the same as ApplyTo();
		const bool   Is     (void)  const;       // returns true if any side is not equal to zero;

	public:
		operator const bool (void)  const;
		const CMargins& operator<<(RECT&) const;
	public:
		CMargins&  operator = (const CMargins&);
	public:
		CMargins&  operator <<(LPCWSTR _lp_sz_style); // creates this class object from XML fragment text;
	};
}}

typedef ex_ui::controls::CPosition        TPosition  ;
typedef ex_ui::controls::CAlign           TAlign     ;
typedef ex_ui::controls::CAlign::eHorz    THorzAlign ;
typedef ex_ui::controls::CAlign::eVert    TVertAlign ;
typedef ex_ui::controls::CMargins         TMargins   ;

#endif/*_UIXCTRLCOMMONDEFINITIONS_H_AD2F5A63_32F0_40b2_935F_DFEBF2590E79_INCLUDED*/