#ifndef _EBOSHAMEDIACONN_H_B4415F1D_4E69_4A36_86A2_0DA47D21D0FF_INCLUDED
#define _EBOSHAMEDIACONN_H_B4415F1D_4E69_4A36_86A2_0DA47D21D0FF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Dec-2020 at 3:12:00.396 pm, UTC+7, Novosibirsk, Friday;
	This is Ebo Sha optima tool media iPod connected device sub-view interface declaration file;
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.view.empty.h"
#include "ebo.sha.gui.cmp.cap.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CiPod_Device : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CiPod_Device;
		private:
			CiPod_Device& m_view   ;
			TCapComp      m_caps[1];
			TEmptyView    m_empty  ;

		private:
			 CViewWnd (CiPod_Device&);
			~CViewWnd (void);

		private:   // TWindow
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CiPod_Device (void);
		~CiPod_Device (void);
	};

}}}}

typedef ebo::sha::gui::view::CiPod_Device TiPodDevice;

#endif/*_EBOSHAMEDIACONN_H_B4415F1D_4E69_4A36_86A2_0DA47D21D0FF_INCLUDED*/