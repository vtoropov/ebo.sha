#ifndef _SHAREDLITESYSTEMERROR_H_E4336D27_03C6_4005_B45C_D443D6D971ED_INCLUDED
#define _SHAREDLITESYSTEMERROR_H_E4336D27_03C6_4005_B45C_D443D6D971ED_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 09-Jul-2010 at 6:36:26p , GMT+3, Rostov-on-Don, Friday;
	This is Row27 project data model error class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Pulspay project on 15-May-2012 at 9:39:48pm, GMT+3, Rostov-on-Don, Tuesday;
	Adopted to Platinum project on 19-Mar-2014 at 7:02:24am, GMT+4, Taganrog, Wednesday;
	Adopted to VS v15 on 16-May-2018 at 5:28:25p, UTC+7, Phuket, Rawai, Monday;
	Adopted to FakeGPS driver project on 13-Dec-2019 at 10:19:37a, UTC+7, Novosibirsk, Friday;
*/
#include "shared.gen.syn.obj.h"

#if defined(_UNICODE)
#define __MODULE__  __FUNCTIONW__
#else
#define __MODULE__  __FUNCTION__
#endif

#define __HresultToDword(_hres) (_hres & 0x0000FFFF)
#define __DwordToHresult(_word)  HRESULT_FROM_WIN32(_word)
#define __LastErrToHresult()   __DwordToHresult(::GetLastError())
#define __I_am_lucky(_hresult)  (S_OK == _hresult)

namespace shared { namespace sys_core
{
	struct CLang {
		DWORD  dwPrimary;  // primary language Id
		DWORD  dwSecond;   // sub-language Id

		CLang(void) : dwPrimary(LANG_NEUTRAL), dwSecond(SUBLANG_DEFAULT) {}
		CLang(const DWORD _primary, const DWORD _second) : dwPrimary(_primary), dwSecond(_second) {}

		DWORD  Id (void) const { return MAKELANGID(dwPrimary, dwSecond); }
	};

	typedef const CLang& TLangRef;

	class CErr_Base {
	protected:
		DWORD         m_code   ;
		HRESULT       m_result ;
		CLang         m_lang   ;
		CSyncObject   m_lock   ;
	public:
		CErr_Base(void);
		CErr_Base(const DWORD  dwError, const CLang&);
		CErr_Base(const HRESULT hError, const CLang&);
	public:
		CErr_Base& operator= (const DWORD   _code)    ;    // sets error result from win 32 error code;
		CErr_Base& operator= (const HRESULT _hres)    ;    // sets error result; S_OK is acceptable;
		CErr_Base& operator= (TLangRef) ;
	public:
		operator DWORD    (void) const;   // gets error state Win API code;
		operator HRESULT  (void) const;   // gets error state COM result  ;
		operator TLangRef (void) const;   // gets error description language;
		operator TSyncRef (void)      ;   // gets synchronize object referecnce;
	};

	class CErr_State : public CErr_Base {
	                  typedef CErr_Base TBase;
	protected:
		CStringW   m_buffer; // error details' buffer;
	public:
		 CErr_State (void);
		 CErr_State (const CErr_State&);
		~CErr_State (void);
	public:
		LPCWSTR Get (void) const;        // gets error state details;
		VOID    Set (const bool _reset); // if _reset is true, error state is OLE_E_BLANK; otherwise, the state is set to false (i.e. S_OK);
		VOID    Set (const DWORD   _err_code);
		VOID    Set (const DWORD   _err_code, LPCWSTR _lp_sz_desc, ...); // sets the object state manually;
		VOID    Set (const DWORD   _err_code, const UINT resId); // sets the object state manually, description is loaded from string resource specified by identifier
		VOID    Set (const HRESULT _err_code, LPCWSTR _lp_sz_desc, ...); // sets the object state manually;
		VOID    Set (const HRESULT _err_code, const UINT resId); // sets the object state manually, description is loaded from string resource specified by identifier
		VOID    Set (LPCWSTR  _sz_desc)     ; // sets a state description;
		VOID    Set (LPCWSTR  _sz_desc, ...); // sets a state description from pattern and multiple arguments;
	public:
		operator bool     (void) const;   // returns TRUE when state indicates a failure (i.e. is not S_OK);
		operator LPCWSTR  (void) const;   // gets error state details     ;
	public:
		CErr_State&   operator= (const bool _reset); // if _reset is true, error state is a failure ; otherwise, the state is false (i.e. S_OK);
		CErr_State&   operator= (const CErr_State&);
		CErr_State&   operator= (LPCWSTR  _sz_desc);
	};

	bool operator==(const bool _lhs, const CErr_State& _rhs);
	bool operator!=(const bool _lhs, const CErr_State& _rhs);
	bool operator==(const CErr_State& _lhs, const bool _rhs);
	bool operator!=(const CErr_State& _lhs, const bool _rhs);

	typedef const CErr_State&  TErrStateRef;

	class CError {
	protected:
		mutable
		CErr_State    m_state ;   // error state;
		CStringW      m_source;   // a name of a component that spawns an error;
		CStringW      m_module;   // fully qualified path to a routine where an error is spawned;
	public:
		CError(void);
		CError(const CError&);
	   ~CError(void);
	public:
		virtual
		HRESULT       Result(const HRESULT _new); // sets new error state result and returns a previous one;
	public:
		VOID          Clear (void)       ;        // sets the error object to success state, i.e. no error state;
		DWORD         Code  (void) const ;        // gets Win API error code;
		VOID          Code  (const DWORD);        // sets Win API error code;
		LPCWSTR       Desc  (void) const ;        // gets the current description;
		bool          Is    (void) const ;        // returns true if the object is in error state, otherwise, false, i.e. no error;
		TLangRef&     Lang  (void) const ;        // gets language identifier;
		VOID          Last  (void)       ;        // updates error state by getting last error code by ::GetLastError() function;
		LPCWSTR       Module(void) const ;        // gets module name that produces the error, if any
		VOID          Module(LPCWSTR)    ;        // sets module name that produces the error
		VOID          Reset (void)       ;        // re-sets the error object to blank state (OLE_E_BLANK)
		HRESULT       Result(void) const ;        // gets the current result code
		DWORD         Show  (const HWND  = ::GetActiveWindow()) const;
		CErr_State&   State (void)       ;
		TErrStateRef  State (void) const ;
		LPCTSTR       Source(void) const ;                               // gets the error source;
		VOID          Source(LPCWSTR)    ;                               // sets the error source;
		VOID          Source(LPCWSTR _lp_sz_val, const bool bFormatted); // sets the error source;
	public:
		CError& operator<<(const HRESULT _hr);    // sets error result; intended for using in routines for error initial state set;
		CError& operator<<(LPCWSTR)          ;    // sets error module; intended for using in routines for error initial state set;
		CError& operator= (const _com_error&);    // sets error info from COM error object;
		CError& operator= (const CError&)    ;    // sets error info from other error object;
		CError& operator= (const DWORD _code);    // sets error result from win 32 error code;
		CError& operator= (const HRESULT _hr);    // sets error result; S_OK is acceptable;
		CError& operator= (LPCWSTR _lp_sz_ds);    // sets error description;
		CError& operator>>(LPCWSTR _lp_sz_sc);    // sets error source; intended for using in routines for error initial state set;
	public:
		operator const bool  (void) const    ;    // returns true if error object is in error state, otherwise false;
		operator HRESULT     (void) const    ;    // returns error result;
		operator LPCWSTR     (void) const    ;    // returns error description;
		operator CErr_State& (void)          ;    // returns error state (rw) ;
		operator TErrStateRef(void) const    ;    // returns error state (ra) ;
	};

	bool operator==(const bool _lhs, const CError& _rhs);
	bool operator!=(const bool _lhs, const CError& _rhs);
	bool operator==(const CError& _lhs, const bool _rhs);
	bool operator!=(const CError& _lhs, const bool _rhs);

	class CErr_Format {
	protected:
		const
		CError&    m_error_ref;

	public:
		 CErr_Format (const CError&);
		~CErr_Format (void);

	public:
		CStringW   Do  (LPCWSTR _lp_sz_sep = NULL) const; // gets formatted string: {code|@sep|description|@sep|module|@sep|source};
		CStringW   Do_2(LPCWSTR _lp_sz_sep = NULL) const; // gets formatted string: {code|@sep|description|@sep|module};
		CStringW   Do_4(LPCWSTR _lp_sz_sep = NULL) const; // gets formatted string: {code|@sep|description};
		CStringW   Do_6(LPCWSTR _lp_sz_pattern   ) const; // pattern: $(x) - hresult; $(c) - code; $(d) - desc; $(m) - module; $(s) - source;
		                                                  // for example, output to console:
		                                                  // This is error:\n\tcode=%d$(c)\n\tdesc=%s$(d)\n\tmodule=%s$(m)\n\tsrc=%s$(s);
	};

}}
typedef const shared::sys_core::CError&  TErrorRef;

#endif/*_SHAREDLITESYSTEMERROR_H_E4336D27_03C6_4005_B45C_D443D6D971ED_INCLUDED*/