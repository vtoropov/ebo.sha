#ifndef _DRIVERDEVLOCGUI_H_8F9EB3CA_B9BD_4AA3_92C6_2E6D959D8599_INCLUDED
#define _DRIVERDEVLOCGUI_H_8F9EB3CA_B9BD_4AA3_92C6_2E6D959D8599_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Jan-2021 at 5:19:02.876 pm, UTC+7, Novosibirsk, Thursday;
	This is generic driver executable file locator GUI extension interface declaration file;
*/
#include "driver.dev.loc.h"
#include <atldlgs.h>        // WTL;
#include <shobjidl.h>

namespace shared { namespace driver { namespace client {

	class CDrvLocator_Ext : public TDrvLocatePers { typedef TDrvLocatePers TBase;
	public:
		 CDrvLocator_Ext (void);
		 CDrvLocator_Ext (const CDrvLocator_Ext&);
		~CDrvLocator_Ext (void);

	public:
		HRESULT    Select(const HWND _h_parent);

	public:
		 CDrvLocator_Ext& operator = (const CDrvLocator_Ext&);
	};

}}}

typedef shared::driver::client::CDrvLocator_Ext  TDrvLocateExt;

#endif/*_DRIVERDEVLOCGUI_H_8F9EB3CA_B9BD_4AA3_92C6_2E6D959D8599_INCLUDED*/