#ifndef _SFXSTRIPFMT_H_8DB0C153_79F8_4665_A032_5E862025B372_INCLUDED
#define _SFXSTRIPFMT_H_8DB0C153_79F8_4665_A032_5E862025B372_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Aug-2020 at 12:52:41p, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack Sfx status bar control format interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Sfx Strip project on 6-Sep-2020 at 2:02:21p, UTC+7, Novosibirsk, Sunday;
*/
#include "shared.uix.ctrl.base.fmt.h"
#include "shared.uix.ctrl.defs.h"
namespace ST_Ctrls { namespace format { namespace strip {

	using ex_ui::controls::CBorder ;
	using ex_ui::controls::CBorders;

	using ex_ui::controls::format::CBase;
	using ex_ui::controls::format::TVisualStateAssoc;

	class CItem {
	protected:
		CBorder    m_border;
		WORD       m_res_id;

	public:
		 CItem (void);
		 CItem (const CItem&);
		~CItem (void);

	public:
		const
		CBorder&   Border (void) const;
		CBorder&   Border (void)      ;
		WORD       Image  (void) const;  // returns resource identifier of the image or 0 if not set;
		WORD&      Image  (void)      ;  // sets image resource identifier; 0 is acceptable - no image;

	public:
		CItem& operator = (const CItem&);
		CItem& operator <<(const CBorder&);
		CItem& operator <<(const WORD _res_id);
	};

	class CItem_State {
	public:
		enum _state : UINT {
			e_active = 0, e_clicked, e_disabled, e_hovered, e_normal};
	public:
		static const UINT n_count = _state::e_normal + 1;
	}; typedef CItem_State::_state TState;

	class CItems {
	protected:
		CItem   m_items[CItem_State::n_count];
		UINT    m_width;  // a width of each item; is used by layout module for item rectangle calculation;

	public:
		 CItems (void);
		 CItems (const CItems&);
		~CItems (void);

	public:
		const
		CItem&   Item   (const TState) const;
		CItem&   Item   (const TState)      ;
		const
		CItem&   Normal (void) const;
		CItem&   Normal (void)      ;
		UINT     Width  (void) const;
		UINT&    Width  (void)      ;

	public:
		CItems& operator = (const CItems&);
		CItems& operator <<(const UINT _n_itm_width);
	};

	class CStrip : public TFmtBase { typedef TFmtBase TFormat;
	protected:
		CItems   m_items;
		DWORD    m_img_res;

	public:
		 CStrip (void);
		 CStrip (const CStrip&);
		~CStrip (void);

	public:
		DWORD    Images(void) const;
		DWORD&   Images(void)      ;
		const
		CItems&  Items (void) const;
		CItems&  Items (void)      ;

	public:
		CStrip&  operator = (const CStrip& );
		CStrip&  operator <<(const CItems&  );
		CStrip&  operator <<(const DWORD _img_res);
	};
}
	class CSymantec_NA_Str : public ST_Ctrls::format::strip::CStrip {
	                        typedef CStrip TBase;
	public:
		 CSymantec_NA_Str (void);
		~CSymantec_NA_Str (void);
	};

	class CWhitespread_Str : public ST_Ctrls::format::strip::CStrip {
	                        typedef CStrip TBase;
	public:
		 CWhitespread_Str (void);
		~CWhitespread_Str (void);
	};
}}

typedef ST_Ctrls::format::strip::CStrip  TStripFmt;

#endif/*_SFXSTAFMT_H_8DB0C153_79F8_4665_A032_5E862025B372_INCLUDED*/