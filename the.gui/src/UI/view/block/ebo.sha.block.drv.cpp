/*
	Created by Tech_dog (ebontrop@gmail.com) on 31-Dec-2020 at 5:34:37.061 am, UTC+7, Novosibirsk, Thursday;
	This is Ebo Sha Optima Tool HTTP connection control driver mode sub-view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.block.drv.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CHttp_Drv_Cmds {
	public:
		enum _cmd :	DWORD {
			e_undef   = 0x0,
			e_driver  = 0x1,
			e_service = 0x2,
		};
	}; typedef CHttp_Drv_Cmds This_Cmds;

	class CHttp_Drv_Ctrls {
	public:
		enum _ctrl {
			e_name    = 0x0, // _T("Name" );
			e_type    = 0x1, // _T("Type" );
			e_path    = 0x2, // _T("Path" );
			e_start   = 0x3, // _T("Start");
			e_state   = 0x4, // _T("State");
			e_desc    = 0x5, // _T("Desc" );
			c__count  = e_desc + 1
		};
	public:    CHttp_Drv_Ctrls (void) {} ~CHttp_Drv_Ctrls(void) {} DWORD Count(void) const { return _ctrl::c__count; }
	}; typedef CHttp_Drv_Ctrls This_Ctrls;

	class CHttp_Drv_Res {
	public:
		enum _res : WORD {
			e_none     = 0,
			e_mode_drv = IDR_EBO_SHA_BLK_DRV_MOD_M ,
			e_info_err = IDS_EBO_SHA_BLK_PAN_INF_E ,
			e_info_ast = IDS_EBO_SHA_BLK_PAN_INF_I ,
			e_info_wrn = IDS_EBO_SHA_BLK_PAN_INF_W ,
		};
	}; typedef CHttp_Drv_Res This_Res;

	class CHttp_Drv_Layout {
	public:
		typedef TCtrlDefLayout::_pos _pos;
	private:
		CHttp_Driver& m_view;
		RECT          m_rect;

	public:
		 CHttp_Drv_Layout (CHttp_Driver& _view) : m_view(_view), m_rect{0} {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
		}
		~CHttp_Drv_Layout (void) {}

	public:
		RECT   GetButton(const DWORD _ndx) {  _ndx;
			RECT rc_edt  = this->GetEdt_Ctrl(This_Ctrls::e_path);
			RECT rc_but  = this->GetEdt_Ctrl(0 == _ndx ? This_Ctrls::e_state : This_Ctrls::e_desc);
			rc_but.left  = rc_but.right + _pos::e_lab_gap;
			rc_but.right = rc_edt.right;
			return rc_but;
		}
		RECT   GetImage (const DWORD _ndx) {
			static const SIZE sz_logo = { 64, 64 };
			RECT rc_logo = {
				0, (0 == _ndx ? 0 : 0), 0 + sz_logo.cx, (0 == _ndx ? sz_logo.cy : 0 + sz_logo.cy)
			};
			return rc_logo;
		}
		RECT   GetTitle (const DWORD _ndx) { return this->_get_cap_rect(_ndx); }
		
	public:
		RECT   GetLab_Ctrl(const DWORD _ndx) {
			_ndx;
			RECT rc_lab = {_pos::e_lab_left, m_rect.top + (_pos::e_cap_height + _pos::e_lab_gap), _pos::e_lab_left + _pos::e_lab_width, 0 };

			rc_lab.top += (_pos::e_lab_height + _pos::e_lab_gap) * _ndx; rc_lab.bottom = rc_lab.top + _pos::e_lab_height;

			return rc_lab;
		}
		RECT   GetEdt_Ctrl(const DWORD _ndx) {
			_ndx;
			RECT rc_edit = {_pos::e_lab_left   + _pos::e_lab_width, m_rect.top + (_pos::e_cap_height + _pos::e_lab_gap), 
				_pos::e_lab_left   + _pos::e_lab_width + _pos::e_edt_width, 0
			};
			rc_edit.top += (_pos::e_edt_height + _pos::e_lab_gap - _pos::e_edt_border_thickness) * _ndx;
			rc_edit.top -= (_pos::e_edt_border_thickness * (1 + _ndx)); rc_edit.bottom = rc_edit.top + _pos::e_edt_height;

			if (2 == _ndx)
				rc_edit.right += 180;
			return rc_edit;
		}
		RECT   InfArea  (VOID) {
			RECT rc_inf = m_rect; rc_inf.top = rc_inf.bottom - 75;
			return rc_inf;
		}

		VOID   Update   (TCapComp* _p_caps, const DWORD _d_count) {
			if (NULL == _p_caps || 0 == _d_count)
				return;
			for (DWORD i_ = 0; i_ < _d_count; i_++)
				_p_caps[i_].Title().Layout().Update(this->_get_cap_rect(i_));
		}

	private:
		RECT  _get_cap_rect(const DWORD _ndx) {
			RECT rc_cap = m_rect; rc_cap.top = (0 ==_ndx ? 0 : 0); rc_cap.bottom = rc_cap.top + TCtrlDefLayout::e_cap_height;
			return rc_cap;
		}
	};

	class CHttp_Drv_Init {
	public:
		 CHttp_Drv_Init (void) {}
		~CHttp_Drv_Init (void) {}

	public:
		HRESULT  Apply  (const TDrvSvcPropSet& _props, TEditRa* _p_ctrls,  const UINT _u_count) { HRESULT hr_ = S_OK;
			_props; _p_ctrls; _u_count;
			if (NULL == _p_ctrls) return (hr_ = E_POINTER);
			if (0 == _u_count) return (hr_ = E_INVALIDARG);

			for (UINT i_ = 0; i_ < _u_count; i_++) {
				if (0 == i_) _p_ctrls[i_].Edit ().Text(_props.Property(TDrvSvcPropSet::e_name ));
				if (1 == i_) _p_ctrls[i_].Edit ().Text(_props.Property(TDrvSvcPropSet::e_type ));
				if (2 == i_) _p_ctrls[i_].Edit ().Text(_props.Property(TDrvSvcPropSet::e_path ));
				if (4 == i_) _p_ctrls[i_].Edit ().Text(_props.Property(TDrvSvcPropSet::e_state));
				if (3 == i_) _p_ctrls[i_].Edit ().Text(_props.Property(TDrvSvcPropSet::e_start));
				if (5 == i_) _p_ctrls[i_].Edit ().Text(_props.Property(TDrvSvcPropSet::e_desc ));
			}
			return hr_;
		}
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CHttp_Driver::CViewWnd:: CViewWnd(CHttp_Driver& _view) : TViewWnd(_view), m_view(_view), m_info(_view), m_btns {{_view}, {_view}} {}
CHttp_Driver::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CHttp_Driver::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	CHttp_Drv_Layout layout_(m_view);

	HRESULT hr_ = m_caps[0].Create(*this, _T("Driver Control"), layout_.GetTitle(0), layout_.GetImage(0));
	if (SUCCEEDED(hr_)) {
		m_caps[0].Format(This_Res::e_mode_drv);
		m_caps[0].Image().Format().Bkgnd().Sectors().Append(shared::Get_Theme().Get(TThemePart::e_caption,  TThemeElement::e_back), TTileSize(80, 30));
		m_caps[0].Image().Format().Bkgnd().Sectors().Append(shared::Get_Format().Panel().Bkgnd().Solid(), TTileSize(80, 50));
		m_caps[0].Image().Layout().Update();
	}

	TCmdSvcState cmd_sta;
	hr_ = cmd_sta.Execute();
	const TDrvSvcPropSet& props = cmd_sta.Service().Properties();
	m_svc_sta << cmd_sta;

	// TODO: must be reviewed for configuring in appropriate way;
	static LPCWSTR lp_sz_ctrl_cap[] = {_T("Name"), _T("Type"), _T("Path"), _T("Start"), _T("State"), _T("Description")};

	for (UINT i_ = 0; i_ < _countof(m_ctrl) && i_ < _countof(lp_sz_ctrl_cap); i_++) {
		shared::Get_Format().Control().Edit() >> m_ctrl[i_].Edit().Borders();
		shared::Get_Format().Control().Edit() >> m_ctrl[i_].Edit().Format();
		shared::Get_Format().Control().Edit() >> m_ctrl[i_].Label().Format();

		m_ctrl[i_].Create(*this, layout_.GetLab_Ctrl(i_), layout_.GetEdt_Ctrl(i_));
		m_ctrl[i_].Label().Text(lp_sz_ctrl_cap[i_]);
	}
	CHttp_Drv_Init().Apply(props, m_ctrl, _countof(m_ctrl));

	for (UINT i_ = 0; i_ < _countof(m_btns); i_++) {
		shared::Get_Format().Control() >> m_btns[i_].Format();
		if (1 == i_) { hr_ = m_btns[i_].Create(*this, layout_.GetButton(i_), This_Cmds::e_driver); m_btns[i_].Layout().Layers().Text() << _T("Browse..."); }
		if (0 == i_) { hr_ = m_btns[i_].Create(*this, layout_.GetButton(i_), This_Cmds::e_service); m_btns[i_].Layout().Layers().Text() << _T("Start"); }
		m_btns[i_].Borders() << (DWORD)1;
		m_btns[i_].Borders() >> shared::Get_Theme().Get(TThemePart::e_button, TThemeElement::e_border);
		m_btns[i_].Layout ().Update();
	}

	if (m_svc_sta.State() == TSvcStateEnum::e_to_install) {
		m_btns[0].Text (_T("Create"));
	}
	if (m_svc_sta.State() == TSvcStateEnum::e_to_start) {
		//	m_btns[0].Text (_T("Create")); // already assigned;
	}
	m_btns[0].State().Highlighted() = true;
	m_btns[0].State() << TButtonState::eSelected;

	const bool b_use_error = m_svc_sta.Error().Is() ;
	const bool b_use_info  = false;
	const bool b_use_warn  = false;

	if (b_use_error) TInfoErrorFmt() >> m_info ;
	if (b_use_info ) TInfoAsterisk() >> m_info ;
	if (b_use_warn ) TInfoWarnFmt () >> m_info ;

	CStringW cs_msg;
	m_info.Create(*this, layout_.InfArea(), cs_msg.GetString(), 0);
	return 0;
}

LRESULT   CHttp_Driver::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	for (UINT i_ = 0; i_ < _countof(m_caps); i_++) m_caps[i_].Destroy();
	for (UINT i_ = 0; i_ < _countof(m_ctrl); i_++) m_ctrl[i_].Destroy();
	for (UINT i_ = 0; i_ < _countof(m_btns); i_++) m_btns[i_].Destroy();
	m_info.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CHttp_Driver::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CHttp_Drv_Layout layout_(m_view); layout_.Update(m_caps, _countof(m_caps));

	if (m_info.Is()) {
		m_info.Layout ().Update(layout_.InfArea());
		m_info.Refresh();
	}

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CHttp_Driver:: CHttp_Driver (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CHttp_Driver::~CHttp_Driver (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CHttp_Driver::IControlEvent_OnClick(const UINT ctrlId) { HRESULT hr_ = S_OK;
	ctrlId;
	switch (ctrlId) {
	case This_Cmds::e_driver : {
		TDrvLocateExt loc_drv;
		hr_ = loc_drv.Select(m_wnd);
		if (S_OK == hr_) {
			CStringW cs_msg;
			cs_msg.Format(_T("A driver is selected: %s"), loc_drv.Path());
			shared::Get_AppUI().IAppUI_PutStatusInfo(cs_msg);
		}
		if (S_OK == hr_) {
			TCmdSvcState cmd_sta;
			hr_ = cmd_sta.Execute();
			const TDrvSvcPropSet& props = cmd_sta.Service().Properties();
			m_wnd.m_svc_sta << cmd_sta;
			CHttp_Drv_Init().Apply(props, m_wnd.m_ctrl, _countof(m_wnd.m_ctrl));
		}

	} break;
	case This_Cmds::e_service: {
		if (m_wnd.m_svc_sta.State() == TSvcStateEnum::e_to_delete) {}
		if (m_wnd.m_svc_sta.State() == TSvcStateEnum::e_to_install) {
			TCmdSvcCreate cmd_;
			hr_ = cmd_.Execute(); m_wnd.m_svc_sta << cmd_;
			if (SUCCEEDED(hr_)) {
			}
			else {
				TInfoErrorFmt() >> m_wnd.m_info; m_wnd.m_info.Layout().Text() << m_wnd.m_svc_sta.Desc();
			}
		}
		if (m_wnd.m_svc_sta.State() == TSvcStateEnum::e_to_start) {
			TCmdSvcStart cmd_;
			hr_ = cmd_.Execute(); m_wnd.m_svc_sta << cmd_;
			if (SUCCEEDED(hr_)) {
			}
			else {
				TInfoErrorFmt() >> m_wnd.m_info; m_wnd.m_info.Layout().Text() << m_wnd.m_svc_sta.Desc();
			}
		}
		if (m_wnd.m_svc_sta.State() == TSvcStateEnum::e_to_stop) {}

	} break;
	default: return hr_;        // nothing to do here;
	}
	m_wnd.m_info.Layout ().Update(); // is required for info panel title re-draw; Refresh() must be fixed;
	m_wnd.m_info.Refresh();
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CHttp_Driver::IsVisible(const bool b_state) {

	if (b_state) {
		m_wnd.m_info.Layout().Text() << m_wnd.m_svc_sta.Desc();
		if (m_wnd.m_svc_sta.Error().Is())
			TInfoErrorFmt() >> m_wnd.m_info;
		else if (m_wnd.m_svc_sta.State() == TSvcStateEnum::e_to_start) {
			TInfoWarnFmt()  >> m_wnd.m_info;
			m_wnd.m_info.Layout ().Cache().Current(MB_USERICON);
			m_wnd.m_info.Layout ().Caption().String() = _T("Suggestion");
		}
		else if (m_wnd.m_svc_sta.State() != TSvcStateEnum::e_to_stop)
			TInfoWarnFmt()  >> m_wnd.m_info;
		else
			TInfoAsterisk() >> m_wnd.m_info;
#if (0)
		if (m_wnd.m_cmd_sta.Error().Is()) {
			TInfoErrorFmt() >> m_wnd.m_info;
			m_wnd.m_info.Layout().Text() << m_wnd.m_svc_sta.Desc();
		}
		else {
			TInfoWarnFmt() >> m_wnd.m_info;
			m_wnd.m_info.Layout().Text() << _T("Driver control service is stopped;\nFor enabling monitor process it must be started;");
		}
#endif
		m_wnd.m_info.Layout ().Update();
		m_wnd.m_info.Refresh();
	}

	return TView::IsVisible(b_state);
}

/////////////////////////////////////////////////////////////////////////////

TSshDrvCmdHandler&  THttpDrvView::Handler(void) { return m_handler; }