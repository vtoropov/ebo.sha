#ifndef _FILTERCALLOUTS_H_01D4AD54_665E_4E57_BB73_A0D2C7D4A69A_INCLUDED
#define _FILTERCALLOUTS_H_01D4AD54_665E_4E57_BB73_A0D2C7D4A69A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Aug-2020 at 9:28:14a, UTC+7, Novosibirsk, Monday;
	This is SSH Tunnel WFP filter device callouts interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 8:09:37.236 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "filter.dev.ext.h"
#include "filter.classify.h"

namespace ssh { namespace driver {
#if (0)
	class CCallout_Types {
	public:
		class CFilter {
		public:
			enum v4 : ULONG { accept = 0,  connect, _max };
			enum class v6 : ULONG { accept = v4::_max, connect, _max };
		};
		class COut {
		public:
			enum v4 : ULONG { accept = CFilter::v6::_max, connect, _max };
			enum class v6 : ULONG { accept = v4::_max, connect, _max };
		};
		enum  CMonitor : ULONG {
			sub_layer = COut::v6::_max, _max
		};
		static const ULONG n_count = CMonitor::_max;
	};

	class CCallout_Enum {
	};
#else
	DEFINE_GUID(WFP_ACCEPT_CALLOUT_V4 , 0xf7893280, 0x2ab9, 0x410c, 0x84, 0x58, 0x14, 0xd5, 0x92, 0x8a, 0x93, 0xa3); // {F7893280-2AB9-410C-8458-14D5928A93A3}
	DEFINE_GUID(WFP_ACCEPT_CALLOUT_V6 , 0x1fb200a3, 0x2c5c, 0x4438, 0xa9, 0x55, 0x30, 0x9f, 0x8a, 0xb2, 0x61, 0xa7); // {1FB200A3-2C5C-4438-A955-309F8AB261A7}

	DEFINE_GUID(WFP_CONNECT_CALLOUT_V4, 0xdb8e4b7c, 0xde4a, 0x470f, 0x8a, 0x8a, 0x51, 0x18, 0xcc, 0x2a, 0xf3, 0x3b); // {DB8E4B7C-DE4A-470F-8A8A-5118CC2AF33B}
	DEFINE_GUID(WFP_CONNECT_CALLOUT_V6, 0x59a8afb4, 0x176a, 0x4034, 0xa1, 0x93, 0xf2, 0x02, 0xe9, 0x03, 0xf8, 0x90); // {59A8AFB4-176A-4034-A193-F202E903F890}

	DEFINE_GUID(WFP_ACCEPT_FILTER_V4  , 0x72da8e07, 0xfae7, 0x45c3, 0xad, 0x8d, 0x18, 0x5a, 0x42, 0x15, 0x35, 0xd9); // {72DA8E07-FAE7-45C3-AD8D-185A421535D9}
	DEFINE_GUID(WFP_ACCEPT_FILTER_V6  , 0x71958663, 0x630b, 0x4ca0, 0x86, 0x95, 0x29, 0xda, 0x23, 0x3d, 0x2d, 0x14); // {71958663-630B-4CA0-8695-29DA233D2D14}

	DEFINE_GUID(WFP_CONNECT_FILTER_V4 , 0x7bfb7ffe, 0xf706, 0x410c, 0xac, 0x82, 0x0f, 0x07, 0x55, 0x94, 0xc5, 0xf2); // {7BFB7FFE-F706-410C-AC82-0F075594C5F2}
	DEFINE_GUID(WFP_CONNECT_FILTER_V6 , 0xd0cb778b, 0x5af7, 0x4221, 0x8b, 0x54, 0x87, 0x2f, 0x6a, 0xef, 0x77, 0xb9); // {D0CB778B-5AF7-4221-8B54-872F6AEF77B9}

	DEFINE_GUID(WFP_MONITOR_SUBLAYER  , 0xb45c5357, 0x2093, 0x4632, 0x85, 0x22, 0xc5, 0x26, 0x95, 0xa7, 0x8b, 0xce); // {B45C5357-2093-4632-8522-C52695A78BCE}
#endif

	class CCallouts {
		// https://docs.microsoft.com/en-us/windows-hardware/drivers/network/unloading-a-callout-driver
	public:
		static NTSTATUS Register  ( PDEVICE_OBJECT );
		static NTSTATUS Unregister(void) ;
	private:
		static NTSTATUS NTAPI Deaf(FWPS_CALLOUT_NOTIFY_TYPE, const GUID*, const FWPS_FILTER0*);
	};
}}


#endif/*_FILTERCALLOUTS_H_01D4AD54_665E_4E57_BB73_A0D2C7D4A69A_INCLUDED*/