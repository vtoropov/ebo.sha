/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Aug-2020 at 5:35:49a, UTC+7, Novosibirsk, Friday;
	This is SSH-Tunnel WFP driver service auxiliary interface implementation file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 7:42:17.173 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "driver.svc.aux.h"

using namespace shared::service;
using namespace shared::driver::client;

/////////////////////////////////////////////////////////////////////////////

CObjectToStr:: CObjectToStr(void) {}
CObjectToStr::~CObjectToStr(void) {}

/////////////////////////////////////////////////////////////////////////////

CStringW   CObjectToStr::Print(const CDrvLocator& _loc) {

	static LPCWSTR lp_sz_pat = _T(
		"\n\tDevice Properties: \n"
		"\t\tname   = %s\n"
		"\t\tpath   = %s\n"
	);

	CStringW cs_props; cs_props.Format(
		lp_sz_pat,
		_loc.Device().GetString(),
		_loc.Path()
	);

	return cs_props;
}

CStringW   CObjectToStr::Print(const CService& _svc) {

	static LPCWSTR lp_sz_pat = _T(
		"\n\tService Properties: \n"
		"\t\tname   = %s\n"
		"\t\ttype   = %s\n"
		"\t\tpath   = %s\n"
		"\t\tstate  = %s\n"
		"\t\tstart  = %s\n"
		"\t\tdesc   = %s\n"
	);

	CCfgToStr out_;

	const CCfg::qry_svc_cfg& cfg_ = _svc.Cfg().Data();

	CStringW cs_props; cs_props.Format(
		lp_sz_pat,
		cfg_.szSericeName.GetString(),
		out_.PrintType(cfg_.dwServiceType).GetString(),
		cfg_.szBinaryPathName.GetString(),
		out_.PrintState(_svc.Status().Data().dwCurrentState).GetString(),
		out_.PrintStart(cfg_.dwStartType).GetString(),
		_svc.Cfg_Ex().Description()
	);

	return cs_props;
}