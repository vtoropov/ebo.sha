/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Oct-2019 at 8:51:42p, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Pack personal account app base view interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 1-Mar-2020 at 10:32:07p, UTC+7, Novosibirsk, Sunday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 3:40:42p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool on 24-May-2020 at 10:44:07p, UTC+7, Novosibirsk, Sunday;
	Adopted to Ebo Sha optima tool on 23-Sep-2020 at 4:15:43p, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "ebo.sha.view.bas.h"

using namespace ebo::sha::gui::view;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;
/////////////////////////////////////////////////////////////////////////////

CViewWnd_Base:: CViewWnd_Base(CView_Base& _view) : m_view(_view) {}
CViewWnd_Base::~CViewWnd_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CViewWnd_Base::OnCommand (UINT, WPARAM, LPARAM, BOOL&) { return 0; }
LRESULT   CViewWnd_Base::OnCreate  (UINT, WPARAM, LPARAM, BOOL&) { return 0; }
LRESULT   CViewWnd_Base::OnDestroy (UINT, WPARAM, LPARAM, BOOL&) { return 0; }
LRESULT   CViewWnd_Base::OnErase   (UINT, WPARAM _w_dc, LPARAM, BOOL& _b_hand) {
	_b_hand = TRUE; _w_dc;
	static const LRESULT l_is_drawn = 1;
	{
		const RECT rc_view = Lay_(*this);
		TZBuffer buf_((HDC)_w_dc, rc_view);

		buf_.FillSolidRect(&rc_view, shared::Get_Format().Panel().Bkgnd().Solid());
	}
	return l_is_drawn;
}
LRESULT   CViewWnd_Base::OnSize    (UINT, WPARAM, LPARAM _lp, BOOL&) {
#if (0)
	CHeader_Layout lay_(*this);

	const SIZE sz_ = {0, lay_.ReqHeight()};
#else
	const SIZE sz_ = {0,  0};
#endif
	const RECT rc_ = {0, sz_.cy, LOWORD(_lp), HIWORD(_lp)}; rc_;
	
	return 0;
}
LRESULT   CViewWnd_Base::OnVisible (UINT, WPARAM, LPARAM _lp, BOOL& _b_hand) {
	// this routine must *not* handle this event,
	// otherwise, main window does not receive changing size message;
	_lp; _b_hand = FALSE;
	if (NULL != _lp)
	{
		LPWINDOWPOS const pPos = reinterpret_cast<LPWINDOWPOS>(_lp);
		if (NULL != pPos && (SWP_SHOWWINDOW & (*pPos).flags)) {
		}
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

CView_Base:: CView_Base(CViewWnd_Base& _wnd) : m_wnd(_wnd) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CView_Base::~CView_Base(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CView_Base::IControlEvent_OnClick(const UINT ctrlId) { HRESULT hr_ = S_OK;
	ctrlId;
#if (0)
	CStringW cs_msg; cs_msg.Format(_T("Event: %s::ctrlId=%u;"), __MODULE__, ctrlId);
	::MessageBoxW(m_wnd, cs_msg.GetString(), _T("Debug"), MB_ICONEXCLAMATION|MB_OK);
#endif
	return hr_;
}

HRESULT     CView_Base::IControlEvent_OnClick(const UINT ctrlId, const LONG_PTR nData)  { HRESULT hr_ = S_OK;
	ctrlId; nData;
	CStringW cs_msg; cs_msg.Format(_T("Event: %s::ctrlId=%u;data=%p"), __MODULE__, ctrlId, (void*)nData);
	::MessageBoxW(m_wnd, cs_msg.GetString(), _T("Debug"), MB_ICONEXCLAMATION|MB_OK);
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CView_Base::ITabEvent_OnAppend (const CTab& _added) { _added; return S_OK; }
HRESULT     CView_Base::ITabEvent_OnFormat (const TTabbedFmt&)  { return S_OK; }
HRESULT     CView_Base::ITabEvent_OnSelect (const DWORD _tab_ndx) { _tab_ndx; return S_OK; }

/////////////////////////////////////////////////////////////////////////////

HRESULT     CView_Base::ISelector_OnBkgChanged (void) { return S_OK;  }
HRESULT     CView_Base::ISelector_OnFontChanged(void) { return S_OK;  }
HRESULT     CView_Base::ISelector_OnItemClick  (const UINT _u_itm_id) { _u_itm_id; return S_OK; }
HRESULT     CView_Base::ISelector_OnItemImages (const UINT _u_res_id) { _u_res_id; return S_OK; }

/////////////////////////////////////////////////////////////////////////////
#if (1)
HRESULT     CView_Base::IInfo_OnBkgChanged (void) { HRESULT hr_ = S_OK; return hr_; }
HRESULT     CView_Base::IInfo_OnFontChanged(void) { HRESULT hr_ = S_OK; return hr_; }
HRESULT     CView_Base::IInfo_OnItemClick  (const UINT _u_itm_id) { _u_itm_id; HRESULT hr_ = S_OK; return hr_; }
HRESULT     CView_Base::IInfo_OnItemImages (const UINT _u_res_id) { _u_res_id; HRESULT hr_ = S_OK; return hr_; }
#endif
/////////////////////////////////////////////////////////////////////////////

bool        CView_Base::CanAccept(const WORD nCmdId ) const {
	nCmdId;
	return false;
}
HRESULT     CView_Base::Create   (const HWND hParent, const RECT& _area, const bool bVisible) {
	m_error << __MODULE__ << S_OK;

	if (!::IsWindow(hParent))
		return (m_error = OLE_E_INVALIDHWND);

	const DWORD dwStyle = WS_CHILD | WS_CLIPCHILDREN | (bVisible ? WS_VISIBLE : NULL);

	RECT area_ = _area;

	m_wnd.Create(hParent, area_, NULL, dwStyle);
	if (m_wnd == NULL)
		m_error = ::GetLastError();

	return m_error;
}
HRESULT     CView_Base::Destroy  (void) {
	m_error << __MODULE__ << S_OK;

	if (NULL == m_wnd || FALSE == m_wnd.IsWindow())
		return (m_error = OLE_E_BLANK);

	m_wnd.SendMessage(WM_CLOSE);

	return m_error;
}
TErrorRef   CView_Base::Error    (void) const { return m_error; }
bool        CView_Base::IsChecked(const UINT nCmdId) const {
	nCmdId; return false;
}
bool        CView_Base::IsValid  (void) const { return (NULL != m_wnd.m_hWnd && !!m_wnd.IsWindow()); }
bool        CView_Base::IsVisible(void) const { return (this->IsValid() && !!m_wnd.IsWindowVisible()); }
HRESULT     CView_Base::IsVisible(const bool _v) {
	m_error << __MODULE__ << S_OK;
	HRESULT hr_ = m_error;
	if (this->IsValid() == false)
		return (hr_ = OLE_E_BLANK);

	if (_v) m_wnd.ShowWindow(SW_SHOW);
	else    m_wnd.ShowWindow(SW_HIDE);

	return  hr_;
}

HRESULT     CView_Base::Refresh  (void) {
	m_error << __MODULE__ << S_OK;

	HRESULT hr_ = m_error;
	return  hr_;
}

HRESULT     CView_Base::OnCommand(const WORD cmdId) {
	cmdId;
	m_error << __MODULE__ << S_OK;
#if (0)
	_out::SetIdleMessage();
#endif
	return  m_error;
}
HRESULT     CView_Base::OnPrepare(const WORD cmdId) {
	m_error << __MODULE__ << S_FALSE;    cmdId;
	return  m_error;
}
HRESULT     CView_Base::Update   (const RECT& _area) {
	m_error << __MODULE__ << S_OK;

	if (::IsRectEmpty(&_area) == TRUE)
		return (m_error = OLE_E_INVALIDRECT);

	m_wnd.MoveWindow(&_area, TRUE);

	return  m_error;
}
CViewWnd&   CView_Base::Window   (void) { return m_wnd; }

/////////////////////////////////////////////////////////////////////////////

HRESULT     CViewWnd_Base::DrawBackground(const HDC hSurface, const RECT& rcDrawArea) { HRESULT hr_ = S_OK;
	hSurface; rcDrawArea;
	TZBuffer z_buf(hSurface, rcDrawArea);

	RECT rc_draw = rcDrawArea; shared::Get_Format().Panel().Bkgnd().Margins().ApplyTo(rc_draw);

	z_buf.DrawSolidRect(rc_draw, shared::Get_Format().Panel().Bkgnd().Solid());

	return hr_;
}

HRESULT     CViewWnd_Base::DrawParentBackground(const HWND hChild, const HDC hSurface, RECT& rcUpdated) { HRESULT hr_ = S_OK;
	hChild; hSurface; rcUpdated;
	TZBuffer z_buf(hSurface, rcUpdated);

	RECT rc_draw = rcUpdated; shared::Get_Format().Panel().Bkgnd().Margins().ApplyTo(rc_draw);

	z_buf.DrawSolidRect(rc_draw, shared::Get_Format().Panel().Bkgnd().Solid());
	return hr_;
}
const
TFmtBkg&    CViewWnd_Base::GetBkgFormat(void) const { return shared::Get_Format().Panel().Bkgnd(); }
