#ifndef _EBOOUTNOTGOOVER_H_8AE04136_DBB7_40f9_8140_7440514DB5BC_INCLUDED
#define _EBOOUTNOTGOOVER_H_8AE04136_DBB7_40f9_8140_7440514DB5BC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Aug-2016 at 6:09:23a, GMT+7, Phuket, Rawai, Tuesday;
	This is Evalu8 Outlook add-in project version declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack generic driver setup project on 12-Jan-2021 at 6:22:49.327 pm, UTC+7, Novosibirsk, Tuesday;
*/

#ifndef WINVER                 // Specifies that the minimum required platform is Windows Vista.
#define WINVER         0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT           // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT   0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS         // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINDOWS 0x0600  // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE              // Specifies that the minimum required platform is Internet Explorer 9.0 (Windows Vista, service pack 2).
#define _WIN32_IE      0x0900  // Change this to the appropriate value to target other versions of IE.
#endif

#endif/*_EBOOUTNOTGOOVER_H_8AE04136_DBB7_40f9_8140_7440514DB5BC_INCLUDED*/