#ifndef _EBOSHAVIEWFAKE_H_044C355B_5046_4520_AF30_92FE788C2EA2_INCLUDED
#define _EBOSHAVIEWFAKE_H_044C355B_5046_4520_AF30_92FE788C2EA2_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Oct-2020 at 10:41:54p, UTC+7, Novosibirsk, Wednesday;
	This is Ebo Sha Optima Tool fake GPS view interface declaration file.
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.gps.drv.h"
#include "ebo.sha.tcp.trace.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CFake : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CFake;
		private:
			CFake&      m_view  ;
			TSelect     m_select;
			TFakeDrv    m_driver;
			TFakeTrace  m_trace ;

		public:
			 CViewWnd (CFake&);
			~CViewWnd (void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CFake (void);
		~CFake (void);
#pragma warning(disable:4481)
	private:    // ISelector_Events
		HRESULT    ISelector_OnItemClick  (const UINT _u_itm_id) override sealed;
#pragma warning(default:4481)
	
	public:
		TFakeDrv&   Driver  (void);
		TSelect&    Select  (void);
		TFakeTrace& Trace   (void);
	};

}}}}

typedef ebo::sha::gui::view::CFake  TFakeView;

#endif/*_EBOSHAVIEWFAKE_H_044C355B_5046_4520_AF30_92FE788C2EA2_INCLUDED*/