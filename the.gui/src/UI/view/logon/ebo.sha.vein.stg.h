#ifndef _EBOSHAVEINSTG_H_02F2450D_7C4D_441A_97AB_C1232F41E9C3_INCLUDED
#define _EBOSHAVEINSTG_H_02F2450D_7C4D_441A_97AB_C1232F41E9C3_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2021 at 6:37:40.349 am, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool vein scan data storage sub-view interface declaration file;
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.view.empty.h"
#include "ebo.sha.gui.cmp.cap.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CVein_Stg : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CVein_Stg;
		private:
			CVein_Stg&    m_view ;
			TCaptionComponent  m_caps[1];

			TEmptyView    m_empty;

		private:
			 CViewWnd (CVein_Stg&);
			~CViewWnd (void);

		private:   // TWindow
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CVein_Stg (void);
		~CVein_Stg (void);
	};

}}}}

typedef ebo::sha::gui::view::CVein_Stg TVeinStg;

#endif/*_EBOSHAVEINSTG_H_02F2450D_7C4D_441A_97AB_C1232F41E9C3_INCLUDED*/