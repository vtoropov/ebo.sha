/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Jan-2021 at 12:54:11.389 pm, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Sha Optima Tool info panel GUI format interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:03:27.531 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "StdAfx.h"
#include "ebo.sha.gui.fmt.info.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::format;

/////////////////////////////////////////////////////////////////////////////

TInfoPanelFmt:: CInfoPanel (void) {}
TInfoPanelFmt::~CInfoPanel (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   TInfoPanelFmt::ApplyTo (TInfoPanel& _panel) const { HRESULT hr_ = S_OK;
	_panel;
	_panel.Format().Bkgnd().Solid() << RGB( 70, 70, 70);

	_panel.Layout ().Caption().Format().Font().Family(_T("Verdana"));
	_panel.Layout ().Caption().Format().Font().Size() = 20;
	_panel.Layout ().Caption().Format().Font().Fore() = shared::Get_Theme().Get(TThemePart::e_caption, TThemeElement::e_fore);
//	_panel.Layout ().Caption().Format().Font().Options() += TFontOpts::eBold;
	_panel.Layout ().Caption().Margins().Left() = 15;
	_panel.Layout ().Caption().Margins().Bottom() = 5;

	_panel.Format ().Font().Family(_T("Verdana"));
	_panel.Format ().Font().Size() = 18;
	_panel.Format ().Font().Fore() = shared::Get_Theme().Get(TThemePart::e_caption, TThemeElement::e_fore);
	_panel.Layout ().Text().Margins().Left() = 15;
	_panel.Layout ().Text().Margins().Top() = 5;
	_panel.Layout ().Text().Margins().Right() = 10;
	_panel.Layout ().Text().Margins().Bottom() = 5;

	_panel.Layout ().ZOrder ().Top(TInfoZElement::e_image);

	_panel.Layout ().Cache().Add(MB_ICONASTERISK, IDR_EBO_SHA_PAN_INF_I);
	_panel.Layout ().Cache().Add(MB_ICONERROR   , IDR_EBO_SHA_PAN_INF_E);
	_panel.Layout ().Cache().Add(MB_ICONWARNING , IDR_EBO_SHA_PAN_INF_W);
	_panel.Layout ().Cache().Add(MB_USERICON    , IDR_EBO_SHA_PAN_INF_U);

	_panel.Format ().TextOutput().Phrases() = false;

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

const TInfoPanelFmt& TInfoPanelFmt::operator >> (TInfoPanel& _panel) const { this->ApplyTo(_panel); return *this; }

/////////////////////////////////////////////////////////////////////////////

TInfoAsterisk:: CInfoAsterisk (void) : TBase() {}
TInfoAsterisk::~CInfoAsterisk (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   TInfoAsterisk::ApplyTo (TInfoPanel& _panel) const { HRESULT hr_ = TBase::ApplyTo(_panel);
	_panel;
	_panel.Borders().Thickness(1);
	_panel.Borders().Color(TColour(RGB( 47, 123, 203)));

	_panel.Layout ().Cache().Current(MB_ICONASTERISK );
	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Clear();

	TTile t_cap_0( TGradient(TColour(RGB( 47, 123, 203)), TColour(RGB( 81,  81,  81))) ); t_cap_0.Size() = TTileSize(330, TTileSize::e_auto);
	TTile t_cap_1( TGradient(TColour(RGB( 81,  81,  81)), TColour(RGB(115, 115, 115))) ); t_cap_1.Size() = TTileSize(100, TTileSize::e_auto);
	TTile t_cap_2( TGradient(TColour(RGB(115, 115, 115)), TColour(RGB( 71,  71,  71))) ); t_cap_2.Size() = TTileSize(100, TTileSize::e_auto);
	TTile t_cap_3( TGradient(TColour(RGB( 71,  71,  71)), TColour(RGB( 61,  61,  61))) ); t_cap_3.Size() = TTileSize(TTileSize::e_auto, TTileSize::e_auto);

	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Append(t_cap_0);
	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Append(t_cap_1);
	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Append(t_cap_2);
	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Append(t_cap_3);

	_panel.Layout ().Caption().String() = _T("Information");
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

const TInfoAsterisk& TInfoAsterisk::operator >> (TInfoPanel& _panel) const { this->ApplyTo(_panel); return *this; }

/////////////////////////////////////////////////////////////////////////////

TInfoErrorFmt:: CInfoError (void) : TBase() {}
TInfoErrorFmt::~CInfoError (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   TInfoErrorFmt::ApplyTo (TInfoPanel& _panel) const { HRESULT hr_ = TBase::ApplyTo(_panel);
	_panel;
	_panel.Borders().Thickness(1);
	_panel.Borders().Color(TColour(RGB( 210, 52, 48)));

	_panel.Layout ().Cache().Current(MB_ICONERROR );
	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Clear();

	TTile t_cap_0( TGradient(TColour(RGB(171,  45,  51)), TColour(RGB( 81,  81,  81))) ); t_cap_0.Size() = TTileSize(330, TTileSize::e_auto);
	TTile t_cap_1( TGradient(TColour(RGB( 81,  81,  81)), TColour(RGB( 97,  76,  78))) ); t_cap_1.Size() = TTileSize(100, TTileSize::e_auto);
	TTile t_cap_2( TGradient(TColour(RGB( 97,  76,  78)), TColour(RGB( 71,  71,  71))) ); t_cap_2.Size() = TTileSize(100, TTileSize::e_auto);
	TTile t_cap_3( TGradient(TColour(RGB( 71,  71,  71)), TColour(RGB( 61,  61,  61))) ); t_cap_3.Size() = TTileSize(TTileSize::e_auto, TTileSize::e_auto);

	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Append(t_cap_0);
	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Append(t_cap_1);
	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Append(t_cap_2);
	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Append(t_cap_3);

	_panel.Layout ().Caption().String() = _T("Error");
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

const TInfoErrorFmt& TInfoErrorFmt::operator >> (TInfoPanel& _panel) const { this->ApplyTo(_panel); return *this; }

/////////////////////////////////////////////////////////////////////////////

TInfoWarnFmt:: CInfoWarn (void) : TBase() {}
TInfoWarnFmt::~CInfoWarn (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   TInfoWarnFmt::ApplyTo (TInfoPanel& _panel) const { HRESULT hr_ = TBase::ApplyTo(_panel);
	_panel;
	_panel.Borders().Thickness(1);
	_panel.Borders().Color(TColour(RGB(237, 148,  15)));

	_panel.Layout ().Cache().Current(MB_ICONWARNING );
	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Clear();

	TTile t_cap_0( TGradient(TColour(RGB(237, 148,  15)), TColour(RGB( 81,  81,  81))) ); t_cap_0.Size() = TTileSize(330, TTileSize::e_auto);
	TTile t_cap_1( TGradient(TColour(RGB( 81,  81,  81)), TColour(RGB(115, 115, 115))) ); t_cap_1.Size() = TTileSize(100, TTileSize::e_auto);
	TTile t_cap_2( TGradient(TColour(RGB(115, 115, 115)), TColour(RGB( 71,  71,  71))) ); t_cap_2.Size() = TTileSize(100, TTileSize::e_auto);
	TTile t_cap_3( TGradient(TColour(RGB( 71,  71,  71)), TColour(RGB( 61,  61,  61))) ); t_cap_3.Size() = TTileSize(TTileSize::e_auto, TTileSize::e_auto);

	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Append(t_cap_0);
	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Append(t_cap_1);
	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Append(t_cap_2);
	_panel.Layout ().Caption().Format().Bkgnd().Sectors().Append(t_cap_3);

	_panel.Layout ().Caption().String() = _T("Warning");
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

const TInfoWarnFmt& TInfoWarnFmt::operator >> (TInfoPanel& _panel) const { this->ApplyTo(_panel); return *this; }