#ifndef SFXCOMBOLAY_H_DAE303F5_230F_4EAD_9F52_1273FBBA8B23_INCLUDED
#define SFXCOMBOLAY_H_DAE303F5_230F_4EAD_9F52_1273FBBA8B23_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 02-Aug-2010 at 11:54:56pm, GMT+3, Rostov-on-Don, Monday;
	This is ST Combo Box control shaper class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Sfx combo project on 22-Nov-2020 at 11:37:17.251 am, UTC+7, Novosibirsk, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.base.lay.h"

namespace ST_Ctrls { class CCombo; namespace layout {

	using shared::sys_core::CError;

	using ex_ui::controls::layout::CLayersEx;

	class CCombo_Layout {
	public:
		class CButton { friend class CCombo_Layout;
		private:
			ST_Ctrls::CCombo&
			             m_ctrl ;
			mutable RECT m_rect ;

		private:
			 CButton (ST_Ctrls::CCombo&);
			~CButton (void);

		public:
			const
			RECT&   Default (void) const;

		};
		class CLabel  { friend class CCombo_Layout;
		private:
			ST_Ctrls::CCombo&
			             m_ctrl ;
			mutable RECT m_rect ;

		private:
			 CLabel (ST_Ctrls::CCombo&);
			~CLabel (void);

		public:
			const
			RECT&   Default (void) const;

		};
	protected:
		ST_Ctrls::CCombo&
		            m_ctrl  ;
		CLayersEx   m_lays  ;
		CButton     m_button;
		CLabel      m_label ;

	public:
		 CCombo_Layout (ST_Ctrls::CCombo&);
		~CCombo_Layout (void);

	public:
		const
		CCombo_Layout::CButton& Button (void) const;
		CCombo_Layout::CButton& Button (void)      ;
		const
		CCombo_Layout::CLabel&  Label  (void) const;
		CCombo_Layout::CLabel&  Label  (void)      ;
		const
		CLayersEx&  Layers (void) const;
		CLayersEx&  Layers (void)      ;
		HRESULT     Update (void)      ;           // recalculates position/rectangle of all elements;
		HRESULT     Update (const RECT& _rc_area); // updates control window position into an area provided;

	public:
		CCombo_Layout&  operator <<(const RECT& _rc_area);        // updates control window position in accordance with area  ;
		const RECT      operator = (const RECT& _rc_area) const;  // returns calculated rectangle of control window for area provided;

	private: // non-copyable;
		CCombo_Layout (const CCombo_Layout&);
		CCombo_Layout& operator = (const CCombo_Layout&);
	};

}}

typedef ST_Ctrls::layout::CCombo_Layout          TComboLay;
typedef ST_Ctrls::layout::CCombo_Layout::CButton TCboButtonLay;
typedef ST_Ctrls::layout::CCombo_Layout::CLabel  TCboLabelLay;

#endif/*SFXCOMBOLAY_H_DAE303F5_230F_4EAD_9F52_1273FBBA8B23_INCLUDED*/