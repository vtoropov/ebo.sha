/*
	Created by Tech_dog (ebontrop@gmail.com) on 25-Jan-2021 at 00:53:45.836 am, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool app main form status bar wrapper interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.gui.sta.bar.h"

using namespace ebo::sha::gui;

/////////////////////////////////////////////////////////////////////////////

TStatusWrap:: CStatusBar_Wrap (IControlEvent& _snk) : m_evt_snk(_snk), m_button(_snk), m_status(*this) { m_error << __MODULE__ << S_OK >> __MODULE__; }
TStatusWrap::~CStatusBar_Wrap (void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef  TStatusWrap::Error (void) const { return m_error; }
const
TStatusCtrl&  TStatusWrap::Ref(void) const { return m_status; }
TStatusCtrl&  TStatusWrap::Ref(void) { return m_status; }

/////////////////////////////////////////////////////////////////////////////

HRESULT    TStatusWrap::Create (const HWND _h_parent) {
	_h_parent;
	m_error << __MODULE__ << S_OK;

	if (NULL == _h_parent || FALSE == ::IsWindow(_h_parent))
		return (m_error << OLE_E_INVALIDHWND);

	if (m_button.Window().IsWindow() || m_status.Window().IsWindow()) {
		return (m_error << __DwordToHresult(ERROR_ALREADY_INITIALIZED));
	}

	TStatusBarFmt() >> m_status;
	HRESULT hr_ = m_status.Create(_h_parent, 2);
	if (FAILED(hr_))
		m_error = m_status.Error();
	else {
		shared::Get_Format().Control() >> m_button.Format();
	//	m_button.Format().State().Normal().Font().Fore() = RGB(10, 10, 10);

		const RECT rc_btn = { 0, 0, 150,  static_cast<LONG>(m_status.Layout().Height() - 7) };

		m_button.Text(_T("Show Trace"));
		m_button.Create(m_status.Window(), rc_btn, 55);
		m_button.Tips(_T("Show/Hide trace sub-view;")); // TODO: tooltip appears one time only; must be checked;

		m_status.Panels().Panel(1).Child() << m_button.Window();
		m_status.Panels().Panel(1).Child().Align().Horz() = THorzAlign::eCenter;

		m_status.Refresh();
	}

	return m_error;
}

HRESULT    TStatusWrap::Destroy(void) {
	m_error << __MODULE__ << S_OK;

	if (m_button.Window().IsWindow())
		m_button.Destroy();

	if (m_status.Window().IsWindow())
		m_status.Destroy();

	return m_error;
}

HRESULT    TStatusWrap::Update (const bool _b_trace) { HRESULT hr_ = S_OK;
	_b_trace; 
	m_button.Text(_b_trace ? _T("Hide Trace") : _T("Show Trace"));
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT    TStatusWrap::IStatusEvt_OnAppend (const ST_Ctrls::CPanel& _added) { _added; return S_OK; }
HRESULT    TStatusWrap::IStatusEvt_OnFormat (const TStatusFmt&) { return S_OK; }
HRESULT    TStatusWrap::IStatusEvt_OnRemove (const DWORD _panel_ndx) { _panel_ndx;  return S_OK; }