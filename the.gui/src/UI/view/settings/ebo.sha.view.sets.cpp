/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Jan-2021 at 10:15:46.648 am, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Sha Optima Tool application settings view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.view.sets.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CSettings_Layout {
	private:
		CSettings& m_view;
		RECT         m_rect;

	public:
		 CSettings_Layout (CSettings& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CSettings_Layout (void) {}

	public:
		RECT   GetEmpty(void) { RECT rc_empty = m_rect; ::InflateRect(&rc_empty, -0x5, -0x5); rc_empty.top += 30; return rc_empty; }
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

TSettings::CViewWnd:: CViewWnd(TSettings& _view) : TViewWnd(_view), m_view(_view) {}
TSettings::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   TSettings::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	CSettings_Layout layout_(m_view); TViewWnd::SetWindowTextW(_T("App Settings"));

	HRESULT hr_ = m_empty.Create(*this, layout_.GetEmpty(), true); hr_;
	m_empty.DrawBorder(true);

	return 0;
}

LRESULT   TSettings::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_empty.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   TSettings::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CSettings_Layout layout_(m_view);
	m_empty.Update(layout_.GetEmpty());

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

TSettings:: CSettings (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
TSettings::~CSettings (void) {}

/////////////////////////////////////////////////////////////////////////////