#ifndef _EBOSHAGUIFMTCAP_H_2B48E93A_2C57_44F2_B01F_7EE3A4964D24_INCLUDED
#define _EBOSHAGUIFMTCAP_H_2B48E93A_2C57_44F2_B01F_7EE3A4964D24_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Dec-2020 at 6:11:23.074 am, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Sha Optima Tool common caption control format interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:03:27.443 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "sfx.label.fmt.h"
#include "sfx.image.fmt.h"

namespace ebo { namespace sha { namespace gui { namespace format {

	class CCaption {
	private:
		TPictureFormat m_logo;
		TLabelFmt  m_title;

	public:
		 CCaption (void);
		 CCaption (const CCaption&);
		~CCaption (void);

	public:
		const
		TPictureFormat&  Logo (void) const;
		TPictureFormat&  Logo (void)      ;
		const
		TLabelFmt& Title (void) const;
		TLabelFmt& Title (void)      ;

	public:
		CCaption&  operator = (const CCaption&);
		CCaption&  operator <<(const TLabelFmt&);
		CCaption&  operator <<(const TPictureFormat&);

	public:
		const CCaption&  operator >>(TLabelFmt&) const;
		const CCaption&  operator >>(TPictureFormat&) const;
	};

}}}}

typedef ebo::sha::gui::format::CCaption  TCaptionFmt;

#endif/*_EBOSHAGUIFMTCAP_H_2B48E93A_2C57_44F2_B01F_7EE3A4964D24_INCLUDED*/