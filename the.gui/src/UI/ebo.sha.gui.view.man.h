#ifndef _EBOBOOFRMMAINMAN_H_28AD0080_1473_4063_871D_D8F1FD9D9AAC_INCLUDED
#define _EBOBOOFRMMAINMAN_H_28AD0080_1473_4063_871D_D8F1FD9D9AAC_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Dec-2018 at 6:42:50p, UTC+7, Novosibirsk, Monday;
	This is USB Drive Detective (bitsphereinc.com) desktop app main form view manager interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 29-Oct-2019 at 9:40:31p, UTC+7, Novosibirsk, Tuesday;
	Adopted to Ebo Sha Optima Tool on 20-Sep-2020 at 11:39:17p, UTC+7, Novosibirsk, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "ebo.sha.view.logon.h"
#include "ebo.sha.view.main.h"
#include "ebo.sha.view.fake.h"
#include "ebo.sha.view.media.h"
#include "ebo.sha.view.block.h"
#include "ebo.sha.view.sets.h"
#include "ebo.sha.view.help.h"
#include "ebo.sha.view.trace.h"
namespace ebo { namespace sha { namespace gui { namespace view {

	using shared::sys_core::CError;

	class CViewType {
	public:
		enum _tp : INT {
			e_none   = -1 ,   // no view, default;
			e_home   = 0x0,   // home;
			e_sets   = 0x1,   // settings;
			e_help   = 0x2,   // help;
			e_logon  = 0x3,   // logon;
			e_fake   = 0x4,   // fake GPS;
			e_media  = 0x5,   // media;
			e_block  = 0x6,   // block;

			e__max_is_not_view
		};
	}; typedef CViewType::_tp TViewType;

	class CManager;
	class CViews  { friend class CManager;
	private:
		CView_Base*   m_active;
		CView_Base*   m_views[CViewType::e__max_is_not_view];
		CError        m_error ;

	private:
		 CViews (void);
		~CViews (void);

	private:
		HRESULT     Create  (void)      ;
		HRESULT     Destroy (void)      ;
		TErrorRef   Error   (void) const;

	public:
		DWORD       Count   (void) const;
		CView_Base* const View (const DWORD _ndx) const;
	};

	class CHandler {
	private:
		CManager&   m_man_ref;
		CError      m_error;

	public:
		 CHandler (CManager&);
		~CHandler (void);

	public:
		bool          Accept(const WORD _cmd_id) const;
		HRESULT       Do    (const WORD _cmd_id)      ;
		TErrorRef     Error (void) const;

	}; typedef CHandler THandler;

	class CManager : public ex_ui::controls::IControlEvent  {
	private:
		CViews      m_views;
		CHandler    m_hand ;
		CError      m_error;
		TTraceView  m_trace; // TODO: TTrace typedef must be re-viewed;

	public:
		 CManager (void);
		~CManager (void);

	public: // view(s) life-cycle;
		HRESULT     Create  (const HWND hParent, const RECT& _area);
		HRESULT     Destroy (void)      ;

	public: // accessor(s)
		TViewType   Active  (void) const;
		HRESULT     Active  (const TViewType);
		CView_Base*
		      const Active_Ptr (void) const;

	public:
		TErrorRef   Error   (void) const;
		const
		THandler&   Handler (void) const;
		THandler&   Handler (void)      ;
		TTraceView& Trace   (void)      ;
		const
		CViews&     Views   (void) const;

	public: // funs;
		HRESULT     Redraw  (void)      ;
		HRESULT     Trigger (const TViewType);
		bool        Visible (void) const;
		HRESULT     Update  (const bool _b_allow);

	private: // IControlEvent
#pragma warning (disable: 4481)
		virtual  HRESULT  IControlEvent_OnClick(const UINT ctrlId) override sealed;
#pragma warning (default: 4481)
	};

}}}}

typedef ebo::sha::gui::view::CManager TViewMan;

#endif/*_EBOBOOFRMMAINMAN_H_28AD0080_1473_4063_871D_D8F1FD9D9AAC_INCLUDED*/