#ifndef _SFXSTRIPCTRL_H_1BAC705D_DCAB_4BCA_99E0_6ADCC0305970_INCLUDED
#define _SFXSTRIPCTRL_H_1BAC705D_DCAB_4BCA_99E0_6ADCC0305970_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-May-2009 at 10:02:53pm, GMT+3, Rostov-on-Don, Monday;
	This is ST Strip Bar Control interface declaration file.
	-----------------------------------------------------------------------------
	Reincarnation to Ebo Pack is made on 22-Aug-2020 at 7:16:49a, UTC+7, Novosibirsk, Saturday;
	Adopted to Sfx Strip project on 6-Sep-2020 at 5:36:22p, UTC+7, Novosibirsk, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "sfx.strip.fmt.h"
#include "sfx.strip.lay.h"
#include "sfx.strip.ext.h"

namespace ST_Ctrls {

	using shared::sys_core::CError;
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::controls::CBorders;

	using ST_Ctrls::strip::CItem ;
	using ST_Ctrls::strip::CItems;

	interface IStripEvents : public ex_ui::controls::IControlEvent {

		virtual HRESULT  IStripEvt_OnAppend (const CItem& _added) PURE;
		virtual HRESULT  IStripEvt_OnFormat (const TStripFmt&) PURE;
		virtual HRESULT  IStripEvt_OnRemove (const DWORD _panel_ndx) PURE;

	};

	class CStripBar : public IStripEvents {
	protected:
		IStripEvents&m_evt_snk;
		HANDLE       m_wnd_ptr;
		CError       m_error  ;
		UINT         m_ctrl_id;
		TStripLay    m_layout ;
		CItems       m_items  ;
		CBorders     m_borders;

	public:
		 CStripBar (IStripEvents&);
		~CStripBar (void);

	public:
		const
		CBorders&    Borders (void) const;
		CBorders&    Borders (void)      ;
		HRESULT      Create  (const HWND hParent, const UINT _ctrl_id);
		HRESULT      Destroy (void)      ;
		const
		TStripFmt&   Format  (void) const;
		TStripFmt&   Format  (void)      ;
		TErrorRef    Error   (void) const;
		const
		CItems&      Items   (void) const;
		CItems&      Items   (void)      ;
		const
		TStripLay&   Layout  (void) const;
		TStripLay&   Layout  (void)      ;
		HRESULT      Renderer(IRenderer*  const _p_parent_renderer);
		HRESULT      Refresh (void)      ;
		CWindow      Window  (void) const;

	public:
		ex_ui::controls::IControlEvent&  MouseEvtSink(void);

	private: // IStripEvents;
#pragma warning(disable: 4481)
		virtual   HRESULT  IStripEvt_OnAppend (const CItem& _added) override sealed;
		virtual   HRESULT  IStripEvt_OnFormat (const TStripFmt&) override sealed;
		virtual   HRESULT  IStripEvt_OnRemove (const DWORD _panel_ndx) override sealed;
#pragma warning(default: 4481)
	private: // non-copyable;
		CStripBar (const CStripBar&);
		CStripBar&  operator = (const CStripBar&);
	};
}

#endif/*_SFXSTRIPCTRL_H_1BAC705D_DCAB_4BCA_99E0_6ADCC0305970_INCLUDED*/