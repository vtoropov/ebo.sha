/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Jan-2021 at 11:49:48.439 pm, UTC+7, Novosibirsk, Wednesday;
	This is Ebo Sha Optima Tool SSH tunnel driver service control handler interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.cmd.hand.ssh.h"

using namespace ebo::sha::handlers;

/////////////////////////////////////////////////////////////////////////////

TSshDrvCmdHandler:: CSshDrvCmdHandler (void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
TSshDrvCmdHandler::~CSshDrvCmdHandler (void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef   TSshDrvCmdHandler::Error  (void) const { return m_error; }

CStringW    TSshDrvCmdHandler::State  (void) {
	m_error << __MODULE__ << S_OK;

	CStringW cs_state;

	HRESULT hr_ = m_man.Status(TKmdfDrv_Crt(), m_svc);
	if (FAILED(hr_)) {
		m_error = m_man.Error();
		cs_state = _T("#error: not installed;");
	}
	else {
		cs_state = CCfgToStr().PrintState(m_svc.Status().Data().dwCurrentState);
	}
	return cs_state;
}