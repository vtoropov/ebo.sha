#ifndef _EBOSVCGOOPANVER_H_6ECEFB93_ABCD_4677_85B0_CEFB85902C6D_INCLUDED
#define _EBOSVCGOOPANVER_H_6ECEFB93_ABCD_4677_85B0_CEFB85902C6D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2019 at 6:16:29p, UTC+7, Phuket, Rawai, Tuesday;
	This is communication data exchange receiver desktop console application version declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 3:24:20a, UTC+7, Novosibirsk, Tuesday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 2:55:16p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool project on 21-May-2020 at 5:31:55p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Shared optimization tool project on 26-Aug-2020 at 6:02:43a, UTC+7, Novosibirsk, Wednesday;
*/

#ifndef WINVER                 // Specifies that the minimum required platform is Windows Vista.
#define WINVER         0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINNT           // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINNT   0x0600  // Change this to the appropriate value to target other versions of Windows.
#endif

#ifndef _WIN32_WINDOWS         // Specifies that the minimum required platform is Windows Vista.
#define _WIN32_WINDOWS 0x0600  // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE              // Specifies that the minimum required platform is Internet Explorer 9.0 (Windows Vista, service pack 2).
#define _WIN32_IE      0x0900  // Change this to the appropriate value to target other versions of IE.
#endif

#endif/*_EBOSVCGOOPANVER_H_6ECEFB93_ABCD_4677_85B0_CEFB85902C6D_INCLUDED*/