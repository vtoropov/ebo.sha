#ifndef _SFXIMAGEEXT_H_80F878AD_3161_437B_BB9B_20F2F37A8CC0_INCLUDED
#define _SFXIMAGEEXT_H_80F878AD_3161_437B_BB9B_20F2F37A8CC0_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Nov-2020 at 4:44:15p, UTC+7, Novosibirsk, Wednesday;
	This is Sfx picture control extension interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.png.wrap.h"

namespace ST_Ctrls {

	using ex_ui::draw::CPngBitmap;
	using ex_ui::draw::CPngBitmapPtr;

	typedef Gdiplus::Bitmap TBitmap;
	using shared::sys_core::CError ;

	class CImage {
	private:
		CPngBitmapPtr  m_png_ptr; // has no copy constructor; must be improved;
		CError   m_error;
		RECT     m_rect ;         // is calculated by layout;

	public:
		 CImage (void);
		~CImage (void);

	public:
		HRESULT   Create (const WORD _res_id);
		HRESULT   Destroy(void);
		TErrorRef Error  (void) const;
		bool      Is     (void) const; // must be reviewed: it is possible that a validation of png pointer is not properly defined;
		const
		RECT&     Rect   (void) const;
		RECT&     Rect   (void)      ;
		SIZE      Size   (void) const; // if bitmap object is invalid, returns zeroed size;

	public:
		operator  HBITMAP  const (void) const;
		operator  TBitmap* const (void) const;

	private: // non-copyable;
		CImage (const CImage&);
		CImage& operator = (const CImage&);
	};
	
}

typedef ST_Ctrls::CImage TPictureImage;

#endif/*_SFXIMAGEEXT_H_80F878AD_3161_437B_BB9B_20F2F37A8CC0_INCLUDED*/