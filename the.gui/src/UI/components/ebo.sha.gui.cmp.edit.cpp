/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2021 at 6:31:32.469 pm, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool generic label-edit pair GUI component interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:01:04.611 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "StdAfx.h"
#include "ebo.sha.gui.cmp.edit.h"

using namespace ebo::sha::gui;

/////////////////////////////////////////////////////////////////////////////

TEditRa:: CEdit_Ra (void) {}
TEditRa::~CEdit_Ra (void) {}

/////////////////////////////////////////////////////////////////////////////
const
TLabelCtrl&   TEditRa::Edit (void) const { return m_edit ; }
TLabelCtrl&   TEditRa::Edit (void)       { return m_edit ; }
const
TLabelCtrl&   TEditRa::Label(void) const { return m_label; }
TLabelCtrl&   TEditRa::Label(void)       { return m_label; }

/////////////////////////////////////////////////////////////////////////////

HRESULT   TEditRa::Create (const HWND _parent, const RECT& _rc_label, const RECT& _rc_edit) { HRESULT hr_ = S_OK;
	_parent; _rc_label; _rc_edit;
	if (NULL == _parent || FALSE == ::IsWindow(_parent)) return (hr_ = OLE_E_INVALIDHWND);
	if (::IsRectEmpty(&_rc_label)) return (hr_ = OLE_E_INVALIDRECT);
	if (::IsRectEmpty(&_rc_edit)) return (hr_ = OLE_E_INVALIDRECT);

	hr_ = m_label.Create(_parent, _rc_label, NULL); if (FAILED(hr_)) return hr_;
	hr_ = m_edit.Create (_parent, _rc_edit , NULL);

	m_label.Layout().Update();
	m_edit .Layout().Update();

	return hr_;
}

HRESULT   TEditRa::Destroy(void) { HRESULT hr_ = S_OK;
	hr_  = m_label.Destroy();
	hr_  = m_edit .Destroy();
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////