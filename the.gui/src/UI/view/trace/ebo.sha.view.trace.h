#ifndef _EBOSHAVIEWTRACE_H_374E100B_CDC0_41E1_A3AF_C3AB80632816_INCLUDED
#define _EBOSHAVIEWTRACE_H_374E100B_CDC0_41E1_A3AF_C3AB80632816_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Jan-2021 at 05:32:19.426 am, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha Optima Tool driver/service debug view interface declaration file.
*/
#include "shared.uix.trace.h"
#include "ebo.sha.view.bas.h"
#include "ebo.sha.view.empty.h"

#pragma comment(lib, "_log_v15.lib")
#pragma comment(lib, "_uix.trace_v15.lib")

#include "ebo.sha.gui.fmt.trace.h"
#include "ebo.sha.gui.fmt.status.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	using namespace ex_ui::trace;

	class CTrace : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base, ITrace_Events { typedef CViewWnd_Base TViewWnd; friend class CTrace;
		private:
			CTrace&        m_view  ;
			TEmptyView     m_empty ;
			CTraceEx       m_trace ;
			ex_ui::controls::format::CBorder_Ex m_top;

		public:
			 CViewWnd (CTrace&);
			~CViewWnd (void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
		private:
			HRESULT   ITrace_OnEntryClick(const DWORD _ent_id) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CTrace (void);
		~CTrace (void);

	public:
		CTraceEx&    Output (void) ;
	};

}}}}

typedef ebo::sha::gui::view::CTrace TTraceView;

#endif/*_EBOSHAVIEWTRACE_H_374E100B_CDC0_41E1_A3AF_C3AB80632816_INCLUDED*/