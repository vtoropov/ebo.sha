#ifndef _EBOSHAGUIFMTTRACE_H_BC392F39_8D27_484B_97B8_2FEC17992203_INCLUDED
#define _EBOSHAGUIFMTTRACE_H_BC392F39_8D27_484B_97B8_2FEC17992203_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com on 22-Jan-2021 at 8:42:20.547 am, UTC+7, Novosibirsk, Friday;
	This is Ebo Sha Optima Tool desktop app trace view color theme format interface declaration file;
*/
#include "shared.uix.trace.h"
#include "shared.uix.cmp.clrs.h"

#include "shared.uix.ctrl.base.fmt.h"
#include "ebo.sha.gui.theme.h"

namespace ebo { namespace sha { namespace gui { namespace format {

	using namespace ex_ui::trace;
	using namespace ex_ui::draw ;
	using namespace ex_ui::controls::format;
	using namespace ex_ui::controls;
	//
	// TODO: trace header must be improved for providing required functionality for managing buttons/toolbar control/format;
	//
	class CTrace_Images {
	private:
		TVisualStateAssoc  m_img_assoc;

	public:
		 CTrace_Images (void);
		 CTrace_Images (const CTrace_Images&);
		~CTrace_Images (void);

	public:
		HRESULT   Add  (const CState::_e _ctrl_state, const WORD _img_res_id);
		const
		TVisualStateAssoc& Get (void) const;

	public:
		 CTrace_Images& operator = (const CTrace_Images&);
		 CTrace_Images& operator <<(const TVisualStateAssoc&); // appends image associations;
	};

	class CTrace_Button {
	private:
		DWORD   m_cmd_id;
		CTrace_Images  m_img_assoc;

	public:
		 CTrace_Button (void);
		 CTrace_Button (const CTrace_Button&);
		~CTrace_Button (void);

	public:
		DWORD   CmdID  (void) const;
		DWORD&  CmdID  (void)      ;
		const
		CTrace_Images& Images (void) const;
		CTrace_Images& Images (void)      ;

	public:
		CTrace_Button&  operator = (const CTrace_Button&);
		CTrace_Button&  operator <<(const DWORD _cmd_id );
		CTrace_Button&  operator <<(const CTrace_Images&);
	};

	typedef ::std::vector<CTrace_Button> TTraceButtons;

	class CTrace_Toolbar {
	private:
		TTraceButtons  m_buttons;

	public:
		 CTrace_Toolbar (void);
		 CTrace_Toolbar (const CTrace_Toolbar&);
		~CTrace_Toolbar (void);

	public:
		const
		TTraceButtons&   Buttons (void) const;
		TTraceButtons&   Buttons (void)      ;
		const
		CTrace_Button&   Get (const UINT _ndx) const;
		CTrace_Button&   Get (const UINT _ndx)      ;

	public:
		CTrace_Toolbar& operator = (const CTrace_Toolbar&);
		CTrace_Toolbar& operator <<(const TTraceButtons&);   // appends button(s);
	};

	class CTrace {
	private:
		CTrace_Toolbar m_toolbar;

	public:
		 CTrace (void) ;
		 CTrace (const CTrace&);
		~CTrace (void);

	public:
		HRESULT   ApplyTo  (TTrace&) const;
		const
		CTrace_Toolbar&  Toolbar(void) const;
		CTrace_Toolbar&  Toolbar(void)      ;

	public:
		const
		CTrace& operator >>(TTrace&) const;
		CTrace& operator = (const CTrace&);
	};

}}}}

typedef ebo::sha::gui::format::CTrace  TTraceOutFmt;

#endif/*_EBOSHAGUIFMTTRACE_H_BC392F39_8D27_484B_97B8_2FEC17992203_INCLUDED*/