#ifndef _EBOSHAGUILAYOUT_H_5085B2FC_6DDF_45A8_A9C1_C2B6AB016AB9_INCLUDED
#define _EBOSHAGUILAYOUT_H_5085B2FC_6DDF_45A8_A9C1_C2B6AB016AB9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Dec-2020 at 8:15:15.158 pm, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha optima tool generic GUI parts layout interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:03:27.651 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.uix.ctrl.border.h"
#include "sfx.select.lay.h"
#include "sfx.select.ctrl.h"

namespace ebo { namespace sha { namespace gui { namespace layout {

	class CCtrl_Def_Layout {
	public:
		enum _pos : LONG {
			e_avt_shift = 180,
			e_cap_shift = 180, e_cap_height =  30,
			e_lab_gap   =  15, e_lab_height =  30,
			e_lab_width = 100, e_lab_left   =  90,
			e_edt_width = 380, e_edt_height =  32, e_edt_border_thickness = 1,
		};
	};

	class CSelect {
	public:
		 CSelect (void);
		 CSelect (const CSelect&);
		 CSelect (TSelectLay& _target);
		~CSelect (void);

	public:
		HRESULT   ApplyTo(TBorders& _ctrl_borders) const;   // applies pre-defined settings to control borders;
		HRESULT   ApplyTo(TSelectLay& _ctrl_layout) const;  // applies pre-defined settings to control layout;

	public:
		const CSelect&  operator >>(TBorders&) const;
		const CSelect&  operator >>(TSelectLay&) const;
		/*:*/ CSelect&  operator = (const CSelect&);
	};

	class CLayout {
	private:
		CSelect    m_select;

	public:
		 CLayout (void);
		 CLayout (const CLayout&);
		~CLayout (void);

	public:
		const
		CSelect&  Select (void) const;
		CSelect&  Select (void)      ;

	public:
		CLayout&  operator = (const CLayout&);
	};

}}}}

typedef ebo::sha::gui::layout::CSelect TSelectLayout;
typedef ebo::sha::gui::layout::CLayout TLayout;

typedef ebo::sha::gui::layout::CCtrl_Def_Layout TCtrlDefLayout;

#endif/*_EBOSHAGUILAYOUT_H_5085B2FC_6DDF_45A8_A9C1_C2B6AB016AB9_INCLUDED*/