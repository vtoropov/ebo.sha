#ifndef _SHAREDUIXCTRLBASEFMT_H_A3FD8B05_8647_4B5C_8A1E_47699B650F23_INCLUDED
#define _SHAREDUIXCTRLBASEFMT_H_A3FD8B05_8647_4B5C_8A1E_47699B650F23_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Aug-2020 at 6:02:47a, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Pack shared control format base interface declaration file.
*/
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.border.h"
#include "shared.uix.gen.font.h"
#include "shared.uix.gdi.draw.shape.h"
#include "shared.uix.gdi.draw.tile.h"

namespace ex_ui { namespace controls { namespace format {
	using ex_ui::draw::shape::CCorner;
	using ex_ui::draw::shape::CCorners;
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getsyscolor

	class CBkgnd {
	protected:
		CColour_Ex    m_clr_solid;
		CGradient     m_clr_grad ;
		UINT          m_img_res  ; // background image resource identifier; when zero then is not applicable;
		CMargins      m_margins  ; // a control must provide margins for its background and all components;
		TTiledSurface m_sectors  ;

	public:
		 CBkgnd (void);
		 CBkgnd (const CBkgnd&);
		~CBkgnd (void);

	public:
		const
		CGradient&     Gradient(void) const;
		CGradient&     Gradient(void)      ;
		UINT      Image  (void) const; // identifier of image resource, if any;
		UINT&     Image  (void)      ;
		const
		CMargins& Margins(void) const;
		CMargins& Margins(void)      ;
		const
		TTiledSurface& Sectors(void) const;
		TTiledSurface& Sectors(void)      ;
		const
		CColour_Ex&    Solid(void) const;
		CColour_Ex&    Solid(void)      ;

	public:
		CBkgnd&  operator = (const CBkgnd& _ref);   // makes a copy from the object provided;
		CBkgnd&  operator <<(const CColour_Ex& );   // sets solid color;
		CBkgnd&  operator <<(const CGradient&  );   // sets gradient;
		CBkgnd&  operator <<(const TTiledSurface&); // sets color sectors;
		CBkgnd&  operator <<(const UINT _res_id);   // loads an image by resource identifier;

	public:
		CBkgnd&  operator <<(LPCWSTR _lp_sz_fragment); // creates this class object from XML fragment text;
	};

	using ex_ui::draw::CFont;
	using ex_ui::draw::CFontOptions;

	class CFontSpec {
	protected:
		CStringW    m_name    ;  // font name/family, by default 'verdana';
		COLORREF    m_fore    ;  // fore color of the font;
		DWORD       m_size    ;  // font size, by default it is a font size of desktop item labels: 10 pt;
		CAlign      m_align   ;  // text alignment;
		TFontOpts   m_opts    ;  // include font options for creating a font in accordance with required options;

	public:
		 CFontSpec (void) ;
		 CFontSpec (const CFontSpec&) ;
		~CFontSpec (void);

	public:
		const
		CAlign&     Align  (void) const;
		CAlign&     Align  (void)      ;
		LPCWSTR     Family (void) const;
		HRESULT     Family (LPCWSTR)   ;
		COLORREF    Fore   (void) const;
		COLORREF&   Fore   (void)      ;
		const
		TFontOpts&  Options(void) const;
		TFontOpts&  Options(void)      ;
		DWORD       Size   (void) const;
		DWORD&      Size   (void)      ;

	public:
		CFontSpec&  operator = (const CFontSpec&);
		CFontSpec&  operator <<(const CAlign&);
		CFontSpec&  operator >>(const COLORREF _clr_fore); // error C2535: member function already defined or declared; there is a mix with DWORD of size attribute;
		CFontSpec&  operator <<(const DWORD _dw_size );
		CFontSpec&  operator <<(const TFontOpts&);
		CFontSpec&  operator <<(LPCWSTR _lp_sz_family);
	};

	class CBase {
	protected:
		CBkgnd      m_bkgnd  ;
		CBorders_Ex m_borders;
		CFontSpec   m_spec   ;

	public:
		 CBase (void);
		 CBase (const CBase&);
		~CBase (void);

	public:
		const
		CBkgnd&   Bkgnd (void) const;
		CBkgnd&   Bkgnd (void)      ;
		const
		CBorders_Ex& Borders (void) const;
		CBorders_Ex& Borders (void)      ;
		const
		CFontSpec&Font  (void) const;
		CFontSpec&Font  (void)      ;

	public:
		CBase&  operator = (const CBase& );
		CBase&  operator <<(const CBkgnd&);
		CBase&  operator <<(const CBorders_Ex&);
		CBase&  operator <<(const CFontSpec&);
	};

	using ex_ui::controls::CState ;
	typedef ::std::map<CState::_e, WORD> TVisualStateAssoc;

	class CLabel {
	private:
		CAlign    m_align;
		CFontSpec m_font ;
		CBkgnd    m_bkgnd;
		CMargins  m_margins; // this is text margins and it cannot be applied to all parts of the label;

	public:
		 CLabel (void);
		 CLabel (const CLabel&);
		~CLabel (void);

	public:
		const
		CAlign&   Align (void) const;
		CAlign&   Align (void)      ;
		const
		CBkgnd&   Bkgnd (void) const;
		CBkgnd&   Bkgnd (void)      ;
		const
		CFontSpec&Font  (void) const;
		CFontSpec&Font  (void)      ;
		const
		CMargins& Margins(void) const;
		CMargins& Margins(void)      ;
		
	public:
		CLabel&   operator = (const CLabel&);
		CLabel&   operator <<(const CAlign&);
		CLabel&   operator <<(const CBkgnd&);
		CLabel&   operator <<(const CFontSpec&);
		CLabel&   operator <<(const CMargins&);

	public:
		CLabel&   operator <<(LPCWSTR _lp_sz_xml);   // creates this class object from XML fragment text; not done yet;
	};
}}}

typedef ex_ui::controls::format::CBkgnd    TFmtBkg  ;
typedef ex_ui::controls::format::CBase     TFmtBase ;
typedef ex_ui::controls::format::CFontSpec TFontSpec;

#endif/*_SHAREDUIXCTRLBASEFMT_H_A3FD8B05_8647_4B5C_8A1E_47699B650F23_INCLUDED*/