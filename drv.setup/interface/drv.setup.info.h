#ifndef _DRVSETUPINFO_H_71F71BA0_B230_42F8_9D54_0B224D58154D_INCLUDED
#define _DRVSETUPINFO_H_71F71BA0_B230_42F8_9D54_0B224D58154D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Sep-2020 at 9:26:35a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack generic driver setup information device interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 12-Jan-2021 at 7:11:13.176 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "drv.setup.defs.h"

namespace shared { namespace driver { namespace setup {

	typedef  SP_DEVINFO_DATA TDevInfoData;      // declared in <setupapi.h>
	typedef PSP_DEVINFO_DATA TDevInfoDataPtr;

	// https://docs.microsoft.com/en-us/windows/win32/api/setupapi/ns-setupapi-sp_devinfo_data
	// https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/device-node-status-flags
	class CDevInfo {
	protected:
		TDevInfoData   m_data;

	public:
		 CDevInfo (void);
		 CDevInfo (const CDevInfo&);
		~CDevInfo (void);

	public:
		const GUID&    Class (void) const;
		HRESULT        Class (const GUID);
		const bool     Is    (void) const; // TODO: DevInst field must be taken into account; DevInst == DevNode; the reference is above;
		const
		TDevInfoData&  Ref   (void) const;
		TDevInfoData&  Ref   (void) ;
		HRESULT        Reset (void) ;
		const
		TDevInfoDataPtr  Ptr (void) ;
		CStringW       ToStr (LPCWSTR _lp_sz_sep = _T(";")) const;
		
	public:
		CDevInfo&  operator = (const CDevInfo&);
		CDevInfo&  operator = (const TDevInfoData&);
		CDevInfo&  operator <<(const GUID& _class);

	public:
		operator const TDevInfoData&   (void) const;
		operator       TDevInfoData&   (void)      ;
		operator const TDevInfoDataPtr (void)      ;
	};

	// https://docs.microsoft.com/en-us/windows/win32/api/setupapi/nf-setupapi-setupdigetclassdevsw      | used here for getting device info set;
	// https://docs.microsoft.com/en-us/windows-hardware/drivers/install/device-information-sets         | common defs | architecture;

	class CDevInfoSet {
	protected:
		HDEVINFO   m_p_set;
		CError     m_error;

	public:
		 CDevInfoSet (void) ;
		 CDevInfoSet (const CDevInfoSet&) ;
		~CDevInfoSet (void) ;

	public:
		HRESULT   Erase (void) ;
		TErrorRef Error (void) const;
		bool      Is    (void) const;
		HRESULT   Get   (const GUID& _dev_iface, LPCWSTR _lp_sz_enum_name = NULL, const HWND _h_gui = NULL, const DWORD _dw_flags = CTypes::e_unspecified);

	public:
		CDevInfoSet& operator = (const CDevInfoSet&);  // the info being copied is erased here; this is made for proper releasing info pointer;
		CDevInfoSet& operator <<(const GUID&);         // initializes the device info pointer with particular data;

	public:
		operator const HDEVINFO& (void) const;
	};
}}}

#endif/*_DRVSETUPINFO_H_71F71BA0_B230_42F8_9D54_0B224D58154D_INCLUDED*/