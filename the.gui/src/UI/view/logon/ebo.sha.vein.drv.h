#ifndef _EBOSHAVEINDRV_H_FCCDFC45_C139_40A0_9930_4D24BDEC3A71_INCLUDED
#define _EBOSHAVEINDRV_H_FCCDFC45_C139_40A0_9930_4D24BDEC3A71_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Nov-2020 at 3:23:56p, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha Optima Tool vein scan driver sub-view interface declaration file;
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.view.empty.h"
#include "ebo.sha.gui.cmp.cap.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CVein_Drv : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CVein_Drv;
		private:
			CVein_Drv&    m_view ;
			TCaptionComponent  m_caps[1];

			TEmptyView    m_empty;

		private:
			 CViewWnd (CVein_Drv&);
			~CViewWnd (void);

		private:   // TWindow
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CVein_Drv (void);
		~CVein_Drv (void);

	public: // CView_Base
		HRESULT    Refresh (void) override;
	};

}}}}

typedef ebo::sha::gui::view::CVein_Drv TVeinDrv;

#endif/*_EBOSHAVEINDRV_H_FCCDFC45_C139_40A0_9930_4D24BDEC3A71_INCLUDED*/