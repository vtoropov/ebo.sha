#ifndef _SHAREDUIXCTRLBASERND_H_52AD29E8_4263_4CE0_A167_79F1A5C3B3D9_INCLUDED
#define _SHAREDUIXCTRLBASERND_H_52AD29E8_4263_4CE0_A167_79F1A5C3B3D9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Oct-2010 at 10:52:17pm, GMT+3, Rostov-on-Don, Sunday;
	This is Sfx Pack rendering base class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 29-Jul-2018 at 10:47:11a, UTC+7, Novosibirsk, Sunday;
	Adopted to FakeGPS project on 24-Apr-2020 at 10:46:59p, UTC+7, Novosibirsk, Friday;
*/
#include "shared.uix.ctrl.base.crt.h"
#include "shared.uix.gdi.object.h"
#include "shared.uix.png.wrap.h"
#include "shared.uix.ctrl.base.fmt.h"
#include "shared.uix.ctrl.base.lay.h"

namespace ex_ui { namespace controls {

	using ex_ui::draw::defs::IRenderer;
	using ex_ui::draw::CZBuffer   ;
	using ex_ui::draw::CImageCache;
	using ex_ui::draw::CPngBitmap ;

	interface IRenderer_Ex : public IRenderer {
		virtual const TFmtBkg& GetBkgFormat(void) const PURE;
	};

	class CRenderer : public IRenderer {
	protected:
		CWindow    m_owner;            // a window object, which holds/owns this renderer instance;
		TFmtBkg    m_bkg_format;

	public:
		 CRenderer (void);
		 CRenderer (const CRenderer&); // useless yet;
		~CRenderer (void);

	public: //  IRenderer;
		virtual HRESULT   DrawBackground(const HDC hSurface, const RECT& rcDrawArea) override;
		virtual HRESULT   DrawParentBackground(const HWND hChild, const HDC hSurface, RECT& rcUpdated) override;

	public:
		const
		TFmtBkg&   BkgFormat (void) const;
		TFmtBkg&   BkgFormat (void)      ;
		const
		CWindow&   Owner     (void) const;
		CWindow&   Owner     (void)      ;

	public:
		CRenderer& operator = (const CRenderer&);
		CRenderer& operator <<(const HWND _owner);
		CRenderer& operator <<(const TFmtBkg&);
	};

	class CRenderer_Ex : public CRenderer { // actually, it is not used so often as expected; must be re-designed or deprecated;
	                    typedef CRenderer TBase;
	protected:
		CControlCrt       m_crt;
		CImageCache       m_images;

	public:
		 CRenderer_Ex (void);
	explicit CRenderer_Ex (CControlCrt&);
	explicit CRenderer_Ex (const CRenderer_Ex&);
		~CRenderer_Ex (void);

	public: //  IRenderer;
		virtual HRESULT   DrawBackground(const HDC hSurface, const RECT& rcDrawArea) override;
		virtual HRESULT   DrawParentBackground(const HWND hChild, const HDC hSurface, RECT& rcUpdated) override;

	public:
#if (0)
		virtual HRESULT   DrawBkgnd(CZBuffer&);
#endif
		virtual HRESULT   DrawImage(CZBuffer&);
		virtual HRESULT   DrawImage(CZBuffer&, const RECT& rcDrawArea);
		virtual HRESULT   DrawImage(CZBuffer&, const RECT& rcDrawArea, const DWORD dwState);

	public:
		const CControlCrt&   Crt   (void) const; CControlCrt&   Crt   (void)      ;
		const CImageCache&   Images(void) const; CImageCache&   Images(void)      ;
		SIZE           RequiredSize(void) const;

	public:
		CRenderer_Ex& operator = (const CRenderer_Ex&);
		CRenderer_Ex& operator <<(const CControlCrt&);
		CRenderer_Ex& operator <<(const CImageCache&);
	};
}}

typedef ex_ui::controls::CRenderer       TCtrlRender;
typedef ex_ui::controls::CRenderer_Ex    TCtrlRender_Ex;

#endif/*_SHAREDUIXCTRLBASERND_H_52AD29E8_4263_4CE0_A167_79F1A5C3B3D9_INCLUDED*/