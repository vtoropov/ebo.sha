/*
	Created by Tech_dog (ebontrop@gmail.com) on 27-Dec-2020 at 5:57:10.336 pm, UTC+7, Novosibirsk, Sunday;
	This is Ebo Sha optima tool running process control view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.block.proc.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

#include "shared.uix.gdi.draw.defs.h"
#include "shared.uix.gdi.renderer.h"
using namespace ex_ui::draw;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CBlock_Proc_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_tab  = IDC_EBO_SHA_BLK_PRC_TAB,
		};
	}; typedef CBlock_Proc_Ctl This_Ctl;

	class CBlock_Proc_Res {
	public:
		enum _res : WORD {
			e_res_tab_0_a  = IDR_EBO_SHA_BLK_PRC_TAB_0a,
			e_res_tab_0_n  = IDR_EBO_SHA_BLK_PRC_TAB_0n,
			e_res_tab_1_a  = IDR_EBO_SHA_BLK_PRC_TAB_1a,
			e_res_tab_1_n  = IDR_EBO_SHA_BLK_PRC_TAB_1n,
		};
	}; typedef CBlock_Proc_Res This_Res;

	class CBlock_Proc_Layout {
	private:
		CBlock_Proc& m_view;
		RECT         m_rect;

	public:
		 CBlock_Proc_Layout (CBlock_Proc& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CBlock_Proc_Layout (void) {}

	public:
		RECT GetTabsRect (void) const {
			RECT rc_tabs = m_rect;
			::InflateRect(&rc_tabs, -0x5, -0x5);
			return rc_tabs;
		}

		VOID   Update (void) {
			TTabCtrl& tabbed  = m_view.Tabbed(); tabbed.Layout() << this->GetTabsRect();
		}
	};

	class CBlock_Proc_Init {
	private:
		CBlock_Proc& m_view ;
		CError       m_error;

	public:
		 CBlock_Proc_Init (CBlock_Proc& _view) : m_view(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }
		~CBlock_Proc_Init (void) {}

	public:
		HRESULT   OnCreate  (void) { m_error << __MODULE__ << S_OK;
#pragma region __tab_create
			TTabCtrl& tabs = m_view.Tabbed();

			tabs.Format() = shared::Get_Format().Tabbed();
			tabs.Layout().Tabs().Gap() = 0;

			tabs.Tabs().Append(_T("")); // monitor service mode;
			tabs.Tabs().Tab(0).Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_tab_0_n );
			tabs.Tabs().Tab(0).Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_tab_0_a );

			tabs.Tabs().Append(_T("")); // black list of processes to terminate;
			tabs.Tabs().Tab(1).Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_tab_1_n );
			tabs.Tabs().Tab(1).Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_tab_1_a );

			tabs.Create(m_view.Window(), CBlock_Proc_Layout(m_view).GetTabsRect(), This_Ctl::e_ctl_tab);
			tabs.ParentRenderer(NULL);

			tabs.Layout().Margins().Left() = tabs.Layout().Margins().Top() = tabs.Layout().Margins().Right() = tabs.Layout().Margins().Bottom() = 10;

			tabs.Tabs().Active(0);
#pragma endregion
			return m_error;
		}
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CBlock_Proc::CViewWnd:: CViewWnd(CBlock_Proc& _view) : TViewWnd(_view), m_view(_view), m_tabbed(_view) {}
CBlock_Proc::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CBlock_Proc::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	const RECT rc_ = Lay_(*this); rc_;

	CBlock_Proc_Init init_(m_view); HRESULT hr_ = init_.OnCreate();
	CBlock_Proc_Layout layout_(m_view); m_tabbed.Layout() << layout_.GetTabsRect();

	const RECT rc_page = m_tabbed.Layout().Page(); rc_page;

	if (SUCCEEDED(hr_)) {
	}
	if (SUCCEEDED(hr_)) {
		hr_ = m_empty.Create(m_tabbed.Window(), rc_page, true);
	}

	return 0;
}

LRESULT   CBlock_Proc::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_empty.Destroy();
	m_tabbed.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CBlock_Proc::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CBlock_Proc_Layout layout_(m_view);
	layout_.Update();
	RECT rc_page = m_tabbed.Layout().Page();
	m_empty.Window().MoveWindow(&rc_page);

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CBlock_Proc:: CBlock_Proc (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CBlock_Proc::~CBlock_Proc (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CBlock_Proc::ITabEvent_OnSelect (const DWORD _tab_ndx) { _tab_ndx;
	TView::m_error << __MODULE__ << S_OK;
	m_wnd.m_empty.IsVisible(10 != _tab_ndx);
	return TView::m_error;
}

/////////////////////////////////////////////////////////////////////////////

TTabCtrl&    CBlock_Proc::Tabbed (void) { return m_wnd.m_tabbed; }