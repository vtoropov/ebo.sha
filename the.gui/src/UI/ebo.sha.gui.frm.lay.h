#ifndef _EBOBOOFRMMAINLAY_H_B915121B_1007_4EF9_ADE3_EFD8BB4B3169_INCLUDED
#define _EBOBOOFRMMAINLAY_H_B915121B_1007_4EF9_ADE3_EFD8BB4B3169_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Aug-2018 at 8:10:08a, UTC+7, Novosibirsk, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop application main form layout interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack personal account app on 27-Oct-2019 at 3:05:30p, UTC+7, Novosibirsk, Sunday;
	Adopted to Ebo Game Dyna Inject project on 27-Feb-2020 at 5:07:44a, UTC+7, Novosibirsk, Thursday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 3:59:28p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool on 21-May-2020 at 7:07:46p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Shared optimization tool project on 26-Aug-2020 at 6:22:11a, UTC+7, Novosibirsk, Wednesday;
*/
#include "shared.gui.page.layout.h"
#include "ebo.sha.gui.frm.main.h"

namespace ebo { namespace sha { namespace gui { namespace _impl {

	using ebo::sha::gui::CMainForm;

	class CLayout {
	private:
		RECT       m_area;
		CMainForm& m_main;
		
	public:
		 CLayout (CMainForm& _main);
		 CLayout (CMainForm& _main , const RECT& _rc_client_area);
		~CLayout (void);

	public:     // it is assumed trace view is created the last; because its placement is dependable on status bar control;
		RECT    Statusbar(const bool _b_default = false) const; // default flag means no target window exists yet, returns just default rectangle;
		RECT    Stripbar (const bool _b_default = false) const;
		RECT    Trace    (const bool _b_default = false) const; // if no default flag is provided, returns empty rectangle in case when trace is not visible;
		VOID    Update   (void)      ;                // adjust positions of all UI elements of main window;
		RECT    ViewArea (void) const;                // calculates a main frame client area acceptable for view(s);

	public:
		static RECT   AdjustRect(const RECT& _rc);    // checks a rectangle against boundaries of main monitor work screen area;
		static RECT   CenterArea(const RECT& _rc);
		static RECT   FormDefPlace(void);             // not used;

	private:
		static RECT   GetAvailableArea(void);
	};
}}}}

typedef ebo::sha::gui::_impl::CLayout TMainLayout;

#endif/*_EBOBOOFRMMAINLAY_H_B915121B_1007_4EF9_ADE3_EFD8BB4B3169_INCLUDED*/