#ifndef _FILTERCLASSIFY_H_A01AA4D0_A4BB_409D_B882_18D9E548BDB4_INCLUDED
#define _FILTERCLASSIFY_H_A01AA4D0_A4BB_409D_B882_18D9E548BDB4_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Aug-2020 at 9:04:31p, UTC+7, Novosibirsk, Monday;
	This is SSH Tunnel WFP filter device classifying functions interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 8:09:37.269 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "filter.notify.h"
#include "filter.ip.ctrl.h"

namespace ssh { namespace driver { namespace classify {

	class CConvert {
	public:
		class v4 {
		// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/fwpsk/ne-fwpsk-fwps_fields_ale_auth_recv_accept_v4_
		// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/fwpsk/ne-fwpsk-fwps_fields_ale_auth_connect_v4_
		public:
			static NTSTATUS To_accept  (const FWPS_INCOMING_VALUES0*, ULONG& protocol, ADDR_THUNK& _remote, ADDR_THUNK& _local );
			static NTSTATUS To_connect (const FWPS_INCOMING_VALUES0*, ULONG& protocol, ADDR_THUNK& _local , ADDR_THUNK& _remote);
		};

		class v6 {
		// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/fwpsk/ne-fwpsk-fwps_fields_ale_auth_recv_accept_v6_
		// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/fwpsk/ne-fwpsk-fwps_fields_ale_auth_connect_v6_
		public:
			static NTSTATUS To_accept  (const FWPS_INCOMING_VALUES0*, ULONG& protocol, ADDR_THUNK& _remote, ADDR_THUNK& _local );
			static NTSTATUS To_connect (const FWPS_INCOMING_VALUES0*, ULONG& protocol, ADDR_THUNK& _local , ADDR_THUNK& _remote);
		};
	};

	// actually, this class is used for sending a notification to client(s);
	class CThunk {
	public:
		class v4 {
			// boths methods are intended for proper filling IP addresses for client notification;
		public:
			static ULONG  Accept ( const ULONG protocol, const ADDR_THUNK& _remote, const ADDR_THUNK& _local ); // remote >> local;
			static ULONG  Connect( const ULONG protocol, const ADDR_THUNK& _local , const ADDR_THUNK& _remote); // local >> remote;
		};
		// it very looks like v6 is the same as v4, but for keeping interface consistency;
		class v6 {
		public:
			static ULONG  Accept ( const ULONG protocol, const ADDR_THUNK& _remote, const ADDR_THUNK& _local ); // remote >> local;
			static ULONG  Connect( const ULONG protocol, const ADDR_THUNK& _local , const ADDR_THUNK& _remote); // local >> remote;
		};
	public:
		// action that is being applied is not dependable from type of connection established: incoming or outgoing;
		// in both cases there is the same participants: local IP and remote IP,
		// and the main purpose is to check those IPs for accordance with some rules;
		static ULONG GetAction (const ULONG protocol, const ADDR_THUNK& _local, const ADDR_THUNK& _remote);
	};

	// this class is for incoming connection from remote IP to local one;
	class CAction_Accept  : private CThunk {
	                        typedef CThunk TBase;
	public:
		class v4 {
		public:
			static NTSTATUS NTAPI Do (const FWPS_INCOMING_VALUES0*, const FWPS_INCOMING_METADATA_VALUES0*, VOID*, const FWPS_FILTER0*, UINT64, FWPS_CLASSIFY_OUT0*);
		};
		class v6 {
		public:
			static NTSTATUS NTAPI Do (const FWPS_INCOMING_VALUES0*, const FWPS_INCOMING_METADATA_VALUES0*, VOID*, const FWPS_FILTER0*, UINT64, FWPS_CLASSIFY_OUT0*);
		};
	};

	// this class is for outgoing connection from local IP to remote one;
	class CAction_Connect : private CThunk {
	                        typedef CThunk TBase;
	public:
		class v4 {
		public:
			static NTSTATUS NTAPI Do (const FWPS_INCOMING_VALUES0*, const FWPS_INCOMING_METADATA_VALUES0*, VOID*, const FWPS_FILTER0*, UINT64, FWPS_CLASSIFY_OUT0*);
		};
		class v6 {
		public:
			static NTSTATUS NTAPI Do (const FWPS_INCOMING_VALUES0*, const FWPS_INCOMING_METADATA_VALUES0*, VOID*, const FWPS_FILTER0*, UINT64, FWPS_CLASSIFY_OUT0*);
		};
	};

}}}

#endif/*_FILTERCLASSIFY_H_A01AA4D0_A4BB_409D_B882_18D9E548BDB4_INCLUDED*/