#ifndef _SHAREDLOGJOURNAL_H_1871B7EC_A14A_4277_9775_8B33073C4AF1_INCLUDED
#define _SHAREDLOGJOURNAL_H_1871B7EC_A14A_4277_9775_8B33073C4AF1_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Oct-2015 at 2:33:00pm, GMT+7, Phuket, Rawai, Friday;
	This is Ebo Pack shared log library application event journal wrapper class declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 at 9-Jul-2018 at 8:25:17p, UTC+7, Phuket, Rawai, Monday;
*/
#include "shared.log.defs.h"
#include "shared.gen.sys.err.h"

namespace shared { namespace log
{
	using shared::sys_core::CError;
	using shared::sys_core::CErr_Format;

	class CEventJournal
	{
	private:
		CONSOLE_FONT_INFOEX m_font;  // default console font;

	public:
		 CEventJournal(void);
		~CEventJournal(void);

	public:
		HRESULT   SetFont(const CONSOLE_FONT_INFOEX&);
		HRESULT   SetFont(LPCWSTR _lp_sz_name, const SHORT _fn_size);

	public:
		CEventJournal& operator << (const CStringW&);     // puts info;
		CEventJournal& operator << (LPCWSTR _p_sz_info);  // puts info;
		CEventJournal& operator << (TErrorRef);           // puts error;
		CEventJournal& operator >> (LPCWSTR _p_sz_warn);  // puts warning;

	public:
		static VOID  LogEmptyLine(void);
		static VOID  LogError(const CError&);
		static VOID  LogError(LPCWSTR pszFormat, ...);
		static VOID  LogInfo (LPCWSTR pszFormat, ...);
		static VOID  LogWarn (LPCWSTR pszFormat, ...);
		static VOID  LogInfoFromResource(const UINT nResId, ...);
		// sets a flag to output all messages to host console window; it's used in debug mode;
		// it must be set on an application start once;
		static VOID  OutputToConsole(const bool);
		static VOID  VerboseMode(const bool);

	};

	VOID DumpToFile(LPCWSTR lpszFilePath, const _variant_t& vData);
}}

#endif/*_SHAREDLOGJOURNAL_H_1871B7EC_A14A_4277_9775_8B33073C4AF1_INCLUDED*/