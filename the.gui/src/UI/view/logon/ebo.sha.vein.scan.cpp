/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Nov-2020 at 5:24:47a, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha optima tool vein scan sub-view interface implementation file;
*/
#include "StdAfx.h"
#include "ebo.sha.vein.scan.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;
using namespace ex_ui::controls;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CVein_Scan_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_cap_0  = 0, // not defined yet;
		};
	}; typedef CVein_Scan_Ctl This_Ctl;

	class CVein_Scan_Res {
	public:
		enum _res : WORD {
			e_none      = 0,
			e_logo_scan = IDR_EBO_SHA_FNG_SCN_Y,
		};
	}; typedef CVein_Scan_Res This_Res;

	class CVein_Scan_Layout {
	public:
		enum _pos : LONG {
			e_avt_shift = 180,
			e_cap_shift = 180, e_cap_height =  30,
			e_lab_gap   =  15, e_lab_height =  30,
			e_lab_width = 100, e_lab_left   =  90,
			e_edt_width = 380, e_edt_height =  32, e_edt_border_thickness = 1,
		};
	private:
		CVein_Scan& m_view;
		RECT         m_rect;

	public:
		 CVein_Scan_Layout (CVein_Scan& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CVein_Scan_Layout (void) {}

	public:
		RECT   GetEmpty (const DWORD _ndx) { _ndx; RECT rc_empty = m_rect; rc_empty.top = GetImage(0).bottom; return rc_empty; }
		RECT   GetImage (const DWORD _ndx) {
			static const SIZE sz_logo = { 80, 80 };
			RECT rc_logo = {
				0, (0 == _ndx ? 0 : _pos::e_avt_shift), 0 + sz_logo.cx, (0 == _ndx ? sz_logo.cy : _pos::e_avt_shift + sz_logo.cy)
			};
			return rc_logo;
		}
		RECT   GetTitle (const DWORD _ndx) { return this->_get_cap_rect(_ndx); }

	public:
		RECT   GetLab_Scan(const DWORD _ndx) {
			_ndx;
			RECT rc_lab = {_pos::e_lab_left, m_rect.top + (_pos::e_cap_height + _pos::e_lab_gap), _pos::e_lab_left + _pos::e_lab_width, 0 };

			rc_lab.top += (_pos::e_lab_height + _pos::e_lab_gap) * _ndx; rc_lab.bottom = rc_lab.top + _pos::e_lab_height;

			return rc_lab;
		}

		RECT   GetEdt_Scan(const DWORD _ndx) {
			_ndx;
			RECT rc_edit = {_pos::e_lab_left   + _pos::e_lab_width, m_rect.top + (_pos::e_cap_height + _pos::e_lab_gap), 
			                _pos::e_lab_left   + _pos::e_lab_width + _pos::e_edt_width, 0
			};
			rc_edit.top += (_pos::e_edt_height + _pos::e_lab_gap - _pos::e_edt_border_thickness) * _ndx;
			rc_edit.top -= (_pos::e_edt_border_thickness * (1 + _ndx)); rc_edit.bottom = rc_edit.top + _pos::e_edt_height;

			if (2 == _ndx)
				rc_edit.right += 180;
			return rc_edit;
		}
	
	public:
		VOID   Update (TLabelCtrl* _p_caps, const DWORD _d_count) {
			if (NULL == _p_caps || 0 == _d_count)
				return;
			for (DWORD i_ = 0; i_ < _d_count; i_++)
				_p_caps[i_].Layout().Update(this->_get_cap_rect(i_));
		}
	private:
		RECT  _get_cap_rect(const DWORD _ndx) {
			RECT rc_cap = m_rect; rc_cap.top = (0 ==_ndx ? 0 : _pos::e_cap_shift); rc_cap.bottom = rc_cap.top + _pos::e_cap_height;
			return rc_cap;
		}
	};

	class CVein_Scan_Init {
	private:
		CVein_Scan& m_view ;
		CError       m_error;

	public:
		 CVein_Scan_Init (CVein_Scan& _view) : m_view(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }
		~CVein_Scan_Init (void) {}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;

			return m_error;
		}
		/////////////////////////////////////////////////////////////////////////////

		HRESULT   OnFormat_Scan_Labels (CLabel_Ex* _p_labs, const DWORD _d_count) { m_error << __MODULE__ << S_OK;
			_p_labs; _d_count;
			if (NULL == _p_labs || 0 == _d_count)
				return (m_error << E_INVALIDARG);

			static const COLORREF clr_back = RGB( 70,  70,  70);
			static const COLORREF clr_fore = RGB(230, 230, 230);

			static const CMargins margins_(5,0,0,3);

			for (DWORD i_ = 0; i_ < _d_count; i_++) {
				TLabFmt& fmt_ = _p_labs[i_].Format();
				fmt_.Family(_T("Segoe UI")); fmt_.FontSize(23); fmt_.ForeColour(clr_fore); fmt_.BkgColour(clr_back); fmt_.Margins() = margins_;
			}

			return m_error;
		}

		HRESULT   OnFormat_Scan_Edits  (CLabel_Ex* _p_edts, const DWORD _d_count) { m_error << __MODULE__ << S_OK;
			_p_edts; _d_count;
			if (NULL == _p_edts || 0 == _d_count)
				return (m_error << E_INVALIDARG);

			static const COLORREF clr_back = RGB( 70,  70,  70);
			static const COLORREF clr_fore = RGB(230, 230, 230);

			static const CMargins margins_(5,0,0,3);
			static const CColour clr_borders(RGB(115, 115, 115));

			for (DWORD i_ = 0; i_ < _d_count; i_++) {
				TLabFmt& fmt_ = _p_edts[i_].Format();
				fmt_.Family(_T("Verdana")); fmt_.FontSize(18); fmt_.ForeColour(/*clr_fore*/clr_borders); fmt_.BkgColour(clr_back); fmt_.Margins() = margins_;

				_p_edts[i_].Borders().Thickness(CVein_Scan_Layout::e_edt_border_thickness);
				_p_edts[i_].Borders().Color(clr_borders);
			}

			return m_error;
		}
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CVein_Scan::CViewWnd:: CViewWnd(CVein_Scan& _view) : TViewWnd(_view), m_view(_view) {}
CVein_Scan::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CVein_Scan::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	CVein_Scan_Layout lay_(m_view);
	CVein_Scan_Init  init_(m_view); init_.OnCreate();

#pragma region __format
	init_.OnFormat_Scan_Labels (m_scan_labs, _countof(m_scan_labs));
	init_.OnFormat_Scan_Edits  (m_scan_edts, _countof(m_scan_edts));
#pragma endregion

	HRESULT hr_ = m_caps[0].Create(*this, _T("Palm or Finger Print"), lay_.GetTitle(0), lay_.GetImage(0));
	if (SUCCEEDED(hr_)) {
		m_caps[0].Format(This_Res::e_logo_scan);
		m_caps[0].Image().Format().Bkgnd().Sectors().Append(shared::Get_Theme().Get(TThemePart::e_caption,  TThemeElement::e_back), TTileSize(80, 40));
		m_caps[0].Image().Format().Bkgnd().Sectors().Append(shared::Get_Format().Panel().Bkgnd().Solid(), TTileSize(80, 40));
		m_caps[0].Image().Layout().Update();
	}

#pragma region __sec_scan
	m_scan_labs[0].Create(*this, lay_.GetLab_Scan(0), _T("Code" )); m_scan_edts[0].Create(*this, lay_.GetEdt_Scan(0), _T("[#empty]"));
#pragma endregion

	hr_ = m_empty.Create(*this, lay_.GetEmpty(0), true);
	return 0;
}

LRESULT   CVein_Scan::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_empty.Destroy();
	m_caps[0].Destroy();
	m_scan_labs[0].Destroy();
	m_scan_edts[0].Destroy();

	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CVein_Scan::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CVein_Scan_Layout layout_(m_view); layout_.Update(&m_caps[0].Title(), _countof(m_caps));
	m_empty.Update(layout_.GetEmpty(0));

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CVein_Scan:: CVein_Scan (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CVein_Scan::~CVein_Scan (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CVein_Scan::Refresh (void) {
	TView::m_error << __MODULE__ << S_OK;
	return TView::Refresh();
}