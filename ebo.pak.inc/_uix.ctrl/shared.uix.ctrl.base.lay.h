#ifndef _SHAREDUIXCTRLBASESHAPE_H_68F21840_B7FD_4459_969C_E130155CF201_INCLUDED
#define _SHAREDUIXCTRLBASESHAPE_H_68F21840_B7FD_4459_969C_E130155CF201_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Jul-2018 at 9:13:36a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack UIX shared library control base shaper/layout interface declaration file.
*/
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.gdi.object.h"
#include "shared.uix.png.wrap.h"
#include "shared.uix.ctrl.base.fmt.h"

namespace ex_ui { namespace controls { namespace layout {
	// https://docs.microsoft.com/en-us/windows/win32/api/commctrl/nf-commctrl-imagelist_geticonsize
	//
	// this class is used as a base one for providing an ability of saving current rectangle data of GUI component;
	// it is expected the rectangle is calculated by layout class and is saved for using by drawing function(s);
	//
	class CPlacement {
	protected:
		RECT       m_rect;
	public:
		 CPlacement (void);
		 CPlacement (const CPlacement&);
		~CPlacement (void);

	public:
		const
		RECT&      Rect  (void) const;
		RECT&      Rect  (void)      ;

	public:
		CPlacement& operator = (const CPlacement&);
		CPlacement& operator <<(const RECT&);
	};
	//
	// this class holds layout parameter values only; no graphical object is handled here;
	//
	class CImage : public CPlacement {
	protected:
		CMargins   m_margins ; // image rectangle magins; is used for creating a gap between an image and item text;
		SIZE       m_size    ; // image size; by default it's calculated by means of image list control, but can be set manually;

	public:
		 CImage (void);
		 CImage (const CImage&);
		~CImage (void);

	public:
		const
		CMargins&  Margins (void) const ;
		CMargins&  Margins (void)       ;
		const bool Is      (void) const ; // returns true if size exists, i.e. cx and cy are specified; otherwise, false;
		const bool IsSquare(void) const ; // returns true if size specified and cx == cy;
		const
		SIZE    &  Size    (void) const ;
		SIZE    &  Size    (void)       ;
		HRESULT    Size    (const HIMAGELIST);

	public:
		CImage&    operator =  (const CImage&);
		CImage&    operator << (const CMargins&);
		CImage&    operator << (const HIMAGELIST); // sets a size of an image;
		CImage&    operator << (const SIZE&);      // sets a size of an image;
	};

	using ex_ui::draw::CPngBitmap;
	//
	// This class emulates an overlay picture similar to arrow sign in shortcut's image;
	// Of course this class can be used as a regular picture object if necessary;
	//
	class COverlay : public CPlacement {
	protected:
		CPngBitmap   m_p_png;
		CAlign       m_align;

	public:
		 COverlay (void);
		 COverlay (const COverlay&);
		 COverlay (const WORD _w_res_id); // creates overlay image by loading a resource of given identifier;
		~COverlay (void);

	public:
		const
		CAlign&      Align (void) const;
		CAlign&      Align (void)      ;
		const
		CPngBitmap&  Image (void) const;
		CPngBitmap&  Image (void)      ;
		const bool   Is    (void) const;

	public:
		COverlay&    operator = (const COverlay&);
		COverlay&    operator <<(const CAlign&);
		COverlay&    operator <<(const CPngBitmap&);
		COverlay&    operator <<(const WORD _w_res_id);
	};
	//
	// This class is intended for keeping text layout data stored for further output by drawing function(s);
	//
	class CText : public CPlacement {
	protected:
		CStringW   m_text ;
		CAlign     m_align;   // h:left|v:center by default;
		TMargins   m_margins;

	public:
		 CText (void) ;
	explicit CText (const CText&) ;
	explicit CText (LPCWSTR _lp_sz_text, const TMargins& = TMargins{0, 0, 0, 0} );
		~CText (void) ;

	public:
		const
		CAlign&    Align  (void) const;
		CAlign&    Align  (void)      ;
		bool       Is     (void) const;
		const
		TMargins&  Margins(void) const;
		TMargins&  Margins(void)      ;
		LPCWSTR    String (void) const;
		CStringW&  String (void)      ;

	public:
		CText&     operator = (const CText&);
		CText&     operator <<(const TAlign&);
		CText&     operator <<(const TMargins&);
		CText&     operator <<(LPCWSTR _lp_sz_str);
	};

	using ex_ui::draw::CImageCache;
	//
	// an extension of image cache: an overlay image is added as additional layer;
	// in many cases, direct usage of image cache is sufficient for performing draw functions;
	//
	class CLayers {
	protected:
		CImageCache  m_img_layers;
		COverlay     m_overlay;

	public:
		 CLayers (void);
		 CLayers (const CLayers&);
		~CLayers (void);

	public:
		const
		CImageCache& Cache   (void) const;
		CImageCache& Cache   (void)      ;
		const bool   Is      (void) const;
		const
		COverlay&    Overlay (void) const;
		COverlay&    Overlay (void)      ;

	public:
		CLayers&  operator = (const CLayers&) ;
	};
	//
	// extended layer collection class: text layer is added; also, a rectangle provided for storing all layers' rectangle;
	//
	class CLayersEx {
	protected:
		CLayers    m_img_lays;
		CText      m_txt_lay ;
		RECT       m_lay_rect;           // this area/rectangle is used for drawing all image layer(s) and text, if any;
		                                 // overlay layer has own rectangle for cases when its image size is defferent from all others;
	public:
		 CLayersEx (void);
		 CLayersEx (const CLayersEx&);
		~CLayersEx (void);

	public:
		const
		CLayers&   Images (void) const;
		CLayers&   Images (void)      ;
		const
		RECT&      Rect   (void) const;   // this method is used for getting rectangle corners that are required for mouse and draw;
		RECT&      Rect   (void)      ;   // rectangle height is dependable on control height by default;
		const
		CText&     Text   (void) const;
		CText&     Text   (void)      ;
		UINT       Width  (const UINT);   // sets item rectangle width; returns previous one just for kidding;

	public:
		CLayersEx&  operator = (const CLayersEx&);

	public: // everything that is suitable for making a copy;
		CLayersEx&  operator <<(const CLayers&);
		CLayersEx&  operator <<(const CText&);
		CLayersEx&  operator <<(const RECT&);
	};
	//
	// base class of layout that is enough for saving basic parameter values of GUI part;
	//
	class CBase { // is used very seldom; must be re-viewed or deprecated;
	protected:
		CWindow    m_wnd   ;      // a control window handle wrapper; is not used in class implementation; may be ignored;
		CImage     m_image ;
		CText      m_text  ;
		CMargins   m_margins;     // generic margins of internal area from borders;

	public:
		 CBase (void);
		 CBase (const CWindow& _host);
		 CBase (const HWND _host);
		~CBase (void);

	public:
		virtual HRESULT Update (const RECT& _area);

	public:
		const
		CImage&    Image(void) const;
		CImage&    Image(void)      ;
		const
		CMargins&  Margins (void) const;
		CMargins&  Margins (void)      ;
		const
		CText&     Text  (void) const;
		CText&     Text  (void)      ;
		CWindow    Window(void) const;
		CWindow&   Window(void)      ;

	public:
		CBase& operator = (const CBase&  );
		CBase& operator <<(const CWindow&);
		CBase& operator <<(const HWND );

	public:
		operator RECT(void) const;  // returns window client area rectangle if possible;
	};

}}}

typedef ex_ui::controls::layout::CPlacement     TPlacement ;
typedef ex_ui::controls::layout::CBase          TLayoutBase;
typedef ex_ui::controls::layout::CImage         TImageLay;
typedef ex_ui::controls::layout::CLayers        TLayers  ;
typedef ex_ui::controls::layout::CLayersEx      TLayersEx;
typedef ex_ui::controls::layout::COverlay       TOverlay ;
typedef ex_ui::controls::layout::CText          TTextLay ;

#endif/*_SHAREDUIXCTRLBASESHAPE_H_68F21840_B7FD_4459_969C_E130155CF201_INCLUDED*/