#ifndef _SFXCOMBOFMT_H_394E2F61_FFD2_4122_8B05_CAE9ADD3CE3A_INCLUDED
#define _SFXCOMBOFMT_H_394E2F61_FFD2_4122_8B05_CAE9ADD3CE3A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Aug-2010 at 11:55:14pm, GMT+3, Rostov-on-Don, Tuesday;
	This is ST Combo Box control renderer base class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Sfx combo project on 22-Nov-2020 at 11:54:21.696 am, UTC+7, Novosibirsk, Sunday;
*/
#include "shared.uix.ctrl.base.fmt.h"
#include "shared.uix.ctrl.defs.h"

namespace ST_Ctrls { namespace format {

	using ex_ui::controls::CBorder ;
	using ex_ui::controls::CBorders;

	using ex_ui::draw::shape::CCorner;
	using ex_ui::draw::shape::CCorners;

	using ex_ui::controls::format::CBkgnd;

	class CCombo_Format {
	public:
		class CState {
		private:
			TFmtBase   m_disable;
			TFmtBase   m_hovered;
			TFmtBase   m_normal ;
			TFmtBase   m_pressed;

		public:
			 CState (void);
			 CState (const CState&);
			~CState (void);

		public:
			const TFmtBase& Disable (void) const;
			      TFmtBase& Disable (void)      ;
			const TFmtBase& Hovered (void) const;
			      TFmtBase& Hovered (void)      ;
			const TFmtBase& Normal  (void) const;
			      TFmtBase& Normal  (void)      ;
			const TFmtBase& Pressed (void) const;
			      TFmtBase& Pressed (void)      ;

		public:
			CState&   operator = (const CState&);
		};
	private:
		CState   m_state;
	public:
		 CCombo_Format (void);
		 CCombo_Format (const CCombo_Format&);
		~CCombo_Format (void);

	public:
		const
		CState&   State (void) const;
		CState&   State (void)      ;

	public:
		const
		TFmtBase& Current(const ex_ui::controls::CState&) const;
		TFmtBase& Current(const ex_ui::controls::CState&)      ;

	public:
		CCombo_Format& operator = (const CCombo_Format&);
	};

}}

typedef ST_Ctrls::format::CCombo_Format         TComboFmt  ;
typedef ST_Ctrls::format::CCombo_Format::CState TCboStateFmt;

#endif/*_SFXCOMBOFMT_H_394E2F61_FFD2_4122_8B05_CAE9ADD3CE3A_INCLUDED*/