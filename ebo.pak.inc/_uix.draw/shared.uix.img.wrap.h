#ifndef _UIXIMGWRAP_H_79F33EAF_DDFB_43f1_9FC1_3E41CD7C9916_INCLUDED
#define _UIXIMGWRAP_H_79F33EAF_DDFB_43f1_9FC1_3E41CD7C9916_INCLUDED
/*
	Created by Tech_dog (VToropov) on 7-Aug-2015 at 1:33:57pm, GMT+7, Phuket, Rawai, Friday;
	This is UIX library GDI+ image related class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 28-May-2018 at 11:15:06p, UTC+7, Phuket, Rawai, Monday;
*/
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw {

	// https://docs.microsoft.com/en-us/windows/win32/api/gdiplusimagecodec/nf-gdiplusimagecodec-getimageencoders
	class CImageEncoderMime {
	public:
		enum _e {
			ePng   = 0,
			eBmp   = 1,
			eJpg   = 2,
		};
	};

	class CImageEncoderEnum
	{
	public:
		 CImageEncoderEnum(void);
		~CImageEncoderEnum(void);
	public:
		static HRESULT    GetEncoderClsid(const CImageEncoderMime::_e eMimeType, CLSID& __in_out_ref);
	private:
		CImageEncoderEnum(const CImageEncoderEnum&);
		CImageEncoderEnum& operator= (const CImageEncoderEnum&);
	};
}}

#endif/*_UIXIMGWRAP_H_79F33EAF_DDFB_43f1_9FC1_3E41CD7C9916_INCLUDED*/