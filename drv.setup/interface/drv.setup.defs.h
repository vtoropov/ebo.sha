#ifndef _DRVSETUPDEFS_H_D056F888_F064_4F8A_A247_5C3FF0F10A51_INCLUDED
#define _DRVSETUPDEFS_H_D056F888_F064_4F8A_A247_5C3FF0F10A51_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Sep-2020 at 8:05:54p, UTC+7, Novosibirsk, Saturday;
	This is Ebo Pack generic driver common setup interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 12-Jan-2021 at 6:56:18.053 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include <setupapi.h>
#include <initguid.h>
#include <objbase.h>                 // https://docs.microsoft.com/en-us/windows/win32/api/combaseapi/nf-combaseapi-stringfromclsid
#include "shared.gen.sys.err.h"

#pragma comment(lib, "SetupAPI.lib") // does not work; must be included via project properties dialog; it's necessary to make this clear;
#pragma comment(lib, "Ole32.lib")

/////////////////////////////////////////////////////////////////////////////

namespace shared { namespace driver { namespace setup {

	using shared::sys_core::CError;

	class CMemAlloca {
	public:
		PBYTE    m_p_raw_data;  // raw data buffer;
		DWORD    m_size ;       // allocated data size; (in bytes);
		CError   m_error;

	public:
		 CMemAlloca (void);
		 CMemAlloca (const CMemAlloca&);
		~CMemAlloca (void);

	public:
		HRESULT     Create (const DWORD _dw_size);
		HRESULT     Destroy(void);
		TErrorRef   Error  (void) const;

	public:
		const bool  Is  (void) const;
		PBYTE const Ptr (void) const;
		PBYTE       Ptr (void)      ;
		DWORD       Size(void) const;

	public:
		CMemAlloca& operator = (const CMemAlloca&);
		CMemAlloca& operator <<(const DWORD _size);

	public:
		operator const PBYTE (void) const;
		operator       PBYTE (void)      ;
	};

	// actually, this class is just enumeration of device setup types, that is interested from point of view for connecting device to;
	//           extended by zero point for indicating invalid query type for enumerations which are useless;
	// https://docs.microsoft.com/en-us/windows-hardware/drivers/install/device-instance-ids
	class CTypes { // TODO: class name looks to be interfering with other ones; must be re-viewed;
	public:
		enum _type : DWORD {
			e_unspecified = 0x0,
			e_all = DIGCF_ALLCLASSES, e_iface = DIGCF_DEVICEINTERFACE, e_present = DIGCF_PRESENT
		};
	protected:
		DWORD m_types;
	public:
		 CTypes (void);
		 CTypes (const CTypes&);
		 CTypes (const DWORD _dw_types_requested);
		~CTypes (void);

	public:
		DWORD      Get   (void) const;
		const bool Has   (const _type) const;
		const bool Is    (void) const;  // some type(s) presented;
		const bool Not   (void) const;  // indicates type emptiness;
		DWORD&     Set   (void)      ;

	public:
		CTypes& operator = (const CTypes&);
		CTypes& operator+= (const DWORD _dw_types_added); // adds types;
		CTypes& operator-= (const DWORD _dw_types_extra); // removes types;
		CTypes& operator <<(const DWORD _dw_types_apply); // actually, sets the types provided, previous value is not kept;

	public:
		const bool  operator == (const DWORD _dw_type_of_interest); // returns true if type provided is contained in the current value of types; just condition;
		operator const DWORD (void) const; // for direct application to function(s) as a set of flags;
	};

	#define dw_presented (CTypes::e_present | CTypes::e_unspecified) // the latter one is just for kidding;
	#define dw_supported (CTypes::e_present | CTypes::e_iface)

}}}

typedef shared::driver::setup::CTypes  TSetupTypes;

#endif/*_DRVSETUPDEFS_H_D056F888_F064_4F8A_A247_5C3FF0F10A51_INCLUDED*/