#ifndef _DRVSETUPIFACE_H_257F598A_D4FB_4CBC_8901_09D875E833E6_INCLUDED
#define _DRVSETUPIFACE_H_257F598A_D4FB_4CBC_8901_09D875E833E6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Sep-2020 at 8:20:27p, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack generic driver device interface setup information interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 12-Jan-2021 at 7:35:35.646 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "drv.setup.defs.h"
#include "drv.setup.info.h"
#include "drv.setup.registry.h"

namespace shared { namespace driver { namespace setup {

	using shared::sys_core::CError;

	// https://docs.microsoft.com/en-us/windows/win32/api/setupapi/nf-setupapi-setupdigetdeviceinterfacedetailw
	// https://docs.microsoft.com/en-us/windows/win32/api/setupapi/ns-setupapi-sp_device_interface_data
	typedef  SP_DEVICE_INTERFACE_DATA        TDevIfaceData;
	typedef PSP_DEVICE_INTERFACE_DATA        TDevIfaceDataPtr;
	typedef PSP_DEVICE_INTERFACE_DETAIL_DATA TDevIfaceDetailsPtr;

	// actually, this class is a wrapper around device interface detailed information;
	class CDevIfaceData {
	protected:
		TDevIfaceData   m_data;

	public:
		 CDevIfaceData (void);
		 CDevIfaceData (const CDevIfaceData&);
		~CDevIfaceData (void);

	public:
		const GUID&     Class(void) const;
		HRESULT         Class(const GUID&);
		DWORD           Flags(void) const;
		DWORD&          Flags(void)      ;
		const bool      Is   (void) const;
		const
		TDevIfaceData&  Ref  (void) const;
		TDevIfaceData&  Ref  (void)      ;
		VOID            Reset(void)      ;  // clears internal structure fields;
		CStringW        ToStr(LPCWSTR _lp_sz_sep = _T("|")) const;

	public:
		CDevIfaceData&  operator = (const CDevIfaceData&) ;
		CDevIfaceData&  operator >>(CStringW&) ;
		CDevIfaceData&  operator <<(const GUID&);
		CDevIfaceData&  operator <<(const DWORD);
		CDevIfaceData&  operator +=(const DWORD);
		CDevIfaceData&  operator -=(const DWORD);
	public:
		operator const CStringW        (void) const;
		operator const TDevIfaceData&  (void) const;
		operator       TDevIfaceData&  (void)      ;
		operator       TDevIfaceDataPtr(void) const;
	};

	class CDevIfaceDetails : public CMemAlloca { typedef CMemAlloca TBase;
	public:
		 CDevIfaceDetails (void);
		 CDevIfaceDetails (const CDevIfaceDetails&);
		~CDevIfaceDetails (void);

	public:
		HRESULT       Get (const CDevInfoSet&, const CDevIfaceData&); // gets interface details;
		CStringW      Path(void) const;                               // gets driver installation path;

	public:
		CDevIfaceDetails& operator = (const CDevIfaceDetails&);
	};

	class CDevIfaceInfo {
	protected:
		CDevIfaceData    m_data ;
		CDevIfaceDetails m_details;
		CError           m_error;

	public:
		 CDevIfaceInfo (void);
		 CDevIfaceInfo (const CDevIfaceInfo&);
		~CDevIfaceInfo (void);

	public:
		const
		CDevIfaceData&    Data   (void) const;
		CDevIfaceData&    Data   (void)      ;
		const
		CDevIfaceDetails& Details(void) const;

	public:
		HRESULT    Erase (void)      ;
		TErrorRef  Error (void) const;
		HRESULT    Get   (const CDevInfoSet&);                       // gets device interface info details; it is assumed internal data is already set;
		HRESULT    Get   (const CDevInfoSet&, const CDevIfaceData&); // gets device interface info details;
		const bool Is    (void) const;
		CStringW   ToStr (LPCWSTR _lp_sz_sep = _T("|")) const;

	public:
		CDevIfaceInfo& operator = (const CDevIfaceInfo&);
		CDevIfaceInfo& operator <<(const CDevIfaceData&);
		CDevIfaceInfo& operator <<(const CDevIfaceDetails&);
	};

	typedef ::std::vector<CDevIfaceInfo>  TDevIfaceDetais;
}}}

#endif/*_DRVSETUPIFACE_H_257F598A_D4FB_4CBC_8901_09D875E833E6_INCLUDED*/