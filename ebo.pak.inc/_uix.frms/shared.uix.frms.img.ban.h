#ifndef _UIXFRAMEIMAGEHEADER_H_ADF4AA40_1A25_43f7_9ABF_B02BC5319F25_INCLUDED
#define _UIXFRAMEIMAGEHEADER_H_ADF4AA40_1A25_43f7_9ABF_B02BC5319F25_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Mar-2014 at 3:17:13am, GMT+4, Saint-Petersburg, Monday;
	This is Ebo Pack shared UIX frame library image banner interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to VS v15 on 8-Aug-2018 at 11:50:26a, UTC+7, Novosibirsk, Wednesday; 
*/
#include "shared.uix.gdi.provider.h"

namespace ex_ui { namespace frames {
	
	using ex_ui::draw::defs::IRenderer;

	class CImageBanner;
	class CImageBan_Layout {
	private:
		CImageBanner&   m_banner;

	public:
		 CImageBan_Layout (CImageBanner&);
		~CImageBan_Layout (void);

	public:
		HRESULT     Update (void)      ;           // recalculates position/rectangle of banner window;
		HRESULT     Update (const RECT& _rc_area); // updates banner window position into an area provided;

	public:
		CImageBan_Layout& operator <<(const RECT& _rc_area);        // updates banner window position in accordance with area  ;
		const RECT        operator = (const RECT& _rc_area) const;  // returns calculated rectangle of banner window for area provided;
	};
	
	class CImageBanner {
		
	private:
		CImageBan_Layout
		            m_layout ;
		HANDLE      m_ban_wnd;

	public: 
		 CImageBanner(void);
		 CImageBanner(const WORD _w_res_id);
		~CImageBanner(void);

	public:
		HRESULT     Create  (const HWND hParent, const UINT ctrlId = 0);
		HRESULT     Destroy (void);
		HRESULT     Image   (const WORD u_res_id); // sets banner image resource id;
		WORD        Image   (void) const;          // gets banner image resource id;
		bool        IsValid (void) const;          // returns true if banner window is valid;
		const
		CImageBan_Layout&   Layout (void) const;
		CImageBan_Layout&   Layout (void)      ;

		HRESULT     Renderer(IRenderer* _p_parent_rnd);
		SIZE        ReqSize (void) const; // returns required size of the banner window; actually, banner height is important only in many cases;
		bool        Visible (void) const;
		HRESULT     Visible (const bool);
		CWindow     Window  (void) const; // gets banner window handle;

	private: // non-copyable;
		CImageBanner(const CImageBanner&);
		CImageBanner& operator= (const CImageBanner&);
	};
}}

typedef ex_ui::frames::CImageBanner     TBanner;
typedef ex_ui::frames::CImageBan_Layout TBannerLayout;

#endif/*_UIXFRAMEIMAGEHEADER_H_ADF4AA40_1A25_43f7_9ABF_B02BC5319F25_INCLUDED*/