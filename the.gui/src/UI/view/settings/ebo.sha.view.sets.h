#ifndef _EBOSHAVIEWSETS_H_734E1160_ECC3_4AEA_9386_BBCDA23212CE_INCLUDED
#define _EBOSHAVIEWSETS_H_734E1160_ECC3_4AEA_9386_BBCDA23212CE_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Jan-2021 at 10:02:13.637 am, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Sha Optima Tool application settings view interface declaration file.
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.view.empty.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CSettings : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CSettings;
		private:
			CSettings&    m_view  ;
			TEmptyView    m_empty ;

		public:
			 CViewWnd (CSettings&);
			~CViewWnd (void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CSettings (void);
		~CSettings (void);

	public:
	};

}}}}

typedef ebo::sha::gui::view::CSettings TSettings;

#endif/*_EBOSHAVIEWSETS_H_734E1160_ECC3_4AEA_9386_BBCDA23212CE_INCLUDED*/