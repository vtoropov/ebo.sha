#ifndef _EBOSHAUSERSIGN_H_81B862C1_B028_4E2E_B82A_131E648883F4_INCLUDED
#define _EBOSHAUSERSIGN_H_81B862C1_B028_4E2E_B82A_131E648883F4_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Nov-2020 at 2:32:51a, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha optima tool user sign-in sub-view interface declaration file;
*/
#include "shared.gen.sys.err.h"

#include "ebo.sha.view.bas.h"
#include "ebo.sha.vein.scan.h"
#include "ebo.sha.vein.drv.h"
#include "ebo.sha.vein.stg.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CLogon_Sign : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CLogon_Sign;
		private:
			CLogon_Sign&  m_view  ;
			TTabCtrl      m_tabbed;
			TVeinScan     m_scan  ;
			TVeinDrv      m_driver;
			TVeinStg      m_stg   ;

		public:
			 CViewWnd (CLogon_Sign&);
			~CViewWnd (void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CLogon_Sign (void);
		~CLogon_Sign (void);

#pragma warning(disable:4481)
	private:    // ITabEvents
		HRESULT    ITabEvent_OnSelect (const DWORD _tab_ndx) override sealed;
#pragma warning(default:4481)
	
	public:
		TVeinDrv&  Driver (void);
		TVeinScan& Scan   (void);
		TVeinStg&  Storage(void);
		TTabCtrl&  Tabbed (void);
	};

}}}}

typedef ebo::sha::gui::view::CLogon_Sign TSignIn;

#endif/*_EBOSHAUSERSIGN_H_81B862C1_B028_4E2E_B82A_131E648883F4_INCLUDED*/