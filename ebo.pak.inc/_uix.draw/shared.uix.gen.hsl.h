#ifndef _SHAREDUIXGENHSL_H_F07A158F_6046_4638_A81E_30F8D567F7AF_INCLUDED
#define _SHAREDUIXGENHSL_H_F07A158F_6046_4638_A81E_30F8D567F7AF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) 26-Aug-2010 at 4:52:07pm, GMT+3, Rostov-on-Don, Thursday;
	This is UIX shared draw library generic HSL/RGB (hue-saturation-lightness) conversion interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 26-Sep-2018 at 5:34:48a, UTC+7, Novosibirsk, Wednesday;
*/
#include "shared.uix.gdi.draw.defs.h"
#include "shared.uix.clr.quad.h"

namespace ex_ui { namespace draw {
	// http://colorizer.org/
	class C_hls {
	private:
		DWORD      m_hls;

	public:
		 C_hls (void);
		 C_hls (const BYTE _h, const BYTE _l, const BYTE _s);
		 C_hls (const COLORREF _rgb);
		~C_hls (void);

	public:
		BYTE       Get_H(void) const;   // gets hue component;
		BYTE       Get_L(void) const;   // gets luminosity component;
		BYTE       Get_S(void) const;   // gets a saturation component;
		DWORD      ToHLS(const BYTE _h, const BYTE _l, const BYTE _s);
		DWORD      ToHLS(const COLORREF _rgb);
		DWORD      ToHLS(const COLORREF _rgb, __int8 percent_L, __int8 percent_S);
		COLORREF   ToRGB(void) const;

	public:
		C_hls& operator = (const COLORREF _rgb);

	public:
		operator COLORREF (void) const;
	};
	// https://en.wikipedia.org/wiki/HSL_and_HSV
	class C_hsl {
		//
		// TODO: take into account a compatibility with Adobe Photoshop 'Hue/Saturation Adjustment' command;
		//
	private:
		DOUBLE     m_h;      // hue value;
		DOUBLE     m_l;      // lightness value;
		DOUBLE     m_s;      // saturation value;

	public:
		C_hsl(void);
		C_hsl(const DOUBLE _h, const DOUBLE _l, const DOUBLE _s);
		C_hsl(const COLORREF _rgb);

	public:
		DOUBLE     _h(void) const  ;   // gets hue value;
		HRESULT    _h(const DOUBLE);   // sets hue value;
		DOUBLE     _l(void) const  ;   // gets lightness value;
		HRESULT    _l(const DOUBLE);   // sets lightness value;
		DOUBLE     _s(void) const  ;   // gets saturation value;
		HRESULT    _s(const DOUBLE);   // sets saturation value;

	public:
		bool       IsValid(void) const;
		COLORREF   To_rgb (void) const;


	public:
		operator const COLORREF(void) const;
		C_hsl& operator= (const COLORREF);  // assumes all components of HSL are clear before setting to new values;

	public:
		static bool H_is_valid(const DOUBLE _val);
		static bool L_is_valid(const DOUBLE _val);
		static bool S_is_valid(const DOUBLE _val);
	};

	class C_hsl_clr {
	private:
		COLORREF     m_rgb;     // color RGB value;
		USHORT       m_hue;     // hue value in degree; 0 - 360;
		UCHAR        m_chr;     // a saturation in percent, i.e. chroma;
		UCHAR        m_lgt;     // a lightness percentage;
		CAtlString   m_cga;     // cga(colour graphics adapter) name/number or alias;
		CAtlString   m_web;     // color friendly name for web;

	public:
		 C_hsl_clr(void);
		~C_hsl_clr(void);

	public:
		LPCTSTR      Cga   (void) const  ;
		VOID         Cga   (LPCTSTR)     ;
		UCHAR        Chrome(void) const  ;
		FLOAT        Chrome(const COLORREF _rgb);
		HRESULT      Chrome(const UCHAR) ;
		USHORT       Hue   (void) const  ;
		HRESULT      Hue   (const USHORT);
		UCHAR        Light (void) const  ;
		HRESULT      Light (const UCHAR) ;
		COLORREF     Rgb   (void) const  ;
		HRESULT      Rgb   (const COLORREF);
		LPCTSTR      Web   (void) const  ;
		VOID         Web   (LPCTSTR)     ;
	};

	typedef ::std::vector<C_hsl_clr>  T_hsl_clr_set;

	class C_hsl_clr_set {
	public:
		enum _r : UINT {
			na = 0xFFFF
		};
	private:
		T_hsl_clr_set   m_set;

	public:
		 C_hsl_clr_set(void);
		~C_hsl_clr_set(void);

	public:
		HRESULT     Append(const C_hsl_clr&);
		VOID        Clear (void);
		UINT        Count (void) const;
		const
		C_hsl_clr&  Colour(const INT _ndx) const;  // provides read-only access to HSL colour element by index provided; no checking available range;
	};

	class C_hsl_clr_std : public C_hsl_clr_set {
	                     typedef C_hsl_clr_set T_base;
	public:
		C_hsl_clr_std(void);
	};
}}

#endif/* _SHAREDUIXGENHSL_H_F07A158F_6046_4638_A81E_30F8D567F7AF_INCLUDED*/