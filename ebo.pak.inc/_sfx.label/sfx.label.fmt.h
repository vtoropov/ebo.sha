#ifndef __PULSEPAYSHAREDLABELCONTROLFORMAT_H_0770E057_36C9_413f_B21E_AE9B9F005F3C_INCLUDED
#define __PULSEPAYSHAREDLABELCONTROLFORMAT_H_0770E057_36C9_413f_B21E_AE9B9F005F3C_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 1-Nov-2012 at 8:13:38pm, GMT+3, Rostov-on-Don, Thursday;
	This is Pulsepay Shared Label Control Format class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Sfx Label control project on 24-Nov-2020 at 6:13:14.067 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "shared.uix.ctrl.base.fmt.h"
#include "shared.uix.ctrl.defs.h"

namespace ST_Ctrls { namespace format {

	class CLabel_Format : public ex_ui::controls::format::CLabel { typedef ex_ui::controls::format::CLabel TBase;
	public:
		 CLabel_Format (void);
		 CLabel_Format (const CLabel_Format&);
		~CLabel_Format (void);

	public:
		CLabel_Format& operator = (const CLabel_Format&);
	};

}}

typedef ST_Ctrls::format::CLabel_Format   TLabelFmt ;

#endif/*__PULSEPAYSHAREDLABELCONTROLFORMAT_H_0770E057_36C9_413f_B21E_AE9B9F005F3C_INCLUDED*/