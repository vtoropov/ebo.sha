/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Aug-2020 at 7:32:54ap, UTC+7, Novosibirsk, Monday;
	This is SSH Tunnel WFP filter device extension interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 8:09:37.504 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "filter.dev.ext.h"

using namespace ssh::driver;

t_dev_ext* g_dev_ext = NULL;
/////////////////////////////////////////////////////////////////////////////

NTSTATUS   ssh::driver::InitDeviceExtension(DEV_EXTENSION* _p_dev_ext) {

	NTSTATUS nt_status = S_OK;

	if (NULL == _p_dev_ext)
		return (nt_status = STATUS_INVALID_PARAMETER);

	DbgPrint( "__ssh.tnl.drv: [INFO] InitDeviceExtension()::fn_begin();\n" );

	::KeInitializeSpinLock (&_p_dev_ext->rangeslock);
	::KeInitializeSpinLock (&_p_dev_ext->destinationportslock);
	::KeInitializeSpinLock (&_p_dev_ext->sourceportslock);

	DbgPrint( "__ssh.tnl.drv: [INFO] InitDeviceExtension()::spin locks inited;\n" );

	//nt_status = InitNotificationQueue(&_p_dev_ext->queue);

	_p_dev_ext->blockedranges   = NULL;
	_p_dev_ext->blockedcount    = 0;
	_p_dev_ext->blockedlabelsid = 0;

	_p_dev_ext->allowedranges   = NULL;
	_p_dev_ext->allowedcount    = 0;
	_p_dev_ext->allowedlabelsid = 0;

	_p_dev_ext->destinationports  = NULL;
	_p_dev_ext->destinationportcount = 0;

	_p_dev_ext->sourceports  = NULL;
	_p_dev_ext->sourceportcount = 0;

	_p_dev_ext->block    = 0;

	_p_dev_ext->connect4 = 0;
	_p_dev_ext->accept4  = 0;
	_p_dev_ext->connect6 = 0;
	_p_dev_ext->accept6  = 0;

	DbgPrint( "__ssh.tnl.drv: [INFO] InitDeviceExtension()::fn_end();\n" );

	return nt_status;
}