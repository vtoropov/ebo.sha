#ifndef _EBOSHAGUICMPCAP_H_198DBE06_EC3B_4307_AA69_49197D429D71_INCLUDED
#define _EBOSHAGUICMPCAP_H_198DBE06_EC3B_4307_AA69_49197D429D71_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Dec-2020 at 9:27:32.238 pm, UTC+7, Novosibirsk, Wednesday;
	This is Ebo Sha Optima Tool generic caption GUI component interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:01:04.581 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.gen.clrs.h"
#include "sfx.image.ctrl.h"
#include "sfx.label.ctrl.h"

namespace ebo { namespace sha { namespace gui {

	class CCaption {
	private:
		TLabelCtrl    m_title;
		TImageCtrl    m_image;

	public:
		 CCaption (void);
		~CCaption (void);

	public:
		const
		TImageCtrl&   Image (void) const;
		TImageCtrl&   Image (void)      ;
		const
		TLabelCtrl&   Title (void) const;
		TLabelCtrl&   Title (void)      ;

	public:
		HRESULT   Create (const HWND _parent, LPCWSTR _lp_sz_text, const RECT& _rc_title, const RECT& _rc_image);
		HRESULT   Destroy(void);
		HRESULT   Format (const WORD _img_res_id, const ex_ui::controls::CMargins = ex_ui::controls::CMargins(95, 0, 0, 3));

	private: // non-copyable;
		CCaption (const CCaption&);
		CCaption& operator = (const CCaption&);
	};

}}}

typedef ebo::sha::gui::CCaption  TCaptionComponent;
typedef ebo::sha::gui::CCaption  TCapComp;

#endif/*_EBOSHAGUICMPCAP_H_198DBE06_EC3B_4307_AA69_49197D429D71_INCLUDED*/