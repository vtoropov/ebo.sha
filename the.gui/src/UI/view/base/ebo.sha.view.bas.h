#ifndef _EBOBOOMAINVIEWBAS_H_4EAFFCB6_B092_4F4E_9F32_9B057D1B7713_INCLUDED
#define _EBOBOOMAINVIEWBAS_H_4EAFFCB6_B092_4F4E_9F32_9B057D1B7713_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Oct-2019 at 8:51:42p, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Pack personal account app base view interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Game Dyna Inject project on 1-Mar-2020 at 10:27:27p, UTC+7, Novosibirsk, Sunday;
	Adopted to FakeGPS driver project on 16-Apr-2020 at 3:39:53p, UTC+7, Novosibirsk, Thursday;
	Adopted to Ebo Xor Java Eclipse build tool on 24-May-2020 at 10:41:29p, UTC+7, Novosibirsk, Sunday;
	Adopted to Ebo Sha optima tool on 23-Sep-2020 at 4:09:06p, UTC+7, Novosibirsk, Wednesday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.base.fmt.h"
#include "shared.uix.ctrl.base.rnd.h"

#include "shared.uix.gdi.draw.defs.h"
#include "shared.uix.gdi.renderer.h"

#include "shared.uix.ctrl.label.h"
#include "shared.uix.ctrl.button.h"
#include "shared.uix.info.panel.h"
#include "shared.gui.page.layout.h"

#include "sfx.image.ctrl.h"
#include "sfx.image.fmt.h"
#include "sfx.label.ctrl.h"
#include "sfx.select.ctrl.h"
#include "sfx.tabs.ctrl.h"
#include "sfx.button.ctrl.h"

#include "ebo.sha.gui.fmt.info.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	using shared::sys_core::CError;

	using ex_ui::controls::IRenderer    ;
	using ex_ui::controls::IRenderer_Ex ;
	using ex_ui::controls::IControlEvent;

	using ex_ui::controls::CLabel_Ex  ;
	using ex_ui::controls::CLab_Format;
	using ex_ui::controls::CButton_Ex ;

	using ST_Ctrls::CTab;
	using ST_Ctrls::CTabbed    ; typedef CTabbed TTabCtrl;
	using ST_Ctrls::ITabEvents ;

	using ST_Ctrls::CSelector  ; typedef CSelector TSelect;
	using ST_Ctrls::ISelector_Events;

	using namespace ex_ui::draw;
	using namespace ex_ui::controls;
	using namespace shared::gui::layout;

	class CView_Base;
	class CViewWnd_Base : public  ATL::CWindowImpl<CViewWnd_Base>, public IRenderer_Ex {
	                     typedef  ATL::CWindowImpl<CViewWnd_Base> TWindow; friend class CView_Base;
	protected:
		CView_Base&  m_view ;

	protected:
		 CViewWnd_Base(CView_Base&);
		~CViewWnd_Base(void);
	
	public:

#define WM_ERASE    WM_ERASEBKGND
#define WM_VISIBLE  WM_WINDOWPOSCHANGED

		DECLARE_WND_CLASS_EX(_T("ebo::sha::view::base"), 0, 0);
		BEGIN_MSG_MAP(CViewWnd_Base)
			MESSAGE_HANDLER (WM_COMMAND,   OnCommand)
			MESSAGE_HANDLER (WM_CREATE ,   OnCreate )
			MESSAGE_HANDLER (WM_DESTROY,   OnDestroy)
			MESSAGE_HANDLER (WM_ERASE  ,   OnErase  )
			MESSAGE_HANDLER (WM_SIZE   ,   OnSize   )
			MESSAGE_HANDLER (WM_VISIBLE,   OnVisible)
		END_MSG_MAP()

	protected:
		virtual LRESULT   OnCommand (UINT, WPARAM, LPARAM, BOOL&);
		virtual LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&);
		virtual LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&);
		virtual LRESULT   OnErase   (UINT, WPARAM, LPARAM, BOOL&);
		virtual LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&);
		virtual LRESULT   OnVisible (UINT, WPARAM, LPARAM, BOOL&);
#pragma warning(disable:4481)
	protected: // IRenderer;
		HRESULT   DrawBackground(const HDC hSurface, const RECT& rcDrawArea) override;
		HRESULT   DrawParentBackground(const HWND hChild, const HDC hSurface, RECT& rcUpdated) override;
	protected: // IRenderer_Ex;
		const
		TFmtBkg&  GetBkgFormat(void) const override;
#pragma warning(default:4481)
	};

	using shared::sys_core::CError; typedef CViewWnd_Base CViewWnd;

	class CView_Base : /*public IControlEvent, */public ITabEvents, public ISelector_Events, public TIInfo_Events {
	
	protected:
		CViewWnd_Base&  m_wnd  ;
		CError          m_error;

	protected:
		 CView_Base(CViewWnd_Base&);
		~CView_Base(void);

	protected:  // IControlEvent
#pragma warning (disable: 4481)
		HRESULT    IControlEvent_OnClick(const UINT ctrlId) override;
		HRESULT    IControlEvent_OnClick(const UINT ctrlId, const LONG_PTR nData) override;

	protected:  // ITabEvents
		HRESULT    ITabEvent_OnAppend (const CTab& _added  ) override;
		HRESULT    ITabEvent_OnFormat (const TTabbedFmt&   ) override;
		HRESULT    ITabEvent_OnSelect (const DWORD _tab_ndx) override;

	protected:  // ISelector_Events
		HRESULT    ISelector_OnBkgChanged (void) override;
		HRESULT    ISelector_OnFontChanged(void) override;
		HRESULT    ISelector_OnItemClick  (const UINT _u_itm_id) override;
		HRESULT    ISelector_OnItemImages (const UINT _u_res_id) override;

	private:    // TIInfo_Events
		HRESULT    IInfo_OnBkgChanged (void) override;
		HRESULT    IInfo_OnFontChanged(void) override;
		HRESULT    IInfo_OnItemClick  (const UINT _u_itm_id) override;
		HRESULT    IInfo_OnItemImages (const UINT _u_res_id) override;
#pragma warning(default:4481)

	public:
		virtual bool        CanAccept(const WORD nCmdId ) const;
		virtual HRESULT     Create   (const HWND hParent, const RECT& _area, const bool bVisible);
		virtual HRESULT     Destroy  (void);
		virtual TErrorRef   Error    (void) const;
		virtual bool        IsChecked(const UINT nCmdId) const;
		virtual bool        IsValid  (void) const;
		virtual bool        IsVisible(void) const;
		virtual HRESULT     IsVisible(const bool);
		virtual HRESULT     OnCommand(const WORD  cmdId);
		virtual HRESULT     OnPrepare(const WORD  cmdId);
		virtual HRESULT     Refresh  (void);
		virtual HRESULT     Update   (const RECT& _area);
		virtual CViewWnd&   Window   (void);

	private:
		CView_Base (const CView_Base&);
		CView_Base& operator= (const CView_Base&);
	};

}}}}

#endif/*_EBOBOOMAINVIEWBAS_H_4EAFFCB6_B092_4F4E_9F32_9B057D1B7713_INCLUDED*/