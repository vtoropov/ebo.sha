#ifndef _DRIVERSVCCRT_H_852341CF_576B_4941_946F_D8FBACB591D6_INCLUDED
#define _DRIVERSVCCRT_H_852341CF_576B_4941_946F_D8FBACB591D6_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Aug-2020 at 4:40:11a, UTC+7, Novosibirsk, Friday;
	This is SSH-Tunnel WFP driver service create data interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 7:42:16.893 pm, UTC+7, Novosibirsk, Wednesday;
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 6:43:30.762 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "sys.svc.crt.h"
#include "driver.dev.loc.h"

#include "shared.fs.gen.folder.h"

namespace shared { namespace driver { namespace client {

	using shared::service::CCrtData  ;
	using shared::service::CCrtOption;
	using namespace shared::ntfs;

	class CKmdfDrv_Crt : public CCrtData {
	                    typedef CCrtData TBase;
	public:
		 CKmdfDrv_Crt (void);
		~CKmdfDrv_Crt (void);

	public:
		 CKmdfDrv_Crt& operator << (const TDrvLocate&);
	};

	class CKmdfDrv_Crt_SSH : public CKmdfDrv_Crt {
	                        typedef CKmdfDrv_Crt TBase;
	public:
		 CKmdfDrv_Crt_SSH (void);
		~CKmdfDrv_Crt_SSH (void);

	public:
		operator LPCWSTR (void) const; // returns driver service name;
	};
}}}

typedef shared::driver::client::CKmdfDrv_Crt  TKmdfDrv_Crt;

#endif/*_DRIVERSVCCRT_H_852341CF_576B_4941_946F_D8FBACB591D6_INCLUDED*/