#ifndef _SFXIMAGELAY_H_7D8B44C2_49CA_408D_B26F_5F35D0F24EE3_INCLUDED
#define _SFXIMAGELAY_H_7D8B44C2_49CA_408D_B26F_5F35D0F24EE3_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Nov-2020 at 2:14:42p, UTC+7, Novosibirsk, Wednesday;
	This is Sfx picture control layout interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.base.lay.h"

namespace ST_Ctrls { class CPicture; namespace layout {

	using shared::sys_core::CError;

	using ex_ui::controls::CAlign;
	using ex_ui::controls::CSides;
	using ex_ui::controls::TSide ;
	using ex_ui::controls::CMargins;

	class CPicture_Layout {
	private:
		ST_Ctrls::CPicture&  m_control;
		mutable
		CError     m_error  ;
		CAlign     m_align  ;
		CMargins   m_margins;

	public:
		 CPicture_Layout (void);
		 CPicture_Layout (ST_Ctrls::CPicture& _ctrl);
		~CPicture_Layout (void);

	public:
		const
		CAlign&    Align  (void) const;
		CAlign&    Align  (void)      ;
		TErrorRef  Error  (void) const;
		const
		CMargins&  Margins(void) const;
		CMargins&  Margins(void)      ;
		HRESULT    Update (void)      ;           // recalculates position/rectangle of all elements;
		HRESULT    Update (const RECT& _rc_area); // updates picture control window position into an area provided;

	public:
		CPicture_Layout&  operator <<(const RECT& _rc_area);        // updates picture control window position in accordance with area  ;
		const RECT        operator = (const RECT& _rc_area) const;  // returns calculated rectangle of picture control window for area provided;
		CPicture_Layout&  operator <<(const CAlign&);

	private: // non-copyable;
		CPicture_Layout (const CPicture_Layout&);
		CPicture_Layout& operator = (const CPicture_Layout&);
	};
}}

typedef ST_Ctrls::layout::CPicture_Layout   TPictureLay;

#endif/*_SFXIMAGELAY_H_7D8B44C2_49CA_408D_B26F_5F35D0F24EE3_INCLUDED*/