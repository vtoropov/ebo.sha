/*
	Created by Tech_dog (ebontrop@gmail.com on 22-Jan-2021 at 8:48:59.203 am, UTC+7, Novosibirsk, Friday;
	This is Ebo Sha Optima Tool desktop app trace view color theme format interface implementation file;
*/
#include "StdAfx.h"
#include "ebo.sha.gui.fmt.trace.h"

using namespace ebo::sha::gui::format;

/////////////////////////////////////////////////////////////////////////////

CTrace_Images:: CTrace_Images (void) {}
CTrace_Images:: CTrace_Images (const CTrace_Images& _ref) : CTrace_Images() { *this = _ref; }
CTrace_Images::~CTrace_Images (void) { if (m_img_assoc.empty() == false) m_img_assoc.clear(); }

/////////////////////////////////////////////////////////////////////////////

HRESULT   CTrace_Images::Add (const CState::_e _ctrl_state, const WORD _img_res_id) { HRESULT hr_ = S_OK;
	_ctrl_state, _img_res_id;

	if (0 == _img_res_id)
		return (hr_ = E_INVALIDARG);

	try {
		m_img_assoc.insert(
			::std::make_pair(_ctrl_state, _img_res_id)
		);
	}
	catch (const ::std::bad_alloc&) { hr_ = E_OUTOFMEMORY; }

	return hr_;
}
const
TVisualStateAssoc& CTrace_Images::Get (void) const { return m_img_assoc; }

/////////////////////////////////////////////////////////////////////////////

CTrace_Images&  CTrace_Images::operator = (const CTrace_Images& _ref) { *this << _ref.Get(); return *this; }
CTrace_Images&  CTrace_Images::operator <<(const TVisualStateAssoc& _assoc) {
	try {
		this->m_img_assoc.insert(_assoc.begin(), _assoc.end()); } catch (const ::std::bad_alloc&){}
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CTrace_Button:: CTrace_Button (void) : m_cmd_id(0) {}
CTrace_Button:: CTrace_Button (const CTrace_Button& _ref) : CTrace_Button() { *this = _ref; }
CTrace_Button::~CTrace_Button (void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD    CTrace_Button::CmdID (void) const { return m_cmd_id; }
DWORD&   CTrace_Button::CmdID (void)       { return m_cmd_id; }
const
CTrace_Images&  CTrace_Button::Images (void) const { return m_img_assoc; }
CTrace_Images&  CTrace_Button::Images (void)       { return m_img_assoc; }

/////////////////////////////////////////////////////////////////////////////

CTrace_Button&  CTrace_Button::operator = (const CTrace_Button& _ref) { *this << _ref.CmdID() << _ref.Images(); return *this; }
CTrace_Button&  CTrace_Button::operator <<(const DWORD _cmd_id) { this->CmdID() = _cmd_id; return *this; }
CTrace_Button&  CTrace_Button::operator <<(const CTrace_Images& _images) { this->Images() = _images; return *this; }

/////////////////////////////////////////////////////////////////////////////

CTrace_Toolbar:: CTrace_Toolbar (void) {
	try {
	m_buttons.push_back(CTrace_Button());
	m_buttons.push_back(CTrace_Button());
	} catch (const ::std::bad_alloc&){}
}
CTrace_Toolbar:: CTrace_Toolbar (const CTrace_Toolbar& _ref) : CTrace_Toolbar() { *this = _ref; }
CTrace_Toolbar::~CTrace_Toolbar (void) {}

/////////////////////////////////////////////////////////////////////////////
const
TTraceButtons&   CTrace_Toolbar::Buttons (void) const { return m_buttons; }
TTraceButtons&   CTrace_Toolbar::Buttons (void)       { return m_buttons; }
const
CTrace_Button&   CTrace_Toolbar::Get (const UINT _ndx) const {
	_ndx;
	if (_ndx >= m_buttons.size()) { static CTrace_Button na_btn; return na_btn; }
	else return m_buttons.at(_ndx);
}
CTrace_Button&   CTrace_Toolbar::Get (const UINT _ndx){
	_ndx;
	if (_ndx >= m_buttons.size()) { static CTrace_Button na_btn; return na_btn; }
	else return m_buttons.at(_ndx);
}

/////////////////////////////////////////////////////////////////////////////

CTrace_Toolbar&  CTrace_Toolbar::operator = (const CTrace_Toolbar& _ref) { *this << _ref.Buttons(); return *this; }
CTrace_Toolbar&  CTrace_Toolbar::operator <<(const TTraceButtons& _buttons) {
	// https://stackoverflow.com/questions/2551775/appending-a-vector-to-a-vector
	try {
		this->m_buttons.insert(this->m_buttons.end(), _buttons.begin(), _buttons.end());
	} catch (const ::std::bad_alloc&) {}

	return *this;
}

/////////////////////////////////////////////////////////////////////////////

TTraceOutFmt:: CTrace (void) {}
TTraceOutFmt:: CTrace (const TTraceOutFmt& _ref) : TTraceOutFmt() { *this = _ref; }
TTraceOutFmt::~CTrace (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   TTraceOutFmt::ApplyTo (TTrace& _trace) const { HRESULT hr_ = S_OK;
	_trace;

	CCmpClrs_Enum clrs_;
	const CCmpClrs& pair_ = clrs_[0]; pair_;
	const COLORREF clr_bkg_solid = shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_back);
	const COLORREF clr_dark = RGB( 40, 40, 40);
	const COLORREF clr_fore = RGB(230,230,230);

	_trace.Format().BackColour(clr_dark);
	_trace.Format().Font().Colour(TEntryType::e_dont_care , clr_fore);
	_trace.Format().Font().Colour(TEntryType::e_info , clr_fore);
	_trace.Format().Font().Colour(TEntryType::e_error, RGB(237, 28, 36));
	_trace.Format().Font().Colour(TEntryType::e_warn , RGB(240,240,  0)); // pair_.Light();
	_trace.Format().HorzAlign    (THorzAlign::eLeft );
	_trace.Format().Font().Family(/*_T("Comic Sans MS")*/_T("Consolas"));
	_trace.Format().Font().Size  (20/*TConsoleFont().Size()*/);
	_trace.Format().Font().Options() += TFontOpts::eExactSize;

	CBorders& borders = _trace.Format().Borders(); borders.Thickness(1);

	(CColour&)borders.Left().Color() << pair_.Light(); borders.Left().Color() -= 90;
	(CColour&)borders.Top().Color() << pair_.Light(); borders.Top().Color().Alpha(TAlpha::eTransparent80);
	(CColour&)borders.Right().Color() << pair_.Light(); borders.Right().Color() -= 90;
	(CColour&)borders.Bottom().Color() << pair_.Light(); borders.Bottom().Color() -= 90;

	_trace.Layout().LineGap() = 3;
	_trace.Indents().Left().Size() = 32;

	(CColour&)_trace.Indents().Left().Colors().Back() << clr_dark;
	_trace.Format().Borders().ExcludeIndents() = false;

	THeader& header = _trace.Header();

	header.Format().BkgColour (clr_bkg_solid);
	header.Format().ForeColour(pair_.Light());
	header.Format().HorzAlign (THorzAlign::eLeft);
	header.Format().Family(_T("Verdana"));
	header.Format().FontSize(28);

	header.Format().Margins().Left() = 30;

	header.Borders().Top() << (DWORD)1;
	header.Borders().Left() << (DWORD)1;
	header.Borders().Right() << (DWORD)1;

	(CColour&)header.Borders().Top().Color() << pair_.Light(); header.Borders().Top().Color().Alpha(TAlpha::eTransparent80);
	(CColour&)header.Borders().Bottom().Color() << pair_.Light(); header.Borders().Bottom().Color() -= 90;

	(CColour&)header.Borders().Left().Color() << pair_.Light(); header.Borders().Left().Color() -= 90;
	(CColour&)header.Borders().Right().Color() << pair_.Light(); header.Borders().Right().Color() -= 90;

	const TVisualStateAssoc& img_ass_btn_dn = this->Toolbar().Get(0).Images().Get();
	const TVisualStateAssoc& img_ass_btn_up = this->Toolbar().Get(1).Images().Get();

	CScrolls& scrolls_ = _trace.Scrolls();

	scrolls_.Down().Runtime ().CtrlId(1);
	for (TVisualStateAssoc::const_iterator it_ = img_ass_btn_dn.begin(); it_ != img_ass_btn_dn.end(); ++it_) {
		scrolls_.Down().Renderer().Images().Add(it_->first, it_->second);
	}
	scrolls_.Down().Runtime ().Format().Bkgnd().Solid() << clr_bkg_solid;//pair_.Dark ();
	scrolls_.Down().Enabled (true);
	scrolls_.Down().Tips(_T("Scroll messages down."));

	scrolls_.Up  ().Runtime ().CtrlId(2);
	for (TVisualStateAssoc::const_iterator it_ = img_ass_btn_up.begin(); it_ != img_ass_btn_up.end(); ++it_) {
		scrolls_.Up().Renderer().Images().Add(it_->first, it_->second);
	}
	scrolls_.Up  ().Runtime ().Format().Bkgnd().Solid() << clr_bkg_solid;//pair_.Dark ();
	scrolls_.Up  ().Enabled (true);
	scrolls_.Up  ().Tips(_T("Scroll messages up."));

	(CColour&)_trace.Indents().Left().Borders().Right().Color() << pair_.Light();
	_trace.Indents().Left().Borders().Right().Color().Alpha(TAlpha::eTransparent80);
	_trace.Indents().Left().Borders().Right() << (DWORD) 1;

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////
const
CTrace_Toolbar&  TTraceOutFmt::Toolbar(void) const { return m_toolbar; }
CTrace_Toolbar&  TTraceOutFmt::Toolbar(void)       { return m_toolbar; }

/////////////////////////////////////////////////////////////////////////////
const
TTraceOutFmt&  TTraceOutFmt::operator >>(TTrace& _trace) const { this->ApplyTo(_trace); return *this; }
TTraceOutFmt&  TTraceOutFmt::operator = (const TTraceOutFmt& _ref) { _ref; return *this; }

/////////////////////////////////////////////////////////////////////////////