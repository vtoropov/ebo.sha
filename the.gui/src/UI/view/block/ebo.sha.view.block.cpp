/*
	Created by Tech_dog (ebontrop@gmail.com) on 26-Dec-2020 at 5:54:19.493 am, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha optima tool TCP/IP/Process blocker view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.view.block.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CBlock_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_sel  = IDC_EBO_SHA_BLK_SEL,
		};
	}; typedef CBlock_Ctl This_Ctl;

	class CBlock_Cmd {
	public:
		enum _cmd : DWORD {
			e_cmd_sel_http     = 0x01,
			e_cmd_sel_proc     = 0x02,
		};
	}; typedef CBlock_Cmd This_Cmd;

	class CBlock_Res {
	public:
		enum _res : WORD {
			e_res_sel_overlay  = IDR_EBO_SHA_BLK_SEL_OVR_D,
			e_res_sel_img_lst  = IDR_EBO_SHA_SEL_BLK_LST  ,
			e_res_sel_itm_1_a  = IDR_EBO_SHA_SEL_BLK_01_a ,
			e_res_sel_itm_1_n  = IDR_EBO_SHA_SEL_BLK_01_n ,
			e_res_sel_itm_2_a  = IDR_EBO_SHA_SEL_BLK_02_a ,
			e_res_sel_itm_2_n  = IDR_EBO_SHA_SEL_BLK_02_n ,
		};
	}; typedef CBlock_Res This_Res;

	class CBlock_Layout {
	private:
		CBlock&  m_view;
		RECT     m_rect;

	public:
		 CBlock_Layout (CBlock& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CBlock_Layout (void) {}

	public:
		RECT   GetSelectRect (void) const {
			RECT rc_sel = m_rect; rc_sel.right = rc_sel.left + m_view.Select().Layout().Width();
			return rc_sel;
		}

		RECT   GetSubViewRect(void) const {
			RECT rc_sub = m_rect; rc_sub.left = this->GetSelectRect().right + 0x0;
			::InflateRect(&rc_sub, -0x0, -0x0);
			return rc_sub;
		}

		VOID   Update (void) {

			const RECT rc_sub_view = this->GetSubViewRect();

			TSelect& sel_ctrl = m_view.Select(); sel_ctrl.Layout() << m_rect;
		//	TiPodView& ipod  = m_view.iPod(); ipod.Update (rc_sub_view);
		}
	};

	class CBlock_Init {
	private:
		CBlock&  m_view ;
		CError   m_error;

	public:
		 CBlock_Init (CBlock& _view) : m_view(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }
		~CBlock_Init (void) {}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;
#pragma region __sel_create
			TSelect& sel_ctrl = m_view.Select();
			sel_ctrl.Format() = shared::Get_Format().Select();
			sel_ctrl.Format().Overlay().Image() = This_Res::e_res_sel_overlay;

			shared::Get_Layout().Select() >> sel_ctrl.Layout();
			shared::Get_Layout().Select() >> sel_ctrl.Borders();

			sel_ctrl.Format().Images() = This_Res::e_res_sel_img_lst;

			RECT rc_ = CBlock_Layout(m_view).GetSelectRect();
			HRESULT hr_ = sel_ctrl.Create(m_view.Window(), rc_, _T("ebo_pack::controls::selector"), This_Ctl::e_ctl_sel);

			TSelItems& items = sel_ctrl.Items();

			TSelItem itm_0;
			itm_0.Id() = This_Cmd::e_cmd_sel_http;
			itm_0.Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_sel_itm_1_n);
			itm_0.Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_sel_itm_1_a);
			itm_0.Image () = 0 ;
			items.Append(itm_0);

			TSelItem itm_1;
			itm_1.Id() = This_Cmd::e_cmd_sel_proc;
			itm_1.Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_sel_itm_2_n);
			itm_1.Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_sel_itm_2_a);
			itm_1.Image () = 1 ;
			items.Append(itm_1);

			items.Selected(0);

			if (FAILED(hr_)) {
				m_error = sel_ctrl.Error();
				m_error.Show();
			}
			else {
				sel_ctrl.Window().SetWindowPos(HWND_TOP, &rc_, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE);
				sel_ctrl.Layout().Update(rc_);
			}
#pragma endregion
			return m_error;
		}
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CBlock::CViewWnd:: CViewWnd(CBlock& _view) : TViewWnd(_view), m_view(_view), m_select(_view) {}
CBlock::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CBlock::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	const RECT rc_ = Lay_(*this); rc_; HRESULT hr_ = S_OK; hr_; TViewWnd::SetWindowTextW(_T("HTTP & Process Blocker"));
	CBlock_Init init_(m_view); init_.OnCreate();

	const RECT rc_view = CBlock_Layout(m_view).GetSubViewRect();

	hr_ = m_http.Create(*this, rc_view, true);
	if (SUCCEEDED(hr_)) {
		m_http.Window().SetWindowTextW(_T("block::http::subview"));
	}

	hr_ = m_proc.Create(*this, rc_view, false);
	if (SUCCEEDED(hr_)) {
		m_proc.Window().SetWindowTextW(_T("block::proc::subview"));
	}

	return 0;
}

LRESULT   CBlock::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_b_hand = FALSE;
	m_http.Destroy();
	m_proc.Destroy();
	m_select.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CBlock::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {

	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};
	CBlock_Layout lay_(m_view);

	m_select.Layout() << lay_.GetSelectRect();
	m_select.Refresh();

	m_http.Update(lay_.GetSubViewRect());
	m_proc.Update(lay_.GetSubViewRect());

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CBlock:: CBlock (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CBlock::~CBlock (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CBlock::ISelector_OnItemClick  (const UINT _u_itm_id) { HRESULT hr_ = S_OK;
	_u_itm_id;
	switch (_u_itm_id) {
	case This_Cmd::e_cmd_sel_http: {
		if (m_wnd.m_http.IsValid()) hr_ = m_wnd.m_http.IsVisible(This_Cmd::e_cmd_sel_http == _u_itm_id);
		if (m_wnd.m_proc.IsValid()) hr_ = m_wnd.m_proc.IsVisible(This_Cmd::e_cmd_sel_proc == _u_itm_id);
	} break;
	case This_Cmd::e_cmd_sel_proc: {
		if (m_wnd.m_proc.IsValid()) hr_ = m_wnd.m_proc.IsVisible(This_Cmd::e_cmd_sel_proc == _u_itm_id);
		if (m_wnd.m_http.IsValid()) hr_ = m_wnd.m_http.IsVisible(This_Cmd::e_cmd_sel_http == _u_itm_id);
	} break;
	}

	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

THttpView& CBlock::Http    (void) { return m_wnd.m_http; }
TProcView& CBlock::Proc    (void) { return m_wnd.m_proc; }
TSelect&   CBlock::Select  (void) { return m_wnd.m_select; }