#ifndef _DRVSETUPREGISTRY_H_742C523E_67BD_480B_B8B4_63C5A05638CF_INCLUDED
#define _DRVSETUPREGISTRY_H_742C523E_67BD_480B_B8B4_63C5A05638CF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Sep-2020 at 5:05:40a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack generic driver setup registry property interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 12-Jan-2021 at 7:20:39.534 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "drv.setup.defs.h"
#include "drv.setup.info.h"

namespace shared { namespace driver { namespace setup { namespace registry {

	using shared::sys_core::CError;
	// this class includes types that is important from setup point of view only; can be extended if necessary;
	class CPropTypes {
	public:
		enum _type : DWORD { // zero cannot be used due to description identifier has 0-value;
			/*e_unspecified = 0, */e_desc = SPDRP_DEVICEDESC, e_friend = SPDRP_FRIENDLYNAME,
		};
	};

	using shared::driver::setup::CDevInfo;
	using shared::driver::setup::CDevInfoSet;
	// https://docs.microsoft.com/en-us/windows/win32/api/setupapi/nf-setupapi-setupdigetdeviceregistrypropertyw
	// registry key value data type is defined automatically in dependence on property type provided;
	class CProperty {
	protected:
		CError   m_error;
	public:
		 CProperty (void);
		 CProperty (const CProperty&);
		~CProperty (void);

	public:
		TErrorRef   Error (void) const;
		CStringW    Get   (const CDevInfoSet&, const CDevInfo&, const CPropTypes::_type);

	public:
		CProperty&  operator = (const CProperty&);
	};

}}}}

typedef shared::driver::setup::registry::CPropTypes::_type TRegPropType; // need to be changed later (possibly, not sure);

#endif/*_DRVSETUPREGISTRY_H_742C523E_67BD_480B_B8B4_63C5A05638CF_INCLUDED*/