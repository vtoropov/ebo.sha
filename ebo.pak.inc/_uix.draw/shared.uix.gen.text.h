#ifndef __SFXSOACLIENTSERVICEUIDRAWING_H_FF3BB206_8691_4a10_9FEC_65AC71A76E0D_INCLUDED
#define __SFXSOACLIENTSERVICEUIDRAWING_H_FF3BB206_8691_4a10_9FEC_65AC71A76E0D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Nov-2007 at 9:57:00pm, GMT+3, Rostov-on-Don, Sunday;
	This is Sfx Pack owner-drawn related classe(s) declaration file.
	----------------------------------------------------------------------
	Adopted to Ebo Pack project on 3-May-2020 at 00:33:05.200 am, UTC+7, Novosibirsk, Sunday;
*/
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw {

	class CText_Base {
	protected:
		HDC      m_hdc;               // device context which this text works for;
		HFONT    m_fnt;               // font handle which this text is drawn by ;

	protected:
		 CText_Base (void);
		~CText_Base (void);

	public:
		bool     Is (const bool _b_re_check = false) const;

	public:
		CText_Base&   operator <<(const HFONT)  ;  // sets a font for making text size calculation;
		CText_Base&   operator <<(const HDC  )  ;  // sets a context device for getting text size ;
	//	CText_Base&   operator <<(const HWND )  ;  // sets a font and a device of given window; not applicable: device must be released;

	public:
		CText_Base&   operator = (const CText_Base&);

	public:
		static bool   Is (const HDC  );
		static bool   Is (const HFONT);
	};

	class CText : public CText_Base {
	             typedef CText_Base TBase;
	public:
		 CText(void);
	explicit CText(const CText&);
	explicit CText(const HDC hDC, const HFONT hFont);
		~CText(void);
	public:
		SIZE     Size (LPCWSTR lpText) const; // gets a size of text provided;
		SIZE     Size (LPCWSTR lpText, RECT& _rc_out) const;
	public:
		const
		SIZE     operator = (LPCWSTR) const;  // gets a size of text provided;
		CText&   operator = (const CText&) ;
	};

	class CTextLine : public CText {
	                 typedef CText TText;
	private:
		SIZE       m_size; // dependable on font and device context;
		CStringW   m_text; // copy of original text;

	public:
		 CTextLine (void);
		 CTextLine (const CTextLine&);
		~CTextLine (void);

	public:
		const
		SIZE&      Size (void) const;
		LPCWSTR    Text (void) const;
		HRESULT    Text (LPCWSTR)   ;
		HRESULT    Text (LPCWSTR, RECT& _rc_out) ;

	public:
		const
		SIZE&      operator = (LPCWSTR);
		CTextLine& operator = (const CTextLine&) ;
	};

	typedef ::std::vector<CTextLine>   TTextLines;

	class CTextBlock : private CText_Base {
	                   typedef CText_Base TBase;
	protected:
		TTextLines    m_lines;
		RECT          m_rc_out;
		CStringW      m_ln_sep;  // line separator; by default is '\n'; but can be assined to defferent value(s);

	public:
		 CTextBlock (void);
		 CTextBlock (const CTextBlock&);
		~CTextBlock (void);

	public:
		size_t        Count (void) const;
		const
		TTextLines&   Lines (void) const;
		LPCWSTR       Separator (void) const;
		HRESULT       Separator (LPCWSTR) ; // may be set as an array of symbols, but not for this version though;

	public:
		const
		CTextLine&    Get   (const size_t _ndx) const;  // gets a line by index; if line is not exist, a reference to fake line is returned;
		CTextLine&    Get   (const size_t _ndx)      ;  // gets a line by index; if line is not exist, a reference to fake line is returned;
		const bool    Is    (void) const;

	public:
		const
		SIZE          operator = (LPCWSTR)      ;       // gets a size of a text provided;
		CTextBlock&   operator <<(const RECT&)  ;       // sets an outer/boundary rectangle;
		CTextBlock&   operator = (const CTextBlock&);
		CTextBlock&   operator <<(const HFONT)  ;
		CTextBlock&   operator <<(const HDC  )  ;
	};
}}

#endif/*__SFXSOACLIENTSERVICEUIDRAWING_H_FF3BB206_8691_4a10_9FEC_65AC71A76E0D_INCLUDED*/