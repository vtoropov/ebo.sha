/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Aug-2020 at 11:18:00a, UTC+7, Novosibirsk, Monday;
	This is SSH Tunnel WFP filter device callouts interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 8:09:37.361 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "filter.callouts.h"

using namespace ssh::driver;

///////////////////////////////////////////////////////////////////////////////

namespace ssh { namespace driver { namespace _impl {
#if (0)
	const GUID& Callout_get_type (const ULONG _type_id) {

		static bool b_1st_time = true;
		static GUID m_ids[CCallout_Types::n_count] = {0};

		if (b_1st_time) {
			b_1st_time = false;

			for (DWORD i_ = 0; i_ < CCallout_Types::n_count; i_++) {
				if (0 == i_) m_ids[i_] = GUID_NULL;
			}
		}

		switch (_type_id) {
		case CCallout_Types::CFilter::v4::accept : {} break;
		case CCallout_Types::CFilter::v6::accept : {} break;
		case CCallout_Types::CFilter::v4::connect: {} break;
		case CCallout_Types::CFilter::v6::connect: {} break;
		case CCallout_Types::COut::v4::accept    : {} break;
		case CCallout_Types::COut::v6::accept    : {} break;
		case CCallout_Types::COut::v4::connect   : {} break;
		case CCallout_Types::COut::v6::connect   : {} break;
		case CCallout_Types::CMonitor::sub_layer : {} break;
		default: ;
		}


		return m_ids[0];
	}
#endif
}}}
using namespace ssh::driver::_impl;
///////////////////////////////////////////////////////////////////////////////

NTSTATUS CCallouts::Deaf(FWPS_CALLOUT_NOTIFY_TYPE, const GUID*, const FWPS_FILTER0*) {
	return STATUS_SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////

NTSTATUS   CCallouts::Register  (PDEVICE_OBJECT _p_device) {

	NTSTATUS nt_status = S_OK;

	if ( NULL ==  g_dev_ext) return (nt_status = STATUS_INVALID_PARAMETER);
	if ( NULL == _p_device ) return (nt_status = STATUS_INVALID_PARAMETER);

	FWPS_CALLOUT0 cout = {0};
	cout.notifyFn   = (FWPS_CALLOUT_NOTIFY_FN0)CCallouts::Deaf;

	cout.calloutKey = WFP_ACCEPT_CALLOUT_V4;
	cout.classifyFn = (FWPS_CALLOUT_CLASSIFY_FN0)ssh::driver::classify::CAction_Accept::v4::Do;

	nt_status = FwpsCalloutRegister0(_p_device, &cout, &g_dev_ext->accept4);
	if(FAILED( nt_status )) {
		DbgPrint( "__ssh.tnl.drv: [#ERR] CCallouts::Register()::accept4: code=%X;", nt_status);
		return nt_status ;
	}

	cout.calloutKey = WFP_ACCEPT_CALLOUT_V6;
	cout.classifyFn = (FWPS_CALLOUT_CLASSIFY_FN0)ssh::driver::classify::CAction_Accept::v6::Do;

	nt_status = FwpsCalloutRegister0(_p_device, &cout, &g_dev_ext->accept6);
	if(FAILED( nt_status )) {
		DbgPrint( "__ssh.tnl.drv: [#ERR] CCallouts::Register()::accept6: code=%X;", nt_status);
		return nt_status ;
	}

	cout.calloutKey = WFP_CONNECT_CALLOUT_V4;
	cout.classifyFn = (FWPS_CALLOUT_CLASSIFY_FN0)ssh::driver::classify::CAction_Connect::v4::Do;

	nt_status = FwpsCalloutRegister0(_p_device, &cout, &g_dev_ext->connect4);
	if(FAILED( nt_status )) {
		DbgPrint( "__ssh.tnl.drv: [#ERR] CCallouts::Register()::connect4: code=%X;", nt_status);
		return nt_status ;
	}

	cout.calloutKey = WFP_CONNECT_CALLOUT_V6;
	cout.classifyFn = (FWPS_CALLOUT_CLASSIFY_FN0)ssh::driver::classify::CAction_Connect::v6::Do;

	nt_status = FwpsCalloutRegister0(_p_device, &cout, &g_dev_ext->connect6);
	if(FAILED( nt_status )) {
		DbgPrint( "__ssh.tnl.drv: [#ERR] CCallouts::Register()::connect6: code=%X;", nt_status);
		return nt_status ;
	}

	return nt_status;
}

NTSTATUS   CCallouts::Unregister( void ) {

	NTSTATUS nt_status = S_OK;

	if ( NULL == g_dev_ext )
		return (nt_status = STATUS_INVALID_PARAMETER);

	if (g_dev_ext->accept4) { nt_status = FwpsCalloutUnregisterById0(g_dev_ext->accept4); }
	if (g_dev_ext->accept6) { nt_status = FwpsCalloutUnregisterById0(g_dev_ext->accept6); }

	if (g_dev_ext->connect4) { nt_status = FwpsCalloutUnregisterById0(g_dev_ext->connect4); }
	if (g_dev_ext->connect6) { nt_status = FwpsCalloutUnregisterById0(g_dev_ext->connect6); }

	return nt_status;
}