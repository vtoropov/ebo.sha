#ifndef _EBOSHAGUIFMTEDIT_H_DD913C40_D7C6_446F_BB56_6BFD32F7DD7F_INCLUDED
#define _EBOSHAGUIFMTEDIT_H_DD913C40_D7C6_446F_BB56_6BFD32F7DD7F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Dec-2020 at 9:10:55.010 pm, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool common edit control format interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack UM test project on 14-Jan-2021 at 12:03:27.508 pm, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.uix.ctrl.border.h"
#include "shared.uix.gen.clrs.h"
#include "shared.uix.ctrl.defs.h"
#include "sfx.label.fmt.h"

namespace ebo { namespace sha { namespace gui { namespace format {

	using ex_ui::controls::CState;

	class CEdit {
	private:
		CState      m_state  ;
		TBorders    m_borders;
		TLabelFmt   m_box_ra ; // read-only edit box replacement;

	public:
		 CEdit (void);
		 CEdit (const CEdit&);
		~CEdit (void);

	public:
		const
		TBorders&   Borders (void) const;
		TBorders&   Borders (void)      ;
		const
		TLabelFmt&  Box (void) const;
		TLabelFmt&  Box (void)      ;
		const
		CState&     State (void) const;
		CState&     State (void)      ;

	public:
		CEdit&  operator = (const CEdit&);
		CEdit&  operator <<(const TBorders&);
		CEdit&  operator <<(const TLabelFmt&);

	public:
		const CEdit&  operator >>(TBorders&) const;
		const CEdit&  operator >>(TLabelFmt&) const;
	};

}}}}

typedef ebo::sha::gui::format::CEdit  TEditFmt;

#endif/*_EBOSHAGUIFMTEDIT_H_DD913C40_D7C6_446F_BB56_6BFD32F7DD7F_INCLUDED*/