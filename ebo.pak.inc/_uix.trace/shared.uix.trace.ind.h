#ifndef _SHAREDUIXTRACEIND_H_63A027E6_CC26_4A00_832E_C3DB53912D0B_INCLUDED
#define _SHAREDUIXTRACEIND_H_63A027E6_CC26_4A00_832E_C3DB53912D0B_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 28-Apr-2020 at 11:22:43p, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Pack UIX trace view indentation interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.border.h"
#include "shared.uix.gdi.object.h"

namespace ex_ui { namespace trace {

	using shared::sys_core::CError ;
	using ex_ui::controls::CMargins;
	using ex_ui::controls::TSide   ;
	using ex_ui::controls::CBorders; typedef const CBorders& TBorders;

	using ex_ui::draw::CColour;
	using ex_ui::draw::CColour_Ex; typedef const CColour_Ex& TColorRef;

	class CIndent_Clrs {
	protected:
		CColour_Ex  m_back;
		CColour_Ex  m_fore;

	public:
		 CIndent_Clrs (void);
		~CIndent_Clrs (void);

	public:
		TColorRef   Back   (void) const;
		CColour_Ex& Back   (void)      ;
		TColorRef   Fore   (void) const;
		CColour_Ex& Fore   (void)      ;
		const bool  Is     (void) const;
	};

	typedef CIndent_Clrs TColours;

	class CIndents;
	class CIndent {
		friend class CIndents;
	protected:
		LONG        m_size; // height or width of indent that depands of intend side;
		TColours    m_clrs;
		TSide       m_side;
		CBorders    m_borders;
	public:
		 CIndent(void);
		~CIndent(void);

	public:
		const
		CBorders&   Borders(void) const;
		CBorders&   Borders(void)      ;
		const
		TColours&   Colors (void) const;
		TColours&   Colors (void)      ;
		const bool  Is     (void) const;
		const TSide Side   (void) const;
		const
		LONG&       Size   (void) const;
		LONG&       Size   (void)      ;
	};

	typedef const CIndent& TIndentRef;

	class CIndents {
	protected:
		CIndent     m_indents[TSide::e_bottom + 1];

	public:
		 CIndents (void);
		~CIndents (void);

	public:
		TIndentRef  SideOf (const TSide) const;
		CIndent&    SideOf (const TSide)      ;
	public:
		TIndentRef  Bottom (void) const;
		CIndent&    Bottom (void)      ;
		TIndentRef  Left   (void) const;
		CIndent&    Left   (void)      ;
		TIndentRef  Right  (void) const;
		CIndent&    Right  (void)      ;
		TIndentRef  Top    (void) const;
		CIndent&    Top    (void)      ;

	public:
		TIndentRef operator << (const TSide) const;
		CIndent&   operator << (const TSide)      ;
	};

}}

typedef const ex_ui::trace::CIndents&   TIndentsRef;

#endif/*_SHAREDUIXTRACEIND_H_63A027E6_CC26_4A00_832E_C3DB53912D0B_INCLUDED*/