#ifndef _SHAREDUIXCTRLSELECTORITEMS_H_BC95061E_E924_4B33_9111_129F400D28A3_INCLUDED
#define _SHAREDUIXCTRLSELECTORITEMS_H_BC95061E_E924_4B33_9111_129F400D28A3_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 17-Apr-2020 at 9:20:08p, UTC+7, Novosibirsk, Friday;
	This is shared UIX library task/view selector control group/item interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to shared Sfx library on 27-Aug-2020 at 0:06:50a, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.base.lay.h"
#include "shared.uix.ctrl.base.rnd.h"

namespace ST_Ctrls { class CSelector; namespace select {

	using shared::sys_core::CError;
	using ex_ui::controls::CState ;
	using ex_ui::controls::layout::CLayersEx;

	class CItemType {
	public:
		enum _type  {
			e_cmd = 0x0,    // command item type, default;
			e_url = 0x1,    // web link;
			e_cap = 0x2,    // caption/group ; can act as collapsing/expanding element; not produce event outside;
		};
	}; typedef CItemType::_type TItemType;

	class CItem {
	protected:
		DWORD      m_id    ;
		CLayersEx  m_layers;
		CState     m_state ;
		TItemType  m_type  ;
		INT        m_image ; // image index of imagelist control; -1 - is no image; this feature is deprecated; but still in use;

	public:
		 CItem (void);
		 CItem (const CItem&);
		 CItem (const UINT _id, LPCWSTR _lp_sz_text);
		~CItem (void);

	public:
		DWORD      Id    (void) const;
		DWORD&     Id    (void)      ;  // item collection does not control changing identifier this way;
		INT        Image (void) const;
		INT&       Image (void)      ;
		const
		CLayersEx& Layers(void) const;
		CLayersEx& Layers(void)      ;
		const
		CState&    State (void) const;
		CState&    State (void)      ;
		TItemType  Type  (void) const;
		TItemType& Type  (void)      ;

	public:
		CItem&  operator = (const CItem&);
		CItem&  operator <<(const DWORD _id);
		CItem&  operator <<(const CState&);
		CItem&  operator <<(const CStringW& _cs_text);
		CItem&  operator <<(LPCWSTR _lp_sz_text);
	};

	typedef ::std::vector <CItem>  TItems; // selector control supposes unsorted item sequence;

	class CItems {
	public:
		enum _na : INT { e_na = -1 };
	protected:
		CSelector& m_control ;
		TItems     m_items   ;
		CError     m_error   ;

	public:
		 CItems (CSelector&);
		~CItems (void);

	public:
		HRESULT    Append  (const CItem&);
		HRESULT    Append  (const UINT& _id, LPCWSTR _lp_sz_text);
		size_t     Count   (void) const;
		TErrorRef  Error   (void) const;
		const INT  Has     (const POINT& pt) const;   // returns index of item which the point belongs to rectangle, otherwise -1;
		const
		CItem&     Item    (const INT _ndx ) const;
		CItem&     Item    (const INT _ndx )      ;
		const
		TItems&    Raw     (void) const;
		TItems&    Raw     (void)      ;
		const INT  Selected(void) const;
		HRESULT    Selected(const INT _ndx);

	public:
		operator const TItems& (void) const;
	public:
		CItems& operator += (const CItem&);
		CItems& operator << (const UINT u_cmd);  // selects item by command identifier provided;
	};
}}

typedef ST_Ctrls::select::CItem  TSelItem ;
typedef ST_Ctrls::select::CItems TSelItems;

#endif/*_SHAREDUIXCTRLSELECTORITEMS_H_BC95061E_E924_4B33_9111_129F400D28A3_INCLUDED*/