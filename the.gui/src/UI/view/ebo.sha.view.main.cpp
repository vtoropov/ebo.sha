/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Oct-2020 at 6:20:07p, UTC+7, Novosibirsk, Thursday;
	This is Ebo Sha optima tool main view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.view.main.h"

using namespace ebo::sha::gui::view;

#include "shared.uix.gdi.draw.defs.h"
#include "shared.uix.gdi.renderer.h"
using namespace ex_ui::draw;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

CMain::CMainWnd:: CMainWnd(CMain& _view) : TViewWnd(_view), m_view(_view), m_browser(_view) {}
CMain::CMainWnd::~CMainWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CMain::CMainWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	const RECT rc_ = Lay_(*this); rc_; TViewWnd::SetWindowTextW(_T("Sweet Home"));

	HRESULT hr_ = m_browser.Create(*this, rc_, true);
	if (SUCCEEDED(hr_)) {
		CStringW cs_url(_T("res://%s/view_main_ctx.html"));
		CUrlLocator u_loc_(ex_ui::web::CUrlLocateType::eHostExecutable);

		hr_ = u_loc_.URL((LPCTSTR)cs_url, cs_url);
		if (FAILED(hr_))
			u_loc_.Error().Show();
		else {
			m_browser.Open(_T(""));
			m_browser.SetInitialized();
			m_browser.Open(cs_url.GetString());
		}
	}

	return 0;
}

LRESULT   CMain::CMainWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_b_hand = FALSE;
	m_browser.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CMain::CMainWnd::OnErase   (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = TRUE;

	static const LRESULT l_is_drawn = 1;
	{
		RECT rc_view = Lay_(*this);

		CZBuffer buf_((HDC)_w_prm, rc_view);
		buf_.FillSolidRect(&rc_view, shared::Get_Format().Panel().Bkgnd().Solid());
	}
	return l_is_drawn;
}

LRESULT   CMain::CMainWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {

	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	m_browser.GetAxWindowRef().SetWindowPos(HWND_TOP, &rc_, SWP_NOACTIVATE);

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}
/////////////////////////////////////////////////////////////////////////////

CMain:: CMain (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CMain::~CMain (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CMain::Refresh (void) {
	TView::m_error << __MODULE__ << S_OK;
	return TView::Refresh();
}
/////////////////////////////////////////////////////////////////////////////

HRESULT   CMain::BrowserEvent_BeforeNavigate  (LPCTSTR lpszUrl) {
	lpszUrl;
	static LPCWSTR lp_sz_pat = _T("inter::");

	CStringW cs_url(lpszUrl);
	const INT n_pos = cs_url.Find(lp_sz_pat);

	if (-1 != n_pos) {
		LPCWSTR lp_sz_cmd = cs_url.GetString() + ::wcslen(lp_sz_pat);
		const INT cmd_id = ::_wtoi(lp_sz_cmd);

		shared::Get_AppUI().IAppUI_ExecCommand((DWORD)cmd_id);

		return S_FALSE;
	}
	return S_OK;
}
HRESULT   CMain::BrowserEvent_DocumentComplete(LPCTSTR lpszUrl, const bool bStreamObject) {
	lpszUrl; bStreamObject;
	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////