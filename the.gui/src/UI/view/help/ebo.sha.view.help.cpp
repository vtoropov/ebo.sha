/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Jan-2021 at 10:36:16.733 am, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Sha Optima Tool application help view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.view.help.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CHelp_Layout {
	private:
		CHelp&  m_view;
		RECT    m_rect;

	public:
		 CHelp_Layout (CHelp& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CHelp_Layout (void) {}

	public:
		RECT   GetEmpty(void) { RECT rc_empty = m_rect; ::InflateRect(&rc_empty, -0x5, -0x5); rc_empty.top += 30; return rc_empty; }
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

THelp::CViewWnd:: CViewWnd(THelp& _view) : TViewWnd(_view), m_view(_view) {}
THelp::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   THelp::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	CHelp_Layout layout_(m_view); TViewWnd::SetWindowTextW(_T("Probably Useful Help"));

	HRESULT hr_ = m_empty.Create(*this, layout_.GetEmpty(), true); hr_;
	m_empty.DrawBorder(true);

	return 0;
}

LRESULT   THelp::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_empty.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   THelp::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CHelp_Layout layout_(m_view);
	m_empty.Update(layout_.GetEmpty());

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

THelp:: CHelp (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
THelp::~CHelp (void) {}

/////////////////////////////////////////////////////////////////////////////