/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Dec-2020 at 3:23:09.242 pm, UTC+7, Novosibirsk, Friday;
	This is Ebo Sha optima tool media iPod connected device sub-view interface implementation file;
*/
#include "StdAfx.h"
#include "ebo.sha.media.conn.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CiPod_Dev_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_cap_0  = 0, // not defined yet;
		};
	}; typedef CiPod_Dev_Ctl This_Ctl;

	class CiPod_Dev_Res {
	public:
		enum _res : WORD {
			e_none     = 0,
			e_logo_dev = IDR_EBO_SHA_MED_POD_DEV ,
		};
	}; typedef CiPod_Dev_Res This_Res;

	class CiPod_Dev_Layout {
	public:
		typedef TCtrlDefLayout _pos;
	private:
		CiPod_Device& m_view;
		RECT          m_rect;

	public:
		 CiPod_Dev_Layout (CiPod_Device& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CiPod_Dev_Layout (void) {}

	public:
		RECT   GetEmpty (const DWORD _ndx) { _ndx; RECT rc_empty = m_rect; rc_empty.top = GetImage(0).bottom; return rc_empty; }
		RECT   GetImage (const DWORD _ndx) {
			static const SIZE sz_logo = { 80, 80 };
			RECT rc_logo = {
				0, (0 == _ndx ? 0 : _pos::e_avt_shift), 0 + sz_logo.cx, (0 == _ndx ? sz_logo.cy : _pos::e_avt_shift + sz_logo.cy)
			};
			return rc_logo;
		}
		RECT   GetTitle (const DWORD _ndx) { return this->_get_cap_rect(_ndx); }

	public:
		VOID   Update (TCapComp* _p_caps, const DWORD _d_count) {
			if (NULL == _p_caps || 0 == _d_count)
				return;
			for (DWORD i_ = 0; i_ < _d_count; i_++)
				_p_caps[i_].Title().Layout().Update(this->_get_cap_rect(i_));
		}

	private:
		RECT  _get_cap_rect(const DWORD _ndx) {
			RECT rc_cap = m_rect; rc_cap.top = (0 ==_ndx ? 0 : _pos::e_cap_shift); rc_cap.bottom = rc_cap.top + _pos::e_cap_height;
			return rc_cap;
		}
	};

	class CiPod_Dev_Init {
	private:
		CiPod_Device& m_view ;
		CError        m_error;

	public:
		 CiPod_Dev_Init (CiPod_Device& _view) : m_view(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }
		~CiPod_Dev_Init (void) {}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK; return m_error;
		}

		HRESULT   OnFormat_Caps (TLabelCtrl* _p_caps, const DWORD _d_count) {
			m_error << __MODULE__ << S_OK;
			if (NULL == _p_caps || 0 == _d_count)
				return (m_error << E_INVALIDARG);

			static const CMargins margins_( 95,0,0,3);

			for (DWORD i_ = 0; i_ < _d_count; i_++) {
				TLabelFmt& fmt_ = _p_caps[i_].Format(); fmt_ = shared::Get_Format().Control().Label(); fmt_.Margins() = margins_;
			}

			return m_error;
		}

		HRESULT   OnFormat_Logos(TImageCtrl* _p_logos, const DWORD _d_count) {
			m_error << __MODULE__ << S_OK;
			if (NULL == _p_logos || 0 == _d_count)
				return (m_error << E_INVALIDARG);

			CColour_Ex clr_back(shared::Get_Format().Panel().Bkgnd().Solid());

			for (DWORD i_ = 0; i_ < _d_count; i_++) {
				TImageCtrl& logo_ = _p_logos[i_];
				logo_.Format().Bkgnd().Gradient().c0() << clr_back;
				logo_.Format().Bkgnd().Gradient().c1() << clr_back; logo_.Image().Create(i_ == 0 ? This_Res::e_logo_dev : This_Res::e_none);
			}

			return m_error;
		}
		/////////////////////////////////////////////////////////////////////////////
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CiPod_Device::CViewWnd:: CViewWnd(CiPod_Device& _view) : TViewWnd(_view), m_view(_view) {}
CiPod_Device::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CiPod_Device::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	CiPod_Dev_Layout layout_(m_view);
	CiPod_Dev_Init init_(m_view); init_.OnCreate();

	HRESULT hr_ = m_caps[0].Create(*this, _T("Device"), layout_.GetTitle(0), layout_.GetImage(0));
	if (SUCCEEDED(hr_)) {
		m_caps[0].Format(This_Res::e_logo_dev);
		m_caps[0].Image().Format().Bkgnd().Sectors().Append(shared::Get_Theme().Get(TThemePart::e_caption,  TThemeElement::e_back), TTileSize(80, 30));
		m_caps[0].Image().Format().Bkgnd().Sectors().Append(shared::Get_Format().Panel().Bkgnd().Solid(), TTileSize(80, 50));
		m_caps[0].Image().Layout().Update();
	}

	hr_ = m_empty.Create(*this, layout_.GetEmpty(0), true);

	return 0;
}

LRESULT   CiPod_Device::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_empty.Destroy();
	m_caps[0].Destroy();

	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CiPod_Device::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CiPod_Dev_Layout layout_(m_view); layout_.Update(m_caps, _countof(m_caps));
	m_empty.Update(layout_.GetEmpty(0));

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CiPod_Device:: CiPod_Device (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CiPod_Device::~CiPod_Device (void) {}

/////////////////////////////////////////////////////////////////////////////