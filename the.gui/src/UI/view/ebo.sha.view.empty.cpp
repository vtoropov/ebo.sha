/*
	Created by Tech_dog (ebontrop@gmail.com) on 30-Nov-2019 at 8:25:23.394 am, UTC+7, Novosibirsk, Monday;
	This is Ebo Pack no functionality test view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.view.empty.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

#include "shared.uix.gdi.draw.defs.h"
#include "shared.uix.gdi.renderer.h"
using namespace ex_ui::draw;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CNa_Layout {
	private:
		CWindow   m_view;
		RECT      m_rect;

	public:
		CNa_Layout (const HWND _view_wnd) : m_view(_view_wnd), m_rect{0} {
			if (m_view.IsWindow()) {
				m_view.GetClientRect(&m_rect);
			}
		}

	public:
		RECT   GetImageRect (const TImageCtrl& _ctrl) const {

			const SIZE sz_image = _ctrl.Image().Size();
			const LONG nLeft = m_rect.left + (__W(m_rect) - sz_image.cx) / 2;
			const LONG nTop = m_rect.top + (__H(m_rect) - sz_image.cy) / 2;

			RECT rc_image = {0};
			::SetRect(&rc_image, nLeft, nTop, nLeft + sz_image.cx, nTop + sz_image.cy);

			return rc_image;
		}
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CEmptyView::CViewWnd:: CViewWnd(CEmptyView& _view) : TViewWnd(_view), m_view(_view), m_borders(false) {}
CEmptyView::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CEmptyView::CViewWnd::OnCreate  (UINT _u_msg, WPARAM _w_p, LPARAM _l_p, BOOL& _b_hand) {
	_b_hand;
	LRESULT l_result = TViewWnd::OnCreate  (_u_msg, _w_p, _l_p, _b_hand);

	m_na_image.Image().Create(IDR_EBO_SHA_GEN_VIEW_BRK_L);
	m_na_image.Create(*this, CNa_Layout(*this).GetImageRect(m_na_image), 0x1);

	return  l_result;
}

LRESULT   CEmptyView::CViewWnd::OnDestroy (UINT _u_msg, WPARAM _w_p, LPARAM _l_p, BOOL& _b_hand) {
	m_na_image.Destroy ();
	return TViewWnd::OnDestroy(_u_msg, _w_p, _l_p, _b_hand);
}

LRESULT   CEmptyView::CViewWnd::OnErase   (UINT _u_msg, WPARAM _w_p, LPARAM _l_p, BOOL& _b_hand) {
	_u_msg; _w_p; _l_p; _b_hand = TRUE;

	static const LRESULT l_is_drawn = 1;

	RECT rc_this = {0};
	TViewWnd::GetClientRect(&rc_this);

	const HDC h_dc = reinterpret_cast<HDC>(_w_p);

	CZBuffer dc_(h_dc, rc_this);
	dc_.DrawSolidRect( rc_this, shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_back)); if (m_borders)
	dc_.DrawRectangle( rc_this, shared::Get_Theme().Get(TThemePart::e_panel, TThemeElement::e_border), 1);

	return l_is_drawn;
}

LRESULT   CEmptyView::CViewWnd::OnSize    (UINT _u_msg, WPARAM _w_p, LPARAM _l_p, BOOL& _b_hand) {
	_u_msg; _w_p; _l_p; _b_hand = FALSE;
	const RECT rc_ = {0, 0, LOWORD(_l_p), HIWORD(_l_p)};

	m_na_image.Layout().Update(CNa_Layout(*this).GetImageRect(m_na_image));

	return 0;
}
/////////////////////////////////////////////////////////////////////////////

CEmptyView:: CEmptyView(void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CEmptyView::~CEmptyView(void) {}

/////////////////////////////////////////////////////////////////////////////

VOID   CEmptyView::DrawBorder(const bool _b_on_off) { m_wnd.m_borders = _b_on_off; }