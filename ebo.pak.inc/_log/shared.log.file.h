#ifndef _SHAREDLITEEVENTLOGGER_H_48DF5BDB_D9A2_4b59_8925_7B36976454A9_INCLUDED
#define _SHAREDLITEEVENTLOGGER_H_48DF5BDB_D9A2_4b59_8925_7B36976454A9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Mar-2014 at 7:36:14am, GMT+3, Taganrog, Wednesday;
	This is Shared Lite Common Event Logger class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 18-May-2020 at 7:37:39p, UTC+7, Novosibirsk, Monday;
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.syn.obj.h"
#include "shared.gen.sys.date.h"

#include "shared.log.defs.h"

namespace shared { namespace log {

	using shared::sys_core::CError;
	using shared::sys_core::CSyncObject;

	using shared::common::CDateTime; typedef const CDateTime&  TDateTimeRef;

	class eEventType : public eLogType {};

	class eEventPersistance : public eLogOption {};

	class eEventLoggerOption {
	public:
		enum _enum {
			eNone           = 0x0,
			eUseSingleFile  = 0x1,
			eOverwriteFile  = 0x2,
			eLogErrorsOnly  = 0x4,
			eLoggingIsOff   = 0x8
		};
	};

	class CFileLogger
	{
	private:
		DWORD         m_dwOptions ;
		FILE*         m_file      ;
		CSyncObject   m_sync_obj  ;
		CError        m_log_error ;
		DWORD         m_pers_state;

	public:
		 CFileLogger(const eEventPersistance::_e = eEventPersistance::eUseFile, const DWORD dwOptions = eEventLoggerOption::eNone);
		~CFileLogger(void);

	public:
		TErrorRef     Error     (void) const;
		bool          IsTurnedOn(void) const;
#if (0)
		HRESULT       ToEvent(LPCTSTR pMessage, const eEventType::_e = eEventType::eInfo) const;
		HRESULT       ToEvent(LPCTSTR pMessage, const DWORD   dError) const;
		HRESULT       ToEvent(LPCTSTR pMessage, const HRESULT hError) const;
#endif
		HRESULT       ToFile (LPCTSTR pMessage, const DWORD   dError) const;
		HRESULT       ToFile (LPCTSTR pMessage, const eEventType::_e, const CDateTime&  ) const;
		HRESULT       ToFile (LPCTSTR pMessage, const eEventType::_e = eEventType::eInfo) const;
		HRESULT       ToFile (LPCTSTR pMessage, const HRESULT hError) const;

		HRESULT       ToLog  (LPCTSTR pMessage, const eEventType::_e = eEventType::eInfo) const;
		HRESULT       ToLog  (LPCTSTR pMessage, const DWORD   dError) const;
		HRESULT       ToLog  (LPCTSTR pMessage, const HRESULT hError) const;
	private:
		CFileLogger(const CFileLogger&);
		CFileLogger& operator= (const CFileLogger&);
	};

	CFileLogger&  Logger(void);

	class CFileLog__function_auto_entry
	{
	private:
		const CFileLogger&  m__log_ref    ;
		::ATL::CAtlString   m__func_name  ;
		bool                m__removal_ctx;
	public:
		 CFileLog__function_auto_entry(const CFileLogger&, LPCTSTR func_name);
		~CFileLog__function_auto_entry(void);
	};
}}

#define TRACE_FUNC ()    shared::log::CFileLog__function_auto_entry entry(shared::log::Logger(), _T(__FUNCTION__));
#define TRACE_INFO (msg) shared::log::Logger().ToLog(msg, shared::log::eEventType::eInfo);
#define TRACE_WARN (msg) shared::log::Logger().ToLog(msg, shared::log::eEventType::eWarning);
#define TRACE_ERROR(msg) shared::log::Logger().ToLog(msg, shared::log::eEventType::eError);

#define TRACE_ERR__a1(pattern, arg) { ::ATL::CAtlString cs__fmt; cs__fmt.Format(pattern, arg); TRACE_ERROR(cs__fmt.GetString());}
#define TRACE_INFO_a1(pattern, arg) { ::ATL::CAtlString cs__fmt; cs__fmt.Format(pattern, arg); TRACE_INFO (cs__fmt.GetString());}
#define TRACE_WARN_a1(pattern, arg) { ::ATL::CAtlString cs__fmt; cs__fmt.Format(pattern, arg); TRACE_WARN (cs__fmt.GetString());}

#define TRACE_ERR__a2(pattern, arg1, arg2) { ::ATL::CAtlString cs__fmt; cs__fmt.Format(pattern, arg1, arg2); TRACE_ERROR(cs__fmt.GetString());}

#endif/*_SHAREDLITEEVENTLOGGER_H_48DF5BDB_D9A2_4b59_8925_7B36976454A9_INCLUDED*/