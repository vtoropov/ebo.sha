#ifndef _EBOSHAVIEWHELP_H_60405C0A_A9A9_41D3_973C_A7FCB44E09C5_INCLUDED
#define _EBOSHAVIEWHELP_H_60405C0A_A9A9_41D3_973C_A7FCB44E09C5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 12-Jan-2021 at 10:32:59.537 am, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Sha Optima Tool application help view interface declaration file.
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.view.empty.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CHelp : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CHelp;
		private:
			CHelp&        m_view  ;
			TEmptyView    m_empty ;

		public:
			 CViewWnd (CHelp&);
			~CViewWnd (void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CHelp (void);
		~CHelp (void);

	public:
	};

}}}}

typedef ebo::sha::gui::view::CHelp THelp;

#endif/*_EBOSHAVIEWHELP_H_60405C0A_A9A9_41D3_973C_A7FCB44E09C5_INCLUDED*/