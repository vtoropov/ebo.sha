#ifndef _SHAREDSYSTEMSERVICEIMPL_H_A661449C_D435_49cc_AEB9_DE1FF278D5CD_INCLUDED
#define _SHAREDSYSTEMSERVICEIMPL_H_A661449C_D435_49cc_AEB9_DE1FF278D5CD_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Jun-2016 at 6:50:03p, UTC+7, Phuket, Rawai, Tuesday;
	This is shared lite library system service base interface declaration file.
	( project: thefileguardian.com )
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 12-Jul-2020 at 4:50:10p, UTC+7, Novosibirsk, Sunday;
*/
#include "sys.svc.crt.h"
#include "sys.svc.service.h"

namespace shared { namespace service {

	class CServiceBaseImpl
	{
	protected:
		CCrtData              m_crt_data;
		SERVICE_STATUS        m_status  ;        // the status of the service
		SERVICE_STATUS_HANDLE m_statusHandle;

	protected:
		CServiceBaseImpl(const CCrtData&);
		virtual ~CServiceBaseImpl(void);

	protected:  // events handlers
		virtual VOID     OnContinue(void);
		virtual VOID     OnPause   (void);
		virtual VOID     OnShutdown(void);
		virtual VOID     OnStart   (const DWORD dwArgc, LPWSTR* pszArgv);
		virtual VOID     OnStop    (void);
	protected:
		VOID   SetStatus(
				const DWORD dwCurrentState,
				const DWORD dwWin32ExitCode = NO_ERROR,
				const DWORD dwWaitHint = 0
			);
	private:
		VOID   Continue(void);
		VOID   Pause   (void);
		VOID   Shutdown(void);

	public:
		VOID   Exit (void);  // exits itself and informs service manager about this
		VOID   Start(DWORD dwArgc, PWSTR* pszArgv);
		VOID   Stop (void);

	protected:
		static VOID WINAPI ServiceCtrlHandler(DWORD dwControl);
	private:
		static VOID WINAPI ServiceMain(DWORD dwArgc, LPWSTR* lpszArgv);
	public:
		static BOOL WINAPI Run(CServiceBaseImpl&);
	};

	class CServiceBaseImplEx : public CServiceBaseImpl {
	                          typedef CServiceBaseImpl TBase;
	protected:
		HDEVNOTIFY       m_notify;
		DWORD            m_last_error;

	protected:
		CServiceBaseImplEx(const CCrtData&);

	protected:  // events handlers
		virtual VOID     OnDeviceEvent(const DWORD dwEventType, LPVOID const lpEventData) PURE;
		virtual VOID     OnStart(const DWORD dwArgc, LPWSTR* pszArgv) override;
		virtual VOID     OnStop(void) override;

	private:
		static DWORD WINAPI ServiceCtrlHandlerEx(DWORD dwControl, DWORD dwEventType, LPVOID lpEventData, LPVOID lpContext);
		static VOID  WINAPI ServiceMain(DWORD dwArgc, LPWSTR* lpszArgv);

	public:
		static BOOL  WINAPI Run(CServiceBaseImplEx&);
	};
}}

#endif/*_SHAREDSYSTEMSERVICEIMPL_H_A661449C_D435_49cc_AEB9_DE1FF278D5CD_INCLUDED*/