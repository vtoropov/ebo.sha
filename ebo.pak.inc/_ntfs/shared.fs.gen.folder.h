#ifndef _SHAREDFILESYSTEM_H_42494520_2884_4ba8_9BBD_450E6620FC22_INCLUDED
#define _SHAREDFILESYSTEM_H_42494520_2884_4ba8_9BBD_450E6620FC22_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jun-2016 at 1:11:46p, GMT+7, Phuket, Rawai, Saturday;
	This is shared NTFS library generic folder class(es) declaration file; (thefileguardian.com)
	-----------------------------------------------------------------------------
	Adopted to VS v15 on 9-Jul-2018 at 6:02:31p, UTC+7, Phuket, Rawai, Monday;
*/
#include "shared.fs.defs.h"

namespace shared { namespace ntfs
{
	using shared::sys_core::CError; typedef WIN32_FIND_DATA TFindData;

	class CGenericFolder : public CBaseObject { typedef CBaseObject TObject;
	public:
		 CGenericFolder(LPCWSTR lpszPath);
		~CGenericFolder(void);

	public:
		HRESULT      Create  (VOID);
		TFindData    FindLastModified(VOID)const;
		HRESULT      EnumerateItems(TFileList& _files, TFolderList& _subfolders, LPCWSTR lpszMask = _T("*")); // enumerates items in specific folder only, no recursion
		HRESULT      EnumerateItems(TFolderItemCollection& _items)const;
		bool         SelectObject  (LPCWSTR lpszFileName)const;
	};

	class CSpecialFolder
	{
	private:
		CError       m_error;
	public:
		CSpecialFolder(void);

	public:
		TErrorRef    Error(void) const;
		CStringW     PersonalDocs  (void); // gets user's personal document folder path;
		CStringW     ProgramFiles  (void); // the program files folder;
		CStringW     ProgramFiles86(void); // the program files folder for 32-bit programs on 64-bit platform;
		CStringW     PublicUserData(void); // the file system directory that contains application data for all users;
		CStringW     UserTemporary (void); // current user temporary folder;
		CStringW     Windows       (void); // get MS Windows installation folder;
	};

	class CCurrentFolder : public CBaseObject { typedef CBaseObject TObject;
	public:
		CCurrentFolder(void);
	public:
		CStringW     GetPath(void)const;
		CStringW     GetPathFromPattern(LPCWSTR lpszPattern)const;
		bool         IsRelative(LPCWSTR lpszPath)const;
	};
}}

#endif/*_SHAREDFILESYSTEM_H_42494520_2884_4ba8_9BBD_450E6620FC22_INCLUDED*/