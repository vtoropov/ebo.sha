#ifndef _SHAREDREGHIVEKEY_H_BD8C26A1_9637_490D_8455_DB00F5430C16_INCLUDED
#define _SHAREDREGHIVEKEY_H_BD8C26A1_9637_490D_8455_DB00F5430C16_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Oct-2019 at 1:34:01a, UTC+7, Novosibirsk, Thursday;
	This is Ebo Pack shared registry wrapper library key entry interface declaration file.
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.sys.date.h"
#include "shared.reg.hive.val.h"

namespace shared { namespace registry {

	using shared::sys_core::CError;
	using shared::common::CSystemTime;

	class CRoot {
	public:
		/*  https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2003/cc778196(v=ws.10)
			+-------------------------------+---------------+
			|     hive root name            |   abbreviate  |
			+-------------------------------+---------------+
			| HKEY_CLASSES_ROOT             |      HKCR     |
			| HKEY_CURRENT_USER             |      HKCU     |
			| HKEY_LOCAL_MACHINE            |      HKLM     |
			| HKEY_USERS                    |      HKU      |
			| HKEY_CURRENT_CONFIG           |      HKCC     |
			+-------------------------------+---------------+
		*/
		enum _e_keys {
			e_cls_root    = 0x0  , // https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2003/cc739822(v=ws.10)
			e_cur_user    = 0x1  , // https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2003/cc779816(v=ws.10)
			e_loc_machine = 0x2  , // https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2003/cc784983(v=ws.10)
			e_loc_users   = 0x3  , // https://en.wikipedia.org/wiki/Windows_Registry#HKEY_USERS_(HKU)
			e_cur_cfg     = 0x4  , // https://docs.microsoft.com/en-us/previous-versions/windows/it-pro/windows-server-2003/cc776168(v=ws.10)
		};
	private:
		_e_keys      m_active ;    // default value is HKCR, i.e. e_cls_root;
	public:
		 CRoot (void) ;
		 CRoot (const CRoot&) ;
		~CRoot (void) ;

	public:
		CAtlString AsText  (void)  const  ;  // returns registry root node as a text;
		HKEY       Handle  (void)  const  ;
		HKEY       Handle  (const _e_keys);
		
	public:
		operator _e_keys   (void)  const  ;
		operator CAtlString(void)  const  ;  // returns registry root node as a text;
		operator HKEY      (void)  const  ;

	public:
		CRoot& operator =  (const _e_keys);
		CRoot& operator =  (const HKEY   );
		CRoot& operator =  (const CRoot& );
	};

	class CKey {
	protected:
		mutable
		CError     m_error;
		CRoot      m_root ;
		CAtlString m_path ;
		TValues    m_vals ;
		CSystemTime
		           m_date ; // a time when a key is changed/written/created;
	public:
		 CKey (void);
		 CKey (const CRoot&);
		 CKey (const CRoot&, LPCWSTR _lp_sz_path);
		 CKey (const CRoot&, LPCWSTR _lp_sz_path, const TValues&);
		~CKey (void);

	public:
		const
		CSystemTime&Date  (void) const;
		CSystemTime&Date  (void)      ;
		TErrorRef   Error (void) const;
		const
		CAtlString& Path  (void) const;
		CAtlString& Path  (void)      ;
		const
		CRoot&      Root  (void) const;
		CRoot&      Root  (void)      ;
		const bool  Valid (void) const;
		const
		TValues&    Values(void) const;
		TValues&    Values(void)      ;

	public:
		CKey& operator += (const CValue&) ;
		CKey& operator << (const CRoot& ) ;
		CKey& operator << (LPCWSTR _path) ;

	public:
		operator LPCWSTR  (void) const; // returns key path;
	};

	class CKey_App : public CKey { // returns path to HKEY root folder + '\\Software' + this app name;
	                typedef CKey TBase;
	public:
		 CKey_App (void) ;
		~CKey_App (void) ;
	};

	class CKey_Sft : public CKey { // returns path to HKEY root folder + '\\Software';
	                typedef CKey TBase;
	public:
		 CKey_Sft (void) ;
		~CKey_Sft (void) ;
	};

	class CKey_Ver : public CKey { // returns path to HKEY root folder + '\\Software' + this app name + version;
	                typedef CKey TBase;
	public:
		 CKey_Ver (void) ;
		~CKey_Ver (void) ;
	};
}}

#endif/*_SHAREDREGHIVEKEY_H_BD8C26A1_9637_490D_8455_DB00F5430C16_INCLUDED*/