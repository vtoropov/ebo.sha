#ifndef __SYSSVCSECURITY_H_3D8D0B95_1DE7_434A_AD84_C5D71384BD38_INCLUDED
#define __SYSSVCSECURITY_H_3D8D0B95_1DE7_434A_AD84_C5D71384BD38_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Aug-2020 at 7:20:09p, UTC+7, Novosibirsk, Tuesday;
	This is service manager security wrapper interface declaration file.
*/
#include <aclapi.h>
#include "sys.svc.error.h"
#include "sys.svc.service.h"

namespace shared { namespace service {

	typedef PSECURITY_DESCRIPTOR p_sec_desc;
	// https://docs.microsoft.com/en-us/windows/win32/api/aclapi/nf-aclapi-buildexplicitaccesswithnamew
	class CExp_Access {
	private:
		CSvcError   m_error;
		EXPLICIT_ACCESSW m_ex_acc;

	public:
		 CExp_Access (void);
		 CExp_Access (const CExp_Access&);
		~CExp_Access (void);

	public:
		const
		EXPLICIT_ACCESSW& Get(void) const;
		EXPLICIT_ACCESSW& Get(void)      ;
		TErrorRef   Error (void) const;
		HRESULT     Query (const LPCWSTR _lp_sz_account);
		HRESULT     Reset (void) ;

	public:
		CExp_Access&  operator  = (const CExp_Access&);
		CExp_Access&  operator << (const LPCWSTR _lp_sz_account);
	};
	// https://docs.microsoft.com/en-us/windows/win32/api/securitybaseapi/nf-securitybaseapi-getsecuritydescriptordacl
	class CSec_Desc {
	private:
		CSvcError   m_error;
		PSECURITY_DESCRIPTOR m_p_desc;
		PACL        m_pdacl;
	public:
		 CSec_Desc (void);
		 CSec_Desc (const CSec_Desc&);
		~CSec_Desc (void);

	public:
		p_sec_desc  Get   (void) const;
		TErrorRef   Error (void) const;
		const bool  Is    (void) const;
		HRESULT     Query (const SC_HANDLE _p_service);
		HRESULT     Reset (void) ;

	public:
		CSec_Desc&  operator  = (const CSec_Desc&);
		CSec_Desc&  operator << (const SC_HANDLE _p_service);

	public:
		operator p_sec_desc const (void) const;
		operator PACL const (void) const;
	};
	// https://docs.microsoft.com/en-us/windows/win32/api/securitybaseapi/nf-securitybaseapi-initializesecuritydescriptor
	class CSecurity {
	private:
		CSvcError   m_error ;
		CSec_Desc   m_sec_d ;
		CExp_Access m_access;

	public:
		 CSecurity (void);
		~CSecurity (void);

	public:
		TErrorRef   Error (void) const;
		HRESULT     Set   (SC_HANDLE _p_svc_man, LPCWSTR _lp_sz_svc_name);  // sets permissions for current user account;
		HRESULT     Set   (SC_HANDLE _p_svc_man, LPCWSTR _lp_sz_svc_name, LPCWSTR _lp_sz_account);
	};

}}

#endif/*__SYSSVCSECURITY_H_3D8D0B95_1DE7_434A_AD84_C5D71384BD38_INCLUDED*/