/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Nov-2020 at 10:17:29.952 am, UTC+7, Novosibirsk, Thursday;
	This is Ebo Sha optima tool media iPod sub-view interface implementation file;
*/
#include "StdAfx.h"
#include "ebo.sha.media.ipod.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

#include "shared.uix.gdi.draw.defs.h"
#include "shared.uix.gdi.renderer.h"
using namespace ex_ui::draw;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CMedia_iPod_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_tab  = IDC_EBO_SHA_MED_POD_TAB,
		};
	}; typedef CMedia_iPod_Ctl This_Ctl;

	class CMedia_iPod_Res {
	public:
		enum _res : WORD {
			e_res_tab_0_a  = IDR_EBO_SHA_MED_POD_TAB_0a,
			e_res_tab_0_n  = IDR_EBO_SHA_MED_POD_TAB_0n,
		};
	}; typedef CMedia_iPod_Res This_Res;

	class CMedia_iPod_Layout {
	private:
		CMedia_iPod& m_view;
		RECT         m_rect;

	public:
		 CMedia_iPod_Layout (CMedia_iPod& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CMedia_iPod_Layout (void) {}

	public:
		RECT GetTabsRect (void) const {
			RECT rc_tabs = m_rect;
			::InflateRect(&rc_tabs, -0x5, -0x5);
			return rc_tabs;
		}

		VOID   Update (void) {
			TTabCtrl& tabbed  = m_view.Tabbed(); tabbed.Layout() << this->GetTabsRect(); RECT rc_page = tabbed.Layout().Page();
			TiPodDevice& ipod = m_view.Device(); ipod.Window().MoveWindow(&rc_page);
		}
	};

	class CMedia_iPod_Init {
	private:
		CMedia_iPod& m_view ;
		CError       m_error;

	public:
		 CMedia_iPod_Init (CMedia_iPod& _view) : m_view(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }
		~CMedia_iPod_Init (void) {}

	public:
		HRESULT   OnCreate  (void) { m_error << __MODULE__ << S_OK;
#pragma region __tab_create
			TTabCtrl& tabs = m_view.Tabbed();

			tabs.Format() = shared::Get_Format().Tabbed();
			tabs.Layout().Tabs().Gap() = 0;

			tabs.Tabs().Append(_T("")); // connected
			tabs.Tabs().Tab(0).Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_tab_0_n );
			tabs.Tabs().Tab(0).Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_tab_0_a );

			tabs.Create(m_view.Window(), CMedia_iPod_Layout(m_view).GetTabsRect(), This_Ctl::e_ctl_tab);
			tabs.ParentRenderer(NULL);

			tabs.Layout().Margins().Left() = tabs.Layout().Margins().Top() = tabs.Layout().Margins().Right() = tabs.Layout().Margins().Bottom() = 10;

			tabs.Tabs().Active(0);
#pragma endregion
			return m_error;
		}
	};
}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CMedia_iPod::CViewWnd:: CViewWnd(CMedia_iPod& _view) : TViewWnd(_view), m_view(_view), m_tabbed(_view) {}
CMedia_iPod::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CMedia_iPod::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	const RECT rc_ = Lay_(*this); rc_;

	CMedia_iPod_Init init_(m_view); HRESULT hr_ = init_.OnCreate();

	if (SUCCEEDED(hr_)) {
		const RECT rc_page = m_tabbed.Layout().Page(); rc_page;
		hr_ = m_device.Create(m_tabbed.Window(), rc_page, false);
		if (SUCCEEDED(hr_))
			m_device.Window().SetWindowTextW(_T("media::ipod::device::subview"));
		else
			m_device.Error().Show();
		if (SUCCEEDED(hr_)) {
			m_tabbed.Tabs().Active(0);
			((ITabEvents&)m_view).ITabEvent_OnSelect(0);
		}
	}

	return 0;
}

LRESULT   CMedia_iPod::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_device.Destroy();
	m_tabbed.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CMedia_iPod::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CMedia_iPod_Layout(m_view).Update();

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CMedia_iPod:: CMedia_iPod (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CMedia_iPod::~CMedia_iPod (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CMedia_iPod::ITabEvent_OnSelect (const DWORD _tab_ndx) { _tab_ndx;
	TView::m_error << __MODULE__ << S_OK;
	m_wnd.m_device.IsVisible(0 == _tab_ndx);
	return TView::m_error;
}

/////////////////////////////////////////////////////////////////////////////

TiPodDevice& CMedia_iPod::Device (void) { return m_wnd.m_device; }
TTabCtrl&    CMedia_iPod::Tabbed (void) { return m_wnd.m_tabbed; }