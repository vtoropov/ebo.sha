/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Nov-2020 at 9:45:31p, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Sha optima tool fake GPS driver sub-view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.gps.drv.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CFake_Drv_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_tab  = IDR_EBO_SHA_FAK_DRV_TAB,
		};
	}; typedef CFake_Drv_Ctl This_Ctl;

	class CFake_Drv_Res {
	public:
		enum _res : WORD {
			e_res_tab_0_a  = IDR_EBO_SHA_FAK_DRV_TAB_0a,
			e_res_tab_0_n  = IDR_EBO_SHA_FAK_DRV_TAB_0n,
			e_res_tab_1_a  = IDR_EBO_SHA_FAK_DRV_TAB_1a,
			e_res_tab_1_n  = IDR_EBO_SHA_FAK_DRV_TAB_1n,
			e_res_tab_2_a  = IDR_EBO_SHA_FAK_DRV_TAB_2a,
			e_res_tab_2_n  = IDR_EBO_SHA_FAK_DRV_TAB_2n,
		};
	}; typedef CFake_Drv_Res This_Res;

	class CFake_Drv_Layout {
	private:
		CFake_Drv& m_view;
		RECT       m_rect;

	public:
		 CFake_Drv_Layout (CFake_Drv& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CFake_Drv_Layout (void) {}

	public:
		RECT GetTabsRect (void) const {
		RECT rc_tabs = m_rect;
			::InflateRect(&rc_tabs, -0x5, -0x5);
			return rc_tabs;
		}

		VOID   Update (void) {
			TTabCtrl& tabbed   = m_view.Tabbed()  ; tabbed.Layout() << this->GetTabsRect(); RECT rc_page = tabbed.Layout().Page();
			TFakeDev& device   = m_view.Device()  ; device.Window().MoveWindow(&rc_page);
			TFakePos& position = m_view.Position(); position.Window().MoveWindow(&rc_page);
			TFakeConn& connect = m_view.Connect() ; connect.Window().MoveWindow(&rc_page);
		}
	};

	class CFake_Drv_Init {
	private:
		CFake_Drv& m_view ;
		CError     m_error;

	public:
		 CFake_Drv_Init (CFake_Drv& _view) : m_view(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }
		~CFake_Drv_Init (void) {}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK;
#pragma region __tab_create

			TTabCtrl& tabs = m_view.Tabbed();

			tabs.Format()  = shared::Get_Format().Tabbed();

			tabs.Tabs().Append(_T("")); // position
			tabs.Tabs().Tab(0).Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_tab_0_n );
			tabs.Tabs().Tab(0).Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_tab_0_a );
			tabs.Tabs().Append(_T("")); // connection
			tabs.Tabs().Tab(1).Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_tab_1_n );
			tabs.Tabs().Tab(1).Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_tab_1_a );
			tabs.Tabs().Append(_T("")); // device iface
			tabs.Tabs().Tab(2).Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_tab_2_n );
			tabs.Tabs().Tab(2).Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_tab_2_a );

			tabs.Create(m_view.Window(), CFake_Drv_Layout(m_view).GetTabsRect(), This_Ctl::e_ctl_tab);
			tabs.ParentRenderer(NULL);

			tabs.Layout().Margins().Left() = tabs.Layout().Margins().Top() = tabs.Layout().Margins().Right() = tabs.Layout().Margins().Bottom() = 10;
			tabs.Layout().Tabs().Gap() = 0;

#pragma endregion
			return m_error;
		}
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CFake_Drv::CViewWnd:: CViewWnd(CFake_Drv& _view) : TViewWnd(_view), m_view(_view), m_tabbed(_view) {}
CFake_Drv::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CFake_Drv::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	const RECT rc_ = Lay_(*this); rc_;

	CFake_Drv_Init init_(m_view); HRESULT hr_ = init_.OnCreate();

	if (SUCCEEDED(hr_)) {
		const RECT rc_page = m_tabbed.Layout().Page();
		hr_ = m_device.Create(m_tabbed.Window(), rc_page, false);
		if (SUCCEEDED(hr_))
			m_device.Window().SetWindowTextW(_T("fake::gps::device::subview"));
		else
			m_device.Error().Show();
		if (SUCCEEDED(hr_)) {
			m_tabbed.Tabs().Active(2);
			((ITabEvents&)m_view).ITabEvent_OnSelect(2);
		}

		hr_ = m_pos.Create(m_tabbed.Window(), rc_page, false);
		if (SUCCEEDED(hr_))
			m_pos.Window().SetWindowTextW(_T("fake::gps::position::subview"));
		else
			m_pos.Error().Show();

		hr_ = m_connect.Create(m_tabbed.Window(), rc_page, false);
		if (SUCCEEDED(hr_))
			m_connect.Window().SetWindowTextW(_T("fake::gps::connect::subview"));
		else
			m_connect.Error().Show();
	}

	return 0;
}

LRESULT   CFake_Drv::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_pos.Destroy();  m_connect.Destroy();
	m_device.Destroy();
	m_tabbed.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CFake_Drv::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CFake_Drv_Layout(m_view).Update();

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CFake_Drv:: CFake_Drv (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CFake_Drv::~CFake_Drv (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CFake_Drv::ITabEvent_OnSelect (const DWORD _tab_ndx) { _tab_ndx;
	TView::m_error << __MODULE__ << S_OK;
	m_wnd.m_pos.IsVisible(0 == _tab_ndx);
	m_wnd.m_device.IsVisible(2 == _tab_ndx);
	m_wnd.m_connect.IsVisible(1 == _tab_ndx);
	return TView::m_error;
}

/////////////////////////////////////////////////////////////////////////////

TFakeConn& CFake_Drv::Connect (void) { return m_wnd.m_connect; }
TFakeDev&  CFake_Drv::Device  (void) { return m_wnd.m_device ; }
TFakePos&  CFake_Drv::Position(void) { return m_wnd.m_pos    ; }
TTabCtrl&  CFake_Drv::Tabbed  (void) { return m_wnd.m_tabbed ; }