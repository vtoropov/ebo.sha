#ifndef _SFXIMAGEFMT_H_8CF1B4CE_F60F_46BC_B072_722C05A92806_INCLUDED
#define _SFXIMAGEFMT_H_8CF1B4CE_F60F_46BC_B072_722C05A92806_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Nov-2020 at 12:53:03p, UTC+7, Novosibirsk, Wednesday;
	This is Sfx pucture control format interface declaration file.
*/
#include "shared.uix.ctrl.base.fmt.h"
#include "shared.uix.ctrl.defs.h"

namespace ST_Ctrls { namespace format {

	using ex_ui::controls::CBorder ;
	using ex_ui::controls::CBorders;

	using ex_ui::draw::shape::CCorner;
	using ex_ui::draw::shape::CCorners;

	using ex_ui::controls::format::CBkgnd;

	class CFrame {
	protected:
		CBorders   m_borders;

	public:
		 CFrame (void);
		 CFrame (const CFrame&);
		~CFrame (void);

	public:
		const
		CBorders&  Borders (void) const;
		CBorders&  Borders (void)      ;

	public:
		CFrame& operator = (const CFrame&);
		CFrame& operator <<(const CBorders&);
	};

	class CPicture_Format {
	protected:
		CBkgnd    m_bkgnd;
		CFrame    m_frame;

	public:
		 CPicture_Format (void);
		 CPicture_Format (const CPicture_Format&);
		~CPicture_Format (void);

	public:
		const
		CBkgnd&   Bkgnd (void) const;
		CBkgnd&   Bkgnd (void)      ;
		const
		CFrame&   Frame (void) const;
		CFrame&   Frame (void)      ;

	public:
		CPicture_Format& operator = (const CPicture_Format&);
		CPicture_Format& operator <<(const CBkgnd&);
		CPicture_Format& operator <<(const CFrame&);
	};

	class CSymantec_NA_Pic : public CPicture_Format {
	                        typedef CPicture_Format TBase;
	public:
		 CSymantec_NA_Pic (void);
		~CSymantec_NA_Pic (void);
	};

	class CWhitespread_Pic : public CPicture_Format {
	                        typedef CPicture_Format TBase;
	public:
		 CWhitespread_Pic (void);
		~CWhitespread_Pic (void);
	};
}}

typedef ST_Ctrls::format::CFrame           TFrameFormat;
typedef ST_Ctrls::format::CPicture_Format  TPictureFormat;

#endif/*_SFXIMAGEFMT_H_8CF1B4CE_F60F_46BC_B072_722C05A92806_INCLUDED*/