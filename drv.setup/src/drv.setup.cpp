/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Sep-2020 at 7:24:45p, UTC+7, Novosibirsk, Thursday;
	This is Ebo Pack generic driver installation interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 12-Jan-2021 at 7:47:48.618 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "drv.setup.h"

using namespace shared::driver::setup;

/////////////////////////////////////////////////////////////////////////////

CSetup_Base:: CSetup_Base (void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CSetup_Base:: CSetup_Base (const CSetup_Base& _ref) : CSetup_Base() { *this = _ref; }
CSetup_Base::~CSetup_Base (void) {}

/////////////////////////////////////////////////////////////////////////////

TErrorRef     CSetup_Base::Error (void) const { return m_error; }

/////////////////////////////////////////////////////////////////////////////

CSetup_Base&  CSetup_Base::operator = (const CSetup_Base& _ref) { this->m_error = _ref.Error(); return *this; }

/////////////////////////////////////////////////////////////////////////////

CSetup_Ifaces:: CSetup_Ifaces (void) : TBase() { m_error << __MODULE__ << S_OK >> __MODULE__; }
CSetup_Ifaces:: CSetup_Ifaces (const CSetup_Ifaces& _ref) : CSetup_Ifaces() { *this = _ref; }
CSetup_Ifaces::~CSetup_Ifaces (void) { this->Reset(); }

/////////////////////////////////////////////////////////////////////////////
const TDevIfaceDetais&
             CSetup_Ifaces::Details(void) const { return m_details; }
const bool   CSetup_Ifaces::Is     (void) const { return(m_details.empty() == false); }

HRESULT      CSetup_Ifaces::Reset  (void) {

	m_error << __MODULE__ << S_OK; HRESULT hr_ = S_OK;

	if (this->Is() == false)
		return hr_;

	for (size_t i_ = 0; i_ < m_details.size(); i_++) {
		CDevIfaceInfo& details = m_details[i_];

		hr_ = details.Erase();
		if (FAILED(hr_)) {
			m_error = details.Error();
			break;
		}
	}

	if (SUCCEEDED(hr_))
		m_details.clear();

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT      CSetup_Ifaces::Get    (const GUID& _dev_iface_cls) {
	_dev_iface_cls;
	m_error << __MODULE__ << S_OK;

	HRESULT hr_  = S_OK;
	DWORD n_iter = 0;

	if (this->Is())
		hr_ = this->Reset();
	if (FAILED(hr_))
		return hr_;

	CDevInfoSet dev_set; hr_ = dev_set.Get(_dev_iface_cls, NULL, NULL, dw_supported);

	if (dev_set.Is() == false) {
		return (m_error = dev_set.Error());
	}

	for (;;) {

		CDevIfaceInfo iface_details;

		const BOOL b_result = ::SetupDiEnumDeviceInterfaces (
			dev_set, 0, &_dev_iface_cls, n_iter, &iface_details.Data().Ref()
		);
		const DWORD dw_error = ::GetLastError();

		if (b_result) {

			hr_ = iface_details.Get(dev_set);

			if (FAILED(hr_) || iface_details.Is() == false)
				return (m_error = iface_details.Error());
			else
				n_iter ++; // it is very important to increase the iterator value, otherwise infinite loop will occur;

			try {
				m_details.push_back(iface_details);
			}
			catch (::std::bad_alloc&) {
				return (m_error = E_OUTOFMEMORY);
			}
		}
		else if (ERROR_NO_MORE_ITEMS != dw_error) {
			m_error = __DwordToHresult( dw_error); break;
		}
		else {
#if (0)
			m_error << __DwordToHresult(dw_error); // should be no more items error
#endif
			break;
		}
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CSetup_Ifaces& CSetup_Ifaces::operator = (const CSetup_Ifaces& _ref) {

	m_error << __MODULE__ << S_OK;

	if (this->Is())
		this->Reset();

	CSetup_Ifaces& ref_ = const_cast<CSetup_Ifaces&>(_ref);
	try {
		for (size_t i_ = 0; i_ < ref_.m_details.size(); i_ ++) {
			// actually, after pushing back this reference must be clean, no detailed info there;
			// TODO: must be checked;
			CDevIfaceInfo& details = ref_.m_details[i_];
			m_details.push_back(details);
		}
	}
	catch (const ::std::bad_alloc&) { m_error << OLE_E_BLANK; }

	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CSetup_Present:: CSetup_Present (void) : TBase() { m_error << __MODULE__ << S_OK; }
CSetup_Present:: CSetup_Present (const CSetup_Present& _ref) : CSetup_Present() { *this = _ref; }
CSetup_Present::~CSetup_Present (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT          CSetup_Present::Get (const GUID& _dev_iface_cls, const DWORD _flags, LPCWSTR _lp_sz_flt) {
	_dev_iface_cls; _flags; _lp_sz_flt;
	m_error << __MODULE__ << S_OK;
	// https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/hidsdi/nf-hidsdi-hidd_gethidguid
	GUID guid = _dev_iface_cls;
	if (::IsEqualCLSID(_dev_iface_cls, GUID_NULL))
		::HidD_GetHidGuid(&guid);

	HRESULT hr_  = S_OK;
	DWORD n_iter = 0;
	const bool b_flt = (NULL != _lp_sz_flt && 0 != ::lstrlenW(_lp_sz_flt));

	CDevInfoSet info_set; hr_ = info_set.Get(guid, NULL, NULL, dw_presented);
	if (info_set.Is() == false) {
		return (m_error = info_set.Error());
	}

	CDevInfo info_data;

	for (;;) {
		// https://docs.microsoft.com/en-us/windows/win32/api/setupapi/nf-setupapi-setupdienumdeviceinfo
		const BOOL b_result = ::SetupDiEnumDeviceInfo(info_set, n_iter, info_data);
		DWORD the_lst_err = ::GetLastError();

		if (ERROR_NO_MORE_ITEMS == the_lst_err) {
			break;
		} else if (ERROR_SUCCESS != the_lst_err ) {
			m_error = __DwordToHresult(the_lst_err); break;
		} else {
			n_iter += 1;
		}

		if (::IsEqualCLSID(info_data.Class(), GUID_NULL)) {
			continue;
		}

		CDevInstInfo inst_info;
		hr_ = inst_info.Get(info_set, info_data);
		if (FAILED(hr_)) {
			m_error = inst_info.Error(); break;
		}

		if (b_flt) {
			CStringW cs_id = inst_info.Att_Of(CDevInstInfo::e_att_id);
			if (-1 == cs_id.Find(_lp_sz_flt))
				continue;
		}

		try {
			m_instances.push_back(inst_info);
		}
		catch (const ::std::bad_alloc&) {
			m_error << E_OUTOFMEMORY; break;
		}
	}

	return m_error;
}
const
TDevInstances&   CSetup_Present::Instances(void) const { return m_instances; }
const bool       CSetup_Present::Is  (void) const { return m_instances.empty() == false; }

/////////////////////////////////////////////////////////////////////////////

CSetup_Present&  CSetup_Present::operator = (const CSetup_Present& _ref) { _ref; return *this; }

/////////////////////////////////////////////////////////////////////////////