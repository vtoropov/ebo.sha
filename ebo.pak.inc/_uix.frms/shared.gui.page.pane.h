#ifndef _UDDGENDLGSETBAS_H_8274E2CC_092E_4A7C_94F6_8C8A65012387_INCLUDED
#define _UDDGENDLGSETBAS_H_8274E2CC_092E_4A7C_94F6_8C8A65012387_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Nov-2018 at 5:58:36p, UTC+7, Novosibirsk, Friday;
	This is USB Drive Detective (bitsphereinc.com) desktop app property page base panes interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 10-Sep-2019 at 6:37:59a, UTC+7, Novosibirsk, Tuesday;
	Adopted to Ebo Pack personal account app on 18-Oct-2019 at 10:56:11a, UTC+7, Novosibirsk, Friday;
*/
#include "shared.gen.sys.err.h"

namespace shared { namespace gui { namespace layout {
	
	using shared::sys_core::CError;
	using shared::sys_core::CErr_Format;

	class CPage_Ava {
	private:
		UINT     m_ctl;   // image placeholder control identifier;
		UINT     m_res;   // image resource identifier;
	public:
		 CPage_Ava (void);
		 CPage_Ava (const UINT _ctl_id, const UINT _res_id);
		~CPage_Ava(void);
	public:
		const UINT&   Control (void) const; // returns a reference to identifier of an image control  (ra);
		      UINT&   Control (void)      ; // returns a reference to identifier of an image control  (rw);
		const UINT&   Resource(void) const; // returns a reference to identifier of an image resource (ra);
		      UINT&   Resource(void)      ; // returns a reference to identifier of an image resource (rw);
	public:
		const bool    IsValid(void) const;
	public:
		operator bool(void) const;
	};

	class CPage_Text {
	private:
		UINT     m_res;   // resource of a text being displayed;
		UINT     m_ctl;   // label control identifier;
	public:
		 CPage_Text(void);
		 CPage_Text(const UINT _ctl_id, const UINT _res_id);
		~CPage_Text(void);

	public:
		const
		bool     IsValid (void) const;
		UINT     Label   (void) const;      // gets a label control identifier value;
		HRESULT  Label   (const UINT);      // sets a label control identifier value;
		UINT     Resource(void) const;      // gets a text resource identifier value;
		UINT&    Resource(void)      ;      // gets a text resource identifier reference;
		HRESULT  Resource(const UINT);      // sets a text resource identifier value;

	public:
		operator bool (void) const;
	};

	class CPage_Ban {
	private:
		CPage_Ava     m_image;  // image placeholder control identifier;
		CPage_Text    m_label;  // text label control identifier;
	public:
		 CPage_Ban (void);
		 CPage_Ban (const UINT _img_id, const UINT _lab_id);
		~CPage_Ban (void);
	public:
		const
		CPage_Ava&    Image(void) const;
		CPage_Ava&    Image(void)      ;
		const
		CPage_Text&   Label(void) const;
		CPage_Text&   Label(void)      ;
	public:
		const bool    IsValid(void) const;
	public:
		operator bool (void) const;
	};

	class CPage_Info {
	private:
		CPage_Ava     m_logo;   // image information;
		CPage_Text    m_text;   // text related settings;
	public:
		 CPage_Info(void);
		~CPage_Info(void);

	public:
		const
		CPage_Ava &   Image(void) const;
		CPage_Ava &   Image(void)      ;
		const
		CPage_Text&   Text (void) const;
		CPage_Text&   Text (void)      ;
	};

	class CPage_Pane {
	private:
		CWindow       m_host;    // a handle of tab page dialog;
	public:
		 CPage_Pane(const HWND _host);
		~CPage_Pane(void);

	public:
		HRESULT       Error(const CPage_Info&, TErrorRef);   // shows an error information on the page;
		HRESULT       Info (const CPage_Info&);              // show regular/normal information on the page;
	};

}}}

#endif/*_UDDGENDLGSETBAS_H_8274E2CC_092E_4A7C_94F6_8C8A65012387_INCLUDED*/