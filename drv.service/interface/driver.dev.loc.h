#ifndef _DRIVERDEVLOC_H_481181F9_01F1_44C7_87D7_7ACCA6B52030_INCLUDED
#define _DRIVERDEVLOC_H_481181F9_01F1_44C7_87D7_7ACCA6B52030_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Aug-2020 at 3:05:48a, UTC+7, Novosibirsk, Friday;
	This is SSH-Tunnel WFP driver locator interface declaration file;
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 7:42:16.893 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "shared.gen.sys.err.h"
#include "shared.reg.hive.h"

#include "filter.defs.h"
// the names below can be virtual and actually may not coincident with phisycal name of the driver on its service;
#if (0)
#define DEVICE_NAME_NT    _T("\\Device\\ssh_filter")
#define DEVICE_NAME_WIN32 _T("\\\\.\\ssh_filter")
#define DEVICE_NAME_DOS   _T("\\DosDevices\\ssh_filter")
#endif

namespace shared { namespace driver { namespace client {

	using shared::sys_core::CError;

	class CDrvLocator {
	protected:
		CError    m_error;
		CStringW  m_alias;
		CStringW  m_path ;

	public:
		 CDrvLocator (void);
		 CDrvLocator (const CDrvLocator&);
		~CDrvLocator (void);

	public:
		LPCWSTR   Alias  (void) const;
		HRESULT   Alias  (LPCWSTR)   ;
		CStringW  Device (void) const;  // gets device name for creating a connection to driver; (defined in driver library);
		CStringW  Folder (void) const;  // gets full absolute path to folder of driver; (it's assumed the driver resides in app's folder;)
		TErrorRef Error  (void) const;  // not used yet;
		LPCWSTR   Path   (void) const;  // gets complete absolute driver path: i.e. path + file name;
		HRESULT   Path   (LPCWSTR)   ;
		CStringW  Service(void) const;  // gets driver service name; actually, it is the alias value;

	public:
		CDrvLocator& operator = (const CDrvLocator&);
	};

	class CDrvLocator_Pers : public CDrvLocator { typedef CDrvLocator TLocator;
	public:
		 CDrvLocator_Pers (void);
		 CDrvLocator_Pers (const CDrvLocator_Pers&);
		~CDrvLocator_Pers (void);

	public:
		HRESULT   Load (void);
		HRESULT   Save (void);

	public:
		CDrvLocator_Pers& operator = (const CDrvLocator_Pers&);
	};

}}}

typedef shared::driver::client::CDrvLocator        TDrvLocate;
typedef shared::driver::client::CDrvLocator_Pers   TDrvLocatePers;

#endif/*_DRIVERDEVLOC_H_481181F9_01F1_44C7_87D7_7ACCA6B52030_INCLUDED*/