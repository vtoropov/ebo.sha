#ifndef _EBOSHATCPTRACE_H_22EBEECF_CF3A_43DC_916D_C1A76F5666F5_INCLUDED
#define _EBOSHATCPTRACE_H_22EBEECF_CF3A_43DC_916D_C1A76F5666F5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Jan-2021 at 5:02:16.863 pm, UTC+7, Novosibirsk, Monday;
	This is Ebo Sha Optima Tool GPS TCP/IP trace  sub-view interface declaration file;
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.view.empty.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CFake_Trace : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CFake_Trace;
		private:
			CFake_Trace&  m_view  ;
			TTabCtrl      m_tabbed;
			TEmptyView    m_empty ;

		public:
			 CViewWnd (CFake_Trace&);
			~CViewWnd (void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CFake_Trace (void);
		~CFake_Trace (void);

	public:
		TTabCtrl&  Tabbed (void);
	};

}}}}

typedef ebo::sha::gui::view::CFake_Trace TFakeTrace;

#endif/*_EBOSHATCPTRACE_H_22EBEECF_CF3A_43DC_916D_C1A76F5666F5_INCLUDED*/