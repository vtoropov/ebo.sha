#ifndef _STDAFX_H_C9283464_6654_42A9_99CC_D2DF317CE943_INCLUDED
#define _STDAFX_H_C9283464_6654_42A9_99CC_D2DF317CE943_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 at 7:47:34a, UTC+7, Phuket, Rawai, Friday;
	This is generic shared library precompiled headers declaration file.
	-----------------------------------------------------------------------------
	Adopted to SSH-Tunnel project on 1-Aug-2020 at 10:37:00a, UTC+7, Novosibirsk, Saturday;
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 7:42:17.253 pm, UTC+7, Novosibirsk, Wednesday;
*/

#define WIN32_LEAN_AND_MEAN

#ifndef  WINVER
#define  WINVER         0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif   WINVER

#ifndef _WIN32_WINNT
#define _WIN32_WINNT    0x0600  // this is for use Windows Vista (or later) specific feature(s)
#endif  _WIN32_WINNT

#ifndef _WIN32_IE
#define _WIN32_IE       0x0700  // this is for use IE 7 (or later) specific feature(s)
#endif  _WIN32_IE

#pragma warning(disable: 4481)  // nonstandard extension used: override specifier 'override'
#pragma warning(disable: 4996)  // security warning: function or variable may be unsafe

#include <atlbase.h>
#include <atlwin.h >
#include <atlstr.h >
#include <atlsafe.h>
#include <comdef.h >
#include <comutil.h>
#include <vector>
#include <map>

namespace std {
	#include <math.h>
	#include <time.h>
}

#endif/*_STDAFX_H_C9283464_6654_42A9_99CC_D2DF317CE943_INCLUDED*/