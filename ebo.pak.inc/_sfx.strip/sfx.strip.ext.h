#ifndef _SFXSTRIPEXT_H_706041B4_32C4_48E8_B705_1DA2C30FAB29_INCLUDED
#define _SFXSTRIPEXT_H_706041B4_32C4_48E8_B705_1DA2C30FAB29_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Aug-2020 at 6:37:00a, UTC+7, Novosibirsk, Monday;
	This is Ebo Pack shared status bar control panel interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Sfx Strip project on 6-Sep-2020 at 5:25:17p, UTC+7, Novosibirsk, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.base.lay.h"
#include "shared.uix.ctrl.base.rnd.h"
#include "sfx.strip.lay.h"

namespace ST_Ctrls { namespace strip {

	using shared::sys_core::CError;

	using ex_ui::controls::CAlign ;
	using ex_ui::controls::CState ;

	class CItem { friend class ST_Ctrls::layout::CStrip;
	protected:
		DWORD     m_id   ;
		TLayersEx m_layers;
		CState    m_state;
		
	public:
		 CItem (void);
		 CItem (const CItem&);
		 CItem (const DWORD _id, LPCWSTR _lp_sz_text);
		~CItem (void);

	public:
		DWORD      Id   (void) const;
		DWORD&     Id   (void)      ;
		const bool Is   (void) const;    // returns true if identifier is grater than zero;
		const
		TLayersEx& Layers (void) const;
		TLayersEx& Layers (void)      ;
		const
		CState&    State(void) const;
		CState&    State(void)      ;

	public:
		CItem&    operator = (const CItem& );
		CItem&    operator <<(const CState&);
		CItem&    operator <<(const DWORD _id);
	public: // indirect accessing text layer;
		CItem&    operator <<(const CStringW& _cs_text);
		CItem&    operator <<(LPCWSTR _lp_sz_text);
	};

	typedef ::std::vector<CItem> TItems;

	class CItems { friend class ST_Ctrls::layout::CStrip;
	public:
		enum _na : INT { e_na = -1 };
	private:
		TItems    m_items;
		CError    m_error;

	public:
		 CItems (void);
		 CItems (const CItems&);
		~CItems (void);

	public:
		const INT Active(void) const;
		HRESULT   Active(const INT _ndx);
		HRESULT   Add   (const CItem&)  ;
		HRESULT   Add   (const DWORD _id, LPCWSTR _lp_sz_text);
		size_t    Count (void) const    ;
		const INT Has   (const DWORD _id) const; // returns an index of item if it's found, otherwise e_na (-1);
		const INT Has   (const POINT& pt) const; // returns an index of item if it's found, otherwise e_na (-1);
		TErrorRef Error (void) const;
		const
		CItem&    Item  (const UINT _ndx) const; // may return an invalid item (id == 0) in cases when an item does not exist;
		CItem&    Item  (const UINT _ndx)      ; // may return an invalid item (id == 0) in cases when an item does not exist;
		const
		TItems&   Raw   (void) const;
		TItems&   Raw   (void)      ;
		HRESULT   Remove(const CItem&)   ;       // might be dangerous in case if provided item belongs to internal data vector;
		HRESULT   Remove(const DWORD _id);

	public:
		CItems&   operator = (const CItems&) ;
	public:
		CItems&   operator+= (const CItem& ) ;
		CItems&   operator-= (const CItem& ) ;   // might be dangerous in case if provided item belongs to internal data vector;
		CItems&   operator-= (const DWORD _id);
		bool      operator== (const DWORD _id);

	public:
		INT operator << (const DWORD _id) const; // returns an index of item in internal data vector;
	};
}}

#endif/*_SFXSTRIPEXT_H_706041B4_32C4_48E8_B705_1DA2C30FAB29_INCLUDED*/