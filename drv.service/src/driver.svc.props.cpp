/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Jan-2021 at 9:34:16.142 pm, UTC+7, Novosibirsk, Thursday;
	This is Ebo Pack generic driver service property set interface implementation file.
*/
#include "StdAfx.h"
#include "driver.svc.props.h"

using namespace shared::driver::client;

/////////////////////////////////////////////////////////////////////////////

TDrvSvcPropSet:: CServiceProps (void) {}
TDrvSvcPropSet:: CServiceProps (const TDrvSvcPropSet& _ref) : TDrvSvcPropSet() { *this = _ref; }
TDrvSvcPropSet::~CServiceProps (void) {}

/////////////////////////////////////////////////////////////////////////////

UINT     TDrvSvcPropSet::Count   (void) const { return _countof(m_values); }
LPCWSTR  TDrvSvcPropSet::Property(const UINT _ndx) const { if (_ndx >= this->Count()) return NULL; else return (LPCWSTR)m_values[_ndx]; }
HRESULT  TDrvSvcPropSet::Property(const UINT _ndx, LPCWSTR _lp_sz_value) { HRESULT hr_ = S_OK;
	_ndx;
	if (_ndx >= this->Count())
		return (hr_ = DISP_E_BADINDEX);

	m_values[_ndx] = _lp_sz_value;
	return hr_;
}

VOID     TDrvSvcPropSet::Undef   (void) {
	static LPCWSTR lp_sz_undef = _T("#undef");
	this->Property(_id::e_desc , lp_sz_undef);
	this->Property(_id::e_name , lp_sz_undef);
	this->Property(_id::e_path , lp_sz_undef);
	this->Property(_id::e_start, lp_sz_undef);
	this->Property(_id::e_state, lp_sz_undef);
	this->Property(_id::e_type , lp_sz_undef);
}

VOID     TDrvSvcPropSet::Update  (const CCrtData& _crt) {

	shared::ntfs::CGenericPath drv_path;
	drv_path.BuildFrom(_crt.Path(), shared::ntfs::CObjectType::eFolder); // the second argument value is very discussible; needs to be reviewed;

	this->Property(TDrvSvcPropSet::e_name , _crt.Name());
	this->Property(TDrvSvcPropSet::e_type , _T("KERNEL_MODE_DRIVER"));
	this->Property(TDrvSvcPropSet::e_path , drv_path);
	this->Property(TDrvSvcPropSet::e_start, _T("Controlled by service;"));
	this->Property(TDrvSvcPropSet::e_state, _T("Service is not created;"));
	this->Property(TDrvSvcPropSet::e_desc , _crt.Description());
}

VOID     TDrvSvcPropSet::Update  (const CService& _svc) {
	CCfgToStr out_;
	const CCfg::qry_svc_cfg& cfg_ = _svc.Cfg().Data();
	this->Property(TDrvSvcPropSet::e_name , cfg_.szSericeName);
	this->Property(TDrvSvcPropSet::e_type , out_.PrintType(cfg_.dwServiceType));
	this->Property(TDrvSvcPropSet::e_path , cfg_.szBinaryPathName);
	this->Property(TDrvSvcPropSet::e_start, out_.PrintStart(cfg_.dwStartType));
	this->Property(TDrvSvcPropSet::e_state, out_.PrintState(_svc.Status().Data().dwCurrentState));
	this->Property(TDrvSvcPropSet::e_desc , _svc.Cfg_Ex().Description());
}

/////////////////////////////////////////////////////////////////////////////

TDrvSvcPropSet&  TDrvSvcPropSet::operator = (const TDrvSvcPropSet& _ref) {
	for (UINT i_ = 0; i_ < this->Count() && i_ < _ref.Count(); i_++)
		this->Property(i_, _ref.Property(i_));
	return *this;
}

TDrvSvcPropSet&  TDrvSvcPropSet::operator <<(const CService& _svc) { this->Update(_svc); return *this; }
TDrvSvcPropSet&  TDrvSvcPropSet::operator <<(const CCrtData& _crt) { this->Update(_crt); return *this; }