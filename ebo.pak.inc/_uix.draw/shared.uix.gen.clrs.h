#ifndef _SHAREDUIXGENCLRS_H_A1B4CB5B_C15C_4E5D_929D_D284CB99FB48_INCLUDED
#define _SHAREDUIXGENCLRS_H_A1B4CB5B_C15C_4E5D_929D_D284CB99FB48_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Nov-2007 at 9:57pm, GMT+3, Rostov-on-Don, Sunday;
	This is Sfx package owner-drawn colour related classe(s) declaration.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 2-May-2020 at 1:36:19p, UTC+7, Novosibirsk, Saturday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw {

	class CDebugPrint {
	public:
		 CDebugPrint  (void);
		 CDebugPrint  (const CDebugPrint&);
		~CDebugPrint  (void);
	public:
		CStringW Print(const COLORREF) const;
	public:
		CDebugPrint&   operator = (const CDebugPrint&);
		CStringW       operator <<(const COLORREF) const ; // Print() is called;
	};

	typedef Gdiplus::Color TColor;
	// https://en.wikipedia.org/wiki/RGB_color_space
	class CColour {
	protected:
		TColor m_clr;

	public:
		 CColour(void);
		 CColour(const CColour&);
		 CColour(const COLORREF clr, const BYTE alpha = eAlphaValue::eOpaque);
		 CColour(const TColor& );
		~CColour(void);

	public:
		BYTE     Alpha  (void) const;
		VOID     Alpha  (const eAlphaValue::_e = eAlphaValue::eSemitransparent);
		VOID     Create (const COLORREF clr, const BYTE _alpha = eAlphaValue::eOpaque);
		const
		TColor&  Data   (void) const;
		VOID     Empty  (void)      ;
		bool     Is     (void) const; // returns true if m_clr is not CLR_NONE;
#if defined(_DEBUG)
		CStringW Print  (void) const;
#endif
		COLORREF Rgb    (void) const;

	public:
		operator const TColor& (void) const;
		operator const TColor* (void) const;
		operator const COLORREF(void) const;

	public:
		CColour& operator = (const CColour &);
		CColour& operator <<(const COLORREF&);
		CColour& operator = (const TColor  &);
	};

	class CColour_Ex : public CColour {
	                  typedef CColour TBase;
	public:
		 CColour_Ex(void);
		 CColour_Ex(const CColour&);
		 CColour_Ex(const CColour_Ex&);
		 CColour_Ex(const COLORREF clr, const BYTE alpha = eAlphaValue::eOpaque);
		~CColour_Ex(void);

	public:
		HRESULT  Darken (const __int8 btValue);
		HRESULT  Lighten(const __int8 btValue);

	public:
		CColour_Ex& operator+= (const __int8 btValue);  // makes color light;
		CColour_Ex& operator-= (const __int8 btValue);  // makes color dark ;
		CColour_Ex& operator = (const CColour&);        // regular color object is assigned;
		CColour_Ex& operator = (const CColour_Ex&);
	};

	// https://en.wikipedia.org/wiki/Gradient
	// https://en.wikipedia.org/wiki/Color_gradient
	// https://docs.microsoft.com/en-us/windows/win32/api/wingdi/nf-wingdi-gradientfill
	// https://www.dictionary.com/browse/gradient
	class CGradient {
	protected:
		CColour   m_clr_0 ;          // the first color or the color which the gradient starts from;
		CColour   m_clr_1 ;          // the second color or the color which the gradient ends up;
		bool      m_b_vert;          // a direction of a gradient either vertical (default) or horizontal;

	public:
		 CGradient (void);
		 CGradient (const CColour& _from, const CColour& _to);
		 CGradient (const CGradient&);
		~CGradient (void);

	public:
		const bool  Is    (void) const; // returns true if one of the gradient colors is true;
		const bool  IsVert(void) const;
		      bool& IsVert(void)      ;
		const
		CColour& c0(void) const;     // gets first color (ro);
		CColour& c0(void)      ;     // gets first color (rw);
		const
		CColour& c1(void) const;     // gets second color (ro);
		CColour& c1(void)      ;     // gets second color (rw);

		const
		CColour& First (void) const;
		CColour& First (void)      ;
		const
		CColour& Second(void) const;
		CColour& Second(void)      ;

	public:
		CGradient& operator = (const CGradient&);
	};
}}

typedef ex_ui::draw::CColour     TColour;
typedef ex_ui::draw::CColour_Ex  TColourEx;
typedef ex_ui::draw::CGradient   TGradient;

typedef ex_ui::draw::CDebugPrint TDbgPrint;

#endif/*_SHAREDUIXGENCLRS_H_A1B4CB5B_C15C_4E5D_929D_D284CB99FB48_INCLUDED*/