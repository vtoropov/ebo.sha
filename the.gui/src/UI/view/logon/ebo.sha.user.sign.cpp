/*
	Created by Tech_dog (ebontrop@gmail.com) on 14-Nov-2020 at 2:42:02a, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha optima tool user sign-in sub-view interface implementation file;
*/
#include "StdAfx.h"
#include "ebo.sha.user.sign.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CLogon_Sign_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_tab  = IDC_EBO_SHA_LGN_SCN_TAB,
		};
	}; typedef CLogon_Sign_Ctl This_Ctl;

	class CLogon_Sign_Res {
	public:
		enum _res : WORD {
			e_res_tab_0_a  = IDR_EBO_SHA_LGN_SCN_TAB_0a,
			e_res_tab_0_n  = IDR_EBO_SHA_LGN_SCN_TAB_0n,
			e_res_tab_1_a  = IDR_EBO_SHA_LGN_SCN_TAB_1a,
			e_res_tab_1_n  = IDR_EBO_SHA_LGN_SCN_TAB_1n,
			e_res_tab_2_a  = IDR_EBO_SHA_LGN_SCN_TAB_2a,
			e_res_tab_2_n  = IDR_EBO_SHA_LGN_SCN_TAB_2n,
		};
	}; typedef CLogon_Sign_Res This_Res;

	class CLogon_Sign_Layout {
	private:
		CLogon_Sign& m_view;
		RECT         m_rect;

	public:
		 CLogon_Sign_Layout (CLogon_Sign& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CLogon_Sign_Layout (void) {}

	public:
		RECT GetTabsRect (void) const {
			RECT rc_tabs = m_rect;
			::InflateRect(&rc_tabs, -0x5, -0x5);
			return rc_tabs;
		}

		VOID   Update (void) {
			TTabCtrl& tabbed  = m_view.Tabbed(); tabbed.Layout() << this->GetTabsRect(); RECT rc_page = tabbed.Layout().Page();
			TVeinScan& scan = m_view.Scan(); scan.Window().MoveWindow(&rc_page);
			TVeinDrv&  drv  = m_view.Driver(); drv.Window().MoveWindow(&rc_page);
			TVeinStg&  stg  = m_view.Storage(); stg.Window().MoveWindow(&rc_page);
		}
	};

	class CLogon_Sign_Init {
	private:
		CLogon_Sign& m_view ;
		CError       m_error;

	public:
		 CLogon_Sign_Init (CLogon_Sign& _view) : m_view(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }
		~CLogon_Sign_Init (void) {}

	public:
		HRESULT   OnCreate  (void) { m_error << __MODULE__ << S_OK;
#pragma region __tab_create
			TTabCtrl& tabs = m_view.Tabbed();

			tabs.Format() = shared::Get_Format().Tabbed();
			tabs.Layout().Tabs().Gap() = 0;

			tabs.Tabs().Append(_T("")); // scan
			tabs.Tabs().Tab(0).Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_tab_0_n );
			tabs.Tabs().Tab(0).Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_tab_0_a );
			tabs.Tabs().Append(_T("")); // settings
			tabs.Tabs().Tab(1).Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_tab_1_n );
			tabs.Tabs().Tab(1).Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_tab_1_a );
			tabs.Tabs().Append(_T("")); // history
			tabs.Tabs().Tab(2).Layers().Images().Cache().Add(ex_ui::controls::CState::eNormal  , This_Res::e_res_tab_2_n );
			tabs.Tabs().Tab(2).Layers().Images().Cache().Add(ex_ui::controls::CState::eSelected, This_Res::e_res_tab_2_a );

			tabs.Create(m_view.Window(), CLogon_Sign_Layout(m_view).GetTabsRect(), This_Ctl::e_ctl_tab);
			tabs.ParentRenderer(NULL);

			tabs.Layout().Margins().Left() = tabs.Layout().Margins().Top() = tabs.Layout().Margins().Right() = tabs.Layout().Margins().Bottom() = 10;
#pragma endregion
			return m_error;
		}
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CLogon_Sign::CViewWnd:: CViewWnd(CLogon_Sign& _view) : TViewWnd(_view), m_view(_view), m_tabbed(_view) {}
CLogon_Sign::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CLogon_Sign::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	const RECT rc_ = Lay_(*this); rc_;

	CLogon_Sign_Init init_(m_view); HRESULT hr_ = init_.OnCreate();

	if (SUCCEEDED(hr_)) {
		const RECT rc_page = m_tabbed.Layout().Page(); rc_page;
		hr_ = m_scan.Create(m_tabbed.Window(), rc_page, false);
		if (SUCCEEDED(hr_))
			m_scan.Window().SetWindowTextW(_T("logon::vein::scan::subview"));
		else
			m_scan.Error().Show();
		if (SUCCEEDED(hr_)) {
			m_tabbed.Tabs().Active(0);
			((ITabEvents&)m_view).ITabEvent_OnSelect(0);
		}
		hr_ = m_driver.Create(m_tabbed.Window(), rc_page, false);
		if (SUCCEEDED(hr_))
			m_scan.Window().SetWindowTextW(_T("logon::vein::driver::subview"));
		else
			m_scan.Error().Show();
		hr_ = m_stg.Create(m_tabbed.Window(), rc_page, false);
		if (SUCCEEDED(hr_))
			m_stg.Window().SetWindowTextW(_T("logon::vein::storage::subview"));
		else
			m_stg.Error().Show();
	}

	return 0;
}

LRESULT   CLogon_Sign::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_stg.Destroy();
	m_scan.Destroy();
	m_driver.Destroy();
	m_tabbed.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CLogon_Sign::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CLogon_Sign_Layout(m_view).Update();

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CLogon_Sign:: CLogon_Sign (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CLogon_Sign::~CLogon_Sign (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CLogon_Sign::ITabEvent_OnSelect (const DWORD _tab_ndx) { _tab_ndx;
	TView::m_error << __MODULE__ << S_OK;
	m_wnd.m_scan.IsVisible(0 == _tab_ndx);
	m_wnd.m_driver.IsVisible(1 == _tab_ndx);
	m_wnd.m_stg.IsVisible(2 == _tab_ndx);
	return TView::m_error;
}

/////////////////////////////////////////////////////////////////////////////

TVeinDrv&  CLogon_Sign::Driver (void) { return m_wnd.m_driver; }
TVeinScan& CLogon_Sign::Scan   (void) { return m_wnd.m_scan  ; }
TVeinStg&  CLogon_Sign::Storage(void) { return m_wnd.m_stg   ; }
TTabCtrl&  CLogon_Sign::Tabbed (void) { return m_wnd.m_tabbed; }