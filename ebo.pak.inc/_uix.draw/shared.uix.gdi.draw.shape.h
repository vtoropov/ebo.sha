#ifndef _SHAREDCONTROLSCUSTOMDRWAINGUTILS_H_79B00661_02B5_4c2f_A018_6C481A4B9F15_INCLUDED
#define _SHAREDCONTROLSCUSTOMDRWAINGUTILS_H_79B00661_02B5_4c2f_A018_6C481A4B9F15_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-May-2012 at 9:57:09am, GMT+3, Rostov-on-Don, Friday;
	This is Pulsepay Shared Control Custom Drawing related class(es) declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack generic shape interface on 2-Jul-2018 at 3:21:50p, UTC+7, Phuket, Rawai, Monday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw { namespace shape
{
	class CCorner {
	protected:
		BYTE   m_radius;
	public:
		 CCorner (void);
		 CCorner (const BYTE _radius);
		 CCorner (const CCorner&);
		~CCorner (void);

	public:
		BYTE  Radius (void) const;
		BYTE& Radius (void)      ;

	public:
		CCorner& operator = (const CCorner&);
		CCorner& operator <<(const BYTE _radius);
	};
	typedef ::std::map<DWORD, CCorner>   TCorners; // key - is a corner marker of placement; value is corner object itself;
	// taking into account all objects that are used for data representation are displayed on rectangular screens, there is only four corners;
	class CCorners { public: enum _e : DWORD {
		eNone = 0x0, eTopLeft = 0x1, eTopRight = 0x2, eBottomRight = 0x4, eBottomLeft = 0x8, eAll = 0x1|0x2|0x4|0x8
	};
	private:
		TCorners  m_corners;
	public:
		 CCorners (void);
		 CCorners (const CCorners&);
		 CCorners (const DWORD _marker);
		~CCorners (void);
	public:
		HRESULT    Add (const DWORD _marker, const CCorner&); // a marker can be any, but draw function(s) will take care only the above enum;
		const
		CCorner&   Get (const _e)   const; // returns a corner, if it found, otherwize, fake corner is returned; read-only access, but is not drawn;
		CCorner&   Get (const _e)        ; // returns a corner, if it found, otherwize, fake corner is returned; read-write access, but is not drawn;
		const bool Has (const _e)   const; // checks for existing a corner by value provided;
		const bool Is  (void) const;       // returns true if at least one corner is set;
		const
		TCorners&  Raw (void) const;       // a reference to corner(s) which are included to corner map; read-only;
		TCorners&  Raw (void)      ;       // a reference to corner(s) which are included to corner map; read-write;
		HRESULT    Rem (const DWORD _marker);
		HRESULT    Set (const DWORD _marker, const BYTE _radius); // all corners will be added, those are existing, will be replaced;

	public:
		CCorners&  operator  =(const CCorners&);
		CCorners&  operator <<(const DWORD _marker); // a marker can be combined by different values of enumeration (above); it can include any value, however;
		CCorners&  operator +=(const _e);
		CCorners&  operator -=(const _e);
	};
	class CRectEdge { public: enum _e : DWORD {
		eNone = 0x0, eLeft = 0x1, eTop = 0x2, eRight = 0x4, eBottom = 0x8, eAll = eLeft|eTop|eRight|eBottom
	};};

	class CPosition {
	private:
		POINT   m_points[2];

	public:
		 CPosition (void);
		 CPosition (const CPosition&);
		~CPosition (void);

	public:
		VOID  Clear (void);
		const POINT& End   (void) const;
		      POINT& End   (void)      ;
		const bool   In    (const RECT&) const;   // returns true if points inside this rectangle; it's dependable on position orientation;
		const bool   IsHorz(void) const;          // returns true if points have the same value of y-coordinate;
		const bool   IsVert(void) const;          // returns true if points have the same value of x-coordinate;
		const DWORD  Length(void) const;
		const POINT& Start (void) const;
		      POINT& Start (void)      ;
	public:
		CPosition& operator = (const CPosition&);
		CPosition& operator <<(const RECT&);      // transfers rectangle to position as follows: start={x<<left|y<<top}, end={x<<right|y<<bottom};
	public:
		operator const RECT (void) const;         // transfers position to rectangle as follows: {left<<start.x|top<<start.y|right<<end.x|bottom<<end.y};
	};

	class CRectEx {
	private:
		Gdiplus::Rect  m_rc;
	public:
		 CRectEx (void);
		 CRectEx (const CRectEx& );
		~CRectEx (void);
	public:
		 CRectEx (const CPosition&);
		 CRectEx (const INT left_, const INT top_, const INT width_, const INT height_);
		 CRectEx (const POINT& _left_top, const POINT& _right_bottom);
		 CRectEx (const RECT& rc_);
		
	public:
		const
		Gdiplus::Rect&  Get (void) const;
		Gdiplus::Rect&  Get (void)      ;
		Gdiplus::Rect   Set (const CPosition&);
		Gdiplus::Rect   Set (const INT left_, const INT top_, const INT width_, const INT height_);
		Gdiplus::Rect   Set (const POINT& _left_top, const POINT& _right_bottom);
		Gdiplus::Rect   Set (const RECT&);

	public:
		operator Gdiplus::Rect*(void);
		operator Gdiplus::Rect&(void);
		operator const RECT (void) const;

	public:
		CRectEx& operator = (const CRectEx&);
		CRectEx& operator = (const RECT&);
		CRectEx& operator <<(const CPosition&);
	};

	class CRectFEx {
	private:
		Gdiplus::RectF m_rc;
	public:
		 CRectFEx(void);
		 CRectFEx(const CRectFEx&);
		 CRectFEx(const RECT& rc_);
		 CRectFEx(const INT left_, const INT top_, const INT width_, const INT height_);
		~CRectFEx(void);
	public:
		const
		Gdiplus::RectF& Get (void) const;
		Gdiplus::RectF& Get (void)      ;
		Gdiplus::RectF  Set (const CRectEx&);
		Gdiplus::RectF  Set (const INT left_, const INT top_, const INT width_, const INT height_);
		Gdiplus::RectF  Set (const RECT&);

	public:
		operator Gdiplus::RectF*(void);
		operator Gdiplus::RectF&(void);
		operator const RECT (void) const;

	public:
		CRectFEx& operator = (const CRectFEx&);
		CRectFEx& operator = (const RECT&);
	};

	typedef Gdiplus::GraphicsPath TPath;
	// actually, creating a rounded corner rectangle should seem to be easy, like Photoshop does it;
	// but, it's just an illusion: everything is a set of points, in this matrix;
	class CRounded {
	protected:
		BYTE     m_radius ; // deprecated: each corner may have its own radius;
		CCorners m_corners;
		RECT     m_shape  ; // outline rectangle;
		TPath    m_path;    // https://docs.microsoft.com/en-us/windows/win32/api/gdipluspath/nl-gdipluspath-graphicspath
		                    // https://docs.microsoft.com/en-us/windows/win32/api/gdipluspath/nf-gdipluspath-graphicspath-getpointcount
	public:
		 CRounded (void);
		 CRounded (const CRounded&);
		~CRounded (void);

	public:
		HRESULT   Create(void);       // deprecated: corner radius value cannot be less than 2 and more than outlet rectangle height / 3(three);
		HRESULT   Create(const CCorners&);
		bool      Is    (void) const; // returns true if graphics path is not empty;
		HRESULT   Reset (void);       // https://docs.microsoft.com/en-us/windows/win32/api/gdipluspath/nf-gdipluspath-graphicspath-reset

	public: // accessor(s);
		const
		CCorners& Corners (void) const  ;
		CCorners& Corners (void)        ;
		const
		RECT&     Outline (void) const  ;
		HRESULT   Outline (const RECT&) ;
		const                             // there is no obvious way to make a copy of the path provided, maybe this one is okay:
		TPath&    Path    (void) const  ; // https://docs.microsoft.com/en-us/windows/win32/api/gdipluspath/nf-gdipluspath-graphicspath-addpath
		HRESULT   Path    (const TPath&); // sets a path directly, actually, makes a clone from the original provided; may make inconsistency with other attributes;
		BYTE      Radius  (void) const  ; // gets a common/generic radius, actually, each corner may have its owe radius;
		HRESULT   Radius  (const BYTE ) ; // sets a common/generic radius value, all corners will be assigned to this value;

	public:
		CRounded& operator  = (const CRounded&);
		CRounded& operator << (const BYTE _radius);
		CRounded& operator << (const CCorners&   );
		CRounded& operator << (const RECT& _shape);
		CRounded& operator << (const TPath& _path);
	};
}}}

#endif/*_SHAREDCONTROLSCUSTOMDRWAINGUTILS_H_79B00661_02B5_4c2f_A018_6C481A4B9F15_INCLUDED*/