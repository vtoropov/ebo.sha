#ifndef _SYSSVCSERVICEENUM_H_465B5A5D_8ACC_4E41_87A2_E0DDBECAD982_INCLUDED
#define _SYSSVCSERVICEENUM_H_465B5A5D_8ACC_4E41_87A2_E0DDBECAD982_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Aug-2020 at 6:55:02p, UTC+7, Novosibirsk, Wednesday;
	This is Ebo Pack system service control management service enumerator interface declaration file.
*/
#include "sys.svc.error.h"
#include "sys.svc.service.h"
namespace shared { namespace service {

	class CEnum_Filter {
	public:
		enum _state : DWORD {
			e_state_active   = SERVICE_ACTIVE,
			e_state_inactive = SERVICE_INACTIVE
		};
		enum _type  : DWORD {
			e_drv_type       = SERVICE_DRIVER,
			e_win_32         = SERVICE_WIN32 ,
		};

	private:
		DWORD     m_state_mask;
		DWORD     m_type_mask ;
		CStringW  m_svc_name  ; // full or part of service name;

	public:
		 CEnum_Filter (void);
		~CEnum_Filter (void);

	public:
		LPCWSTR Name (void) const;
		VOID    Name (LPCWSTR)   ;
		DWORD   State(void) const;
		DWORD&  State(void)      ;
		DWORD   Type (void) const;
		DWORD&  Type (void)      ;

	};

	typedef ::std::vector<CService> TServices;

	// https://docs.microsoft.com/en-us/windows/win32/api/winsvc/nf-winsvc-enumservicesstatusw
	class CServiceEnum {
	private:
		CSvcError    m_error;
		TServices    m_list ;

	public:
		 CServiceEnum (void);
		~CServiceEnum (void);

	public:
		TErrorRef   Error (void) const;
		HRESULT     Get   (const CEnum_Filter&);
		const
		TServices&  List  (void) const;
		HRESULT     Reset (void)      ;
	};

}}
#endif/*_SYSSVCSERVICEENUM_H_465B5A5D_8ACC_4E41_87A2_E0DDBECAD982_INCLUDED*/