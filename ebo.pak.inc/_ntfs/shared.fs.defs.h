#ifndef _SHAREDFSCOMMONDEFS_H_B57D89F2_9784_471f_B070_06D8EBCA84E5_INCLUDED
#define _SHAREDFSCOMMONDEFS_H_B57D89F2_9784_471f_B070_06D8EBCA84E5_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Mar-2016 at 11:35:39am, GMT+7, Phuket, Rawai, Thursday;
	This is shared NTFS library common definitions declaration file.
	-----------------------------------------------------------------------------
	Adopted to v15 on 30-Jun-2018 at 01:06:00a, UTC+7, Phuket, Rawai, Monday;
	Adopted to Ebo Pack on 5-Sep-2019 at 06:08:00a, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.gen.sys.err.h"

#include <shlobj.h>
#include <shellapi.h>

#pragma comment(lib, "shell32.lib")
#ifndef _LONG_PATH
#define _LONG_PATH (_MAX_PATH + _MAX_DRIVE + _MAX_DIR + _MAX_FNAME + _MAX_EXT)
#endif
namespace shared { namespace ntfs
{
	typedef WIN32_FIND_DATA TFileInfo;

	typedef ::std::vector<CStringW > TDriveList ;
	typedef ::std::vector<CStringW > TFolderList;
	typedef ::std::vector<CStringW > TFileList  ;
	typedef ::std::vector<TFileInfo> TFolderItemCollection;

	interface IFileEnumCallback
	{
		virtual VOID IFileEnum_OnBegin(VOID)                  PURE;
		virtual VOID IFileEnum_OnFileAppend(const TFileList&) PURE;
		virtual VOID IFileEnum_OnFinish(VOID)                 PURE;
		virtual BOOL IFileEnum_OnInterruptRequest(VOID)       PURE;
	};

	HRESULT  FileSystem_CreateLongPath(LPCWSTR lpszPath, ::ATL::CStringW& _path);
	HRESULT  FileSystem_GetAttributes (LPCWSTR lpszPath, WIN32_FILE_ATTRIBUTE_DATA& _data);

	using shared::sys_core::CError;

	class CObjectType {
	public:
		enum _e {
			eUnknown    = 0,
			eFile       = 1,
			eFolder     = 2,
		};
	};

	/*
		***Important*** These are types of file system event for user mode application;
		system event types cannot be extended naturally by new types, which are supported by kernel mode mini-filter driver;
	*/
	class COperateType
	{
	public:
		enum _e {
			eUndefined   = 0x0,
			eFileChanged = FILE_ACTION_MODIFIED,
			eFileCreated = FILE_ACTION_ADDED   ,
			eFileDeleted = FILE_ACTION_REMOVED ,
			eFileRenamed = FILE_ACTION_RENAMED_OLD_NAME|FILE_ACTION_RENAMED_NEW_NAME
		};

		static const INT nTypes = COperateType::eFileRenamed;

	public:
		COperateType(void);
	public:
		static INT    Count(void);
		static INT    Index(const COperateType::_e);
		static  _e    Type (const INT nIndex);
	};

	class CGenericPath
	{
	protected:
		CStringW       m_path;

	public:
		 CGenericPath (void);
		 CGenericPath (const CGenericPath&)    ;
	explicit CGenericPath (LPCWSTR lpszPath);
	explicit CGenericPath (LPCWSTR lpszFile , LPCWSTR lpszFolder); // constructs object from file name and folder (used for JavaScript ext)
		~CGenericPath (void);

	public:                                               // initializes path from string of expacted type;
		                     /* TODO: must be reviewed */ // it is very discussible cutting off the file name from the path;
		const bool     BuildFrom (LPCWSTR lpszPath, const CObjectType::_e _expected);
		const bool     IsAbsolute(VOID)const;             // returns true if the path is absolute, otherwise, it is relative;
		const bool     IsFolder  (VOID)const;             // checks the type of the path against folder object (not file, link, etc.);
		const bool     IsEmpty   (VOID)const;             // if path is empty string, returns true;
		const bool     IsExist   (VOID)const;             // checks the path existence;
		const bool     Normalize (const CObjectType::_e); // normalizes the path to specified type, returns true on success, otherwise, false;
		CStringW       ToAbsolute(VOID)const;             // if the path is relative, converts to absolute one, otherwise returns the same path;
		TFolderList    ToFolders (VOID)const;             // splits directory part to folder names;
	
	public:
		static HRESULT ConvertDosToUnc(LPCWSTR pszDosPath, CStringW& _unc_result);
		static HRESULT ConvertUncToDos(LPCWSTR pszUncPath, CStringW& _dos_result);
		static HRESULT PathFromHandle (const ULONG _pid  , CStringW& _dos_result);

	public:
		operator       LPCWSTR     (void) const;

	public:
		CGenericPath&  operator= (const CGenericPath&);
		CGenericPath&  operator= (LPCWSTR);
	};

	class CBaseObject
	{
	protected:
		mutable
		CError            m_error;
		CGenericPath      m_path;
		CObjectType::_e   m_type;
	protected:
		CBaseObject(void);
		CBaseObject(const CBaseObject&);
		CBaseObject(const CObjectType::_e, LPCWSTR pszPath);
		virtual ~CBaseObject(void);
	public:
		TErrorRef         Error  (VOID)const;
		bool              IsValid(VOID)const;          // checks actual type of the object
		const
		CGenericPath&     Path   (VOID)const;
		CObjectType::_e   Type   (VOID)const;
		CStringW          UncName(VOID)const;          // very often it requires special permission to open a file

	public:
		CBaseObject& operator = (const CBaseObject&);
	};
}}

#endif/*_SHAREDFSCOMMONDEFS_H_B57D89F2_9784_471f_B070_06D8EBCA84E5_INCLUDED*/