#ifndef _SFXBUTTONCTRL_H_03EDB87E_7548_49BA_976D_0D9343C0D482_INCLUDED
#define _SFXBUTTONCTRL_H_03EDB87E_7548_49BA_976D_0D9343C0D482_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 09-Dec-2007 at 07:43:03pm, GMT+3, Rostov-on-Don, Sunday;
	This is ST owner drawn button control class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Sfx button control project on 15-Nov-2020 at 10:22:49a, UTC+7, Novosibirsk, Sunday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.base.rnd.h"

#include "sfx.button.fmt.h"
#include "sfx.button.lay.h"

namespace ST_Ctrls {

	using shared::sys_core::CError ;

	using ex_ui::controls::CBorders;
	using ex_ui::controls::IControlEvent;
	using ex_ui::controls::TCtrlState;
	using ex_ui::controls::IRenderer_Ex;

	using ex_ui::draw::defs::IRenderer;

	class CButton_State : public TCtrlState { typedef TCtrlState TState;
	private:
		bool    m_highlighted;   // if true button tries to attract attention by its different display in comparison with others;

	public:
		 CButton_State (void);
		 CButton_State (const CButton_State&);
		~CButton_State (void);

	public:
		bool    Highlighted (void) const;
		bool&   Highlighted (void)      ;

	public:
		CButton_State& operator = (const CButton_State&);
	};

	class CButton {
	protected:
		IControlEvent&
		               m_evt_snk;
		HANDLE         m_wnd_ptr;
		CError         m_error  ;
		UINT           m_ctrl_id;
		CBorders       m_borders; // these borders are used for setting layout data such as border's start and end points, also thickness is taken into account;
		TButtonFmt     m_format ;
		TButtonLay     m_layout ;
		CButton_State  m_state  ;

	public:
		 CButton (IControlEvent& _evt_snk);
		~CButton (void);

	public:
		HRESULT        Create  (const HWND hParent, const RECT& _rc_area, const UINT _ctrl_id);
		HRESULT        Destroy (void)      ;

	public:
		const
		CBorders&      Borders (void) const;  // gets button frame (read-only);
		CBorders&      Borders (void)      ;  // gets button frame (read-write);
		TErrorRef      Error   (void) const;
		IControlEvent& Events  (void) const;
		IControlEvent& Events  (void)      ;
		const
		TButtonFmt&    Format  (void) const;
		TButtonFmt&    Format  (void)      ;
		const UINT     ID      (void) const;
		const
		TButtonLay&    Layout  (void) const;
		TButtonLay&    Layout  (void)      ;
		HRESULT        Renderer(IRenderer_Ex*  const ); // parent window renderer pointer; i.e. parent renderer;
		IRenderer_Ex*  Renderer(void) const;
		HRESULT        Refresh (void)      ;
		const
		CButton_State& State   (void) const;
		CButton_State& State   (void)      ;
		LPCWSTR        Text    (void) const;   // gets button text;
		HRESULT        Text    (LPCWSTR)   ;   // sets button text; this is a shortcut for this->Layout().Layers().Text() << {some_text};
		HRESULT        Tips    (LPCWSTR)   ;   // sets/removes tooltips of the button;
		CWindow        Window  (void) const;

	private: // non-copyable;
		CButton (const CButton&);
		CButton& operator = (const CButton&);
	};
}

typedef ST_Ctrls::CButton        TButtonCtrl;
typedef ST_Ctrls::CButton_State  TButtonState;

#endif/*_SFXBUTTONCTRL_H_03EDB87E_7548_49BA_976D_0D9343C0D482_INCLUDED*/