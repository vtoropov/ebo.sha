#ifndef _EBOSHAGPSDRV_H_8D70F229_31B7_42E1_9103_42A65C552D5A_INCLUDED
#define _EBOSHAGPSDRV_H_8D70F229_31B7_42E1_9103_42A65C552D5A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Nov-2020 at 9:32:16p, UTC+7, Novosibirsk, Tuesday;
	This is Ebo Sha optima tool fake GPS driver sub-view interface declaration file.
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.gps.device.h"
#include "ebo.sha.gps.pos.h"
#include "ebo.sha.gps.conn.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CFake_Drv : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CFake_Drv;
		private:
			CFake_Drv&  m_view   ;
			TTabCtrl    m_tabbed ;
			TFakeDev    m_device ;
			TFakePos    m_pos    ;
			TFakeConn   m_connect;

		public:
			 CViewWnd (CFake_Drv&);
			~CViewWnd (void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CFake_Drv (void);
		~CFake_Drv (void);

#pragma warning(disable:4481)
	private:    // ITabEvents
		HRESULT    ITabEvent_OnSelect (const DWORD _tab_ndx) override sealed;
#pragma warning(default:4481)
	public:
		TFakeConn& Connect (void);
		TFakeDev&  Device  (void);
		TFakePos&  Position(void);
		TTabCtrl&  Tabbed  (void);
	};

}}}}

typedef ebo::sha::gui::view::CFake_Drv   TFakeDrv;

#endif/*_EBOSHAGPSDRV_H_8D70F229_31B7_42E1_9103_42A65C552D5A_INCLUDED*/