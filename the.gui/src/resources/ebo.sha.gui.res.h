/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-May-2020 at 6:29:47p, UTC+7, Novosibirsk, Thursday;
	This is Ebo Xor Java Eclipse build tool client app resource identifier declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Shared optimization tool project on 26-Aug-2020 at 6:11:59a, UTC+7, Novosibirsk, Wednesday;
*/

#pragma region __generic

#define IDR_EBO_SHA_ICO                1001

#define IDC_EBO_SHA_STR_CTL            1003
#define IDC_EBO_SHA_STA_CTL            1005
#define IDR_EBO_SHA_STA_IMG            IDC_EBO_SHA_STA_CTL

#define IDC_EBO_SHA_BAN_CTL            1007
#define IDR_EBO_SHA_BAN_IMG            IDC_EBO_SHA_BAN_CTL

#define IDC_EBO_SHA_UPDATE_LAYOUT      1009

#pragma endregion

#pragma region __info

#define IDR_EBO_SHA_PAN_INF_E          1013
#define IDR_EBO_SHA_PAN_INF_I          1014
#define IDR_EBO_SHA_PAN_INF_W          1015
#define IDR_EBO_SHA_PAN_INF_U          1016

#pragma endregion

#pragma region __strip_bar

#define IDC_EBO_SHA_STR_CMD_0          1111  // sweet home;
#define IDC_EBO_SHA_STR_CMD_1          1115  // cool settings;
#define IDC_EBO_SHA_STR_CMD_2          1119  // desired help;
#define IDC_EBO_SHA_STR_CMD_3          1123  // fkn logon;
#define IDC_EBO_SHA_STR_CMD_4          1127  // fake GPS;
#define IDC_EBO_SHA_STR_CMD_5          1131  // process blocker; HTTP/TCP/IP blocker;
#define IDC_EBO_SHA_STR_CMD_6          1135  // media;

#define IDS_EBO_SHA_STR_CMD_TIP_0      IDC_EBO_SHA_STR_CMD_0
#define IDS_EBO_SHA_STR_CMD_TIP_1      IDC_EBO_SHA_STR_CMD_1
#define IDS_EBO_SHA_STR_CMD_TIP_2      IDC_EBO_SHA_STR_CMD_2
#define IDS_EBO_SHA_STR_CMD_TIP_3      IDC_EBO_SHA_STR_CMD_3
#define IDS_EBO_SHA_STR_CMD_TIP_4      IDC_EBO_SHA_STR_CMD_4
#define IDS_EBO_SHA_STR_CMD_TIP_5      IDC_EBO_SHA_STR_CMD_5
#define IDS_EBO_SHA_STR_CMD_TIP_6      IDC_EBO_SHA_STR_CMD_6

#define IDR_EBO_SHA_STR_DRK_B          1110
#define IDR_EBO_SHA_STR_DRK_0_n        IDC_EBO_SHA_STR_CMD_0
#define IDR_EBO_SHA_STR_DRK_0_a        1112
#define IDR_EBO_SHA_STR_DRK_0_g        1113
#define IDR_EBO_SHA_STR_DRK_1_n        IDC_EBO_SHA_STR_CMD_1
#define IDR_EBO_SHA_STR_DRK_1_a        1116
#define IDR_EBO_SHA_STR_DRK_1_g        1117
#define IDR_EBO_SHA_STR_DRK_2_n        IDC_EBO_SHA_STR_CMD_2
#define IDR_EBO_SHA_STR_DRK_2_a        1120
#define IDR_EBO_SHA_STR_DRK_2_g        1121
#define IDR_EBO_SHA_STR_DRK_3_n        IDC_EBO_SHA_STR_CMD_3
#define IDR_EBO_SHA_STR_DRK_3_a        1124
#define IDR_EBO_SHA_STR_DRK_4_n        IDC_EBO_SHA_STR_CMD_4
#define IDR_EBO_SHA_STR_DRK_4_a        1128
#define IDR_EBO_SHA_STR_DRK_4_g        1129
#define IDR_EBO_SHA_STR_DRK_5_n        IDC_EBO_SHA_STR_CMD_5
#define IDR_EBO_SHA_STR_DRK_5_a        1132
#define IDR_EBO_SHA_STR_DRK_5_g        1133
#define IDR_EBO_SHA_STR_DRK_6_n        IDC_EBO_SHA_STR_CMD_6
#define IDR_EBO_SHA_STR_DRK_6_a        1136
#define IDR_EBO_SHA_STR_DRK_6_g        1137

#pragma endregion

#pragma region __sel_overlap

#define IDR_EBO_SHA_SEL_OVR_0          1201
#define IDR_EBO_SHA_SEL_OVR_1          1203
#define IDR_EBO_SHA_SEL_OVR_2          1205
#define IDR_EBO_SHA_SEL_OVR_3          1207
#define IDR_EBO_SHA_SEL_OVR_4          1209
#define IDR_EBO_SHA_SEL_OVR_5          1211
#define IDR_EBO_SHA_SEL_OVR_6          1213
#define IDR_EBO_SHA_SEL_OVR_7          1215
#define IDR_EBO_SHA_SEL_OVR_8          1216

#pragma endregion

#pragma region __logon

#define IDC_EBO_SHA_LGN_SEL            1301
#define IDR_EBO_SHA_SEL_IMG_L          IDC_EBO_SHA_LGN_SEL

#define IDR_EBO_SHA_SEL_FNG_01_a       1302
#define IDR_EBO_SHA_SEL_FNG_01_n       1303
#define IDR_EBO_SHA_SEL_FNG_02_a       1304
#define IDR_EBO_SHA_SEL_FNG_02_n       1305

#define IDC_EBO_SHA_LGN_SCN_TAB        1331
#define IDR_EBO_SHA_LGN_SCN_TAB_0a     IDC_EBO_SHA_LGN_SCN_TAB
#define IDR_EBO_SHA_LGN_SCN_TAB_0n     1333

#define IDR_EBO_SHA_LGN_SCN_TAB_1a     1335
#define IDR_EBO_SHA_LGN_SCN_TAB_1n     1336

#define IDR_EBO_SHA_LGN_SCN_TAB_2a     1337
#define IDR_EBO_SHA_LGN_SCN_TAB_2n     1338

#define IDR_EBO_SHA_FNG_SCN_M          1339
#define IDR_EBO_SHA_FNG_SCN_Y          1340
#define IDR_EBO_SHA_FNG_DRV_M          1341
#define IDR_EBO_SHA_FNG_DRV_LOW_A      1342
#define IDR_EBO_SHA_FNG_DRV_LOW_D      1343
#define IDR_EBO_SHA_FNG_STG_S          1345
#define IDR_EBO_SHA_FNG_STG_M          1347

#pragma endregion

#pragma region __fake

#define IDC_EBO_SHA_FAK_SEL            1401
#define IDR_EBO_SHA_FAK_SEL_OVR_0      IDC_EBO_SHA_FAK_SEL

#define IDR_EBO_SHA_SEL_ITM_01_a       1417
#define IDR_EBO_SHA_SEL_ITM_01_n       1418
#define IDR_EBO_SHA_SEL_ITM_02_a       1419
#define IDR_EBO_SHA_SEL_ITM_02_n       1420

#define IDR_EBO_SHA_SEL_LST_01         1431

#define IDR_EBO_SHA_FAK_DRV_TAB        1461
#define IDR_EBO_SHA_FAK_DRV_TAB_0a     1463
#define IDR_EBO_SHA_FAK_DRV_TAB_0n     1464
#define IDR_EBO_SHA_FAK_DRV_TAB_1a     1465
#define IDR_EBO_SHA_FAK_DRV_TAB_1n     1466
#define IDR_EBO_SHA_FAK_DRV_TAB_2a     1467
#define IDR_EBO_SHA_FAK_DRV_TAB_2n     1468

#define IDC_EBO_SHA_FAK_DEV_CAP_0      1473
#define IDC_EBO_SHA_FAK_DEV_LBL_0      1475
#define IDC_EBO_SHA_FAK_DEV_LBL_1      1477
#define IDC_EBO_SHA_FAK_DEV_EDT_0      1479
#define IDC_EBO_SHA_FAK_DEV_EDT_1      1481

#define IDC_EBO_SHA_BUT_REF_H          1483

#define IDR_EBO_SHA_NOU_GAT_M          1485
#define IDR_EBO_SHA_NOU_GAT_L          1486
#define IDR_EBO_SHA_PUZ_AUT_M          1487

#pragma endregion

#define IDR_EBO_SHA_GEN_VIEW_BRK_L     1501
#define IDR_EBO_SHA_GEN_VIEW_BRK_M     1502

#pragma region __media

#define IDC_EBO_SHA_MED_SEL            1601
#define IDR_EBO_SHA_SEL_MED_OVR        1602
#define IDR_EBO_SHA_SEL_MED_LST        1603
#define IDR_EBO_SHA_SEL_MED_01_a       1604
#define IDR_EBO_SHA_SEL_MED_01_n       1605

#define IDC_EBO_SHA_MED_POD_TAB        1616
#define IDR_EBO_SHA_MED_POD_TAB_0a     1617
#define IDR_EBO_SHA_MED_POD_TAB_0n     1618

#define IDR_EBO_SHA_MED_POD_DEV        1626

#pragma endregion

#pragma region __block

#define IDC_EBO_SHA_BLK_SEL            1701
#define IDR_EBO_SHA_BLK_SEL_OVR_B      1702
#define IDR_EBO_SHA_BLK_SEL_OVR_D      1703
#define IDR_EBO_SHA_SEL_BLK_LST        1704
#define IDR_EBO_SHA_SEL_BLK_01_a       1705
#define IDR_EBO_SHA_SEL_BLK_01_n       1706
#define IDR_EBO_SHA_SEL_BLK_02_a       1707
#define IDR_EBO_SHA_SEL_BLK_02_n       1708

#define IDC_EBO_SHA_BLK_WEB_TAB        1716
#define IDR_EBO_SHA_BLK_WEB_TAB_0a     1717
#define IDR_EBO_SHA_BLK_WEB_TAB_0n     1718
#define IDR_EBO_SHA_BLK_WEB_TAB_1a     1719
#define IDR_EBO_SHA_BLK_WEB_TAB_1n     1720

#define IDC_EBO_SHA_BLK_PRC_TAB        1750
#define IDR_EBO_SHA_BLK_PRC_TAB_0a     1751
#define IDR_EBO_SHA_BLK_PRC_TAB_0n     1752
#define IDR_EBO_SHA_BLK_PRC_TAB_1a     1753
#define IDR_EBO_SHA_BLK_PRC_TAB_1n     1754

#define IDR_EBO_SHA_BLK_DRV_MOD_M      1801
#define IDR_EBO_SHA_BLK_DRV_MOD_L      1802

#define IDS_EBO_SHA_BLK_PAN_INF_E      1806
#define IDS_EBO_SHA_BLK_PAN_INF_I      1807
#define IDS_EBO_SHA_BLK_PAN_INF_W      1808


#pragma endregion

#pragma region __trace

#define IDR_EBO_UM_TEST_TRC_DN_N       1821
#define IDR_EBO_UM_TEST_TRC_DN_O       1823
#define IDR_EBO_UM_TEST_TRC_UP_N       1825
#define IDR_EBO_UM_TEST_TRC_UP_O       1827
#define IDR_EBO_UM_TEST_TRC_IMGS       1829

#pragma endregion


