#ifndef _FILTERIPCTRL_H_8EFB1C80_4C41_43E2_A910_C65EC93960B8_INCLUDED
#define _FILTERIPCTRL_H_8EFB1C80_4C41_43E2_A910_C65EC93960B8_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 3-Aug-2020 at 4:13:55p, UTC+7, Novosibirsk, Monday;
	This is SSH Tunnel WFP filter IP address control management interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 8:09:37.300 pm, UTC+7, Novosibirsk, Wednesday;
*/
#include "filter.dev.ext.h"

namespace ssh { namespace driver {

	class CAddress {
	public:
		static ULONG    Fake (const IN6_ADDR* _from);
		static NTSTATUS Fill (const ADDR_THUNK& _by , SSH_TNL_ADDR& _to);
		static NTSTATUS Fill (const ADDR_THUNK& _src, const ADDR_THUNK& _dest, t_raw_notify* _to);
	public:
		static NTSTATUS Reset(ADDR_THUNK&);
	};

	class CPorts {
	public:
		class CDestination {
		public:
			static INT  IsAllowed(const USHORT  _u_port);
			static VOID Set      (const USHORT* _p_ports, const USHORT _count);
		};
	public:
		class CSource {
		public:
			static INT  IsAllowed(const USHORT  _u_port);
			static VOID Set      (const USHORT* _p_ports, const USHORT _count);
		};
	};

	class CRanges {
	public:
		static CAction Get(const ULONG _u_ip);
		static const IP_RANGE* In(const IP_RANGE* _p_ranges, const INT _count, const ULONG _ip);
		static VOID  Set(const IP_RANGES* _p_ranges, const INT _n_block);
	};

}}

#endif/*_FILTERIPCTRL_H_8EFB1C80_4C41_43E2_A910_C65EC93960B8_INCLUDED*/