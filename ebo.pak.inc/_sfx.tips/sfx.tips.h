#ifndef __STCTRLSCOMMONTOOLTIPHELPER_H_
#define __STCTRLSCOMMONTOOLTIPHELPER_H_
/*
	Created by Tech_dog (ebontrop@gmail.com) on 8-Oct-2009 at 1:16:21am, GMT+3, Rostov-on-Don, Thursday;
	This is system tool tip wrap helper class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on 8-May-2020 at 3:06:50a, UTC+7, Novosibirsk, Friday;
*/
namespace ST_Common
{
	class CToolTipHelper
	{
	public:
		static const int MAX_TIP_LEN = 80;
		static const int DEF_MULTILINE_WIDTH = 350;
	public:
		interface ICallback
		{
			virtual LPCTSTR OnToolTipTextRequest(void) = 0;
		};
	private:
		bool               m__bUpdated;
		HWND               m__hOwner;
		WTL::CToolTipCtrl  m__cToolTip;
		ICallback&         m__cSink;
		bool               m__bMultiAuto;   // automatic support of multiline tool-tip
		int                m__nWidth;       // width of multiline tooltip
	public:
		 CToolTipHelper(CToolTipHelper::ICallback&);
		~CToolTipHelper(void);
	public:
		BEGIN_MSG_MAP(CToolTipHelper)
		/*-----------------------------------------------*/
		NOTIFY_CODE_HANDLER(TTN_GETDISPINFO, OnGetDispInfo)
		/*-----------------------------------------------*/
		END_MSG_MAP()
		LRESULT  OnGetDispInfo(int idCtrl, LPNMHDR lpnmHdr, BOOL& bHandled);
	public:
		HRESULT  Create (const HWND hOwner);
		void     Destroy(void);
		bool     IsValid(void) const;
		bool     Reset  (void);
		void     SetMultiLineAuto(const bool, const int nWidth = CToolTipHelper::DEF_MULTILINE_WIDTH);
		bool     Update (void);
	private:
		void   __RelayTooltip(void);
	};
}
typedef ST_Common::CToolTipHelper TToolTipHelper;
#endif/*__STCTRLSCOMMONTOOLTIPHELPER_H_*/