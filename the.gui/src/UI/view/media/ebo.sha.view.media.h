#ifndef _EBOSHAVIEWMEDIA_H_1C36012B_4FE0_40CB_95E9_7FA91D138C09_INCLUDED
#define _EBOSHAVIEWMEDIA_H_1C36012B_4FE0_40CB_95E9_7FA91D138C09_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Dec-2020 at 11:26:08p, UTC+7, Novosibirsk, Sunday;
	This is Ebo Sha optima tool media tools' view interface declaration file.
*/
#include "ebo.sha.view.bas.h"
#include "ebo.sha.media.ipod.h"

namespace ebo { namespace sha { namespace gui { namespace view {

	class CMedia : public CView_Base { typedef CView_Base TView;
	private:
		class CViewWnd : public CViewWnd_Base { typedef CViewWnd_Base TViewWnd; friend class CMedia;
		private:
			CMedia&   m_view  ;
			TSelect   m_select;
			TiPodView m_ipod  ;

		public:
			 CViewWnd (CMedia&);
			~CViewWnd (void);

		private: // TPane
#pragma warning (disable: 4481)
			LRESULT   OnCreate  (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnDestroy (UINT, WPARAM, LPARAM, BOOL&) override sealed;
			LRESULT   OnSize    (UINT, WPARAM, LPARAM, BOOL&) override sealed;
#pragma warning (default: 4481)
		};
	private:
		CViewWnd   m_wnd;

	public:
		 CMedia (void);
		~CMedia (void);

	public:
		TiPodView& iPod    (void) ;
		TSelect&   Select  (void) ;
	};

}}}}

typedef ebo::sha::gui::view::CMedia TMediaView;

#endif/*_EBOSHAVIEWMEDIA_H_1C36012B_4FE0_40CB_95E9_7FA91D138C09_INCLUDED*/