/*
	Created by Tech_dog (ebontrop@gmail.com) on 21-Nov-2020 at 3:33:33.211 pm, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha optima tool vein driver sub-view interface implementation file;
*/
#include "StdAfx.h"
#include "ebo.sha.vein.drv.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;
using namespace ex_ui::controls;

#include "shared.gui.page.layout.h"
using namespace shared::gui::layout;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CVein_Drv_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_cap_0  = 0, // not defined yet;
		};
	}; typedef CVein_Drv_Ctl This_Ctl;

	class CVein_Drv_Res {
	public:
		enum _res : WORD {
			e_none     = 0,
			e_logo_drv = IDR_EBO_SHA_FNG_DRV_M ,
		};
	}; typedef CVein_Drv_Res This_Res;

	class CVein_Drv_Layout {
	public:
		typedef TCtrlDefLayout _pos;
	private:
		CVein_Drv&  m_view;
		RECT         m_rect;

	public:
		 CVein_Drv_Layout (CVein_Drv& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		~CVein_Drv_Layout (void) {}

	public:
		RECT   GetEmpty(const DWORD _ndx) { _ndx; RECT rc_empty = m_rect; rc_empty.top = GetImage(0).bottom; return rc_empty; }
		RECT   GetImage(const DWORD _ndx) {
			static const SIZE sz_logo = { 80, 80 };
			RECT rc_logo = {
				0, (0 == _ndx ? 0 : _pos::e_avt_shift), 0 + sz_logo.cx, (0 == _ndx ? sz_logo.cy : _pos::e_avt_shift + sz_logo.cy)
			};
			return rc_logo;
		}
		RECT   GetTitle(const DWORD _ndx) { return this->_get_cap_rect(_ndx); }

	public:
		VOID   Update  (TCapComp* _p_caps, const DWORD _d_count) {
			if (NULL == _p_caps || 0 == _d_count)
				return;
			for (DWORD i_ = 0; i_ < _d_count; i_++)
				_p_caps[i_].Title().Layout().Update(this->_get_cap_rect(i_));
		}

	private:
		RECT  _get_cap_rect(const DWORD _ndx) {
			RECT rc_cap = m_rect; rc_cap.top = (0 ==_ndx ? 0 : _pos::e_cap_shift); rc_cap.bottom = rc_cap.top + _pos::e_cap_height;
			return rc_cap;
		}
	};

	class CVein_Drv_Init {
	private:
		CVein_Drv&  m_view ;
		CError       m_error;

	public:
		 CVein_Drv_Init (CVein_Drv& _view) : m_view(_view) { m_error << __MODULE__ << S_OK >> __MODULE__; }
		~CVein_Drv_Init (void) {}

	public:
		HRESULT   OnCreate  (void) {
			m_error << __MODULE__ << S_OK; return m_error;
		}
	};
}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CVein_Drv::CViewWnd:: CViewWnd(CVein_Drv& _view) : TViewWnd(_view), m_view(_view) {}
CVein_Drv::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   CVein_Drv::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	CVein_Drv_Layout lay_(m_view);
	CVein_Drv_Init init_(m_view); init_.OnCreate();

	HRESULT hr_ = m_caps[0].Create(*this, _T("Driver Vendor"), lay_.GetTitle(0), lay_.GetImage(0));
	if (SUCCEEDED(hr_)) {
		m_caps[0].Format(This_Res::e_logo_drv);
		m_caps[0].Image().Format().Bkgnd().Sectors().Append(shared::Get_Theme().Get(TThemePart::e_caption,  TThemeElement::e_back), TTileSize(80, 30));
		m_caps[0].Image().Format().Bkgnd().Sectors().Append(shared::Get_Format().Panel().Bkgnd().Solid(), TTileSize(80, 50));
		m_caps[0].Image().Layout().Update();
	}

	hr_ = m_empty.Create(*this, lay_.GetEmpty(0), true);

	return 0;
}

LRESULT   CVein_Drv::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
	m_empty.Destroy();
	m_caps[0].Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   CVein_Drv::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CVein_Drv_Layout layout_(m_view); layout_.Update(m_caps, _countof(m_caps));
	m_empty.Update(layout_.GetEmpty(0));

	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

CVein_Drv:: CVein_Drv (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
CVein_Drv::~CVein_Drv (void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CVein_Drv::Refresh (void) {
	TView::m_error << __MODULE__ << S_OK;
	return TView::Refresh();
}