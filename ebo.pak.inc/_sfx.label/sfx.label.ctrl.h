#ifndef __PULSEPAYSHAREDLABELCONTROL_H_AF399AA9_FD03_4204_893D_E15889DBC987_INCLUDED
#define __PULSEPAYSHAREDLABELCONTROL_H_AF399AA9_FD03_4204_893D_E15889DBC987_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-May-2012 at 1:08:33pm, GMT+3, Rostov-on-Don, Thursday;
	This is Pulsepay Shared Label Control class declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Sfx label control project on 24-Nov-2020 at 7:35:50.221 am, UTC+7, Novosibirsk, Tuesday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.base.rnd.h"
#include "sfx.label.fmt.h"
#include "sfx.label.lay.h"

namespace ST_Ctrls {

	using shared::sys_core::CError ;

	using ex_ui::controls::CBorders;
	using ex_ui::controls::IRenderer_Ex;

	using ex_ui::draw::defs::IRenderer;

	class CLabel {
	protected:
		HANDLE        m_wnd_ptr;
		CError        m_error  ;
		UINT          m_ctrl_id;
		CBorders      m_borders;
		TLabelFmt     m_format ;
		TLabelLay     m_layout ;

	public:
		 CLabel (void);
		~CLabel (void);

	public:
		HRESULT       Create  (const HWND hParent, const RECT& _rc_area, LPCWSTR _lp_sz_text, const UINT _ctrl_id = IDC_STATIC);
		HRESULT       Destroy (void)      ;

	public:
		const
		CBorders&     Borders (void) const;  // gets picture frame (read-only);
		CBorders&     Borders (void)      ;  // gets picture frame (read-write);
		TErrorRef     Error   (void) const;
		const
		TLabelFmt&    Format  (void) const;
		TLabelFmt&    Format  (void)      ;
		const
		TLabelLay&    Layout  (void) const;
		TLabelLay&    Layout  (void)      ;
		HRESULT       Refresh (void)      ;
		HRESULT       Renderer(IRenderer_Ex* const _p_parent); // sets parent render; usual parent window render;
		IRenderer_Ex* Renderer(void) const;                    // gets render implementation for use as a parent one in overlap control(s);
		LPCWSTR       Text    (void) const;
		HRESULT       Text    (LPCWSTR)   ;
		CWindow       Window  (void) const;

	private: // non-copyable;
		CLabel (const CLabel&);
		CLabel& operator = (const CLabel&);
	};
}

typedef ST_Ctrls::CLabel  TLabelCtrl;

#endif/*__PULSEPAYSHAREDLABELCONTROL_H_AF399AA9_FD03_4204_893D_E15889DBC987_INCLUDED*/