#ifndef _FILTERDEFS_H_90AD335F_799E_4DB9_B606_07726D7FE649_INCLUDED
#define _FILTERDEFS_H_90AD335F_799E_4DB9_B606_07726D7FE649_INCLUDED
/*
	Original code base copyright (C) 2004-2005 Cory Nelson;
	-----------------------------------------------------------------------------
	Adopted to SSH Tunnel driver WFP filter on 2-Aug-2020 at 12:25:25p, UTC+7, Novosibirsk, Sunday;
	Adopted to Ebo Sha Optima Tool project on 13-Jan-2021 at 8:09:37.273 pm, UTC+7, Novosibirsk, Wednesday;
*/

//
//	defines the various device type values;
//	range 0-0x7FFF(32767) is reserved values for MS;
//	range 0x8000(32768)-0xFFFF(65535) is acceptable for use in projects;
//

#define FILE_DEVICE_SSH_TNL  0xEEEE

//
//	macro definition for defining IOCTL and FSCTL function control codes;
//	0-0x7FF(2047) function codes are reserved for MS;
//	0x800(2048)-0xFFF(4095) are acceptable for use in projects;
//

#define SSH_TNL_IOCTL_BASE   0x0D0D

#ifdef DEFINE_GUID
#undef DEFINE_GUID
#endif
#define DEFINE_GUID(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8) \
        EXTERN_C const GUID DECLSPEC_SELECTANY name \
            = { l, w1, w2, { b1, b2, b3, b4, b5, b6, b7, b8 }}

#define PROPERTY_SET   FILE_WRITE_DATA
#define PROPERTY_GET   FILE_READ_DATA
//
//	device driver IOCTLs;
//
#define CTL_CODE_SSH_TLN(i,a)    CTL_CODE(FILE_DEVICE_SSH_TNL, SSH_TNL_IOCTL_BASE + (i), METHOD_BUFFERED, (a))

#define IOCTL_SSH_TNL_HOOK       CTL_CODE_SSH_TLN(0, PROPERTY_SET)
#define IOCTL_SSH_TNL_SET_CMD    CTL_CODE_SSH_TLN(1, PROPERTY_SET)
#define IOCTL_SSH_TNL_GET_NOTIFY CTL_CODE_SSH_TLN(2, PROPERTY_GET)

// the names below can be virtual and actually may not coincident with phisycal name of the driver on its service;

#define DEVICE_NAME_NT    _T("\\Device\\ssh_filter")
#define DEVICE_NAME_WIN32 _T("\\\\.\\ssh_filter")
#define DEVICE_NAME_DOS   _T("\\DosDevices\\ssh_filter")

#include <ws2ipdef.h>

// https://docs.microsoft.com/en-us/windows/win32/api/ws2def/ns-ws2def-sockaddr
typedef union {
	SOCKADDR     addr ;
	SOCKADDR_IN  addr4;
	SOCKADDR_IN6 addr6;
} SSH_TNL_ADDR;

typedef struct tag_addr_thunk {
	IN6_ADDR* _addr6  ;
	USHORT    _port   ;
	ULONG     _addr   ;
} ADDR_THUNK;

typedef struct tag_ip_range {
	ULONG start;
	ULONG   end;
} IP_RANGE;

typedef struct tag_ip_ranges{
	ULONG    block    ; // 1 = block, 0 = allow.
	ULONG    labelsid ;
	ULONG    count    ;
	IP_RANGE ranges[1];
} IP_RANGES;

typedef struct tag_command {
	ULONG cmd_id;
	WCHAR cmd_desc[256];
} SSH_TNL_COMMAND;

#pragma pack(push, 1)

typedef union TAG_FAKEV6ADDR {
	IN6_ADDR    addr6;
	struct {
		UINT    prefix; // 0x00000120
		UINT    server;
		USHORT  flags;
		USHORT  clientport;
		UINT    clientip;
	} teredo;
	struct {
		USHORT  prefix; // 0x0220
		UINT    clientip;
		USHORT  subnet;
		unsigned __int64 address;
	} sixtofour;
} FAKEV6ADDR;

#pragma pack(pop)

#endif/*_FILTERDEFS_H_90AD335F_799E_4DB9_B606_07726D7FE649_INCLUDED*/