#ifndef _SHAREDGENERICCONOBJECT_H_4D74D09A_4FA1_4d03_9401_4D064A3AE294_INCLUDED
#define _SHAREDGENERICCONOBJECT_H_4D74D09A_4FA1_4d03_9401_4D064A3AE294_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Dec-2013 at 12:29:53pm, GMT+4, Saint-Petersburg, Sunday;
	This is Pulsepay WWS Server Light Console wrapper interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to FG project (thefileguardian.com) on 6-Jan-2016 at 9:26:48am, GMT+7, Phuket, Rawai, Wednesday;
	Adopted to FakeGPS driver project on 13-Dec-2019 at 9:47:05a, UTC+7, Novosibirsk, Friday;
*/
#include <atlwin.h>
#include "shared.gen.sys.err.h"
namespace shared { namespace user32 {
	using shared::sys_core::CError;
	using shared::sys_core::CErr_Format;
	class CEventType {
	public:
		enum _type {
			eInfo    = 0x0,
			eWarning = 0x1,
			eError   = 0x2
		};
	};

	typedef CEventType::_type TEventType;

	interface IConsoleNotify
	{
		virtual VOID   ConsoleNotify_OnEvent(const TEventType , const UINT  nResId) PURE;
		virtual VOID   ConsoleNotify_OnEvent(const TEventType , LPCWSTR pszDetails) PURE;
		virtual VOID   ConsoleNotify_OnError(const UINT nResId) PURE;
		virtual VOID   ConsoleNotify_OnInfo (const UINT nResId) PURE;
	};

	namespace console {

	class CNotifyDefImpl : public IConsoleNotify
	{
	private: // IConsoleNotify
#pragma warning(disable: 4481)
		virtual VOID   ConsoleNotify_OnEvent(const TEventType , const UINT  nResId) override sealed;
		virtual VOID   ConsoleNotify_OnEvent(const TEventType , LPCWSTR pszDetails) override sealed;
		virtual VOID   ConsoleNotify_OnError(const UINT nResId) override sealed;
		virtual VOID   ConsoleNotify_OnInfo (const UINT nResId) override sealed;
#pragma warning(default: 4481)
	};

	class CFont {
	protected:
		mutable
		CError    m_error;
	public:
		 CFont(void);
		~CFont(void);
	public:
		TErrorRef Error (void) const;
		UINT      Size  (void) const;   // gets font size that is set in console output;
	};

	class CPage {
	private:
		UINT      m_in;
		UINT      m_out;
	public:
		 CPage(const bool bAutoHandle = true);
		~CPage(void);

	public:
		HRESULT   Dos (void);
		HRESULT   Set (const UINT _in, const UINT _out);
	};

	class CWindow { // TODO: not good practice of interfearing with global namespace class; must be re-viewed;
	private:
		ATL::CWindow   m_wnd;

	public:
		CWindow (void);

	public:
		HRESULT   ClearContent(void);
		HWND      GetHwndSafe (void)const;
		bool      IsValid(void)const;
		HRESULT   SetIcon(const UINT nResId);

	public:
		ATL::CWindow& Window (void);

	public:
		static
		HRESULT   Create(LPCWSTR lpszTitle=NULL);
	};

	static LPCWSTR lp_sz_sep = _T("\n\t\t");

	class CConsole {
	private:
		CWindow  m_wnd;

	public:
		 CConsole(void);
		~CConsole(void);

	public:
		HRESULT   OnClose  (void);
		HRESULT   OnCreate (LPCWSTR _lp_sz_cap , LPCWSTR _lp_sz_title);
		HRESULT   OnWait   (LPCWSTR _lp_sz_msg );
		HRESULT   SetIcon  (const WORD _w_res_id );
		HRESULT   Write    (const TEventType, LPCWSTR _lp_sz_msg, LPCWSTR _lp_sz_sep = lp_sz_sep);
		HRESULT   WriteErr (TErrorRef  _err);
		HRESULT   WriteErr (LPCWSTR _lp_sz_msg, LPCWSTR  _lp_sz_sep = lp_sz_sep);
		HRESULT   WriteInfo(LPCWSTR _lp_sz_msg, LPCWSTR  _lp_sz_sep = lp_sz_sep);
		HRESULT   WriteWarn(LPCWSTR _lp_sz_msg, LPCWSTR  _lp_sz_sep = lp_sz_sep);

	public:
		const
		CWindow&  Window (void) const;
		CWindow&  Window (void)      ;

	public:
		CConsole& operator << (const WORD   _id_ref );        // prints out info message from resource identifier;
		CConsole& operator << (LPCWSTR   _lp_sz_info);        // prints out info message;
		CConsole& operator << (TErrorRef _err_oject );        // prints out an error details;
		CConsole& operator >> (LPCWSTR _lp_sz_err );          // prints out an error details;
		CConsole& operator <= (LPCWSTR _lp_sz_warn);          // prints out an warning;
		CConsole& operator << (const CStringW& _info);        // prints out info message;
	};

	}
}}

typedef shared::user32::console::CFont           TConsoleFont  ;
typedef shared::user32::console::CNotifyDefImpl  TNotifyDefImpl;
typedef shared::user32::console::CPage           TConsolePage  ;
typedef shared::user32::console::CWindow         TConsoleWnd   ;
typedef shared::user32::console::CConsole        TConsole;

#endif/*_SHAREDGENERICCONOBJECT_H_4D74D09A_4FA1_4d03_9401_4D064A3AE294_INCLUDED*/