#ifndef _SFXBUTTONLAY_H_73CDD7BD_EAB2_4B92_9E61_0F4265E1BDF8_INCLUDED
#define _SFXBUTTONLAY_H_73CDD7BD_EAB2_4B92_9E61_0F4265E1BDF8_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Nov-2020 at 10:10:21a, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack Sfx button layout interface declaration file. 
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "shared.uix.ctrl.base.lay.h"

namespace ST_Ctrls { class CButton; namespace layout {

	using shared::sys_core::CError;

	using ex_ui::controls::layout::CLayersEx;

	class CButton_Layout {
	protected:
		ST_Ctrls::CButton&
		            m_ctrl;
		CLayersEx   m_lays;
		SIZE        m_size;  // expected button size;

	public:
		 CButton_Layout (ST_Ctrls::CButton&);
		~CButton_Layout (void);

	public:
		const
		CLayersEx&  Layers (void) const;
		CLayersEx&  Layers (void)      ;
		const SIZE& Size   (void) const;           // gets button size; it is updated after creating button or updating its rectangle;
		HRESULT     Size   (const LONG _width, const LONG _hight); // sets a buttons size; it affects button window rectangle too;
		HRESULT     Update (void)      ;           // recalculates position/rectangle of all elements;
		HRESULT     Update (const RECT& _rc_area); // updates control window position into an area provided;

	public:
		CButton_Layout& operator <<(const RECT& _rc_area);        // updates control window position in accordance with area  ;
		const RECT      operator = (const RECT& _rc_area) const;  // returns calculated rectangle of control window for area provided;

	private: // non-copyable;
		CButton_Layout (const CButton_Layout&);
		CButton_Layout& operator = (const CButton_Layout&);
	};

}}

typedef ST_Ctrls::layout::CButton_Layout TButtonLay;

#endif/*_SFXBUTTONLAY_H_73CDD7BD_EAB2_4B92_9E61_0F4265E1BDF8_INCLUDED*/