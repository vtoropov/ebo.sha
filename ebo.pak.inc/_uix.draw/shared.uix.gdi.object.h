#ifndef _UIXDRAWGDIOBJECT_H_758531BE_3252_454f_BDDE_151EB532BF99_INCLUDED
#define _UIXDRAWGDIOBJECT_H_758531BE_3252_454f_BDDE_151EB532BF99_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 19-Oct-2010 at 1:02:30am, GMT+3, Rostov-on-Don, Tuesday;
	This is Sfx Pack project shared UIX draw library GDI wrapper interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack on on 7-Feb-2015 at 10:27:49pm, GMT+3, Taganrog, Saturday;
	Adopted to v15 on 28-May-2018 at 11:08:13p, UTC+7, Phuket, Rawai, Monday;
	Adopted to FakeGPS project on 19-Apr-2020 at 3:09:08a, UTC+7, Novosibirsk, Sunday;
	Dramatically improved for Ebo-Sha project on 15-Sep-2020 at 3:03:03p, UTC+7, Novosibirsk, Tuesday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.gdi.draw.defs.h"

namespace ex_ui { namespace draw {

	// https://www.grammarly.com/blog/know-your-latin-i-e-vs-e-g/
	// *IMPORTANT*
	// all images are expected to have 32-bit-color format, otherwise an error is returned;
	// it is assumed a bitmap row of image pixels is rounded to 4 bytes value, that means a stride = 4-byte * bitmap width;
	// but sometimes implement code does not check of color-bitness and it may lead to an error of memory access; this is must be checked;
	using shared::sys_core::CError;

	// this class does not free/delete an object that is attached;
	class CBitmapInfo : public BITMAP { typedef BITMAP TBase;
	private:
		UINT       m_UID;      // identifier of this object;
		HBITMAP    m_handle;   // attached bitmap handle;

	public:
		 CBitmapInfo (void);
		 CBitmapInfo (const CBitmapInfo&);
		 CBitmapInfo (const HBITMAP hBitmap, const UINT UID = ::GetTickCount());
		~CBitmapInfo (void);

	public:
		HRESULT    Attach (const HBITMAP); // attaches to bitmap handle provided and initializes base structure fields;
		HBITMAP    Detach (void);          // returns attached handle and resets itself;
		HRESULT    Reset  (void);          // zeros this class object attributes and base structure fields;

	public:
		const
		HBITMAP    Handle (void ) const;
		UINT       ID     (void ) const;
		bool       Is     (void ) const;
		BITMAPINFO Raw    (void ) const;
		HRESULT    Size   (SIZE&) const;

	public:
		CBitmapInfo& operator = (const CBitmapInfo&);
		CBitmapInfo& operator <<(const UINT UID);
		CBitmapInfo& operator <<(const HBITMAP );

	public:
		operator const HBITMAP  (void) const;
		operator const BITMAPINFO (void) const;

	public:
		static
		bool      IsValid(const HBITMAP);
	};

	typedef const PBYTE  PCBYTE;
	// https://docs.microsoft.com/en-us/windows/win32/gdi/storing-an-image
	class CDibSection {
	private :
		HBITMAP   m_handle;     // encapsulated bitmap descriptor;
		PBYTE     m_pData ;     // a pointer to bitmap bits;
		SIZE      m_size  ;     // bitmap size;

	public:
		 CDibSection (void);
		 CDibSection (const CDibSection&);
		 CDibSection (const HDC, const SIZE&);
		~CDibSection (void);

	public: // bitmap life cycle;
		HRESULT   Create  (const HDC, const SIZE&); // creates new bitmap by applying input arguments; the previous data is destroyed if any;
		HRESULT   Destroy (void)      ;             // returned result is dependent on current state of DC: ERROR_INVALID_HANDLE|ERROR_INVALID_STATE;
		HBITMAP   Detach  (void)      ;             // detaches from encapsulated bitmap descriptor;
		HRESULT   Reset   (void)      ;             // clears all variables|no destroying bitmap handle; used in const|detach;

	public: // bitmap attributes and other;
		PCBYTE    Bits    (void) const;             // gets bitmap data;
		TBitmap*  Get     (void) const;             // creates gdi+ bitmap object from encapsulated bitmap descriptor;
		HBITMAP   Handle  (void) const;             // gets a bitmap handle if success, otherwise returns NULL;
		bool      Is      (void) const;             // checks a validity of the encapsulated bitmap descriptor;
		SIZE      Size    (void) const;             // returns a size of bitmap;

	public:
		CDibSection& operator = (const CDibSection&);
		
	public:
		operator  HBITMAP (void) const;             // operator overloading, does the same as GetHandle_Safe()
	};

	class CGdiplusBitmapWrap {
	private:
		TBitmap*  m_pBitmap;
		bool      m_bManaged;

	public:
		 CGdiplusBitmapWrap (void) ;
		 CGdiplusBitmapWrap (const CGdiplusBitmapWrap&);   // in case of manageable bitmap, its clone is created;
		 CGdiplusBitmapWrap (const HBITMAP);               // creates GDI+ object from bitmap handle provided;
		 CGdiplusBitmapWrap (TBitmap*, const bool bManaged);
		~CGdiplusBitmapWrap (void);

	public: // bitmap life-cycle control;
		// TODO: attaching bitmap is not safe and requires using shared pointer;
		HRESULT   Attach (const TBitmap*);                 // attaches to bitmap provided, managing of the bitmap is turned off; i.e. not-manageable;
		HRESULT   Create (const HBITMAP );                 // creates manageable bitmap object; 32bpp and alpha channel are expected;
		HRESULT   Create (const TBitmap*);                 // creates manageable clone of bitmap provided;
		TBitmap*  Detach (void);                           // detaches encapsulated bitmap, i.e. cleans this class instance;
		HRESULT   Reset  (void);                           // destroys manageable bitmap object;

	public: // attributes and auxiliary methods;
		// https://docs.microsoft.com/en-us/windows/win32/gdiplus/-gdiplus-drawing-positioning-and-cloning-images-about
		HRESULT   Clip   (const RECT& rcDest, TBitmap*& __out_ptr) const; // the above reference provides more simplistic method of cropping bitmap;
		bool      Is     (void) const;
		bool      Managed(void) const;
		TBitmap*  Ptr    (void) const;
		SIZE      Size   (void) const;
	
	public:
		CGdiplusBitmapWrap& operator = (const CGdiplusBitmapWrap&); // in case of manageable bitmap, its clone is created;
		CGdiplusBitmapWrap& operator <<(const HBITMAP );            // creates manageable bitmap object;
		CGdiplusBitmapWrap& operator <<(const TBitmap*);            // the source bitmap is copied, i.e. (exempli gratia) its clone is created;
		CGdiplusBitmapWrap& operator >>(const TBitmap*);            // the source bitmap is attached, no clone is created;

	public:
		static
		HRESULT   IsValid (const TBitmap*);                // returns S_OK if pointer is valid and the object being pointed is bitmap in okay status;
		static
		SIZE      Required(const TBitmap*);                // returns a size of bitmap provided; it is required for creating a clone, often;
	};

	typedef  ::std::map<DWORD, TBitmap*>  TRawImages;

	class CImageCache {
	private:
		TRawImages m_images;

	public:
		 CImageCache(void);
		 CImageCache(const CImageCache&);                 // makes a copy of image cache;
		~CImageCache(void);

	public:
		HRESULT    Add   (const ULONG uKey, TBitmap* pb); // the bitmap being added will be destroyed by cache, no cloning is performed here;
		HRESULT    Add   (const ULONG uKey, const SIZE& sz_  ,TBitmap** _p_out = NULL);
		HRESULT    Add   (const DWORD uKey, const UINT nResId, const HINSTANCE hResourceModule = NULL);
		HRESULT    Clear (void);
		HRESULT    Clone (const TRawImages& _data);       // destroys existing cache and clones the provided one;
		UINT       Count (void) const;
		TBitmap*   Get   (const ULONG uKey) const;
		bool       Has   (const ULONG uKey) const;
		HRESULT    Remove(const ULONG uKey);
		const
		TRawImages& Raw  (void) const;

	public:
		operator const TRawImages& (void) const;

	public:
		CImageCache& operator=(const CImageCache&);       // makes a copy of image cache;
	};

	class CImageCache_Ex : public CImageCache { typedef CImageCache TBase;
	private:
		ULONG   m_curr_key;   // a key of the image being displayed; 0 - by default;

	public:
		 CImageCache_Ex (void);
		 CImageCache_Ex (const CImageCache_Ex&);
		~CImageCache_Ex (void);

	public:
		ULONG   Current (void) const;
		HRESULT Current (const ULONG _u_key);  // returns S_OK, in case of success, otherwise an error code such as DISP_E_BADINDEX;
		SIZE    Size    (void) const;          // returns max size of all images, i.g. (id est) consists of the maxed height and maxed width;

	public:
		 CImageCache_Ex& operator = (const CImageCache_Ex&);
		 CImageCache_Ex& operator <<(const ULONG _u_current); // sets a key of current image for drawing;
	};
}}

typedef ex_ui::draw::CGdiplusBitmapWrap  TGdiPlusBmpWrap;
typedef ex_ui::draw::CImageCache         TImageCache;
typedef ex_ui::draw::CImageCache_Ex      TImageCache_Ex;

#endif/*_UIXDRAWGDIOBJECT_H_758531BE_3252_454f_BDDE_151EB532BF99_INCLUDED*/