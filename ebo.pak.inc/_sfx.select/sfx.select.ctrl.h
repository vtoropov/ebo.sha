#ifndef _SHAREDUIXCTRLSELECTOR_H_97B01DFB_8AFE_4B7E_9CC9_B8FC391DEE77_INCLUDED
#define _SHAREDUIXCTRLSELECTOR_H_97B01DFB_8AFE_4B7E_9CC9_B8FC391DEE77_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 11-Nov-2019 at 9:16:09p, UTC+7, Novosibirsk, Monday;
	This is shared UIX library task/view selector control interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to shared Sfx library on 27-Aug-2020 at 0:21:54a, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.gen.sys.err.h"
#include "shared.uix.ctrl.defs.h"
#include "sfx.select.ext.h"
#include "sfx.select.fmt.h"
#include "sfx.select.lay.h"

namespace ST_Ctrls {

	using shared::sys_core::CError;
	using ex_ui::draw::defs::IRenderer;
	using ex_ui::controls::CBorders;

	interface ISelector_Events {
		virtual HRESULT   ISelector_OnBkgChanged (void) PURE;
		virtual HRESULT   ISelector_OnFontChanged(void) PURE;
		virtual HRESULT   ISelector_OnItemClick  (const UINT _u_itm_id) PURE;
		virtual HRESULT   ISelector_OnItemImages (const UINT _u_res_id) PURE;
	};

	class CSelector : public ISelector_Events {
	protected:
		ISelector_Events&
		               m_evt_snk;   // control owner event sink reference;
		HANDLE         m_wnd_ptr;
		CError         m_error  ;
		TSelItems      m_items  ;
		TSelectFmt     m_format ;
		TSelectLay     m_layout ;
		UINT           m_ctrl_id;
		CBorders       m_borders;

	public:
		 CSelector(ISelector_Events&);
		~CSelector(void);

	public:
		const
		CBorders&      Borders (void) const;
		CBorders&      Borders (void)      ;
		HRESULT        Create  (const HWND hParent, const RECT& _rc_area, LPCTSTR _lp_sz_cap, const UINT _ctrl_id);
		HRESULT        Destroy (void)      ;
		TErrorRef      Error   (void) const;
		const
		TSelectFmt&    Format  (void) const;
		TSelectFmt&    Format  (void)      ;
		const UINT     Id      (void) const;
		const bool     Is      (void) const; // checks a validity state;
		const
		TSelItems&     Items   (void) const;
		TSelItems&     Items   (void)      ;
		const
		TSelectLay&    Layout  (void) const;
		TSelectLay&    Layout  (void)      ;
		HRESULT        ParentRenderer (IRenderer*  const );
		HRESULT        Refresh (void)      ;
		const bool     Visible (void) const; // returns control visibility state;
		HRESULT        Visible (const bool); // sets control visibility;
		CWindow        Window  (void) const;

	private: // ISelector_Events
		virtual HRESULT   ISelector_OnBkgChanged (void) override;
		virtual HRESULT   ISelector_OnFontChanged(void) override;
		virtual HRESULT   ISelector_OnItemClick  (const UINT _u_itm_id) override;
		virtual HRESULT   ISelector_OnItemImages (const UINT _u_res_id) override;

	private:
		CSelector(const CSelector&);
		CSelector& operator= (const CSelector&);
	};
}

#endif/*_SHAREDUIXCTRLSELECTOR_H_97B01DFB_8AFE_4B7E_9CC9_B8FC391DEE77_INCLUDED*/