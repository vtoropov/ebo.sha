#ifndef _SHAREDLITEGENSTR_H_205FBD43_702A_420F_A816_232E49EC047F_INCLUDED
#define _SHAREDLITEGENSTR_H_205FBD43_702A_420F_A816_232E49EC047F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 24-Oct-2018 at 11:57:12p, UTC+7, Novosibirsk, Wednesday;
	This is Ebo Pack shared lite library string common functionality interface declaration file.
*/
namespace shared { namespace common
{
	typedef ::std::vector<CAtlString> TStdStrVec;

	class CStdString : public CStringW {
	                  typedef CStringW TString;
	public:
		CStdString (void)   ;
		CStdString (LPCWSTR);
	public:
		LPCWSTR      Before(wchar_t _lp_sz_sep = _T('\\'), LPCWSTR _lp_sz_pfx = _T("..."), const bool _b_exc_sep = true);
		LPCWSTR      Format(LPCWSTR _lp_sz_fmt, ...);
		LPCWSTR      Format(LPCWSTR _lp_sz_fmt, va_list);
		TStdStrVec   Split (LPCWSTR _lp_sz_sp , const bool _b_preserve_sep = false) const;   // splits string by separator specified;
#if(0) // already exists in base clase;
	public:
		CStdString&  operator=(const _variant_t&);
#endif
	};
}}

#endif/*_SHAREDLITEGENSTR_H_205FBD43_702A_420F_A816_232E49EC047F_INCLUDED*/