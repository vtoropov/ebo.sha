/*
	Created by Tech_dog (ebontrop@gmail.com) on 13-Sep-2020 at 7:42:35p, UTC+7, Novosibirsk, Sunday;
	This is Ebo Pack generic driver device instance setup information interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Sha Optima Tool project on 12-Jan-2021 at 7:32:01.762 pm, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "drv.setup.instance.h"

using namespace shared::driver::setup;

/////////////////////////////////////////////////////////////////////////////

CDevInstInfo:: CDevInstInfo (void) { m_error << __MODULE__ << S_OK >> __MODULE__; }
CDevInstInfo:: CDevInstInfo (const CDevInstInfo& _ref) : CDevInstInfo() { *this = _ref;}
CDevInstInfo::~CDevInstInfo (void) {}

/////////////////////////////////////////////////////////////////////////////

LPCWSTR    CDevInstInfo::Att_Of(const _att e_att) const {
	switch (e_att) {
	case _att::e_att_desc: case _att::e_att_id: case _att::e_att_name: return m_atts[e_att].GetString();
	}
	static LPCWSTR lp_sz_na = _T("#n/a");
	return lp_sz_na;
}

CStringW&  CDevInstInfo::Att_Of(const _att e_att) {
	switch (e_att) {
	case _att::e_att_desc: case _att::e_att_id: case _att::e_att_name: return m_atts[e_att];
	}
	static CStringW cs_na = _T("#n/a");
	return cs_na;
}

HRESULT    CDevInstInfo::Erase (void) {

	for (size_t i_ = 0; i_ < _countof(m_atts); i_++)
		if (m_atts[i_].IsEmpty() == false)
			m_atts[i_].Empty();
	return S_OK;
}

TErrorRef  CDevInstInfo::Error (void) const { return m_error; }

HRESULT    CDevInstInfo::Get   (const CDevInfoSet& _set, const CDevInfo& _dat) {
	m_error << __MODULE__ << S_OK;

	if (_set.Is() == false)
		return (m_error << E_INVALIDARG) = _T("Device info set is invalid;");

	if (_dat.Is() == false)
		return (m_error << E_INVALIDARG) = _T("Interface data is invalid;");

	CMemAlloca p_buffer;
	DWORD n_required  = 0;             // returns buffer size in character count, i.e. either size of in ansi-chars or wide-chars;
	DWORD the_lst_err = ERROR_SUCCESS; // yes, that's right: success is always an error;

	CDevInfo& dat_ = const_cast<CDevInfo&>(_dat);

	// (1) gets buffer size that is required for getting instance identifier;
	// https://docs.microsoft.com/en-us/windows/win32/api/setupapi/nf-setupapi-setupdigetdeviceinstanceidw
	BOOL b_result = ::SetupDiGetDeviceInstanceIdW(_set, dat_, reinterpret_cast<PWSTR>(p_buffer.Ptr()), p_buffer.Size(), &n_required);
	the_lst_err   = ::GetLastError();

	if (FALSE == b_result && the_lst_err != ERROR_INSUFFICIENT_BUFFER) {
		return (m_error = __DwordToHresult(the_lst_err));
	}
#if (0)
	// https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-localalloc
	// using this function is not recommended; heap allocation must be used; just for sake of memory about memory local management;
	p_buffer = reinterpret_cast<PWCHAR>(::LocalAlloc(LMEM_ZEROINIT, n_required));
	if (NULL == p_buffer) {
		return (m_error = __DwordToHresult(the_lst_err));
	}
	this->Att_Of(TProperty::e_att_id) = p_buffer;
	// https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-localfree
	if (NULL != ::LocalFree(p_buffer)) {
		return (m_error = __DwordToHresult(the_lst_err));
	}
#endif
	HRESULT hr_ = p_buffer.Create(n_required);
	if (FAILED(hr_)) {
		return (m_error = p_buffer.Error());
	}
	b_result = ::SetupDiGetDeviceInstanceIdW(_set, dat_, reinterpret_cast<PWSTR>(p_buffer.Ptr()), p_buffer.Size(), &n_required);
	if (FALSE == b_result) {
		return (m_error = __LastErrToHresult());
	}
	else
		this->Att_Of(_att::e_att_id) = reinterpret_cast<PWSTR>(p_buffer.Ptr());

	registry::CProperty reg_prop;

	// (2) gets instance description;
	this->Att_Of(_att::e_att_desc) = reg_prop.Get(_set, _dat, registry::CPropTypes::e_desc);

	// (3) gets instance friendly name;
	this->Att_Of(_att::e_att_name) = reg_prop.Get(_set, _dat, registry::CPropTypes::e_friend);
	if (reg_prop.Error() == true) {
#if (0)
		CStringW cs_error = _T("#error:"); cs_error += reg_prop.Error().Format(_T("|"));
		this->Att_Of(_att::e_att_name) = cs_error;
#endif
	}

	return m_error;
}

CStringW   CDevInstInfo::ToStr (LPCWSTR _lp_sz_sep) const {

	static LPCWSTR lp_sz_pat = _T(
		"Id    = %s%s"
		"Decs  = %s%s"
		"Name  = %s%s"
	);

	CStringW cs_name = this->Att_Of(_att::e_att_name); if (cs_name.IsEmpty()) cs_name = _T("friendly-name:#undef;");
	CStringW cs_info; cs_info.Format(
		lp_sz_pat, this->Att_Of(_att::e_att_id), _lp_sz_sep, this->Att_Of(_att::e_att_desc), _lp_sz_sep, cs_name.GetString(), _lp_sz_sep
	); 

	return cs_info;
}

/////////////////////////////////////////////////////////////////////////////

CDevInstInfo& CDevInstInfo::operator = (const CDevInstInfo& _ref) {
	for (size_t i_ = 0; i_ < _countof(this->m_atts) && i_ < _countof(_ref.m_atts); i_++)
		this->m_atts[i_] = _ref.m_atts[i_];
	return *this;
}