/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Jan-2021 at 05:36:52.527 am, UTC+7, Novosibirsk, Saturday;
	This is Ebo Sha Optima Tool driver/service debug view interface implementation file.
*/
#include "StdAfx.h"
#include "ebo.sha.view.trace.h"
#include "ebo.sha.gui.res.h"

using namespace ebo::sha::gui::view;

/////////////////////////////////////////////////////////////////////////////

namespace ebo { namespace sha { namespace gui { namespace _impl {

	class CTraceView_Layout {
	private:
		TTraceView&  m_view;
		RECT         m_rect;

	public:
		 CTraceView_Layout (TTraceView& _view) : m_view(_view) {
			if (m_view.Window().IsWindow())
				m_view.Window().GetClientRect(&m_rect);
			else
				::SetRectEmpty(&m_rect);
		}
		 CTraceView_Layout (TTraceView& _view, const RECT& _rect) : m_view(_view), m_rect{ _rect.left, _rect.top, _rect.right, _rect.bottom } {}
		~CTraceView_Layout (void) {}

	public:
		RECT   GetEmpty(void) { RECT rc_empty = m_rect; ::InflateRect(&rc_empty, -0x5, -0x5); return rc_empty; }
		HRESULT Update (ex_ui::controls::format::CBorder_Ex& _border) { HRESULT hr_ = S_OK;
			_border;
			_border.Position().Start().y = _border.Position().End().y = m_rect.top;
			_border.Position().Start().x = m_rect.left;
			_border.Position().End  ().x = m_rect.right;
			return hr_;
		}
	};

}}}}
using namespace ebo::sha::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

TTraceView::CViewWnd:: CViewWnd(TTraceView& _view) : TViewWnd(_view), m_view(_view), m_trace(*this) {}
TTraceView::CViewWnd::~CViewWnd(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT   TTraceView::CViewWnd::OnCreate  (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;

	CTraceView_Layout layout_(m_view); TViewWnd::SetWindowTextW(_T("Debug Trace Output"));
#if defined(__use_empty)
	HRESULT hr_ = m_empty.Create(*this, layout_.GetEmpty(), true); hr_;
	m_empty.DrawBorder(true);
#endif
#pragma region __init_trace
	TTraceOutFmt  fmt_trace;

	fmt_trace.Toolbar().Get(0).Images().Add(CState::eNormal  , IDR_EBO_UM_TEST_TRC_DN_N);
	fmt_trace.Toolbar().Get(0).Images().Add(CState::eHovered , IDR_EBO_UM_TEST_TRC_DN_O);
	fmt_trace.Toolbar().Get(0).Images().Add(CState::ePressed , IDR_EBO_UM_TEST_TRC_DN_O);
	fmt_trace.Toolbar().Get(0).Images().Add(CState::eDisabled, IDR_EBO_UM_TEST_TRC_DN_N);

	fmt_trace.Toolbar().Get(1).Images().Add(CState::eNormal  , IDR_EBO_UM_TEST_TRC_UP_N);
	fmt_trace.Toolbar().Get(1).Images().Add(CState::eHovered , IDR_EBO_UM_TEST_TRC_UP_O);
	fmt_trace.Toolbar().Get(1).Images().Add(CState::ePressed , IDR_EBO_UM_TEST_TRC_UP_O);
	fmt_trace.Toolbar().Get(1).Images().Add(CState::eDisabled, IDR_EBO_UM_TEST_TRC_UP_N);
	fmt_trace >> m_trace;

	CScrolls& scrolls_ = m_trace.Scrolls();

	scrolls_.Down().Runtime ().CtrlId(1);
	scrolls_.Up  ().Runtime ().CtrlId(2);

	ex_ui::trace::CTrace_Layout& lay_ = m_trace.Layout();lay_.Images().Size().cx = lay_.Images().Size().cy = 24;
	m_trace.Format().ImageRes(IDR_EBO_UM_TEST_TRC_IMGS);
	m_trace.Format().ImageNdx(TEntryType::e_warn , 1);
	m_trace.Format().ImageNdx(TEntryType::e_error, 2);
	m_trace.Format().Timestamp() = false;

#pragma endregion

	m_trace.Create(*this, layout_.GetEmpty());
	m_trace.Header().Text(_T("Trace Output"));

	TStatusBarFmt().ApplyTo(m_top);

	return 0;
}

LRESULT   TTraceView::CViewWnd::OnDestroy (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand = FALSE;
#if defined(__use_empty)
	m_empty.Destroy();
#endif
	m_trace.Destroy();
	return TViewWnd::OnDestroy(_msg, _w_prm, _l_prm, _b_hand);
}

LRESULT   TTraceView::CViewWnd::OnErase   (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	LRESULT l_result = TViewWnd::OnErase(_msg, _w_prm, _l_prm, _b_hand);
	// https://docs.microsoft.com/en-us/windows/win32/winmsg/wm-erasebkgnd
	RECT rc_draw = {0};
	TViewWnd::GetClientRect(&rc_draw);

	TZBuffer z_buffer((HDC)_w_prm, rc_draw);

	z_buffer.FillSolidRect(&rc_draw, shared::Get_Format().Panel().Bkgnd().Solid());

	CBorders_Helper::Draw(z_buffer, m_top);
	return  l_result;
}

LRESULT   TTraceView::CViewWnd::OnSize    (UINT _msg, WPARAM _w_prm, LPARAM _l_prm, BOOL& _b_hand) {
	_msg; _w_prm; _l_prm; _b_hand;
	const SIZE sz_ = {0, 0};
	const RECT rc_ = {0, sz_.cy, LOWORD(_l_prm), HIWORD(_l_prm)};

	CTraceView_Layout layout_(m_view, rc_);
#if defined(__use_empty)
	m_empty.Update(layout_.GetEmpty());
#endif
	m_trace.Layout().Update(layout_.GetEmpty()); layout_.Update(m_top);
	return TViewWnd::OnSize(_msg, _w_prm, _l_prm, _b_hand);
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   TTraceView::CViewWnd::ITrace_OnEntryClick(const DWORD _ent_id) { HRESULT hr_ = S_OK;
	_ent_id   ;
	return hr_;
}

/////////////////////////////////////////////////////////////////////////////

TTraceView:: CTrace (void) : TView(m_wnd), m_wnd(*this) { TView::m_error << __MODULE__ << S_OK >> __MODULE__; }
TTraceView::~CTrace (void) {}

/////////////////////////////////////////////////////////////////////////////

CTraceEx&    TTraceView::Output (void) { return m_wnd.m_trace; }